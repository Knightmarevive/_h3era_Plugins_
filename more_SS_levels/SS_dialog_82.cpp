// #include "patcher_x86.hpp"
#include"MyTypes.h"
#include "Storage.h"
#include "heroes.h"
extern int  current_SS_count;

_LHF_(Hook_004E1CC6) {
	auto H = (HERO*) (char*) *(int *) 0x698B70;
	if (H) 
	{
		int N = Storage_current_hero = H->Number; 
		if (!count_SSShow(N)) fix_hero_SSShow(N);
	}
	return EXEC_DEFAULT;
}

char  __stdcall get_hero_SS(HERO* H, int skilllnum);
int  __stdcall _get_hero_SS_(int ptr, int skilllnum) {
	HERO* H = (HERO*)(char*) ptr;

	if (skilllnum < 0)
		return 0;
	
	if (skilllnum >= current_SS_count)
		return 0;

	/*
	if (ptr<0x00100000)
		return 0;
	*/
	int ret = 0;
	if (H)		
		ret = get_hero_SS(H, skilllnum);
	else if (Storage_current_hero >= 0)
		ret = get_hero_SS(Storage_current_hero, skilllnum);
	else ret = 0;

	if (ret < 0)  return 0;
	if (ret > 15) return 15;

	return ret;;
}

typedef void(*voidfun)(void);
voidfun calc_pre_msgbox_ret = (voidfun)0x004DE843;
voidfun calc_in_msgbox_ret  = (voidfun)0x004F5DFA;
voidfun fun_41B2A0			= (voidfun)0x0041B2A0;
voidfun fun_404A20			= (voidfun)0x00404A20;
voidfun fun_404A40			= (voidfun)0x00404A40;

extern char* Skill_Levels[];

void calc_pre_msgbox(void);

// int calc_in_msgbox_edx;

// patched from 0x004F5D0C

extern int SS_0_3_to_SS_0_15[4];
__declspec(naked) void calc_in_msgbox(void) {
	_asm {
		//push edx
		//mov edx, dword ptr ds : [ebx + 4]
		//mov calc_in_msgbox_edx, edx
		//pop edx

		// cmp dword ptr ds : [ebx], 0x14
		// jne label_4F5DF7

		// check if new syntax and fix if it's old
		cmp dword ptr ds : [ebx + 4], _Frame_Shift_
		jge new_dialog

		pushad
		mov eax, dword ptr ds : [ebx + 4]
		sub eax, 3
		mov ecx, 3 
		idiv ecx 
		shl eax, 4 // multiply by 16

		//lea edx, [edx*4 + edx] // TODO: 2021-04-01
		mov edx, [edx*4 + SS_0_3_to_SS_0_15] //done

		add eax, edx
		add eax, 0x10
		inc eax
		mov dword ptr ds : [ebx + 4], eax
		popad
		jmp z_begin

		new_dialog:
		sub dword ptr ds : [ebx+4], _Frame_Shift_

		z_begin:  // the real beginning of code
		cmp dword ptr ds : [ebx + 4], 0
		jge positive
		mov dword ptr ds : [ebx + 4], 0
		positive:
		mov ecx, current_SS_count
		inc ecx
		shl ecx, 4
		cmp dword ptr ds : [ebx + 4] , ecx
		jl not_too_big
		mov dword ptr ds : [ebx + 4] , 0
		not_too_big:
		cdq //some cleaning (from original code)

		push 0x1
		or ecx, 0xFFFFFFFF //some cleaning (from original code)

			mov eax, dword ptr ds : [ebx + 4]
			and eax, 0x0f /// levels are 0 - 15 so this gets level of the skill to EAX

		mov esi, dword ptr ds : [eax * 4 + Skill_Levels]

			xor eax, eax //some cleaning (from original code)
		lea edx, dword ptr ds : [ebx + 0x18]
		mov edi, esi
		repne scasb
		not ecx
		dec ecx
		mov dword ptr ss : [ebp + 0x8] , ecx
		push ecx
		mov ecx, edx
		call fun_404A40
		test al, al
		je label_4F5D5C
		mov ecx, dword ptr ss : [ebp + 0x8]
		mov edi, dword ptr ds : [ebx + 0x1C]
		lea eax, dword ptr ds : [ebx + 0x18]
		mov edx, ecx
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0x3
		rep movsb
		mov ecx, edx
		push ecx
		mov ecx, eax
		call fun_404A20

		label_4F5D5C:
		mov edi, 0x660330
		or ecx, 0xFFFFFFFF
		xor eax, eax
		lea esi, dword ptr ds : [ebx + 0x18]
		repne scasb
		not ecx
		dec ecx
		push ecx
		push 0x660330
		mov ecx, esi
		call fun_41B2A0

		mov ecx, dword ptr ds : [ebx + 0x4]
		
		
		mov edx, ecx
		mov ecx, dword ptr ds : [0x0067DCF0]  // mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		
		and edx, 0xfffffff0
		
		mov edx, dword ptr ds : [ecx +edx*4 - 0x40]

		or ecx, 0xFFFFFFFF
		xor ecx, 0xFFFFFE00 //majaczek
		mov edi, edx
		repne scasb
		or ecx, 0xFFFFFE00 //majaczek
		not ecx
		dec ecx
		push ecx
		push edx
		mov ecx, esi
		xor eax, eax
		call fun_41B2A0

			mov edi, 0x6600F8
			or ecx, 0xFFFFFFFF
			xor ecx, 0xFFFFFE00 //majaczek
			xor eax, eax
			lea edx, dword ptr ds : [ebx + 0x8]
			repne scasb
			or ecx, 0xFFFFFE00 //majaczek

			not ecx
			dec ecx
			push 0x1
			mov dword ptr ss : [ebp + 0x8] , ecx
			push ecx
			mov ecx, edx
			call fun_404A40
			test al, al
			je label_4F5DF7
			mov ecx, dword ptr ss : [ebp + 0x8]
			mov edi, dword ptr ds : [ebx + 0xC]
			lea eax, dword ptr ds : [ebx + 0x8]
			mov edx, ecx
			mov esi, 0x6600F8
			shr ecx, 0x2
			rep movsd
			mov ecx, edx
			and ecx, 0x3
			rep movsb
			mov ecx, edx
			push ecx
			mov ecx, eax

			call fun_404A20

			label_4F5DF7 :
			mov edx, dword ptr ds : [ebx + 0x4]

		    //mov edx, calc_in_msgbox_edx
		jmp calc_in_msgbox_ret
	}
}


int calc_pre_msgbox_tmp; 
// int calc_pre_msgbox_eax;
// patched from 0x004DE7E5
__declspec(naked) void calc_pre_msgbox(void) {
	_asm {
		mov eax, edx
		cmp eax, -1
		je lab1

		// push edx
		shl edx, 4
		lea eax, dword ptr ds : [ecx + edx]
		shr edx, 4
		// pop edx

		lab1:

		mov ecx, dword ptr ds : [0x0067DCF0]
		push 0x0
		push 0xFFFFFFFF
		mov edi, dword ptr ds : [ecx + eax * 4]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		push 0x0
		repne scasb
		not ecx
		sub edi, ecx
		push 0xFFFFFFFF
		mov eax, ecx
		mov esi, edi
		mov edi, 0x697428
		shr ecx, 0x2
		rep movsd
		mov ecx, eax
		mov eax, dword ptr ds : [0x698B70]
		and ecx, 0xf 
		rep movsb

		mov ecx, edx
		
		shl ecx, 4 // shl ecx, 4
		//movsx edx, byte ptr ds : [eax + edx + 0xC9]
		//movsx eax, byte ptr ds : [eax + edx + 0xC9]
		//mov calc_pre_msgbox_eax, eax
		//pusha 
		push ecx
		push edx
		push eax
		call _get_hero_SS_
		pop ecx
		//mov calc_pre_msgbox_tmp, eax
		//popa
		//mov eax, calc_pre_msgbox_eax
		//mov edx, calc_pre_msgbox_tmp
		lea eax, dword ptr ds : [ecx + eax + 0x410]
		
		//// trying something
		//shr ecx, 4
		//lea ecx, byte ptr ds : [ecx + ecx*2]
		//and ecx, 0x3

		push eax
		push 0x14

		// 0x004DE830

		neg bl
		sbb ebx, ebx
		push 0xFFFFFFFF
		and ebx, 0x3 // 0xf 
		push 0xFFFFFFFF
		inc ebx
		mov ecx, 0x697428
		mov edx, ebx
		
		jmp calc_pre_msgbox_ret // 0x004DE843
	}
}





//////////////// originals


// patched from 0x004F5D0C
__declspec(naked) void calc_in_msgbox_original(void) {
	_asm {
		cdq
		mov ecx, 0x3
		push 0x1
		idiv ecx
		or ecx, 0xFFFFFFFF
		xor eax, eax
		mov esi, dword ptr ds : [edx * 4 + Skill_Levels]
		lea edx, dword ptr ds : [ebx + 0x18]
		mov edi, esi
		repne scasb
		not ecx
		dec ecx
		mov dword ptr ss : [ebp + 0x8] , ecx
		push ecx
		mov ecx, edx
		call fun_404A40
		test al, al
		je label_4F5D5C
		mov ecx, dword ptr ss : [ebp + 0x8]
		mov edi, dword ptr ds : [ebx + 0x1C]
		lea eax, dword ptr ds : [ebx + 0x18]
		mov edx, ecx
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0x3
		rep movsb
		mov ecx, edx
		push ecx
		mov ecx, eax
		call fun_404A20

		label_4F5D5C:
		mov edi, 0x660330
		or ecx, 0xFFFFFFFF
		xor eax, eax
		lea esi, dword ptr ds : [ebx + 0x18]
		repne scasb
		not ecx
		dec ecx
		push ecx
		push 0x660330
		mov ecx, esi
		call fun_41B2A0
		mov ecx, dword ptr ds : [ebx + 0x4]
		mov eax, 0x55555556
		imul ecx
		mov ecx, dword ptr ds : [0x0067DCF0]  // mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		mov eax, edx
		shr eax, 0x1F
		add edx, eax
		xor eax, eax
		shl edx, 0x4
		mov edx, dword ptr ds : [edx + ecx - 0x10]
		or ecx, 0xFFFFFFFF
		mov edi, edx
		repne scasb
		not ecx
		dec ecx
		push ecx
		push edx
		mov ecx, esi
		call fun_41B2A0

			mov edi, 0x6600F8
			or ecx, 0xFFFFFFFF
			xor eax, eax
			lea edx, dword ptr ds : [ebx + 0x8]
			repne scasb
			not ecx
			dec ecx
			push 0x1
			mov dword ptr ss : [ebp + 0x8] , ecx
			push ecx
			mov ecx, edx
			call fun_404A40
			test al, al
			je label_4F5DF7
			mov ecx, dword ptr ss : [ebp + 0x8]
			mov edi, dword ptr ds : [ebx + 0xC]
			lea eax, dword ptr ds : [ebx + 0x8]
			mov edx, ecx
			mov esi, 0x6600F8
			shr ecx, 0x2
			rep movsd
			mov ecx, edx
			and ecx, 0x3
			rep movsb
			mov ecx, edx
			push ecx
			mov ecx, eax
			call fun_404A20

			label_4F5DF7:
			mov edx, dword ptr ds : [ebx + 0x4]

		jmp calc_in_msgbox_ret
	}
}

// patched from 0x004DE7E5
__declspec(naked) void calc_pre_msgbox_original(void) {
	_asm {
		lea eax, dword ptr ds : [ecx + edx * 4]
		mov ecx, dword ptr ds : [0x0067DCF0]
		push 0x0
		push 0xFFFFFFFF
		mov edi, dword ptr ds : [ecx + eax * 4]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		push 0x0
		repne scasb
		not ecx
		sub edi, ecx
		push 0xFFFFFFFF
		mov eax, ecx
		mov esi, edi
		mov edi, 0x697428
		shr ecx, 0x2
		rep movsd
		mov ecx, eax
		mov eax, dword ptr ds : [0x698B70]
		and ecx, 0x3
		rep movsb
		lea ecx, dword ptr ds : [edx + edx * 2]
		movsx edx, byte ptr ds : [eax + edx + 0xC9]
		lea eax, dword ptr ds : [ecx + edx + 0x2]
		push eax
		push 0x14
		neg bl
		sbb ebx, ebx
		push 0xFFFFFFFF
		and ebx, 0x3
		push 0xFFFFFFFF
		inc ebx
		mov ecx, 0x697428
		mov edx, ebx
		
		jmp calc_pre_msgbox_ret
	}
}
