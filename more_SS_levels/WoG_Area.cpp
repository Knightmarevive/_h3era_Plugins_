// #include "patcher_x86.hpp"
#include "Storage.h"
#include "heroes.h"
//#include "H3DlgItem.hpp"
//#include "H3Manager.hpp"
#include "MyTypes.h"

extern int current_SS_count;

#ifndef THISCALL_1
#define THISCALL_1(return_type, address, a1) ((return_type(__thiscall *)(UINT))(address))((UINT)(a1))
#endif

#ifndef THISCALL_2
#define THISCALL_2(return_type, address, a1, a2) ((return_type(__thiscall *)(UINT, UINT))(address))((UINT)(a1), (UINT)(a2))
#endif

char  __stdcall get_hero_SS(HERO* H, int skilllnum);
using namespace h3;

void fix_hero_dialog(HERO* current_HERO, H3BaseDlg* current_HERO_DLG) {
	if (current_HERO) 
	{
		for (int i = 1; i <= 8; i++)
		{
			int skill_num = -1; for (int j = 0; j < _skill_limit_; ++j)
				if (i == hero_SSShow_new[current_HERO->Number][j])
					skill_num = j;

			// current_HERO->SSNum = hero_SSShow_new[current_HERO->Number][skill_num];

			H3DlgDef* current_ICON = THISCALL_2(H3DlgDef*, 0x5FF5B0, current_HERO_DLG, 78 + i);
			if (current_ICON) {
				// current_ICON->defFrame = show_hero_SS[i];
				
				// current_ICON->defFrame = get_hero_SS(current_HERO, skill_num) + skill_num * 16 + 16;
				current_ICON->SetFrame(get_hero_SS(current_HERO, skill_num) + skill_num * 16 + 16);
				
				//THISCALL_1(VOID, current_ICON->vTable->draw, current_ICON);
				current_ICON->Draw();
			}
		}
		current_HERO->SSNum = 0;
		for (int k = 0; k < _skill_limit_; ++k)
			if (get_hero_SS(current_HERO, k))
				++current_HERO->SSNum;
	}
}

_LHF_(hook_HERO_Screen_Refresh) {
	auto current_HERO_DLG = (H3BaseDlg*)c->ebx;
	/* static */ HERO* current_HERO = nullptr;
	if(*(int*)0x698B70) current_HERO = (HERO*)*(int*)0x698B70;

	
	if (current_HERO && !count_SSShow(current_HERO->Number))
	{
		fix_hero_SSShow(current_HERO->Number);
		fix_hero_dialog(current_HERO, current_HERO_DLG);
	}

	return EXEC_DEFAULT;
}

_LHF_(Hook_0074485B) {
	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int j = *(int*)(c->ebp - 0x304);

	if (H) c->eax = hero_SSShow_new[H->Number][j];
	else c->eax = 0;

	c->return_address = 0x00744862;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_00744903) {
	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int j = *(int*)(c->ebp - 0x304);

	if (H) hero_SSShow_new[H->Number][j] = 0;

	c->return_address = 0x00744916;
	return NO_EXEC_DEFAULT;
}


_LHF_(Hook_00744918) {
	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int j = *(int*)(c->ebp - 0x3C0);
	char val = *(char*)(c->ebp - 0x3C4);

	if (H) hero_SSShow_new[H->Number][j] = val;

	c->return_address = 0x00744930;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_00744A6E) {

	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = *(int*)(c->ebp - 0x54);

	if (H) hero_SSShow_new[H->Number][skill] = 0;

	c->return_address = 0x00744A75;
	return NO_EXEC_DEFAULT;
}


_LHF_(Hook_00744A9B) {

	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = *(int*)(c->ebp - 0x54);

	c->edx = hero_SSShow_new[H->Number][skill];

	c->return_address = 0x00744AA2;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_00744AE1) {

	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = *(int*)(c->ebp - 0x54);

	hero_SSShow_new[H->Number][skill] = H->SSNum;

	c->return_address = 0x00744AED;
	return NO_EXEC_DEFAULT;
}

extern PatcherInstance* Zecondary_Skills;



_LHF_(Hook_007447AC) {
	HERO* H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = *(int*)(c->ebp - 0x3c0);

	c->edx = int(hero_SSShow_new[H->Number][skill]);
	c->return_address = 0x007447B3;
	return NO_EXEC_DEFAULT;

}


_LHF_(Hook_007448F4) {

	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x304));

	c->eax = hero_SSShow_new[H->Number][skill];
	c->return_address = 0x007448FB;
	return NO_EXEC_DEFAULT;
}


#include "Storage.h"
char  __stdcall get_hero_SS(HERO* H, int skilllnum);
char* __stdcall ptr_hero_SS(HERO* H, int skilllnum);

_LHF_(Hook_0074498F) {
	c->eax = (int)ptr_hero_SS((HERO*)(char*)c->edx, c->ecx);
	c->return_address = 0x00744996;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_007449B1) {
	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x54));

	c->edx = get_hero_SS(H, skill);

	c->return_address = 0x007449B8;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_007449C9) {

	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x54));

	c->ecx = hero_SSShow_new[H->Number][skill];

	c->return_address = 0x007449D0;
	return NO_EXEC_DEFAULT;

}

/*
_LHF_(Hook_007449C9) {

	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x54));

	c->ecx = hero_SSShow_new[H->Number][skill];

	c->return_address = 0x007449D0;
	return NO_EXEC_DEFAULT;

}
*/

_LHF_(Hook_007449E1) {

	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x54));

	c->eax = hero_SSShow_new[H->Number][skill];

	c->return_address = 0x007449E8;
	return NO_EXEC_DEFAULT;

}

_LHF_(Hook_00744A26) {

	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);
	int skill = (int)(*(int*)(c->ebp - 0x304));

	c->ecx = hero_SSShow_new[H->Number][skill];

	c->return_address = 0x00744A2D;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_00744A50) {
	auto H = (HERO*)(char*)*(int*)(c->ebp - 0x380);

	int skill_ecx = (int)(*(int*)(c->ebp - 0x54));
	int skill_eax = (int)(*(int*)(c->ebp - 0x304));

	/// TODO: check
	hero_SSShow_new[H->Number][skill_eax] =
		hero_SSShow_new[H->Number][skill_ecx];

	c->return_address = 0x00744A5C;
	return NO_EXEC_DEFAULT;
}

void patch_ERM() {

	Zecondary_Skills->WriteByte(0x0074477E + 6, current_SS_count - 1);
	Zecondary_Skills->WriteByte(0x007448A2 + 6, current_SS_count - 1);
	Zecondary_Skills->WriteByte(0x00744970 + 3, current_SS_count - 1);

	Zecondary_Skills->WriteByte(0x00744846 + 6, current_SS_count);
	Zecondary_Skills->WriteByte(0x007448DF + 6, current_SS_count);
	Zecondary_Skills->WriteByte(0x00744A11 + 6, current_SS_count);

	Zecondary_Skills->WriteLoHook(0x007447AC, Hook_007447AC);
	Zecondary_Skills->WriteLoHook(0x007448F4, Hook_007448F4);
	Zecondary_Skills->WriteLoHook(0x0074498F, Hook_0074498F);
	Zecondary_Skills->WriteLoHook(0x007449B1, Hook_007449B1);
	Zecondary_Skills->WriteLoHook(0x007449C9, Hook_007449C9);
	Zecondary_Skills->WriteLoHook(0x007449E1, Hook_007449E1);
	Zecondary_Skills->WriteLoHook(0x00744A26, Hook_00744A26);

	Zecondary_Skills->WriteLoHook(0x00744A50, Hook_00744A50);
}

void WoG_Area_install() {

	Zecondary_Skills->WriteLoHook(0x0074485B,Hook_0074485B);

	Zecondary_Skills->WriteLoHook(0x00744903, Hook_00744903);
	Zecondary_Skills->WriteLoHook(0x00744918, Hook_00744918);
	Zecondary_Skills->WriteLoHook(0x00744A6E, Hook_00744A6E);
	Zecondary_Skills->WriteLoHook(0x00744A9B, Hook_00744A9B);
	Zecondary_Skills->WriteLoHook(0x00744AE1, Hook_00744AE1);

	Zecondary_Skills->WriteLoHook(0x00754C10, hook_HERO_Screen_Refresh);

	patch_ERM();

}