#pragma once

#include"call_convention.hpp"
inline int z_MsgBox(char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
	FASTCALL_12(int, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

	int IDummy;
	__asm {
		mov eax, 0x6992D0
		mov ecx, [eax]
		mov eax, [ecx + 0x38]
		mov IDummy, eax
	}
	return IDummy;
}
inline void z_shout(const char* Mes) {
	z_MsgBox((char*)Mes, 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
}

inline bool msg_answer() {
	__asm {
		mov eax, 0x6992D0
		mov ecx, [eax]
		cmp dword ptr[ecx + 0x38], 0x00007805
		jne l_not
	}
	return(1);
	l_not:
	return(0);
}