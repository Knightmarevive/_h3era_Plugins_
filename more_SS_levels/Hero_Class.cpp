// #include "patcher_x86.hpp"

#include "MyTypes.h"

#include "Storage.h"
#include "heroes.h"

struct _HeroType_ {
	int Belong2Town;
	int TypeName;
	int Agression;
	char PSKillStart[4];
	char ProbPSkillToLvl9[4];
	char ProbPSkillAfterLvl10[4];
	char ProbSSkill[28];
	char ProbInTown[9];
	char field_3d[3];
};

int new_ProbSSkill[_hero_class_limit_][_skill_limit_] = {};
int current_hero_class_count = 18;
extern PatcherInstance* Zecondary_Skills;
// auto HeroTypesTablePo = (_HeroType_*)*(int*)0x0067DCEC;

void init_hero_classes() {
	_HeroType_* old_hero_types =(_HeroType_*) *(int*) 0x0067DCEC;
	for (int i = 0; i < current_hero_class_count; ++i)
		for (int j = 0; j < _SOD_SS_Count_; ++j)
			new_ProbSSkill[i][j] = old_hero_types[i].ProbSSkill[j] * 1000;
}

int get_ProbSSkill(_HeroType_* HT, int SS) {
    auto HeroTypesTablePo = (_HeroType_*)*(int*)0x0067DCEC;
    return new_ProbSSkill[(HT - HeroTypesTablePo)][SS];
}

typedef HERO _Hero_; 
extern int SS_Limit_Per_Hero;
int aHerospec_txt = 0x00679CCC;
// auto HeroTypesTablePo = (_HeroType_*)*(int*)0x0067DCEC;
int MagicShoolsSkillsArray[] = {0x0e, 0x0f, 0x10, 0x11};

auto o_GameMgr = /* _GameMgr_* */(*(int*)0x00699538);
int(__fastcall* Random)(int Low, int High) = (int(__fastcall *)(int Low, int High)) 0x0050C7C0;
char  __stdcall get_hero_SS(HERO* H, int skilllnum);

extern int  current_SS_count;

signed int __stdcall /* __fastcall */ Hero_GetSSkillToUpgrade_Hook(HiHook* _hook, _Hero_* hero, signed int minSkillLev, signed int maxSkillLev, int SkipSkill)
{
    auto HeroTypesTablePo = (_HeroType_*)*(int*)0x0067DCEC;

    int vSpec; // eax
    _Hero_* Hero; // edi
    signed int vWisdomCycle; // eax
    signed int vMagicCycle; // esi
    int v8; // ecx
    signed int v9; // eax
    int v10; // eax
    signed int result = 0; // eax
    int v12; // edx
    int* v13; // esi
    int v14; // eax
    int v15; // bl
    int v16; // ebx
    signed int* v17; // esi
    int v18; // dl
    int SSkill; // eax
    int v20; // esi
    int ss_lvl; // bl
    signed int v22; // ecx
    int v23; // esi
    int v24; // dl
    signed int v25; // ecx
    _HeroType_* HeroClass; // [esp+Ch] [ebp-10h]
    _Hero_* v27; // [esp+10h] [ebp-Ch]
    char* o_GameMgr_isBannedSkills; // [esp+14h] [ebp-8h]
    signed int v29; // [esp+18h] [ebp-4h]

    Hero = hero;
    v29 = minSkillLev;
    v27 = hero;
    vSpec = hero->Spec;
    HeroClass = &HeroTypesTablePo[hero->Spec];

    o_GameMgr = /* _GameMgr_* */(*(int*)0x00699538); // todo
    
    o_GameMgr_isBannedSkills = (char*) (o_GameMgr+0x4E658); // o_GameMgr->BannedSkills;
    /*
    if (CampaignGame && o_GameMgr->Campaign.BigCampaignIndex == 14 && hero->Number == 45)
        o_GameMgr_isBannedSkills = Campaign14SolmyrBannedSkills;
    */
    if (hero->SSNum >= SS_Limit_Per_Hero)
    {
        v29 = 1;
        minSkillLev = 1;
    }
    if (minSkillLev >= maxSkillLev)
        return -1;
    if (vSpec == 1 || vSpec == 3 || vSpec == 5 || vSpec == 7 || vSpec == 9 || vSpec == 11 || vSpec == 13 || vSpec == 15)
    {
        vWisdomCycle = 3;
        vMagicCycle = 3;
    }
    else
    {
        vWisdomCycle = 6;
        vMagicCycle = 4;
    }
    v8 = hero->ExpLevel;
    if (vWisdomCycle + (unsigned __int8)/* Hero->LastWisdomOfferLev */ *(0x90+(char*)Hero) > v8
        || (v9 = Hero->SSkill[7], v9 >= maxSkillLev)
        || v9 < minSkillLev)
    {
        v10 = SkipSkill;
    }
    else
    {
        v10 = SkipSkill;
        if (SkipSkill != 7 && !o_GameMgr_isBannedSkills[7])// v28 = o_GameMgr->BannedSkills;
            return 7;
    }
    if (vMagicCycle + /*Hero->LastNewMagicOfferLev*/ (*(short*)(0x3f+(char*)Hero) )> v8 || v10 == 14 || v10 == 15 || v10 == 16 || v10 == 17)
        goto LABEL_50;
    v12 = 0;
    v13 = MagicShoolsSkillsArray;
    while (1)
    {
        v14 = *v13;
        
        //v15 = Hero->SSkill[*v13];
        v15 = get_hero_SS(Hero, *v13);

        if (v15 < maxSkillLev && v15 >= v29 && (v14 >= _SOD_SS_Count_ || !o_GameMgr_isBannedSkills[v14]))
        {
            if (v15 <= 0)
                v12 += get_ProbSSkill(HeroClass,v14);// HeroClass->ProbSSkill[v14];
            else
                ++v12;
        }
        ++v13;
        if ((signed int)v13 >= (signed int)aHerospec_txt)
            break;
        Hero = v27;
    }
    if (v12 <= 0)
    {
    LABEL_49:
        Hero = v27;
    LABEL_50:
        v20 = 0;
        SSkill = 0;
        while (1)
        {
            // ss_lvl = Hero->SSkill[SSkill];
            ss_lvl = get_hero_SS(Hero,SSkill);

            if (ss_lvl >= maxSkillLev || ss_lvl < v29 || SSkill == SkipSkill)
                goto LABEL_59;
            if (SSkill < _SOD_SS_Count_ && o_GameMgr_isBannedSkills[SSkill])
                break;
            v22 = get_ProbSSkill(HeroClass, SSkill);// HeroClass->ProbSSkill[SSkill];
            if (!get_ProbSSkill(HeroClass, SSkill))// (!HeroClass->ProbSSkill[SSkill])
                goto LABEL_56;
        LABEL_58:
            v20 += v22;
        LABEL_59:
            if (++SSkill >= /*28*/ current_SS_count)
            {
                if (v20)
                {
                    v23 = Random(1, v20);
                    // result = 0;
                    while (1)
                    {
                        // v24 = Hero->SSkill[result];
                        v24 = get_hero_SS(Hero,result%current_SS_count);

                        if (v24 < maxSkillLev && v24 >= v29 && result % current_SS_count != SkipSkill)
                            break;
                    LABEL_73:
                        if (++result >= /*28*/ current_SS_count*4)
                            return -1;
                    }
                    if (result % current_SS_count >= _SOD_SS_Count_ || !o_GameMgr_isBannedSkills[result % current_SS_count])
                    {
                        v25 = get_ProbSSkill(HeroClass, result%current_SS_count); // HeroClass->ProbSSkill[result];
                        if (!get_ProbSSkill(HeroClass, result%current_SS_count)) // (!HeroClass->ProbSSkill[result])
                        {
                        LABEL_70:
                            if (v24 > 0)
                                v25 = 1;
                        }
                        v23 -= v25;
                        if (v23 <= 0)
                            return result%current_SS_count;
                        goto LABEL_73;
                    }
                    v25 = 0;
                    goto LABEL_70;
                }
                // return -1;
            }
        }
        v22 = 0;
    LABEL_56:
        if (ss_lvl > 0)
            v22 = 1;
        goto LABEL_58;
    }
    v16 = Random(1, v12);
    v17 = MagicShoolsSkillsArray;
    while (1)
    {
        result = *v17 + result - result % current_SS_count;
        
        // v18 = v27->SSkill[*v17];
        v18 = get_hero_SS(v27,*v17);

        if (v18 < maxSkillLev && v18 >= v29 && (result % current_SS_count >= _SOD_SS_Count_ || !o_GameMgr_isBannedSkills[result % current_SS_count]))
        {
            if (v18 <= 0)
                v16 -= get_ProbSSkill(HeroClass, result%current_SS_count); //  HeroClass->ProbSSkill[result];
            else
                --v16;
            if (v16 <= 0)
                return result%current_SS_count;
        }
        ++v17;
        /*
        if ((signed int)v17 >= (signed int)aHerospec_txt)
            goto LABEL_49;
        */
        if ((int)v17 >= (int)MagicShoolsSkillsArray + sizeof(MagicShoolsSkillsArray) )
            goto LABEL_49;

    }
}

void install_hero_classes() {
	init_hero_classes();

    Zecondary_Skills->WriteHiHook(0x004DAF70, SPLICE_, EXTENDED_, FASTCALL_, Hero_GetSSkillToUpgrade_Hook);
}

#include<cstdio>
void ParseInt(char* buf, char* name, int* result);
extern "C" __declspec(dllexport) void ApplyHeroClass(int skillID, char* config_str) {
    char name[512];
    for (int i = 0; i < _hero_class_limit_; ++i) {
        sprintf(name,"Class_%i_chance=", i);
        ParseInt(config_str,name, &new_ProbSSkill[i][skillID]);
    }
}