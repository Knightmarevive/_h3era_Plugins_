#pragma once

// #include "patcher_x86.hpp"
#include "heroes.h"

typedef unsigned long h3func;
typedef INT8 h3unk;
typedef INT8 BOOL8;
typedef INT32 H3Dlg_proc;
typedef void H3LoadedPCX16;
typedef char H3Vector_Blank[16];
typedef HERO H3Hero;

// * size 38h
// * base manager format
struct H3Manager
{
protected:
	struct {
		h3func managerConstructor; // 0x44D200
		h3func managerDestructor;
		h3func managerUpdate;
		// goes on
	} *h3ManagerVTable;

	H3Manager* parent;
	H3Manager* child;
	h3unk _f_0C[4];
	INT z_order;
	CHAR name[28]; // 0x14
	INT32 nameEnd; // 0x30
	h3unk _f_34[4];
public:
	//_H3API_ VOID SetPreviousManager(H3Manager* prev);
	//_H3API_ VOID SetNextManager(H3Manager* next);
};


// * trading between two armies
struct H3SwapManager : public H3Manager // size 0x68
{
public:
	// * +38
	H3BaseDlg* dlg;
protected:
	h3unk _f_3C[4];
public: // +40
	H3Hero* hero[2]; // similar to https://github.com/potmdehex/homm3tools/blob/master/hd_edition/hde_mod/hde/structures/swapmgr.h
	INT32 heroSelected;
	INT32 heroClicked;
protected:
	h3unk _f_50[4];
public: // +54
	INT32 slotClicked;
protected:
	h3unk _f_58[5];
	BOOL8 samePlayer; // +5D
	h3unk _f_5E[10];
};

struct H3WindowManager : public H3Manager
{
protected:
	UINT32 resultItemID; // 0x38
	h3unk _f_3C[4];
	UINT32 drawBuffer; // 0x40
	struct H3LoadedPCX16* screenPcx16;
	struct H3Dlg* firstDlg;
	struct H3Dlg* lastDlg;
	h3unk _f_58[8];
public:

	enum H3ClickIDs
	{
		H3ID_OK = 30725,
		H3ID_CANCEL = 30726
	};

	//VOID H3Redraw(INT32 x, INT32 y, INT32 dx, INT32 dy);
	//UINT32 ClickedItemID() const;
	//BOOL ClickedOK() const;
	//BOOL ClickedCancel() const;
	//struct H3LoadedPCX16* GetDrawBuffer();
};