// #include "patcher_x86.hpp"
#include "MyTypes.h"

typedef void(*voidfun)(void);
extern char* Skill_Levels[];

// char** z_basic_1 = Skill_Levels + 1;
// int basic_1 = (int) z_basic_1;

int basic_1 = (unsigned int) (Skill_Levels + 1); // 0x6A75D8;

voidfun ret_004DADEC = (voidfun) 0x004DADEC;
voidfun ret_004DAE22 = (voidfun) 0x004DAE22;
voidfun ret_004DAE51 = (voidfun) 0x004DAE51;
voidfun ret_004F9351 = (voidfun)0x004F9351;

voidfun fun_6179DE = (voidfun)0x006179DE;
void* ptr_697428 = (void*) 0x697428;
void* SecondarySkillInfo_ptr = (void*) 0x0067DCF0;

_LHF_(hero_level_B2_Hooked) {

	int* ebp2 = (int*) c->ebp;
	int  eax3 = c->eax;
	int  ebp4 = c->ebp;
	int  ebx5 = c->ebx;

	//asm("cdq");
	ebp2[3] = (eax3 | 15) * 4 + (int)SecondarySkillInfo_ptr;
	//asm("cdq");
	*(int*)(ebp4 - 24) = (ebx5 | 15) * 4 + (int)SecondarySkillInfo_ptr;

	c->return_address = 0x004F9351;
	return NO_EXEC_DEFAULT;
}

// original adress = 0x004F92F7
__declspec(naked) void hero_level_B_patched(void)
{
	_asm {

		z_begin:
		//mov edi, 0x3
		cdq
		//idiv edi
		//mov eax, edx 
		//sub edx, 0x400
		//lea edx, [eax - 0x400]
		//lea eax, [eax - 0x400]
		mov eax, edx

		//shr edx, 4
		dec edx 
		and edx, 0xf
		shr eax, 4

		// mov eax, 0x55555556
		lea edx, dword ptr ds : [edx * 4 + basic_1]
		mov dword ptr ss : [ebp + 0xC] , edx
		//imul ebx
		mov eax, edx
		//shr eax, 0x1F
		add edx, eax
		mov eax, ebx
		mov edi, edx

		//mov ebx, 0x3
		cdq
		//idiv ebx
		//sub edx, 0x400
		///lea edx, [eax - 0x400]
		//lea eax, [eax - 0x400]
		mov eax, edx
		//shr edx, 4
		dec edx 
		and edx, 0xf
		shr eax, 4


		mov eax, dword ptr ds : [SecondarySkillInfo_ptr]
		mov ebx, dword ptr ss : [ebp + 0x8]
		
		// shl edi, 0x6
		// shl edi, 4
		// mov edi, 0x50

		mov ebx, dword ptr ds : [ebx + eax - 0x40]
		mov eax, dword ptr ds : [edi + eax - 0x40]
		push ebx
		mov ebx, dword ptr ss : [ebp + 0xC]
		mov ebx, dword ptr ds : [ebx]
		push ebx
		push eax
		lea edx, dword ptr ds : [edx * 4 + basic_1]
		mov dword ptr ss : [ebp - 0x18] , edx
		mov edx, dword ptr ds : [edx]
		push edx
		push ecx
		push ptr_697428

		jmp ret_004F9351
	}
}

// original adress = 0x004DADA4
__declspec(naked) void hero_level_A_patched(void) {
	_asm {
		// mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		mov edx, esi
		shl edx, 0x6
		mov eax, dword ptr ds : [0x6A5DC4]
		mov edx, dword ptr ds : [edx + ecx]
		push edx
		mov eax, dword ptr ds : [eax + 0x20]
		movsx edx, byte ptr ds : [ebx + esi + 0xC9]
		mov eax, dword ptr ds : [eax + 0x334]
		mov edx, dword ptr ds : [edx * 4 + basic_1]
		push edx
		mov edx, edi
		shl edx, 0x6 
		mov ecx, dword ptr ds : [edx + ecx]
		movsx edx, byte ptr ds : [ebx + edi + 0xC9]
		push ecx
		mov ecx, dword ptr ds : [edx * 4 + basic_1]
		lea edx, dword ptr ss : [ebp - 0x22C]
		push ecx
		push eax
		push edx

		// jmp ret_004DADEC

		call fun_6179DE
		lea edi, dword ptr ss : [ebp - 0x22C]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		add esp, 0x18
		repne scasb
		not ecx
		sub edi, ecx
		mov esi, edi
		mov edx, ecx
		mov edi, ptr_697428
		or ecx, 0xFFFFFFFF
		repne scasb
		mov ecx, edx
		dec edi
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0xF
		rep movsb

		// jmp ret_004DAE22

		mov edi, dword ptr ss : [ebp - 0x18]
		mov esi, dword ptr ss : [ebp - 0x1C]
		movsx ecx, byte ptr ds : [ebx + edi + 0xC9]

		//lea eax, dword ptr ds : [edi + edi * 2 + 0x3]
		mov eax, edi
		shl eax, 4
		lea eax, dword ptr ds : [eax + 0x10]

		//lea edx, dword ptr ds : [esi + esi * 2 + 0x3]
		mov edx, esi
		shl edx, 4
		lea edx, dword ptr ds : [edx + 0x10]

		add eax, ecx
		mov ecx, dword ptr ss : [ebp - 0x14]
		push eax
		movsx eax, byte ptr ds : [ebx + esi + 0xC9]
		add edx, eax
		push edx
		push ecx
		push ebx
		lea ecx, dword ptr ss : [ebp - 0xF8]

		jmp ret_004DAE51
	}
}


// original adress = 0x004DADA4
__declspec(naked) void hero_level_A_original(void) {
	_asm {
		// mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		mov edx, esi
		shl edx, 0x4
		mov eax, dword ptr ds : [0x6A5DC4]
		mov edx, dword ptr ds : [edx + ecx]
		push edx
		mov eax, dword ptr ds : [eax + 0x20]
		movsx edx, byte ptr ds : [ebx + esi + 0xC9]
		mov eax, dword ptr ds : [eax + 0x334]
		mov edx, dword ptr ds : [edx * 4 + basic_1]
		push edx
		mov edx, edi
		shl edx, 0x4
		mov ecx, dword ptr ds : [edx + ecx]
		movsx edx, byte ptr ds : [ebx + edi + 0xC9]
		push ecx
		mov ecx, dword ptr ds : [edx * 4 + basic_1]
		lea edx, dword ptr ss : [ebp - 0x22C]
		push ecx
		push eax
		push edx

		// jmp ret_004DADEC

		call fun_6179DE
		lea edi, dword ptr ss : [ebp - 0x22C]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		add esp, 0x18
		repne scasb
		not ecx
		sub edi, ecx
		mov esi, edi
		mov edx, ecx
		mov edi, ptr_697428
		or ecx, 0xFFFFFFFF
		repne scasb
		mov ecx, edx
		dec edi
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0x3
		rep movsb

		// jmp ret_004DAE22

		mov edi, dword ptr ss : [ebp - 0x18]
		mov esi, dword ptr ss : [ebp - 0x1C]
		movsx ecx, byte ptr ds : [ebx + edi + 0xC9]
		lea eax, dword ptr ds : [edi + edi * 2 + 0x3]
		lea edx, dword ptr ds : [esi + esi * 2 + 0x3]
		add eax, ecx
		mov ecx, dword ptr ss : [ebp - 0x14]
		push eax
		movsx eax, byte ptr ds : [ebx + esi + 0xC9]
		add edx, eax
		push edx
		push ecx
		push ebx
		lea ecx, dword ptr ss : [ebp - 0xF8]

		jmp ret_004DAE51
	}
}


// original adress = 0x004F92F7
__declspec(naked) void hero_level_B_original(void) 
{
	_asm {
		mov edi, 0x3
		cdq
		idiv edi
		mov eax, 0x55555556
		lea edx, dword ptr ds : [edx * 4 + basic_1]
		mov dword ptr ss : [ebp + 0xC] , edx
		imul ebx
		mov eax, edx
		shr eax, 0x1F
		add edx, eax
		mov eax, ebx
		mov edi, edx
		mov ebx, 0x3
		cdq
		idiv ebx
		mov eax, dword ptr ds : [SecondarySkillInfo_ptr]
		mov ebx, dword ptr ss : [ebp + 0x8]
		shl edi, 0x6
		mov ebx, dword ptr ds : [ebx + eax - 0x10]
		mov eax, dword ptr ds : [edi + eax - 0x10]
		push ebx
		mov ebx, dword ptr ss : [ebp + 0xC]
		mov ebx, dword ptr ds : [ebx]
		push ebx
		push eax
		lea edx, dword ptr ds : [edx * 4 + basic_1]
		mov dword ptr ss : [ebp - 0x18] , edx
		mov edx, dword ptr ds : [edx]
		push edx
		push ecx
		push ptr_697428

		jmp ret_004F9351
	}
}