// #include "patcher_x86.hpp"
// #include "call_convention.hpp"

#include "Storage.h"
#include "heroes.h"
#include "MyTypes.h"

extern int max_SS_level;
extern int max_level_UP_Counter;
extern int SS_Limit_Per_Hero;


#define	_dword_ unsigned __int32
#define CALL_2(return_type, call_type, address, a1, a2) \
	((return_type (call_type *)(_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2))
#define CALL_3(return_type, call_type, address, a1, a2, a3) \
	((return_type (call_type *)(_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3))


char hero_SS_ext	[_hero_limit_][_skill_limit_ - _SOD_SS_Count_] = {};
char hero_SSShow_new[_hero_limit_][_skill_limit_] = {};
int Storage_current_hero = -1;

int count_SSShow(int heronum) {
	int ret = 0;

	for (int i = 0; i < _skill_limit_; ++i)
		if (hero_SSShow_new[heronum][i]) ++ret;

	return ret;
}

char  __stdcall get_hero_SS(int heronum, int skilllnum) {
	auto H = (HERO*) GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) return H->SSkill[skilllnum];
	else return hero_SS_ext[heronum][skilllnum - _SOD_SS_Count_];
}

void  __stdcall set_hero_SS(int heronum, int skilllnum, int value) {
	auto H = (HERO*)GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) H->SSkill[skilllnum] = value;
	else hero_SS_ext[heronum][skilllnum - _SOD_SS_Count_] = value;
}

char  __stdcall get_hero_SS(HERO* H, int skilllnum) {
	// auto H = (HERO*)GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) return H->SSkill[skilllnum];
	else return hero_SS_ext[H->Number][skilllnum - _SOD_SS_Count_];
}

void  __stdcall set_hero_SS(HERO* H, int skilllnum, int value) {
	//auto H = (HERO*)GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) H->SSkill[skilllnum] = value;
	else hero_SS_ext[H-> Number][skilllnum - _SOD_SS_Count_] = value;
}

extern "C" __declspec(dllexport) char _get_hero_SS_(void * H, int skilllnum) {
	return get_hero_SS((HERO*)H, skilllnum);
}

extern "C" __declspec(dllexport) void _set_hero_SS_(void * H, int skilllnum,int value) {
	set_hero_SS((HERO*)H, skilllnum, value);
}

char* __stdcall ptr_hero_SS(int heronum, int skilllnum) {
	auto H = (HERO*)GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) return &H->SSkill[skilllnum];
	else return &hero_SS_ext[heronum][skilllnum - _SOD_SS_Count_];
}

char* __stdcall ptr_hero_SS(HERO* H, int skilllnum) {
	// auto H = (HERO*)GetHeroRecord(heronum);
	if (skilllnum < _SOD_SS_Count_) return &H->SSkill[skilllnum];
	else return &hero_SS_ext[H->Number][skilllnum - _SOD_SS_Count_];
}

int __stdcall Hero_SSkillAdd_Hook(HiHook* h, HERO* me, int skill, char val) {
	if (skill < _SOD_SS_Count_ && false) {

		return CALL_3(int, __thiscall, h->GetDefaultFunc(), me, skill, val);
	}
	else {
		// int heronum = me->Number; 
		// char*	skillptr	= &hero_SS_ext[me->Number][skill - _SOD_SS_Count_];
		char* skillptr = ptr_hero_SS(me, skill); char oldval = *skillptr;
		// char	oldval		=  hero_SS_ext[me->Number][skill - _SOD_SS_Count_];

		if (oldval <= 0)
		{
			if (me->SSNum < SS_Limit_Per_Hero)
			{
				*skillptr = val;
				
				//me->SShow[skill] = LOBYTE(me->SSNum) + 1;
				hero_SSShow_new[me->Number][skill] = LOBYTE(me->SSNum) + 1;

				++me->SSNum;
			}
		}
		else
		{
			*skillptr = val + oldval;
		}
		if (*skillptr > max_SS_level)
			*skillptr = max_SS_level;
		return *skillptr - oldval;
	}

	// TODO 2021-04-1
	
}

extern int  current_SS_count;
int __stdcall Hero_sub_004E24C0_hook(HiHook* h, HERO* me, int skill, char level) {
	if (skill < _SOD_SS_Count_ && false) {

		return CALL_3(int, __thiscall, h->GetDefaultFunc(), me, skill, level);
	}
	else {

		// char  v4 =  hero_SS_ext [me->Number][skill - _SOD_SS_Count_];
		char v4 = get_hero_SS(me, skill);
		// char* v5 = &hero_SS_ext [me->Number][skill - _SOD_SS_Count_];
		char* v5 = ptr_hero_SS(me, skill);
		int skilla = v4; int v3 = skill; int v6; char* v7; char* v8;

		if (v4 > 0)
		{
			v6 = v4 - level;
			*v5 = v6;
			if (v6 < 0)
				*v5 = 0;
			if (!*v5)
			{
				v7 = &hero_SSShow_new[me->Number][skill];// v7 = &me->SShow[v3];
				v8 =  hero_SSShow_new[me->Number];// v8 = me->SShow;


				do
				{
					if ((unsigned __int8)*v8 > (unsigned __int8)*v7)
						--* v8;
					++v8;
				} while ((signed int)&v8[ - (signed int)hero_SSShow_new[me->Number] ] < current_SS_count);
				*v7 = 0;
				--me->SSNum;
			}
		}
		return skilla - *v5;
	}

}

int __stdcall Hero_sub_004E2610_hook(HiHook* h, HERO* me, int a2) {
	signed int result; // eax

	result = 0;
	while (*((unsigned __int8*)/* me + result + 229*/ hero_SSShow_new[me->Number] + result) != a2 + 1)
	{
		if (++result >= current_SS_count)
			return -1;
	}
	return result;

}

char __stdcall Hero_sub_7643DE_Hook(HiHook* h) {

	char result; // al
	char v1; // [esp+Fh] [ebp-5h]
	signed int i; // [esp+10h] [ebp-4h]
	signed int j; // [esp+10h] [ebp-4h]
	signed int k; // [esp+10h] [ebp-4h]
	signed int l; // [esp+10h] [ebp-4h]

	int dword_28460EC = *(int*)(char*)0x28460EC;
	HERO* H = (HERO *) dword_28460EC;
	typedef char _BYTE;

	for (i = 0; i < current_SS_count; ++i)
	{
		*(_BYTE*)(i + /*dword_28460EC + 229*/ hero_SSShow_new[((HERO*)(dword_28460EC))->Number]) = 0;
		result = i + 1;
	}
	v1 = 1;
	
	for(int y= max_SS_level; y; --y)
		for (int z = 0; z < current_SS_count; ++z)
		{
			result = z + dword_28460EC;
			if (*(_BYTE*)(/*j + dword_28460EC + 201*/ ptr_hero_SS(H, z)) == y)
			{
				result = v1;
				*(_BYTE*)(z + /*dword_28460EC + 229*/ hero_SSShow_new[((HERO*)(dword_28460EC))->Number]) = v1++;
			}
		}


	return result;

	for (j = 0; j < current_SS_count; ++j)
	{
		result = j + dword_28460EC;
		if (*(_BYTE*)(/*j + dword_28460EC + 201*/ ptr_hero_SS(H,j)) == 3)
		{
			result = v1;
			*(_BYTE*)(j + /*dword_28460EC + 229*/ hero_SSShow_new[((HERO*)(dword_28460EC))->Number]) = v1++;
		}
	}
	for (k = 0; k < current_SS_count; ++k)
	{
		result = k + dword_28460EC;
		if (*(_BYTE*)( /*k + dword_28460EC + 201*/ ptr_hero_SS(H, k)) == 2)
		{
			result = v1;
			*(_BYTE*)(k + /*dword_28460EC + 229*/ hero_SSShow_new[((HERO*)(dword_28460EC))->Number] ) = v1++;
		}
	}
	for (l = 0; l < current_SS_count; ++l)
	{
		result = l + dword_28460EC;
		if (*(_BYTE*)(/*l + dword_28460EC + 201*/ptr_hero_SS(H, l)) == 1)
		{
			result = v1;
			*(_BYTE*)(l + /*dword_28460EC + 229*/ hero_SSShow_new[((HERO*)(dword_28460EC))->Number]) = v1++;
		}
	}
	return result;
}

void fix_hero_SSShow(int heronum) {
	int count = 0;
	for (int i = 0; i < _skill_limit_; ++i)
		if (get_hero_SS(heronum, i))
		{
			hero_SSShow_new[heronum][i] = ++count;
			if (count == 8) return;
		}
}

_LHF_(Net_Copy_Hero_Hook) {
	c->edx = *(int*)c->esi; int heronum = *(int*)(c->edi + 0x1a);
	auto fun = (*(void(__thiscall**)(int, char*, signed int))(*(_DWORD*)c->edx + 8));
	fun(heronum, hero_SS_ext[heronum], _skill_limit_ - _SOD_SS_Count_);

	return EXEC_DEFAULT;

}

_LHF_(Net_Copy_Hero_Hook2) {

	c->edx = *(int*)c->esi; int heronum = *(int*)(c->edi + 0x1a);
	auto fun = (*(void(__thiscall**)(int, char*, signed int))(*(_DWORD*)c->edx + 8));
	fun(heronum, hero_SSShow_new[heronum], _skill_limit_);

	return EXEC_DEFAULT;
}