// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.


#include "stdafx.h"

extern void CreateNewTable(void);
extern void CreateAdditionalTables(void);
extern void NewMonstersMissiles(void);

int conf_creatures=0x34;

void InitMainConfig(char *config_name)
{
    char *buf;

        FILE *fdesc;
        if(fdesc=fopen(config_name,"r"))
        {
            //----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            buf=(char*)malloc(fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            buf[fdesc_size]=0;
            fclose(fdesc);
            //-----------
            char *c=strstr(buf,"Creatures=");
            if(c==0) {conf_creatures=0x34;}
            else     {conf_creatures=atoi(c+strlen("Creatures="))-144; }
            free(buf);
        }
        else
        {
			conf_creatures = 0x34;
            MessageBoxA(0,"Missed file: ",config_name,0);
        }    
}




BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			InitMainConfig("Data\\amethyst.cfg");
			CreateNewTable();
			CreateAdditionalTables();
			NewMonstersMissiles();
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

