#pragma once


#include <windows.h>
#include <stdio.h>
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"




#define PINSTANCE_MAIN "granite"

//Savegame

extern Patcher * globalPatcher;
extern PatcherInstance * granite;


typedef struct
{
	int grail_id;

    void ApplyData()
    {
		granite->WriteDword(0x40EF33,grail_id);
    }
}GAMEDATA;


extern GAMEDATA save;