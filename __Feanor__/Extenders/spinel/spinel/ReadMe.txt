========================================================================
���������� ������������ ����������. ����� �������� spinel
========================================================================

��� ���������� DLL spinel ������� ������������� � 
������� ������� ����������.

   � ���� ����� ������������ ������ ����������� ���� ������, �������� � 
   ������ ���������� spinel.


spinel.vcproj
   �������� ���� ������� VC++, ������������� ����������� � ������� 
   ������� ����������.
   � ����� ������������ �������� � ������ Visual C++, �������������� 
   ��� �������� �����, � ����� � ���������� ���������, ������������ � 
   �������, �������� � ������� ������� ����������.

spinel.cpp
   �������� �������� ���� ���������� DLL.

   ��� �������� ���� ���������� DLL ������� �������� �� �����������. 
   ������� ��� �� ���������� �� ��������� ���� .lib. ����� ���������� 
   ������ � �������� ���������� �� ������� �������, �������� ��� ��� 
   �������� �������� �� ���������� DLL, � ���������� ���� ����� ������� 
   ���������� �������� ����� ����, ����� ������� ���������� ���� ��������� 
   �������, ����� � ����� ������������ �� �������� ������ �������� 
   ���������� ��� ��������� ����������� ������� ���������� �������� ���.

/////////////////////////////////////////////////////////////////////////////
������ ����������� �����:

StdAfx.h, StdAfx.cpp
   ��� ����� ������������ ��� ���������� ����� ����������������� ���������� 
   (PCH) spinel.pch � ����� ����������������� ����� 
   StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
����� �������:

� ������� ������������ �TODO:� � ������� ���������� ������������ ��������� 
��������� ����, ������� ���������� ��������� ��� ��������.

/////////////////////////////////////////////////////////////////////////////
