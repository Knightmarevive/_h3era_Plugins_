========================================================================
���������� ������������ ����������. ����� �������� chalk
========================================================================

��� ���������� DLL chalk ������� ������������� � 
������� ������� ����������.

   � ���� ����� ������������ ������ ����������� ���� ������, �������� � 
   ������ ���������� chalk.


chalk.vcproj
   �������� ���� ������� VC++, ������������� ����������� � ������� 
   ������� ����������.
   � ����� ������������ �������� � ������ Visual C++, �������������� 
   ��� �������� �����, � ����� � ���������� ���������, ������������ � 
   �������, �������� � ������� ������� ����������.

chalk.cpp
   �������� �������� ���� ���������� DLL.

   ��� �������� ���� ���������� DLL ������� �������� �� �����������. 
   ������� ��� �� ���������� �� ��������� ���� .lib. ����� ���������� 
   ������ � �������� ���������� �� ������� �������, �������� ��� ��� 
   �������� �������� �� ���������� DLL, � ���������� ���� ����� ������� 
   ���������� �������� ����� ����, ����� ������� ���������� ���� ��������� 
   �������, ����� � ����� ������������ �� �������� ������ �������� 
   ���������� ��� ��������� ����������� ������� ���������� �������� ���.

/////////////////////////////////////////////////////////////////////////////
������ ����������� �����:

StdAfx.h, StdAfx.cpp
   ��� ����� ������������ ��� ���������� ����� ����������������� ���������� 
   (PCH) chalk.pch � ����� ����������������� ����� 
   StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
����� �������:

� ������� ������������ �TODO:� � ������� ���������� ������������ ��������� 
��������� ����, ������� ���������� ��������� ��� ��������.

/////////////////////////////////////////////////////////////////////////////
