// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include <windows.h>
#include <stdio.h>
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"

void InitWoGSpecials()
{
	if (*(int *)(*(int*)0x69CCFC + 4)!=-1)
	{
		HERO* currhero = (HERO*)GetHeroRecord(*(int *)(*(int*)0x69CCFC + 4));

		*(int*)0x27F9970 = (int)currhero;
		ErmV[998] = currhero->x;
		ErmV[999] = currhero->y;
		ErmV[1000] = currhero->l;

		ErmF[1000]= !CALL_1(int, __thiscall, 0x4BAA60, *(int*)0x69CCFC);
	}
}

void PrintString(char* str, char* color)
{
	hprintf(0x69D800,"{~%s}%s{~}",color, str);

	/*	char *buf=(char*)malloc(32+strlen(str));
		sprintf(buf,"IF:L^{~%s}%s{~}^;\0",color,str);
		ExecErmCmd(buf);
		free(buf);*/
}

char *commands[] = 
{
	"/execerm ",			//0
	"/exec ",				//1	
	"!!",					//2
	"/leveldebugger",		//3
	"/heroid",				//4
	"/showfps",			//5
	"/chalk",				//6
	"/console on",				//7
	"/console off",				//8
};

#define COMMAND_COLOR "lime"
#define  ANSWER_COLOR "green"

void __stdcall  OnChat(PEvent e)
{
	if(EventParams[0] == 1) 
	{
		for(int i=0; i!=sizeof(commands)/4; i++)
		{
			if(!strnicmp((const char*)EventParams[1],commands[i],strlen(commands[i])))
			{
				EventParams[2] = 1;
				char *command = (char*)EventParams[1];

				PrintString(command,COMMAND_COLOR);

				switch (i)
				{
					case 0:
					case 1:
					case 2:
						{
							InitWoGSpecials();
							if(command[strlen(command)-2]==';')
								ExecErmCmd((char*)(EventParams[1]+strlen(commands[i])));
							else
							{
								char buf[512];
								strncpy(buf,(char*)(EventParams[1]+strlen(commands[i])),510);
								

								ExecErmCmd(strcat(buf,";"));
							}

							PrintString("Executed",ANSWER_COLOR);

							break;
						}

					case 3:
						{
							*(int*)0x698A18 = !*(int*)0x698A18;
							PrintString(*(int*)0x698A18?"Level debugger switched on":"Level debugger switched off", ANSWER_COLOR);
							break;
						}
					case 4: 
						{
							int id = *(int *)(*(int*)0x69CCFC + 4);
							if (id!=-1)
							{
								char str[256];
								sprintf(str,"%s: id %i, struct at 0x%08X",
																	((HERO*)GetHeroRecord(id))->Name,
																	id,
																	GetHeroRecord(id)
		     							);
								PrintString(str, ANSWER_COLOR);
							}
							else
							{
								PrintString("Hero not selected", ANSWER_COLOR);
							}
							break;
						}
					case 5: 
						{
							*(int*)((*(int*)0x6992B8)+0x3C) = !*(int*)((*(int*)0x6992B8)+0x3C);
							PrintString("FPS switched off", ANSWER_COLOR);
							break;
						}
					case 6:
						{
							PrintString("chalk.dll", ANSWER_COLOR);
							#define CHALKDUMMY "Compilation date:"  ## __DATE__  
							PrintString(CHALKDUMMY, ANSWER_COLOR);
							#define CHALKDUMMY "Compilation time:"  ## __TIME__  
							PrintString(CHALKDUMMY, ANSWER_COLOR);

							char buf[1024];
							memset(buf,0,1024);
							strcpy(buf,"Commands: ");
							for(int i=0; i!=sizeof(commands)/4; i++)
							{
								sprintf(buf,"%s %s",buf,commands[i]);
							}
							PrintString(buf, ANSWER_COLOR);
							break;
						}
					case 7:
						{
							AllocConsole();
							PrintString("Executed",ANSWER_COLOR);
							break;
						}
					case 8:
						{
							FreeConsole();
							PrintString("Executed",ANSWER_COLOR);
							break;
						}

					default:
						{
							PrintString("This command will be implemented in next versions.", ANSWER_COLOR);
						}

			}
		}
		}
	}
}

		/*EventParams[2] = 1;

		char *buf=(char*)malloc(32+strlen((const char*)EventParams[1]));
		sprintf(buf,"IF:L^{~%s}%s{~}^;\0","yellow",EventParams[1]);
		ExecErmCmd(buf);
		free(buf);

		char str[256];

		if(!stricmp((const char*)EventParams[1],"/showfps"))
			{
				*(int*)((*(int*)0x6992B8)+0x3C) = !*(int*)((*(int*)0x6992B8)+0x3C);
				PrintString("FPS switched off");
			}

		if(!stricmp((const char*)EventParams[1],"/leveldebugger"))
			{
				*(int*)0x698A18 = !*(int*)0x698A18;
				PrintString(*(int*)0x698A18?"Level debugger switched on":"Level debugger switched off");
			}

		if(!stricmp((const char*)EventParams[1],"/heroid"))
			{
				int id = *(int *)(*(int*)0x69CCFC + 4);
				if (id!=-1)
				{
					sprintf(str,"%s: id %i, struct at 0x%08X",
														((HERO*)GetHeroRecord(id))->Name,
														id,
														GetHeroRecord(id)
		     				);
				}
				else
				{
					strcpy(str,"Hero not selected");
				}

				PrintString(str);
			}

		if(!strnicmp((const char*)EventParams[1],"/execerm",strlen("/execerm")))
		{
			InitWoGSpecials();

			ExecErmCmd((char*)(strlen("/execerm")+1+EventParams[1]));
			PrintString("Executed");
		}

		if(!strnicmp((const char*)EventParams[1],"!!",strlen("!!")))
		{
			InitWoGSpecials();

			ExecErmCmd((char*)(strlen("!!")+EventParams[1]));
			PrintString("Executed");
		}
*/

		//ExecErmCmd(str);
		//MessageBoxA(0,(LPCSTR)(EventParams[1]),(LPCSTR)str,0);



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		ConnectEra();	
		RegisterHandler(OnChat, "OnChat");
	}
	return TRUE;
}


