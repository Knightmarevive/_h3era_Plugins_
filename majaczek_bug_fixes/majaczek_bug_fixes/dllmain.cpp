// dllmain.cpp : Defines the entry point for the DLL application.

#include "pch.h"
#include "era.h"
#include "patcher_x86.hpp"
#include <cstdio>

#define PINSTANCE_MAIN "majaczek_bug_fixes"
#define PEvent Era::TEvent*

Patcher* globalPatcher;
PatcherInstance* PI;

bool KnightmareKingdoms;

// #include "call_convention.hpp"

//#include "H3API/lib/H3API.hpp"
#include "../../__include__/H3API/single_header/H3API.hpp"
#include "heroes.h"

typedef h3::H3CombatManager _BattleMgr_;
typedef h3::H3CombatMonster _BattleStack_;

bool check_batlefield_space(_BattleMgr_* BatMan) {
    if (!BatMan) return false;

    int side = BatMan->currentMonSide;
    return( BatMan->heroMonCount[side] < 20) ;

    /*
    if (BatMan->stacks[side][19].numberAlive > 0)
    {
        return false;
    }
    */
    
    //int count = 0;
    //for (int stack = 0; stack < 21; ++stack)
      /*
        if (// BatMan->stacks[side][stack].numberAlive > 0
             BatMan->stacks[side][stack].type >= 0
            )
                 ++count;
        */
        // if(BatMan->stacks[side][stack].def) ++count;
        /*
        if (BatMan->stacks[side][stack].numberAtStart != 0
            || BatMan->stacks[side][stack].IsClone()
            || BatMan->stacks[side][stack].IsSummon())
            ++count;
         */
            
    /*
    if (count >= 21) {
        return false;
    }
   
    return true;

    */
}

int SS15[16]    = { 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5 };
int fire[8]     = {114,114,114,129,302,334,334,334};
int air[8]      = {112,112,112,127,208,332,332,332};
int water[8]    = {115,115,115,123,224,335,335,335};
int earth[8]    = {113,113,113,125,201,333,333,333};

int  __stdcall CombatMan_SummonMonster_HiHook(HiHook* h, _BattleMgr_* BatMan, int spell_id, int MonType, int spell_power, int spell_lvl) {
    int ret = 0; // int count = 0;
    int side = BatMan->currentMonSide;


    if (KnightmareKingdoms) __asm {
        mov esi, [BatMan]
        mov dword ptr ds : [esi + 0x132A8] , -1
        mov dword ptr ds : [esi + 0x132AC] , -1
    }

    if (!check_batlefield_space(BatMan)) return ret;

    if (KnightmareKingdoms) {
        h3::H3Hero* current_hero = BatMan->hero[side];
        if (current_hero) {
            if (spell_id == 66 && MonType == 0x72) MonType = fire   [SS15[current_hero->secSkill[14]]];
            if (spell_id == 67 && MonType == 0x71) MonType = earth  [SS15[current_hero->secSkill[17]]];
            if (spell_id == 68 && MonType == 0x73) MonType = water  [SS15[current_hero->secSkill[16]]];
            if (spell_id == 69 && MonType == 0x70) MonType = air    [SS15[current_hero->secSkill[15]]];
        }
    }

    ret = THISCALL_5(int, h->GetDefaultFunc(), BatMan, spell_id, MonType, spell_power, spell_lvl);

    if (KnightmareKingdoms) __asm{
        mov esi, [BatMan]
        mov dword ptr ds : [esi + 0x132A8] , -1
        mov dword ptr ds : [esi + 0x132AC] , -1
    }
    return ret;
}

char __stdcall CombatMan_SummonClone_HiHook(HiHook *h, _BattleMgr_* BatMan, signed int gex, int spellpower) {

    char ret = 0; // int count = 0;
    //int side = BatMan->currentMonSide;

    if (!check_batlefield_space(BatMan)) return ret;

    ret = THISCALL_3(char, h->GetDefaultFunc(), BatMan,gex,spellpower);

    return ret;
}
signed int __stdcall BattleMgr_CanUseSpell_HiHook(HiHook* h, _BattleMgr_* BatMan, int spell, int side, _BattleStack_* targetMon, int a5, int a6) {
    if (spell == 0xbaadf00d || ((int) targetMon == 0xbaadf00d) || a5 == 0xbaadf00d || a6 == 0xbaadf00d)  return 0;
    return THISCALL_6(signed int, h->GetDefaultFunc(), BatMan, spell, side, targetMon, a5, a6);
}

struct Byte_replace {
    long adress;
    unsigned char value;
};

#pragma warning(disable : 4996)
void __stdcall Fix_KK_Buttons(PEvent e) {
    char* buf;

    FILE* fdesc;
    if (fdesc = fopen("mods\\list.txt", "r"))
    {
        //----------
        fseek(fdesc, 0, SEEK_END);
        int fdesc_size = ftell(fdesc);
        rewind(fdesc);
        //----------
        buf = (char*)malloc(fdesc_size + 1);
        fread(buf, 1, fdesc_size, fdesc);
        buf[fdesc_size] = 0;
        fclose(fdesc);
        //-----------
        {
            char* c = strstr(buf, "Knightmare Kingdoms");
            if (c) KnightmareKingdoms = true;
            else { 
                KnightmareKingdoms = false;
                free(buf);
                return;
            }
        }
        free(buf);
    }

    Byte_replace data[] = {
        {0x0046B2CC + 0,0x26},{0x0046B2CC + 1,0x6A},{0x0046B2CC + 2,0x05},{0x0046B2CC + 3,0x6A},{0x0046B2CC + 4,0x2C},
        {0x0046B351 + 0,0x26},{0x0046B351 + 1,0x6A},{0x0046B351 + 2,0x05},{0x0046B351 + 3,0x6A},{0x0046B351 + 4,0x55},
        {0x0046B3D6 + 0,0x26},
        {0x0046B45B + 0,0x26},{0x0046B45B + 1,0x6A},{0x0046B45B + 2,0x05},{0x0046B45B + 3,0x68},{0x0046B45B + 4,0xA7},
        {0x004E1326 + 0,0x24},{0x004E1326 + 1,0x68},{0x004E1326 + 2,0xAD},{0x004E1326 + 3,0x01},{0x004E1326 + 4,0x00}, {0x004E1326 + 5,0x00} ,{0x004E1326 + 6,0x68}, {0x004E1326 + 7,0x3A},
        {0x004E1377 + 0,0x24},{0x004E1377 + 1,0x68},{0x004E1377 + 2,0xAD},{0x004E1377 + 3,0x01},{0x004E1377 + 4,0x00}, {0x004E1377 + 5,0x00} ,{0x004E1377 + 6,0x68}, {0x004E1377 + 7,0x2A},{0x004E1377 + 8,0x02},
        {0,0} 
    };
    for(int i=0;data[i].adress;++i)
        PI->WriteByte(data[i].adress, data[i].value);
}

_LHF_(StartBATTLE_nullptr_1) {
    int* ptr = (int*) *(int*) 0x00699420;// c->edx;
    if (!*ptr) *ptr = 0x63D3E8; // 0x00462600;
    return EXEC_DEFAULT;
}

_LHF_(ExecMgr_AddManager_nullptr_1) {
    if (c->eax) return EXEC_DEFAULT;
    
    /*
    static int ptr = 0x00462600;
    c->eax = (int) & ptr;
    return EXEC_DEFAULT;
    */
    char message[512] = "Broken ExecMgr_AddManager catched nullptr";
    FASTCALL_12(VOID, 0x4F6C00, message, 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);

    c->edi = c->Pop();
    c->esi = c->Pop();
    c->eax = 0;
    c->return_address = 0x004B0896;
    return NO_EXEC_DEFAULT;
    
/*
    c->return_address = 0x004B0942; // 0x004B088F;
    return NO_EXEC_DEFAULT;
*/

}

_LHF_(ShowParsedDlg8Items_fix_size) {
    int &XSize = *(int*)(c->ebp + 0x30); int &XPos = *(int*)(c->ebp + 0x28);
    int &YSize = *(int*)(c->ebp + 0x34); int &YPos = *(int*)(c->ebp + 0x2c);
    char &scroll_bar_show = *(char*)(c->ebp + 0x38);
    char* text = (char*)*(int*)(c->ebp+0x0C);
    char* medfont = (char*)0x65F2EC;
    int loaded_font= THISCALL_1(int, 0x55BD10, medfont);
    int result[4] = {};
    THISCALL_4(void, 0x4B58F0, loaded_font,text,XSize,result);
    int& first = result[1]; int& last = result[2];
    int rows = (last - first) / 16; int base_size = rows - 14;
    if (rows > 14) {
        scroll_bar_show = false; char charHeight = *(char*)(loaded_font + 33);
        int offset = charHeight * rows ; 
        
        YSize -= offset;
        YPos -= offset/2;
    }

    return EXEC_DEFAULT;
}


void CreExpoFix_Apply();
void H3HeapFix_Apply();

_LHF_(BattleMgr_CastSpell_005A23C8_nullptr) {
    auto her = *(h3::H3Hero**)(c->ebp-0x14);
    if (her) return EXEC_DEFAULT;

    c->return_address = 0x005A2479;
    return SKIP_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {

        globalPatcher = GetPatcher();
        PI = globalPatcher->CreateInstance((char*)PINSTANCE_MAIN);
        Era::ConnectEra();
        PI->WriteHiHook(0x005A6F80, SPLICE_, EXTENDED_, THISCALL_, CombatMan_SummonClone_HiHook);
        //PI->WriteHiHook(0x005A7390, SPLICE_, EXTENDED_, THISCALL_, CombatMan_SummonMonster_HiHook);
        //PI->WriteHiHook(0x005A3F90, SPLICE_, EXTENDED_, THISCALL_, BattleMgr_CanUseSpell_HiHook);
        PI->WriteLoHook(0x004B08BB,ExecMgr_AddManager_nullptr_1);
        PI->WriteLoHook(0x004ADFD6, StartBATTLE_nullptr_1);

        PI->WriteLoHook(0x4F7116, ShowParsedDlg8Items_fix_size);

        PI->WriteLoHook(0x005A23C8, BattleMgr_CastSpell_005A23C8_nullptr);

        CreExpoFix_Apply();
        H3HeapFix_Apply();

        Era::RegisterHandler(Fix_KK_Buttons, "OnGameEnter");
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

