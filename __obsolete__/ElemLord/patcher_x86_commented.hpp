////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ����������� patcher_x86.dll 
// ���������������� ��������(���������)
// ��������� �����: ������� ��������� (baratorch), e-mail: baratorch@yandex.ru
// ����� ���������� �������������� ����� (LoHook) ������� �������������� � Berserker (�� ERA)
////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ��������.
//
// ! ���������� �������������:
//		- ������� ��������������� ���������������� 
//		  ����������� ��� ��������� ������ � �����
//		  � ��� ������� ���������.
//		- �������������� �����������: ������������ ���� ������� � �������
//		  ���������� ��� � ���������� ��������� ������� jmp � call c 
//		  ������������� ����������
// ! ���������� ���������
//		- ������������� ��� ������� ��� � ������� �����.
//		  � �������� �� ��������� ������� ������ ����� ��� �� ������ ��������
//		  ��� � ����������� (���� �� ������� ������ ����� � ������� � ������)
//		- ������������� ��������������� ����, ������� ������������ ������� �
//		  � ������� ���� �� ����, �� �������� � ��������� ����������,
//		  �����, � �������� � ������������ ���.
//		- ������������� ��������������� ���� ���� �� ������
//		  �� �������� � �������� ��� ���� ���������������� �����
//		  ������������� ������ ����������, ��� ����� ����������� ��������� ������������
//		- ������������� �������������� ���� � ��������������� �������� �
//		  ��������� ����������, ��������� ���� � ������ �������� � ���
//		- �������� ����� ���� � ���, ������������� � ������� ���� ����������.
//		- ������ ������������ �� ������������ ���, ������������ ����������
//		- ������ ����� ��� (������������ ����������) ��������� ������������ ����/���
//		- �������� ������ ������ �� ���� ������/�����, ������������� �� ������ ����� 
//		  � ������� ���� ����������
//		- ����� � ������ ���������� ������������� ����� �� ������ �����
//		  (������������ ��� ����������) 1) �������� ���� ����� ��������� ���:
//								- ��������������� �����/���� ������� ������� �� ���� �����
//								- ��������������� �����/���� ������������� ���� ������� �� ���������
//								- ��������������� ����� ������ ����� � ��������
//		  � ��� �� 2) ����� ����������� ���������� ���� (����� �������) ���� ������ 
//		  � ����� ������������� � ������� ���� ���������� � ���������� ������ �������.
////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// �����������.
//
// �� ��������� � patcher_x86.dll ����������� ���������, ����� �������� ���,
// ���������� � ��� �� ����� ������� ���� patcher_x86.ini c ������������
// �������: Logging = 1 (Logging = 0 - ��������� ����������� �����)
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ������� �������������.
//
// 1) ������ ��� ������ 1 ��� ������� ������� GetPatcher(), �������� ���������
//		��������: Patcher* _P = GetPatcher();
// 2) ����� � ������� ������ Pather::CreateInstance ����� �������  
// ��������� Pat�herInstance �� ����� ���������� ������
//		��������:	Pat�herInstance* _PI = _P->CreateInstance("HD");
//		���:		Pat�herInstance* _PI = _P->CreateInstance("HotA");
// 3)  ����� ������������ ������ ������� Pat�her � Pat�herInstance
// ��������������� ��� ������ � ������� � ������
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#define	_byte_ unsigned __int8
#define	_word_ unsigned __int16
#define	_dword_ unsigned __int32


//������� CALL_? ��������� �������� ������������ ������� �� ������������� ������
//������������ � ��� ����� ��� ������ �������
//���������� � ������� HiHook::GetDefaultFunc � HiHook::GetOriginalFunc
#define CALL_0(return_type, call_type, address) \
	((return_type (call_type *)(void))address)()
#define CALL_1(return_type, call_type, address, a1) \
	((return_type (call_type *)(_dword_))(address))((_dword_)(a1))
#define CALL_2(return_type, call_type, address, a1, a2) \
	((return_type (call_type *)(_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2))
#define CALL_3(return_type, call_type, address, a1, a2, a3) \
	((return_type (call_type *)(_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3))
#define CALL_4(return_type, call_type, address, a1, a2, a3, a4) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4))
#define CALL_5(return_type, call_type, address, a1, a2, a3, a4, a5) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5))
#define CALL_6(return_type, call_type, address, a1, a2, a3, a4, a5, a6) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6))
#define CALL_7(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7))
#define CALL_8(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8))
#define CALL_9(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9))
#define CALL_10(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10))
#define CALL_11(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11))
#define CALL_12(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12))
#define CALL_13(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12),(_dword_)(a13))
#define CALL_14(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12),(_dword_)(a13),(_dword_)(a14))
#define CALL_15(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12),(_dword_)(a13),(_dword_)(a14),(_dword_)(a15))
#define CALL_16(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12),(_dword_)(a13),(_dword_)(a14),(_dword_)(a15),(_dword_)(a16))
#define CALL_17(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12),(_dword_)(a13),(_dword_)(a14),(_dword_)(a15),(_dword_)(a16),(_dword_)(a17))

#define CALL_VA(return_type, adress, a1, ...) \
	((return_type (__cdecl *)(_dword_, ...))(adress))((_dword_)(a1), __VA_ARGS__)


// _bool_ - 4-� �������� ���������� ���, ��� BOOL � Win32 API
// ���� ����� ���������, ����� �������� �� BOOL ��� ������������ bool ��������
#define	_bool_ __int32

// ��� ������ � ����� ���������� ���������� ���� �����,
// ���� ��� ������� ��-�������, ������ �������� _ptr_ �� ����� ������ ���, void* ��� int ��������
typedef _dword_	_ptr_;

// �� ���� ���������� � ����������� ���������� ������ ���� ������������ - 4 �����
#pragma pack(push, 4)

// ��������� HookContext
// ������������ � �������� ����������� �� LoHook ����
struct HookContext
{
	int eax; //������� EAX, ������/���������
	int ecx; //������� ECX, ������/���������
	int edx; //������� EDX, ������/���������
	int ebx; //������� EBX, ������/���������
	int esp; //������� ESP, ������/���������
	int ebp; //������� EBP, ������/���������
	int esi; //������� ESI, ������/���������
	int edi; //������� EDI, ������/���������

	_ptr_	return_address; //����� ��������, ������/���������
};

// �������� ������������ �������� ������������� �� LoHook ����
#define EXEC_DEFAULT	1
#define NO_EXEC_DEFAULT 0


// �������� ������������� Patch::GetType()
#define PATCH_  0
#define LOHOOK_ 1
#define HIHOOK_ 2


// �������� ������������ PatcherInstance::Write() � PatcherInstance::CreatePatch()
#define DATA_ 0
#define CODE_ 1


// ����������� ����� Patch
// ������� ��������� ����� �
// ������� ������� ������ PatcherInstance
class Patch
{
public:
	// ���������� ����� �� �������� ��������������� ����
	virtual _ptr_	__stdcall GetAddress() = 0; 

	// ���������� ������ �����
	virtual _dword_	__stdcall GetSize() = 0;	

	// ���������� ���������� ��� ���������� PatcherInstance, � ������� �������� ��� ������ ����
	virtual char*	__stdcall GetOwner() = 0;

	// ���������� ��� �����
	// ��� �� ���� ������ PATCH_
	// ��� LoHook ������ LOHOOK_
	// ��� HiHook ������ HIHOOK_
	virtual int		__stdcall GetType() = 0;

	// ���������� true, ���� ���� �������� � false, ���� ���.
	virtual _bool_	__stdcall IsApplied() = 0;

	// ��������� ���� 
	// ���������� >= 0 , ���� ����/��� ���������� �������
	// (������������ �������� �������� ���������� ������� ����� � ������������������
	// ������, ����������� �� ������� ������, ��� ������ �����, 
	// ��� ������� ��� �������� ����)
	// ���������� -1, ���� ��� (� ������ 1.1 ���� ����������� ������ �������)
	// ���������� -2, ���� ���� ��� ��������
	// ��������� ���������� ������ ��������������� ������� � ���
	// � ������� ������������ ���������� (��. ����� �������� ���������� ����)
	// ����� ����������� ����  (� ������� ��� ��� ����� ����������� ����) ���������� ��� 
	// ������������ (FIXED), � � ��� ������� �������������� � ���������.
	virtual _bool_	__stdcall Apply() = 0;

    // ApplyInsert ��������� ���� � ��������� ����������� ������ �
	// ������������������ ������, ����������� �� ����� ������.
	// ������������ �������� ���������� �������������� � Patch::Apply
	// ��������! ��������� ���� ����� FIXED ������ ������, ������� 
	// ������������ ���������� ����� ����� ���������� �� ���������, ����������� ����������.
	// ������� ApplyInsert ����� ���������� �������� ��������, ������������ 
	// �������� Undo, ����� ��������� ���� � �� �� �����, �� ������� ��� ��� �� ���������.
	virtual _bool_	__stdcall ApplyInsert(int zorder) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// ����� Undo
	// �������� ����(���) (� ������ ���� ���� �������� ��������� - ��������������� �������� ���)
	// ���������� ����� >= 0, ���� ����(���) ��� ������� ������� 
	// (������������ �������� �������� ������� ����� � ������������������
	// ������, ����������� �� ������� ������, ��� ������ �����, 
	// ��� ������� ��� �������� ����)
	// ���������� -2, ���� ���� � ��� ��� ��� ������� (�� ��� ��������)
	// ���������� -3, ���� ���� �������� ������������ (FIXED) (��. ����� Apply)
	// ��������� ���������� ������ ��������������� ������� � ���
	virtual _bool_	__stdcall Undo() = 0;


	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// ����� Destroy
	// ����������
	// ������������ ���������� ����/���
	// ���������� ����� ������ ���������� ����/���.
	// ���������� 1, ���� ����(���) ��������� �������
	// ���������� 0, ���� ���� �� ���������
	// ��������� ����������� ��������������� ������� � ���
	virtual _bool_	__stdcall Destroy() = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// ����� GetAppliedBefore
	// ���������� ���� ����������� ����� ������
	// ���������� NULL ���� ������ ���� �������� ������
	virtual Patch* __stdcall GetAppliedBefore() = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// ����� GetAppliedAfter
	// ���������� ���� ����������� ����� �������
	// ���������� NULL ���� ������ ���� �������� ���������
	virtual Patch* __stdcall GetAppliedAfter() = 0;

};


// ����������� ����� LoHook (����������� �� Patch, �.�. �� ���� ���-��� �������� ������)
// ������� ��������� ����� �
// ������� ������� ������ PatcherInstance
class LoHook : public Patch
{
};

// �������� ������������ ��� �������� hooktype � PatcherInstance::WriteHiHook � PatcherInstance::CreateHiHook
#define CALL_ 0
#define SPLICE_ 1
#define FUNCPTR_ 2

// �������� ������������ ��� �������� subtype � PatcherInstance::WriteHiHook � PatcherInstance::CreateHiHook
#define DIRECT_ 0
#define EXTENDED_ 1

// �������� ������������ ��� �������� calltype � PatcherInstance::WriteHiHook � PatcherInstance::CreateHiHook
#define ANY_		0
#define STDCALL_	0
#define THISCALL_	1
#define FASTCALL_	2 
#define CDECL_		3
#define FASTCALL_1	1 

// ����������� ����� HiHook (����������� �� Patch, �.�. �� ���� ���-��� �������� ������)
// ������� ��������� ����� � ������� ������� ������ PatcherInstance
class HiHook : public Patch
{
public:
	// ���������� ��������� �� ������� (�� ���� � ������� � ������ SPLICE_),
	// ���������� �����
	// ��������! ������� ������� ��� �� ������������ ����, ����� ��������
	// ������������ (�� �������) ��������.
	virtual _ptr_ __stdcall GetDefaultFunc() = 0;

	// ���������� ��������� �� ������������ ������� (�� ���� � ������� � ������ SPLICE_),
	// ���������� �����(������) �� ������� ������
	// (�.�. ���������� GetDefaultFunc() ��� ������� ������������ ���� �� ������� ������)
	// ��������! ������� ������� ��� �� ������������ ����, ����� ��������
	// ������������ (�� �������) ��������.
	virtual _ptr_ __stdcall GetOriginalFunc() = 0;

	// ���������� ����� �������� � ������������ ���
	// ����� ������������ ������ ���-�������
	// SPLICE_ ����, ����� ������ ������ ��� ���� �������
	virtual _ptr_ __stdcall GetReturnAddress() = 0;
};


// ����������� ����� PatcherInstance
// �������/�������� ��������� ����� � ������� ������� CreateInstance � GetInstance ������ Patcher
// ��������������� ��������� ���������/������������� ����� � ���� � ���,
// �������� �� � ������ ���� ������/�����, ��������� ����������� patcher_x86.dll
class PatcherInstance
{
public:
	////////////////////////////////////////////////////////////
	// ����� WriteByte
	// ����� ����������� ����� �� ������ address
	// (������� � ��������� DATA_ ����)
	// ���������� ��������� �� ����
	virtual Patch* __stdcall WriteByte(_ptr_ address, int value) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteWord
	// ����� ������������ ����� �� ������ address
	// (������� � ��������� DATA_ ����)
	// ���������� ��������� �� ����
	virtual Patch* __stdcall WriteWord(_ptr_ address, int value) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteDword
	// ����� ��������������� ����� �� ������ address
	// (������� � ��������� DATA_ ����)
	// ���������� ��������� �� ����
	virtual Patch* __stdcall WriteDword(_ptr_ address, int value) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteJmp
	// ����� jmp to ����� �� ������ address
	// (������� � ��������� CODE_ ����)
	// ���������� ��������� �� ����
	// ���� ��������� ����� ���������� �������,
	// �.�. ������ ����� >= 5, ������� ����������� NOP'���. 
	virtual Patch* __stdcall WriteJmp(_ptr_ address, _ptr_ to) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteHexPatch
	// ����� �� ������ address ������������������ ����,
	// ������������ hex_str
	// (������� � ��������� DATA_ ����)
	// ���������� ��������� �� ����
	// hex_str - ��-������ ����� ��������� ����������������� �����
	// 0123456789ABCDEF (������ ������� �������!) ��������� ������� 
	// ��� ������ ������� hex_str ������������(������������)
	// ������ ������������ � �������� ��������� ����� ������
	// ������������� � ������� Binary copy � OllyDbg
	/* ������:
			pi->WriteHexPatch(0x57b521, "6A 01  6A 00");
	*/
	virtual Patch* __stdcall WriteHexPatch(_ptr_ address, char* hex_str) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteCodePatchVA
	// � ������������ ���� ���������� ������ �� ��������������,
	// �������� (����) �������� ������-�������� WriteCodePatch
	virtual Patch* __stdcall WriteCodePatchVA(_ptr_ address, char* format, _dword_* va_args) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteLoHook
	// ������� �� ������ address �������������� ��� (CODE_ ����) � ��������� ���
	// ���������� ��������� �� ���
	// func - ������� ���������� ��� ������������ ����
	// ������ ����� ��� int __stdcall func(LoHook* h, HookContext* c);
	// � HookContext* c ���������� ��� ������/��������� 
	// �������� ���������� � ����� ��������
	// ���� func ���������� EXEC_DEFAULT, �� 
	// ����� ���������� func ����������� �������� ����� ���.
	// ���� - NO_EXEC_DEFAULT - �������� ��� �� �����������
	virtual LoHook* __stdcall WriteLoHook(_ptr_ address, void* func) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� WriteHiHook
	// ������� �� ������ address ��������������� ��� � ��������� ���
	// ���������� ��������� �� ���
	//
	// new_func - ������� ���������� ������������
	//
	// hooktype - ��� ����:
	//		CALL_ -		��� �� ����� ������� �� ������ address
	//					�������������� ������ E8 � FF 15, � ��������� ������� ��� �� ���������������
	//					� � ��� ������� ���������� �� ���� ������
	//		SPLICE_ -	��� ��������������� �� ���� ������� �� ������ address
	//		FUNCPTR_ -	��� �� ������� � ��������� (����������� �����, � �������� ��� ����� � �������� �������)
	//
	// subtype - ������ ����:
	//		DIRECT_ - new_func ����� ��� �� ��� ��� �
	//					������������ ���������� �������
	//					����������: ������ __thiscall f(this) 
	//								����� ������������ __fastcal f(this_)
	//								������ __thiscall f(this, ...)  ����� ������������ 
	//								__fastcall f(this_, no_used_edx, ...) 
	//		EXTENDED_ - ������� new_func ������ �������� ���������� ����������
	//					��������� �� ��������� HiHook �, � ������ 
	//					���������� �������� �-�� __thiscall � __fastcall
	//					����������� ��������� ���������� ��������� ������� 
	//
	// ����� ������� ��� EXTENDED_ ���� (orig - ���������� �-��):
	//	����					int __stdcall orig(?)	��	int __stdcall new_func(HiHook* h, ?)
	//	����		 int __thiscall orig(int this, ?)	��	int __stdcall new_func(HiHook* h, int this_, ?)
	//	����   int __fastcall orig(int a1, int a2, ?)	��	int __stdcall new_func(HiHook* h, int a1, int a2, ?)
	//	����					  int __cdecl orig(?)	��	int __cdecl new_func(HiHook* h, ?)
	//
	//	��������! EXTENDED_ FASTCALL_ ������������ ������ ������� � 2-�� � ����� �����������
	//	��� __fastcall c 1 ���������� ����������� EXTENDED_ FASTCALL_1 / EXTENDED_ THISCALL_
	//
	//		� ����������� ����������� ������� ������� ������������ EXTENDED_
	//		�� DIRECT_ ����������� ������� ��-�� �������� ����� � ����� ���������� �������
	//
	// calltype - ���������� � ������ ������������ ���������� �-��:
	//		STDCALL_
	//		THISCALL_
	//		FASTCALL_
	//		CDECL_
	// ���������� ����� ��������� ���������� ��� ���� ����� EXTENDED_ ��� ���������
	// �������� ���� � ����� ���������� �������
	//
	// CALL_, SPLICE_ ��� �������� CODE_ ������
	// FUNCPTR_ ��� �������� DATA_ ������
	//
	virtual HiHook* __stdcall WriteHiHook(_ptr_ address, int hooktype, int subtype, int calltype, void* new_func) = 0;

	///////////////////////////////////////////////////////////////////
	// ������ Create...
	// ������� ����/��� ��� �� ��� � ��������������� ������ Write...,
	// �� �� ��������� ���
	// ���������� ��������� �� ����/���
	virtual Patch* __stdcall CreateBytePatch(_ptr_ address, int value) = 0;
	virtual Patch* __stdcall CreateWordPatch(_ptr_ address, int value) = 0;
	virtual Patch* __stdcall CreateDwordPatch(_ptr_ address, int value) = 0;
	virtual Patch* __stdcall CreateJmpPatch(_ptr_ address, _ptr_ to) = 0;
	virtual Patch* __stdcall CreateHexPatch(_ptr_ address, char* hex_str) = 0;
	virtual Patch* __stdcall CreateCodePatchVA(_ptr_ address, char* format, _dword_* va_args) = 0;
	virtual LoHook* __stdcall CreateLoHook(_ptr_ address, void* func) = 0;
	virtual HiHook* __stdcall CreateHiHook(_ptr_ address, int hooktype, int subtype, int calltype, void* new_func) = 0;
	
	////////////////////////////////////////////////////////////
	// ����� ApplyAll
	// ��������� ��� �����/����, ��������� ���� ����������� PatcherInstance
	// ���������� 1 ���� ��� �����/���� ����������� �������
	// ���������� 0 ���� ���� �� ���� ����/��� �� ��� ��������
	// (��. Patch::Apply)
	virtual _bool_ __stdcall ApplyAll() = 0;

	////////////////////////////////////////////////////////////
	// ����� UndoAll
	// �������� ��� �����/����, ��������� ���� ����������� PatcherInstance
	// �.�. ��� ������� �� ������/����� �������� ����� Undo
	// ���������� 0, ���� ���� �� ���� ����/��� ���������� �������� (�������� ������������ (FIXED))
	// ����� ���������� 1
	virtual _bool_ __stdcall UndoAll() = 0;

	////////////////////////////////////////////////////////////
	// ����� DestroyAll
	// ���������� ��� �����/����, ��������� ���� ����������� PatcherInstance
	// �.�. ��� ������� �� ������/����� �������� ����� Destroy
	// ���������� 0, ���� ���� �� ���� ����/��� ���������� ���������� (�������� �����������)
	// ����� ���������� 1
	virtual _bool_ __stdcall DestroyAll() = 0;

	// � ������������ ���� ���������� ������ �� ��������������,
	// �������� (����) �������� ������-�������� WriteDataPatch
	virtual Patch* __stdcall WriteDataPatchVA(_ptr_ address, char* format, _dword_* va_args);
	// � ������������ ���� ���������� ������ �� ��������������,
	// �������� (����) �������� ������-�������� WriteDataPatch
	virtual Patch* __stdcall CreateDataPatchVA(_ptr_ address, char* format, _dword_* va_args);


	// ����� GetLastPatchAt
	// ���������� NULL, ���� �� ������ address �� ��� �������� �� ���� ����/���,
	// ��������� ������ ����������� PatcherInstance
	// ����� ���������� ��������� ���������� ����/��� �� ������ address,
	// ��������� ������ ����������� PatcherInstance
	virtual Patch* __stdcall GetLastPatchAt(_ptr_ address) = 0;

	// ����� UndoAllAt
	// �������� ����� ����������� ������ ����������� PatcherInstance
	// �� ������ address 
	// ���������� 1, ���� ��� ����� ������� ��������,
	// ����� ���������� 0
	virtual _bool_ __stdcall UndoAllAt(_ptr_ address) = 0;

	// ����� GetFirstPatchAt
	// ���������� NULL, ���� �� ������ address �� ��� �������� �� ���� ����/���,
	// ��������� ������ ����������� PatcherInstance
	// ����� ���������� ������ ���������� ����/��� �� ������ address,
	// ��������� ������ ����������� PatcherInstance
	virtual Patch* __stdcall GetFirstPatchAt(_ptr_ address) = 0;


	////////////////////////////////////////////////////////////
	// ����� Write
	// ����� �� ������ address ������/��� �� ������ �� ������ data �������� size ���� 
	// ���� is_code == 1, �� ��������� � ������� CODE_ ����, ���� 0 - DATA_ ����.
	// ���������� ��������� �� ����
	virtual Patch* __stdcall Write(_ptr_ address, _ptr_ data, _dword_ size, _bool_ is_code = 0) = 0;
	///////////////////////////////////////////////////////////////////
	// ����� CreatePatch
	// ������ ���� ��� �� ��� � ����� Write,
	// �� �� ��������� ���
	// ���������� ��������� �� ����
	virtual Patch* __stdcall CreatePatch(_ptr_ address, _ptr_ data, _dword_ size, _bool_ is_code = 0) = 0;



	////////////////////////////////////////////////////////////
	// ����� WriteCodePatch
	// ����� �� ������ address ������������������ ����,
	// ������������ format � ...
	// (������� � ��������� CODE_ ����)
	// ���������� ��������� �� ����
	// format - ��-������ ����� ��������� ����������������� �����
	// 0123456789ABCDEF (������ ������� �������!),
	// � ��� �� ����������� ������-������� (������ �������!):
	// %b - (byte) ����� ������������ ����� �� ...
	// %w - (word) ����� ������������ ����� �� ...
	// %d - (dword) ����� ��������������� ����� �� ...
	// %j - ����� jmp �� ����� �� ...
	// %� - ����� �all ...
	// %m - �������� ��� �� ������ ... �������� ... (�.�. ������ 2 ��������� �� ...)
	//      ����������� ���������� ����������� MemCopyCodeEx (��. ��������)
	// %% - ����� ������ � ������-��������� �� ... 
	// %o - (offset) �������� �� ������ �� ��������� �������� ������� �
	//      Complex ����,  ������������ ������ Complex ����.
	// %n - ����� nop ������, ����������� ������ ...                                  \
 #0: - #9: -������������� ����� (�� 0 �� 9) � ������� ����� ������� � ������� #0 - #9                              \
 #0 -  #9  -����� ������������ ����� ����� ������� EB, 70 - 7F, E8, E9, 0F80 - 0F8F
	//      ��������������� �����; ����� ������ ������� ������ �� �����
	// ~b - ����� �� ... ���������� ����� � ����� ������������� �������� �� ����
	//      �������� � 1 ���� (������������ ��� ������� EB, 70 - 7F)
	// ~d - ����� �� ... ���������� ����� � ����� ������������� �������� �� ����
	//      �������� � 4 ����� (������������ ��� ������� E8, E9, 0F 80 - 0F 8F)
	// %. - ������ �� ������ ( ��� � ����� ������ �� ����������� ���� ������ ����� % ) 
	// ����������� ������:
	//	Patch* p = pi->WriteCodePatch(address,
	//		"#0: %%",
	//		"B9 %d %%", this,					// mov ecx, this  // 
	//		"BA %d %%", this->context,			// mov edx, context  // 
	//		"%c %%", func,						// call func  // 
	//		"83 F8 01 %%",						// cmp eax, 1
	//		"0F 85 #7 %%", 						// jne long to label 7 (if func returns 0)
	//		"83 F8 02 %%",						// cmp eax, 2
	//		"0F 85 ~d %%", 0x445544,			// jne long to 0x445544 (if func returns 0)
	//		"EB #0 %%",							// jmp short to label 0
	//		"%m %%", address2, size,			// exec  code copy from address2
	//		"#7: FF 25 %d %.", &return_address);	// jmp [&return_address]
	inline Patch* WriteCodePatch(_ptr_ address, char* format, ...)
	{
		return WriteCodePatchVA(address, format, (_dword_*)((_ptr_)&format + 4));
	}
	
	////////////////////////////////////////////////////////////
	// ����� CreateCodePatch
	// ������� ���� ��� �� ��� � ����� WriteCodePatch,
	// �� �� ��������� ���
	// ��������e� ��������� �� ����
	inline Patch* CreateCodePatch(_ptr_ address, char* format, ...)
	{
		return CreateCodePatchVA(address, format, (_dword_*)((_ptr_)&format + 4));
	}


	////////////////////////////////////////////////////////////
	// ����� WriteDataPatch
	// ����� �� ������ address ������������������ ����,
	// ������������ format � ...
	// (������� � ��������� DATA_ ����)
	// ���������� ��������� �� ����
	// format - ��-������ ����� ��������� ����������������� �����
	// 0123456789ABCDEF (������ ������� �������!),
	// � ��� �� ����������� ������-������� (������ �������!):
	// %b - (byte) ����� ������������ ����� �� ...
	// %w - (word) ����� ������������ ����� �� ...
	// %d - (dword) ����� ��������������� ����� �� ...
	// %m - �������� ������ �� ������ ... �������� ... (�.�. ������ 2 ��������� �� ...)
	// %% - ����� ������ � ������-��������� �� ... 
	// %o - (offset) �������� �� ������ �� ��������� �������� ������� �
	//      Complex ����,  ������������ ������ Complex ����.
	// %. - ������ �� ������ ( ��� � ����� ������ �� ����������� ���� ������ ����� % ) 
	// ����������� ������:
	//	Patch* p = pi->WriteDataPatch(address,
	//		"FF FF FF %d %%", var,	
	//		"%m %%", address2, size,	
	//		"AE %.");
	inline Patch* WriteDataPatch(_ptr_ address, char* format, ...)
	{
		return WriteDataPatchVA(address, format, (_dword_*)((_ptr_)&format + 4));
	}
	
	////////////////////////////////////////////////////////////
	// ����� CreateDataPatch
	// ������� ���� ��� �� ��� � ����� WriteDataPatch,
	// �� �� ��������� ���
	// ��������e� ��������� �� ����
	inline Patch* CreateDataPatch(_ptr_ address, char* format, ...)
	{
		return CreateDataPatchVA(address, format, (_dword_*)((_ptr_)&format + 4));
	}

};

// ����� Patcher
class Patcher
{
public:
	// �������� ������:

	///////////////////////////////////////////////////
	// ����� CreateInstance
	// ������� ��������� ������ PatcherInstance, ������� 
	// ��������������� ��������� ��������� ����� � ���� �
	// ���������� ��������� �� ���� ���������.
	// owner - ���������� ��� ���������� PatcherInstance
	// ����� ���������� NULL, ���� ��������� � ������ owner ��� ������
	// ���� owner == NULL ��� owner == "" �� 
	// ��������� PatcherInstance ����� ������ � ������ ������ ��
	// �������� ���� ������� �������.
	virtual PatcherInstance* __stdcall CreateInstance(char* owner) = 0;
	
	///////////////////////////////////////////////////
	// ����� GetInstance
	// ���������� ��������� �� ��������� PatcherInstance
	// � ������ owner.
	// ����� ���������� NULL � ������, ���� 
	// ��������� � ������ owner �� ���������� (�� ��� ������)
	// � �������� ��������� ����� ���������� ��� ������.
	// ������������ ��� :
	// - �������� ������� �� ��������� ���, ������������ patcher_x86.dll
	// - ��������� ������� �� ���� ������ � ����� ���������� ����,
	//   ������������� patcher_x86.dll
	virtual PatcherInstance*  __stdcall GetInstance(char* owner) = 0;
	
	///////////////////////////////////////////////////
	// ����� GetLastPatchAt
	// ���������� NULL, ���� �� ������ address �� ��� �������� �� ���� ����/���
	// ����� ���������� ��������� ���������� ����/��� �� ������ address
	// ��������������� �������� �� ���� ������ �� ��������� ������ ����� 
	// ��������� ���� ����� � Patch::GetAppliedBefore
	virtual Patch* __stdcall GetLastPatchAt(_ptr_ address);
	
	///////////////////////////////////////////////////
	// ����� UndoAllAt
	// �������� ��� �����/���� �� ������ address
	// ���������� 0, ���� ���� �� 1 ����/��� �� ���������� �������� (��. Patch::Undo)
	// ����� ���������� 1
	virtual Patch* __stdcall UndoAllAt(_ptr_ address);
	
	///////////////////////////////////////////////////
	// ����� SaveDump
	// ��������� � ���� � ������ file_name
	// - ���������� � ����� ���� ����������� PatcherInstance
	// - ���������� ���� ����������� ������/�����
	// - ������ ���� ����������� ������ � �����
	virtual void __stdcall SaveDump(char* file_name) = 0;
	
	///////////////////////////////////////////////////
	// ����� SaveLog
	// ��������� � ���� � ������ file_name ��� 
	// ���� ����������� ��������� � ���� ����� 0 �������.
	// �������� ����������� ����� ������ � ���������� ����������
	// ��������� ���� patcher_x86.ini c c���������: Logging = 1
	//
	virtual void __stdcall SaveLog(char* file_name) = 0;
	
	///////////////////////////////////////////////////
	// ����� GetMaxPatchSize
	// ���������� patcher_x86.dll ����������� ��������� �����������
	// �� ������������ ������ �����,
	// ����� - ����� ������ � ������� ������ GetMaxPatchSize
	// (�� ������ ������ ��� 8192 ����, �.�. ������� :) )
	virtual int __stdcall GetMaxPatchSize() = 0;
	
	// �������������� ������:

	///////////////////////////////////////////////////
	// ����� WriteComplexDataVA
	// � ������������ ���� ���������� ������ �� ��������������,
	// �������� (����) �������� ������-�������� WriteComplexString
	virtual int __stdcall WriteComplexDataVA(_ptr_ address, char* format, _dword_* args) = 0;
	
	///////////////////////////////////////////////////
	// ����� GetOpcodeLength
	// �.�. ������������ ���� �������
	// ���������� ����� � ������ ������ �� ������ p_opcode
	// ���������� 0, ���� ����� ����������
	virtual int __stdcall GetOpcodeLength(_ptr_ p_opcode) = 0;
	
	///////////////////////////////////////////////////
	// ����� MemCopyCode
	// �������� ��� �� ������ �� ������ src � ������ �� ������ dst
	// MemCopyCode �������� ������ ����� ���������� ������� �������� >= size. ������ �����������!
	// ���������� ������ �������������� ����.
	// ���������� ��������� �� �������� ����������� ������ ���,
	// ��� ��������� �������� ������ E8 (call), E9 (jmp long), 0F80 - 0F8F (j** long)
	// c ������������� ���������� �� ������ � ��� ������, ���� ���������� 
	// ���������� �� ������� ����������� ������.
	// 
	virtual int __stdcall MemCopyCode(_ptr_ dst, _ptr_ src, _dword_ size) = 0;
	
	///////////////////////////////////////////////////
	// ����� GetFirstPatchAt
	// ���������� NULL, ���� �� ������ address �� ��� �������� �� ���� ����/���
	// ����� ���������� ������ ���������� ����/��� �� ������ address
	// ��������������� �������� �� ���� ������ �� ��������� ������ ����� 
	// ��������� ���� ����� � Patch::GetAppliedAfter
	virtual Patch* __stdcall GetFirstPatchAt(_ptr_ address);
	
	///////////////////////////////////////////////////
	// ����� MemCopyCodeEx
	// �������� ��� �� ������ �� ������ src � ������ �� ������ dst
	// ���������� ������ �������������� ����.
	// ���������� �� MemCopyCode ���,
	// ��� ��������� �������� ������ EB (jmp short), 70 - 7F (j** short)
	// c ������������� ���������� �� ������ � ��� ������, ���� ���������� 
	// ���������� �� ������� ����������� ������ (� ���� ������ ��� ���������� ��
	// ��������������� E9 (jmp long), 0F80 - 0F8F (j** long) ������.
	// ��������! ��-�� ����� ������ �������������� ���� ����� ��������� ����������� 
	// ������ �����������.
	virtual int __stdcall MemCopyCodeEx(_ptr_ dst, _ptr_ src, _dword_ size) = 0;
	
	

	////////////////////////////////////////////////////////////////////
	// ����� WriteComplexData
	// �������� ����� ������� �����������  
	// ������ WriteComplexDataVA
	// ���� ����� ��������� ����� � �� � ����������, �.�. ��� ��� 
	// ���������� � �� � �����
	// ���������� ������ ����� ��� �� ��� � � PatcherInstance::WriteCodePatch
	// (��. �������� ����� ������)
	// �� ���� ����� ����� �� ������ address, ������������������ ����,
	// ������������ ����������� format � ...,
	// ��! �� ������� ��������� ������ Patch, �� ����� ����������� (�.�. �� �������� �������� ������, �������� ������ � ������ �� ������� ���� � �.�.)
	// ��������!
	// ����������� ���� ����� ������ ��� ������������� �������� ������
	// ����, �.�. ������ ���� ������� ������ � ���� ������, 
	// � � ��� �������������� ��������� ������ � �������
	// PatcherInstance::WriteCodePatch
	//
	inline _ptr_ WriteComplexData(_ptr_ address, char* format, ...)
	{
		return WriteComplexDataVA(address, format, (_dword_*)((_ptr_)&format + 4));
	}
};

// ��������������� ������������ ������ �������� � �������
#pragma pack(pop)

//////////////////////////////////////////////////////////////////

//������� GetPatcher
//��������� ���������� �, � ������� ������ ������������ �������������� 
//������� _GetPatcherX86@0, ���������� ��������� �� ������ Patcher,
//����������� �������� �������� ���� ���������� ���������� patcher_x86.dll
//���������� NULL ��� �������
//������� �������� 1 ���, ��� �������� �� �� �����������
#include <windows.h>
inline Patcher* GetPatcher()
{
static int calls_count = 0;
	if (calls_count > 0) return NULL;
	calls_count++;
	HMODULE pl = LoadLibraryA("patcher_x86.dll");
	if (pl)
	{
		FARPROC f = GetProcAddress(pl, "_GetPatcherX86@0");
		if (f)	return CALL_0(Patcher*, __stdcall, f);
	}
	return NULL;
}