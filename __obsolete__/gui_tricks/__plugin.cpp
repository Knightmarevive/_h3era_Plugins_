//#include <Windows.h>
#include "Era.h"
#include <cstdio>

//#include "assocarray.h"

using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;


const char BUTTONS_DLL_NAME[200] = "buttons.era";
//const char Backpack_Button_Name[200] = "Knightmare.BCKPCK";
const char KK_Backpack_Button_DEF[200] = "z_bckpck.def";
char Backpack_Button_Number[16] = "8000";
char Backpack_Button_Number_Left[16] = "8000";
char Backpack_Button_Number_Right[16] = "8001";
char* Backpack_Old_Button_Number = nullptr;
char* Backpack_Old_Button_Number_Left = nullptr;
char* Backpack_Old_Button_Number_Right = nullptr;

int ButtonTypeAdvMap  = '0';
int ButtonTypeTown    = '1';
int ButtonTypeHero    = '2';
int ButtonTypeHeroes  = '3';
int ButtonTypeBatttle = '4';
// int ButtonTypeUnk1 = '5';
// int ButtonTypeUnk2 = '6';
// int ButtonTypeUnk3 = '7';
// int ButtonTypeUnk4 = '8';
int ButtonTypeDummy  = '9';
//int  Backpack_Button_id = -1;
char MSG_BUF[2000] = "";

struct ERAButtonRecord {
	int  *screen_type; //char *screen_type;
	char *ButtonNumber;
	char *DEF_name;
	char *x, *y, *w, *h;
	char *Description;
	char *Name;
	int unk2, unk3, unk4;
	int unk5, unk6, unk7;
};

HMODULE hButtons = 0;
int*  ExtButtonsTable = nullptr;
int*  ExtNumButtons = nullptr;
ERAButtonRecord** TheButtonsTable;

int change_buttons_ECX;
int change_buttons_atECX;
// const int sizeof_buttons_record = 144;
const int  change_buttons_id_shift = 0x0c7c;

void change_buttons(void) {
	__asm {
		pushad
		MOV ECX, DWORD PTR DS : [ExtButtonsTable]
		MOV EAX, DWORD PTR DS : [0x5A997F8]
		MOV ECX, DWORD PTR DS : [ECX + EAX]
		mov change_buttons_ECX, ecx
		mov EDX, DWORD PTR DS : [ecx]
		mov change_buttons_atECX, edx
		popad
	}
}

void connect_buttons(void) {
	if (hButtons) return;
	//hButtons = LoadLibrary(BUTTONS_DLL_NAME);
	hButtons = GetModuleHandle(BUTTONS_DLL_NAME);

	/*!*/
	//Assert(hButtons != 0);
	ExtButtonsTable = (int*)   GetProcAddress(hButtons, "ButtonsTable");
	ExtNumButtons =   (int*)   GetProcAddress(hButtons, "NumButtons");
	TheButtonsTable = (ERAButtonRecord**) *ExtButtonsTable;
	/*!*/
	//Assert(ExtButtonsTable != NULL);
	/*!*/
	//Assert(ExtNumButtons != NULL);
}

/*
BOOL __stdcall Hook_Buttons (THookContext* Context)
{
	connect_buttons();
	char* Button_Name = (char*) *(int*)(Context->ESP+8);
	//int*  var1 = (int*) (Context->ESP + 24);
	//int*  Button_Ptr  = (int*) *var1;
	int  &Button_Ptr1 = *(int*)(Context->ESP + 24);
	int  &Button_Number = *(int*)(Button_Ptr1 + change_buttons_id_shift);

	//sprintf_s(MSG_BUF,"ESP: %X",Context->ESP);
	//MessageBoxA(0, MSG_BUF, "debug", MB_OK);
	
	static bool replaced1 = false;
	static bool replaced2 = false;

	if (!replaced1&&Button_Number == 8000) {
		Button_Number = 0; replaced1 = true;
	}

	if (!replaced2 && Button_Number == Backpack_Button_id) {
		Button_Number = 8000; replaced2 = true;
	}
	
	//MessageBoxA(0, Button_Name, "debug", MB_OK);
	//if (strcmp(Button_Name, Backpack_Button) == 0)
	if( replaced2 && Button_Number==Backpack_Button_id)
	{
		//ExecErmCmd("IF:M^Backpack^");
		//MessageBoxA(0, "Backpack", "Backpack", MB_OK);

		sprintf_s(MSG_BUF,"EDI: %X, ESI: %X, EBP: %X, ESP: %X, \n EBX: %X, ECX: %X, EDX: %X, EAX: %X ", 
			Context->EDI, Context->ESI, Context->EBP,Context->ESP,
			Context->EBX, Context->ECX, Context->EDX, Context->EAX );
		MessageBoxA(0, MSG_BUF, "debug", MB_OK);
		

		//change_buttons();
		//sprintf_s(MSG_BUF,"%X \t %X",change_buttons_ECX,change_buttons_atECX);
		//MessageBoxA(0, MSG_BUF, "debug", MB_OK);


		//sprintf_s(MSG_BUF, "IF:L^ %X %X ^;", (int)Button_Ptr1, (int)Button_Number);
		//MessageBoxA(0, MSG_BUF, "debug", MB_OK);
		//ExecErmCmd(MSG_BUF);

		sprintf_s(MSG_BUF, "IF:L^ %X %X ^;", *ExtNumButtons, *ExtButtonsTable);
		ExecErmCmd(MSG_BUF);

		//Button_Number = 8000;
	}

	return EXEC_DEF_CODE;
}
*/

void __stdcall Load_my_Hooks(TEvent* Event) {
	static bool done = false;
	//if (done) return; done = true;
	connect_buttons();

	//Backpack_Button_id = GetButtonID(Backpack_Button);
	//ApiHook((void*)Hook_Buttons, HOOKTYPE_BRIDGE, (void*)(int)GetButtonID);

	/*
	for (int i = 0; i < *ExtNumButtons; ++i) {
		auto ptr = TheButtonsTable[i];
		//if (strcmp(ptr->Name, Backpack_Button_Name) == 0)
		if ( (strcmp(ptr->ButtonNumber, Backpack_Button_Number) == 0)
			&& *ptr->screen_type==ButtonTypeHero)
		{
			//ptr->ButtonNumber = Backpack_Button_Number;
			//ExecErmCmd("IF:M^HD Backpack Detected^;");

			ptr->screen_type = &ButtonTypeDummy;
		}

	}
	*/

	
	for (int i = 0; i < *ExtNumButtons; ++i) {
		auto ptr = TheButtonsTable[i];
		//if (strcmp(ptr->Name, Backpack_Button_Name) == 0)
		if ( (strcmp(ptr->DEF_name, KK_Backpack_Button_DEF) == 0)
			&& *ptr->screen_type == ButtonTypeHero)
		{
			Backpack_Old_Button_Number = ptr->ButtonNumber;
			ptr->ButtonNumber = Backpack_Button_Number;
			//ExecErmCmd("IF:M^KK Backpack Detected^;");
			break;
		}
		if ((strcmp(ptr->DEF_name, KK_Backpack_Button_DEF) == 0)
			&& *ptr->screen_type == ButtonTypeHeroes)
		{
			if (!Backpack_Old_Button_Number_Left) {
				Backpack_Old_Button_Number_Left = ptr->ButtonNumber;
				ptr->ButtonNumber = Backpack_Button_Number_Left;
			}
			else if (!Backpack_Old_Button_Number_Right) {
				Backpack_Old_Button_Number_Right = ptr->ButtonNumber;
				ptr->ButtonNumber = Backpack_Button_Number_Right;
			}
		}
			
	}
	

	auto HD_WoG = GetModuleHandle("HD_WOG.dll");
	if (HD_WoG) {
		//sprintf(MSG_BUF, "HD_WOG.dll handle %d", (int)HD_WoG);
		//MessageBoxA(0, MSG_BUF, "gui_tricks.era", MB_OK);
		
		//auto HD_WoG_Start = GetProcAddress(HD_WoG, "start");
		//if (HD_WoG_Start) {
			//char *ptr = (char*)HD_WoG_Start;
			//char * ptr = (char*)( (*(int*) HD_WoG_Start) - 0x3f430 + 0x73134);
			//char * ptr = (char*)( (int)HD_WoG_Start - 0x02bdf4330 + 0x02c13134);
			//MessageBoxA(0, "ok 1", "gui_tricks.era", MB_OK);

			char* ptr = ((char*) HD_WoG) + 0x06B0E4;
			//sprintf(MSG_BUF, "HD_WOG.dll + 0x06B0E4 = %d", (int)ptr);
			//MessageBoxA(0, MSG_BUF, "gui_tricks.era", MB_OK);

			static DWORD Temp;
			VirtualProtect(ptr, 1024, PAGE_READWRITE, &Temp);
			if (strcmp(ptr, "bckpck.def")==0) {
				//static DWORD Temp;
				//VirtualProtect(ptr, 11, PAGE_READWRITE, &Temp);
				ptr[0] = 'd'; ptr[1] = 'o'; ptr[2] = 't';  ptr[3] = '.';
				ptr[4] = 'd'; ptr[5] = 'e'; ptr[6] = 'f';  ptr[7] = 0x0;
				//VirtualProtect(ptr, 11, Temp, nullptr);
			}
			else if(strcmp(ptr, "dot.def") != 0 )
				MessageBoxA(0, "Unrecognized HD_WoG.dll version", "gui_tricks.era", MB_OK);
			VirtualProtect(ptr, 1024, Temp, nullptr);

		//}
		//else MessageBoxA(0,"Could not find HD_WoG.Start","gui_tricks.era",MB_OK);
	}
	//else MessageBoxA(0, "Could not link HD_WoG.dll", "gui_tricks.era", MB_OK);
}
extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    ConnectEra();

	RegisterHandler(Load_my_Hooks, "OnAfterLoadGame");
	RegisterHandler(Load_my_Hooks, "OnAfterErmInstructions");

	//RegisterHandler(Load_my_Hooks, "OnOpenHeroScreen");
	//RegisterHandler(Load_my_Hooks, "OnCloseHeroScreen");
  }
  else if (reason == DLL_PROCESS_DETACH) {
	  if (Backpack_Old_Button_Number && ExtNumButtons) 
		  for (int i = 0; i < *ExtNumButtons; ++i) {
			  auto ptr = TheButtonsTable[i];
			  if ((strcmp(ptr->DEF_name, KK_Backpack_Button_DEF) == 0)
				  && *ptr->screen_type == ButtonTypeHero
					&& Backpack_Old_Button_Number)
						ptr->ButtonNumber = Backpack_Old_Button_Number;

			  if ((strcmp(ptr->DEF_name, KK_Backpack_Button_DEF) == 0)
				  && *ptr->screen_type == ButtonTypeHeroes) {
						if(Backpack_Old_Button_Number_Left &&
							Backpack_Old_Button_Number_Left != ptr->ButtonNumber)
								ptr->ButtonNumber = Backpack_Old_Button_Number_Left;
						else if (Backpack_Old_Button_Number_Right &&
							Backpack_Old_Button_Number_Right != ptr->ButtonNumber)
								ptr->ButtonNumber = Backpack_Old_Button_Number_Right;
			  }
		  }
	  
  }
  return TRUE;
};
