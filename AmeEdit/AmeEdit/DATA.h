#pragma once
#include "heroes.h"

extern char forced;

extern char Creature_ACAST_CUSTOM_Chance;
extern DWORD Creature_ACAST_CUSTOM_Spell;

extern char StackStep_field_type;

extern int strike_all_around_range;
extern char counterstrike_fury;
extern char counterstrike_twice;

extern char Wog_Spell_Immunites[16];

extern char MovesTwice_Table;
extern char isTeleporter;

extern DWORD CreatureSpellPowerMultiplier;
extern DWORD CreatureSpellPowerDivider;
extern DWORD CreatureSpellPowerAdder;

extern DWORD CreatureMimicArtifact;
extern DWORD CreatureMimicArtifact2;


extern char  resource_type_table;
extern DWORD resource_amount_table;
extern DWORD resource_tax_table;


//// MOP Area begin 
extern char RangeRetaliation_Table;
extern char PreventiveCounterstrikeTable;

extern DWORD RegenerationHitPoints_Table;
extern char  RegenerationChance_Table;
extern WORD  ManaDrain_Table;
extern char  SpellsCostDump_Table;
extern char  SpellsCostLess_Table;
extern char  Counterstrike_Table;
extern char  AlwaysPositiveMorale_Table;
extern char  AlwaysPositiveLuck_Table;
extern char  FireWall_Table;
extern char  ThreeHeadedAttack_Table;

extern char  DeathBlow_Table;
extern char  Fear_Table;
extern char  Fearless_Table;
extern char  NoWallPenalty_Table;
extern char  MagicAura_Table;
extern char  StrikeAndReturn_Table;
extern DWORD  Spells_Table;
extern char  Hate_Table;
extern char  JoustingBonus_Table;
extern char  ImmunToJoustingBonus_Table;
extern char  MagicChannel_Table;
extern char  MagicMirror_Table;
extern char  Sharpshooters_Table;
extern char  ShootingAdjacent_Table;
extern char  ReduceTargetDefense_Table;
extern Word  Demonology_Table;

extern char  ImposedSpells_Table[6];
//// Mop Area end


extern DWORD Necromancy_without_artifacts[16];
extern DWORD Necromancy_with_artifacts[16];

extern char hasSantaGuards;
//extern DWORD SantaGuardsType ;
//extern DWORD UpgradedSantaGuardsType ;
extern DWORD DalionsGuards;

extern float ghost_fraction;
extern char isGhost;
extern char isRogue;

extern char isEnchanter;
extern char isSorceress;

extern char isHellSteed;
extern char isHellSteed2;
extern char isHellSteed3;

extern DWORD PersonalHate[1024];
extern char isDragonSlayer_Table;
extern char isFaerieDragon_Table;
extern char isAmmoCart_Table;
extern char isPassive_Table;

extern char isConstruct_Table;
extern char isAimedCaster_Table;

typedef enum
{
	ACAST_BIND,
	ACAST_BLIND,
	ACAST_DISEASE,
	ACAST_CURSE,
	ACAST_AGE,
	ACAST_STONE,
	ACAST_PARALIZE,
	ACAST_POIZON,
	ACAST_ACID,
	ACAST_DEFAULT
}
AFTERCAST_ABILITY;

extern char aftercast_abilities_table;

//=============================================
typedef enum
{
	ATT_VAMPIRE,
	ATT_THUNDER,
	ATT_DEATHSTARE,
	ATT_DISPEL,
	ATT_DISRUPT,
	ATT_DEFAULT
}
ATTACK_ABILITY;

extern char attack_abilities_table;
//=============================================
typedef enum
{
	RESIST_DWARF20,
	RESIST_DWARF40,
	RESIST_123LVL,
	RESIST_1234LVL,
	RESIST_MAGICIMMUNE,
	RESIST_ASAIR,
	RESIST_ASEARTH,
	RESIST_ASFIRE,
	RESIST_DEFAULT,
	RESIST_DWARF60,
	RESIST_DWARF80,
	RESIST_DWARF100,
	RESIST_1LVL,
	RESIST_12LVL,
	RESIST_SPEED,
	RESIST_TOXIC,
	RESIST_WILL,
	RESIST_NO_EYES,
	RESIST_MASS_DAMAGE,
	RESIST_TO_DISPEL,
	RESIST_TO_DEBUFF

}
MAGIC_RESISTANCE;

extern char magic_resistance_table;
//=============================================

typedef enum
{
	VULN_HALF,
	VULN_QUATER,
	VULN_LIGHTING,
	VULN_SHOWER,
	VULN_ICE,
	VULN_FIRE,
	VULN_GOLD,
	VULN_DIAMOND,
	VULN_DEFAULT,
	VULN_GOLEM_125,
	VULN_GOLEM_150,
	VULN_GOLEM_200,
	VULN_GOLEM_300,
	VULN_GOLEM_400,
	VULN_GOLEM_500,
	VULN_GOLEM_600,
	VULN_GOLEM_700,
	VULN_GOLEM_900
}
MAGIC_VULNERABILITY;

extern char magic_vulnerability_table;
//=============================================
extern char missiles_table;
//=============================================

extern char spell_1_table; //int  spell_1_table_ptr = (int)spell_1_table;
extern char spell_2_table; //int  spell_2_table_ptr = (int)spell_2_table;
extern char spell_3_table; //int  spell_3_table_ptr = (int)spell_3_table;
//=============================================

extern int skeltrans;
//=============================================

extern int upgtable;

extern char special_missiles_table;
extern char missile_size_table;
extern long missile_anim_table;

extern float fire_shield_table;
// extern float respawn_table;
extern float respawn_table_chance;
extern float respawn_table_fraction;
extern float respawn_table_sure;

extern MONSTER_PROP new_monsters;

extern int  after_melee__spell;
extern int  after_shoot__spell;
extern int  after_defend_spell;
extern int  after_wound__spell;
extern char after_action_spell_mastery;
extern int  after_melee__spell2;
extern int  after_shoot__spell2;
extern int  after_defend_spell2;
extern int  after_wound__spell2;

extern float shooting_resistance;
extern float melee_resistance;

extern long mana_regen_table;

extern char do_not_generate;


extern long poison_table;
extern long aging_table;
extern long paralyze_table;
extern long paralyze_chance;

extern char Receptive_Table;
extern char isHellHydra;
extern char isLord_Table;

extern char NoGolemOverflow;
extern char isDragonResistant;
extern char isHeroic;

extern char OverrideSpecialSpells;