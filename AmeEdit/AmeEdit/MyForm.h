#pragma once
//#include <string>

#include <cstdio>
#include <cstdlib>
#include "Tchar.h"

#include <io.h>

#include <fstream>

// #include <WinNls.h>

#include "DATA.h"

void ChangeCreatureValues(char* buf);
void DefaultCreatureValues(void);
bool writeCreatureValues(char* filename);

namespace AmeEdit {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckedListBox^ checkedListBox_monster_flags;
	private: System::Windows::Forms::Label^ label_monster_flags;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::Button^ button_load;
	private: System::Windows::Forms::Button^ Default_Button;
	private: System::Windows::Forms::CheckedListBox^ Custom_Flags;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::TextBox^ CSP_MUL;
	private: System::Windows::Forms::TextBox^ CSP_DIV;
	private: System::Windows::Forms::TextBox^ CSP_ADD;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::ComboBox^ SP_EFF;
	private: System::Windows::Forms::ComboBox^ ATT_EFF;
	private: System::Windows::Forms::ComboBox^ RESIST_EFF;
	private: System::Windows::Forms::ComboBox^ VULN_EFF;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::ComboBox^ Town_;
	private: System::Windows::Forms::ComboBox^ spell1;
	private: System::Windows::Forms::ComboBox^ Spell2;
	private: System::Windows::Forms::ComboBox^ Spell3;
	private: System::Windows::Forms::ComboBox^ LevelBox;
	private: System::Windows::Forms::Label^ label7;

	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::TextBox^ SkeleBox;

	private: System::Windows::Forms::TextBox^ FireShieldBox;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::ComboBox^ Missle_Type_box;
	private: System::Windows::Forms::SaveFileDialog^ saveFileDialog1;
	private: System::Windows::Forms::Button^ button_save;
	private: System::Windows::Forms::Label^ label11;
	private: System::Windows::Forms::Label^ label12;
	private: System::Windows::Forms::Label^ label13;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::TextBox^ HP_Regen;
	private: System::Windows::Forms::TextBox^ HP_Regen_Chance;
	private: System::Windows::Forms::TextBox^ ManaDrain;
	private: System::Windows::Forms::Label^ label15;
	private: System::Windows::Forms::TextBox^ DeathBlow;
	private: System::Windows::Forms::TextBox^ SpellsCostLess;
	private: System::Windows::Forms::TextBox^ SpellsCostDump;


	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::Label^ label17;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::TextBox^ Retaliations;
	private: System::Windows::Forms::Label^ label19;
	private: System::Windows::Forms::TextBox^ CastsSpell;
	private: System::Windows::Forms::Label^ Filename_Label;
	private: System::Windows::Forms::Label^ label20;
	private: System::Windows::Forms::TextBox^ ImpSpell1Number;
	private: System::Windows::Forms::TextBox^ ImpSpell2Number;
	private: System::Windows::Forms::TextBox^ ImpSpell3Number;
	private: System::Windows::Forms::ComboBox^ ImpSpell1Power;
	private: System::Windows::Forms::ComboBox^ ImpSpell2Power;
	private: System::Windows::Forms::ComboBox^ ImpSpell3Power;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::TextBox^ Demonology_Target;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::TextBox^ UpgTo;
	private: System::Windows::Forms::Label^ label23;
	private: System::Windows::Forms::TextBox^ MimicArtifact;
	private: System::Windows::Forms::TextBox^ MimicArtifact2;
	private: System::Windows::Forms::ComboBox^ z_res_type;
	private: System::Windows::Forms::TextBox^ z_res_amount;
	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::TextBox^ TaxBox;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::TextBox^ GuardsBox;
	private: System::Windows::Forms::Label^ label26;
	private: System::Windows::Forms::TextBox^ ReduceTargetDefense_TXT;
	private: System::Windows::Forms::CheckBox^ checkBox_ForcedSave;
	private: System::Windows::Forms::Label^ label27;
	private: System::Windows::Forms::Label^ label28;
	private: System::Windows::Forms::Label^ label29;
	private: System::Windows::Forms::Label^ label30;
private: System::Windows::Forms::TextBox^ AfterMeleeSpell;
private: System::Windows::Forms::TextBox^ AfterShootSpell;
private: System::Windows::Forms::TextBox^ AfterDefendSpell;
private: System::Windows::Forms::Label^ label31;
private: System::Windows::Forms::TextBox^ ShootingResistance;
private: System::Windows::Forms::Label^ label32;
private: System::Windows::Forms::TextBox^ AfterWoundSpell;
private: System::Windows::Forms::Label^ label33;
private: System::Windows::Forms::TextBox^ MeleeResistance;
private: System::Windows::Forms::ComboBox^ SpellAfterActionMastery;
private: System::Windows::Forms::CheckBox^ DoNotGenerate;
private: System::Windows::Forms::TextBox^ AfterMeleeSpell2;
private: System::Windows::Forms::TextBox^ AfterShootSpell2;
private: System::Windows::Forms::TextBox^ AfterDefendSpell2;
private: System::Windows::Forms::TextBox^ AfterWoundSpell2;
private: System::Windows::Forms::Label^ label34;
private: System::Windows::Forms::Label^ label35;
private: System::Windows::Forms::Label^ label36;
private: System::Windows::Forms::TextBox^ rebirth_fraction_box;
private: System::Windows::Forms::TextBox^ rebirth_chance_box;
private: System::Windows::Forms::Label^ label8;
private: System::Windows::Forms::TextBox^ rebirth_sure_box;
private: System::Windows::Forms::Label^ label37;
private: System::Windows::Forms::TextBox^ ManaRegenBox;
private: System::Windows::Forms::Label^ label38;
private: System::Windows::Forms::Label^ label39;
private: System::Windows::Forms::Label^ label40;
private: System::Windows::Forms::Label^ label41;
private: System::Windows::Forms::Label^ label42;
private: System::Windows::Forms::TextBox^ AgingBox;
private: System::Windows::Forms::TextBox^ PoisonBox;
private: System::Windows::Forms::TextBox^ ParalyzeBox;
private: System::Windows::Forms::TextBox^ ParalyzeChanceBox;
private: System::Windows::Forms::Label^ label43;
private: System::Windows::Forms::TextBox^ GhostFractionBox;
private: System::Windows::Forms::CheckedListBox^ Custom_Flags_2;
private: System::Windows::Forms::Label^ label44;
private: System::Windows::Forms::TextBox^ Wog_Spell_immunites_box;
private: System::Windows::Forms::Label^ label45;
private: System::Windows::Forms::Label^ label46;
private: System::Windows::Forms::TextBox^ MissileSizeBox;

private: System::Windows::Forms::TextBox^ MissileAnimBox;
private: System::Windows::Forms::Label^ label47;
private: System::Windows::Forms::TextBox^ AroundRangeBox;
private: System::Windows::Forms::ComboBox^ TrailBox;
private: System::Windows::Forms::Label^ label48;
private: System::Windows::Forms::Label^ label49;
private: System::Windows::Forms::Label^ label50;
private: System::Windows::Forms::Label^ label51;
private: System::Windows::Forms::TextBox^ ACAST_CUSTOM_spell;
private: System::Windows::Forms::TextBox^ ACAST_CUSTOM_chance;





















	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->checkedListBox_monster_flags = (gcnew System::Windows::Forms::CheckedListBox());
			this->label_monster_flags = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->button_load = (gcnew System::Windows::Forms::Button());
			this->Default_Button = (gcnew System::Windows::Forms::Button());
			this->Custom_Flags = (gcnew System::Windows::Forms::CheckedListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->CSP_MUL = (gcnew System::Windows::Forms::TextBox());
			this->CSP_DIV = (gcnew System::Windows::Forms::TextBox());
			this->CSP_ADD = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->SP_EFF = (gcnew System::Windows::Forms::ComboBox());
			this->ATT_EFF = (gcnew System::Windows::Forms::ComboBox());
			this->RESIST_EFF = (gcnew System::Windows::Forms::ComboBox());
			this->VULN_EFF = (gcnew System::Windows::Forms::ComboBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->Town_ = (gcnew System::Windows::Forms::ComboBox());
			this->spell1 = (gcnew System::Windows::Forms::ComboBox());
			this->Spell2 = (gcnew System::Windows::Forms::ComboBox());
			this->Spell3 = (gcnew System::Windows::Forms::ComboBox());
			this->LevelBox = (gcnew System::Windows::Forms::ComboBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->SkeleBox = (gcnew System::Windows::Forms::TextBox());
			this->FireShieldBox = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->Missle_Type_box = (gcnew System::Windows::Forms::ComboBox());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->button_save = (gcnew System::Windows::Forms::Button());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->HP_Regen = (gcnew System::Windows::Forms::TextBox());
			this->HP_Regen_Chance = (gcnew System::Windows::Forms::TextBox());
			this->ManaDrain = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->DeathBlow = (gcnew System::Windows::Forms::TextBox());
			this->SpellsCostLess = (gcnew System::Windows::Forms::TextBox());
			this->SpellsCostDump = (gcnew System::Windows::Forms::TextBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->Retaliations = (gcnew System::Windows::Forms::TextBox());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->CastsSpell = (gcnew System::Windows::Forms::TextBox());
			this->Filename_Label = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->ImpSpell1Number = (gcnew System::Windows::Forms::TextBox());
			this->ImpSpell2Number = (gcnew System::Windows::Forms::TextBox());
			this->ImpSpell3Number = (gcnew System::Windows::Forms::TextBox());
			this->ImpSpell1Power = (gcnew System::Windows::Forms::ComboBox());
			this->ImpSpell2Power = (gcnew System::Windows::Forms::ComboBox());
			this->ImpSpell3Power = (gcnew System::Windows::Forms::ComboBox());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->Demonology_Target = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->UpgTo = (gcnew System::Windows::Forms::TextBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->MimicArtifact = (gcnew System::Windows::Forms::TextBox());
			this->MimicArtifact2 = (gcnew System::Windows::Forms::TextBox());
			this->z_res_type = (gcnew System::Windows::Forms::ComboBox());
			this->z_res_amount = (gcnew System::Windows::Forms::TextBox());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->TaxBox = (gcnew System::Windows::Forms::TextBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->GuardsBox = (gcnew System::Windows::Forms::TextBox());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->ReduceTargetDefense_TXT = (gcnew System::Windows::Forms::TextBox());
			this->checkBox_ForcedSave = (gcnew System::Windows::Forms::CheckBox());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->AfterMeleeSpell = (gcnew System::Windows::Forms::TextBox());
			this->AfterShootSpell = (gcnew System::Windows::Forms::TextBox());
			this->AfterDefendSpell = (gcnew System::Windows::Forms::TextBox());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->ShootingResistance = (gcnew System::Windows::Forms::TextBox());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->AfterWoundSpell = (gcnew System::Windows::Forms::TextBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->MeleeResistance = (gcnew System::Windows::Forms::TextBox());
			this->SpellAfterActionMastery = (gcnew System::Windows::Forms::ComboBox());
			this->DoNotGenerate = (gcnew System::Windows::Forms::CheckBox());
			this->AfterMeleeSpell2 = (gcnew System::Windows::Forms::TextBox());
			this->AfterShootSpell2 = (gcnew System::Windows::Forms::TextBox());
			this->AfterDefendSpell2 = (gcnew System::Windows::Forms::TextBox());
			this->AfterWoundSpell2 = (gcnew System::Windows::Forms::TextBox());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->rebirth_fraction_box = (gcnew System::Windows::Forms::TextBox());
			this->rebirth_chance_box = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->rebirth_sure_box = (gcnew System::Windows::Forms::TextBox());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->ManaRegenBox = (gcnew System::Windows::Forms::TextBox());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->AgingBox = (gcnew System::Windows::Forms::TextBox());
			this->PoisonBox = (gcnew System::Windows::Forms::TextBox());
			this->ParalyzeBox = (gcnew System::Windows::Forms::TextBox());
			this->ParalyzeChanceBox = (gcnew System::Windows::Forms::TextBox());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->GhostFractionBox = (gcnew System::Windows::Forms::TextBox());
			this->Custom_Flags_2 = (gcnew System::Windows::Forms::CheckedListBox());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->Wog_Spell_immunites_box = (gcnew System::Windows::Forms::TextBox());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->MissileSizeBox = (gcnew System::Windows::Forms::TextBox());
			this->MissileAnimBox = (gcnew System::Windows::Forms::TextBox());
			this->label47 = (gcnew System::Windows::Forms::Label());
			this->AroundRangeBox = (gcnew System::Windows::Forms::TextBox());
			this->TrailBox = (gcnew System::Windows::Forms::ComboBox());
			this->label48 = (gcnew System::Windows::Forms::Label());
			this->label49 = (gcnew System::Windows::Forms::Label());
			this->label50 = (gcnew System::Windows::Forms::Label());
			this->label51 = (gcnew System::Windows::Forms::Label());
			this->ACAST_CUSTOM_spell = (gcnew System::Windows::Forms::TextBox());
			this->ACAST_CUSTOM_chance = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// checkedListBox_monster_flags
			// 
			this->checkedListBox_monster_flags->BackColor = System::Drawing::Color::Black;
			this->checkedListBox_monster_flags->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->checkedListBox_monster_flags->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->checkedListBox_monster_flags->FormattingEnabled = true;
			this->checkedListBox_monster_flags->Items->AddRange(gcnew cli::array< System::Object^  >(32) {
				L"00000001 - 0x00 DOUBLE_WIDE-occupies 2 cells",
					L"00000002 - 0x01 flying", L"00000004 - 0x02 shooter", L"00000008 - 0x03 extended attack radius (two cells)", L"00000010 - 0x04 living creature ",
					L"00000020 - 0x05 CATAPULT - can destroy walls", L"00000040 - 0x06 SIEGE_WEAPON", L"00000080 - 0x07 KING_1", L"00000100 - 0x08 KING_2",
					L"00000200 - 0x09 KING_3", L"00000400 - 0x0A Mind spell imunity", L"00000800 - 0x0B Ray Shooting", L"00001000 - 0x0C No Melee penalty",
					L"00002000 - 0x0D ----", L"00004000 - 0x0E IMMUNE_TO_FIRE_SPELLS", L"00008000 - 0x0F Attacks twice", L"00010000 - 0x10 No Enemy Retaliation",
					L"00020000 - 0x11 No Morale Penalty", L"00040000 - 0x12 Undead", L"00080000 - 0x13 hits all enemies around", L"00100000 - 0x14 extended radius of shooting units",
					L"00200000 - 0x15 stack killed\? 41E617 ", L"00400000 - 0x16 Summoned", L"00800000 - 0x17 Clone", L"01000000 - 0x18 already had morale",
					L"02000000 - 0x19 were(already) waiting ", L"04000000 - 0x1a already moved", L"08000000 - 0x1B defending", L"10000000 - 0x1C Sacrifice",
					L"20000000 - 0x1D Don\'t Change Colour", L"40000000 - 0x1E Shades of Gray", L"80000000 - 0x1F dragon nature"
			});
			this->checkedListBox_monster_flags->Location = System::Drawing::Point(0, 0);
			this->checkedListBox_monster_flags->Name = L"checkedListBox_monster_flags";
			this->checkedListBox_monster_flags->Size = System::Drawing::Size(278, 480);
			this->checkedListBox_monster_flags->TabIndex = 0;
			this->checkedListBox_monster_flags->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &MyForm::checkedListBox_monster_flags_ItemCheck);
			this->checkedListBox_monster_flags->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::checkedListBox_monster_flags_KeyUp);
			this->checkedListBox_monster_flags->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::checkedListBox_monster_flags_MouseUp);
			// 
			// label_monster_flags
			// 
			this->label_monster_flags->AutoSize = true;
			this->label_monster_flags->BackColor = System::Drawing::Color::Black;
			this->label_monster_flags->Location = System::Drawing::Point(18, 483);
			this->label_monster_flags->MinimumSize = System::Drawing::Size(260, 16);
			this->label_monster_flags->Name = L"label_monster_flags";
			this->label_monster_flags->Size = System::Drawing::Size(260, 16);
			this->label_monster_flags->TabIndex = 1;
			this->label_monster_flags->Text = L"0";
			this->label_monster_flags->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label_monster_flags->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->Filter = L"CFG files |*.cfg";
			this->openFileDialog1->InitialDirectory = L".";
			// 
			// button_load
			// 
			this->button_load->BackColor = System::Drawing::Color::Black;
			this->button_load->Location = System::Drawing::Point(15, 510);
			this->button_load->Name = L"button_load";
			this->button_load->Size = System::Drawing::Size(75, 23);
			this->button_load->TabIndex = 2;
			this->button_load->Text = L"Load";
			this->button_load->UseVisualStyleBackColor = false;
			this->button_load->Click += gcnew System::EventHandler(this, &MyForm::button_load_Click);
			// 
			// Default_Button
			// 
			this->Default_Button->BackColor = System::Drawing::Color::Black;
			this->Default_Button->Location = System::Drawing::Point(93, 510);
			this->Default_Button->Name = L"Default_Button";
			this->Default_Button->Size = System::Drawing::Size(75, 23);
			this->Default_Button->TabIndex = 3;
			this->Default_Button->Text = L"Default";
			this->Default_Button->UseVisualStyleBackColor = false;
			this->Default_Button->Click += gcnew System::EventHandler(this, &MyForm::Default_Button_Click);
			// 
			// Custom_Flags
			// 
			this->Custom_Flags->BackColor = System::Drawing::Color::Black;
			this->Custom_Flags->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->Custom_Flags->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Custom_Flags->FormattingEnabled = true;
			this->Custom_Flags->Items->AddRange(gcnew cli::array< System::Object^  >(33) {
				L"hasSantaGuards", L"isRogue", L"isGhost", L"isEnchanter",
					L"isSorceress", L"hasFirewall", L"Always + Morale", L"Always + Luck", L"Fear", L"Fearless", L"NoWallPenalty", L"Sharpshooters",
					L"ShootingAdjacent", L"StrikeAndReturn", L"JoustingBonus", L"JoustingImmune", L"MagicAura", L"MagicMirror", L"MagicChannel",
					L"isFaerie", L"isPassive", L"isCerberus", L"isDragonSlayer", L"PreventiveStrike", L"RangedRetaliation", L"isAmmoCart", L"isConstruct",
					L"isAimedCaster", L"isTeleporter", L"MovesTwice", L"Receptive", L"isHellHydra", L"isLord"
			});
			this->Custom_Flags->Location = System::Drawing::Point(284, 0);
			this->Custom_Flags->Name = L"Custom_Flags";
			this->Custom_Flags->Size = System::Drawing::Size(120, 495);
			this->Custom_Flags->TabIndex = 4;
			this->Custom_Flags->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Custom_Flags_SelectedIndexChanged);
			this->Custom_Flags->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::Custom_Flags_KeyUp);
			this->Custom_Flags->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::Custom_Flags_MouseUp);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Black;
			this->label1->Location = System::Drawing::Point(406, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(106, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"Creature Spell Power";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::Black;
			this->label2->Location = System::Drawing::Point(406, 28);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(47, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"multiplier";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::Black;
			this->label3->Location = System::Drawing::Point(415, 52);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(38, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"divider";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::Black;
			this->label4->Location = System::Drawing::Point(419, 77);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(34, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"adder";
			// 
			// CSP_MUL
			// 
			this->CSP_MUL->Location = System::Drawing::Point(459, 27);
			this->CSP_MUL->Name = L"CSP_MUL";
			this->CSP_MUL->Size = System::Drawing::Size(40, 20);
			this->CSP_MUL->TabIndex = 6;
			this->CSP_MUL->Text = L"1";
			this->CSP_MUL->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->CSP_MUL->TextChanged += gcnew System::EventHandler(this, &MyForm::CSP_MUL_TextChanged);
			// 
			// CSP_DIV
			// 
			this->CSP_DIV->Location = System::Drawing::Point(459, 50);
			this->CSP_DIV->Name = L"CSP_DIV";
			this->CSP_DIV->Size = System::Drawing::Size(40, 20);
			this->CSP_DIV->TabIndex = 6;
			this->CSP_DIV->Text = L"1";
			this->CSP_DIV->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->CSP_DIV->TextChanged += gcnew System::EventHandler(this, &MyForm::CSP_DIV_TextChanged);
			// 
			// CSP_ADD
			// 
			this->CSP_ADD->Location = System::Drawing::Point(459, 75);
			this->CSP_ADD->Name = L"CSP_ADD";
			this->CSP_ADD->Size = System::Drawing::Size(40, 20);
			this->CSP_ADD->TabIndex = 6;
			this->CSP_ADD->Text = L"0";
			this->CSP_ADD->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->CSP_ADD->TextChanged += gcnew System::EventHandler(this, &MyForm::CSP_ADD_TextChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::Black;
			this->label5->Location = System::Drawing::Point(647, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(83, 13);
			this->label5->TabIndex = 5;
			this->label5->Text = L"Creature Effects";
			this->label5->Click += gcnew System::EventHandler(this, &MyForm::label5_Click);
			// 
			// SP_EFF
			// 
			this->SP_EFF->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->SP_EFF->FormattingEnabled = true;
			this->SP_EFF->Items->AddRange(gcnew cli::array< System::Object^  >(11) {
				L"ACAST_BIND", L"ACAST_BLIND", L"ACAST_DISEASE", L"ACAST_CURSE",
					L"ACAST_AGE", L"ACAST_STONE", L"ACAST_PARALIZE ", L"ACAST_POIZON ", L"ACAST_ACID", L"ACAST_DEFAULT", L"ACAST_CUSTOM"
			});
			this->SP_EFF->Location = System::Drawing::Point(639, 25);
			this->SP_EFF->Name = L"SP_EFF";
			this->SP_EFF->Size = System::Drawing::Size(142, 21);
			this->SP_EFF->TabIndex = 7;
			this->SP_EFF->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::SP_EFF_SelectedIndexChanged);
			// 
			// ATT_EFF
			// 
			this->ATT_EFF->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->ATT_EFF->FormattingEnabled = true;
			this->ATT_EFF->Items->AddRange(gcnew cli::array< System::Object^  >(6) {
				L"ATT_VAMPIRE", L"ATT_THUNDER", L"ATT_DEATHSTARE",
					L"ATT_DISPEL", L"ATT_DISRUPT", L"ATT_DEFAULT"
			});
			this->ATT_EFF->Location = System::Drawing::Point(639, 49);
			this->ATT_EFF->Name = L"ATT_EFF";
			this->ATT_EFF->Size = System::Drawing::Size(142, 21);
			this->ATT_EFF->TabIndex = 7;
			this->ATT_EFF->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::ATT_EFF_SelectedIndexChanged);
			// 
			// RESIST_EFF
			// 
			this->RESIST_EFF->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->RESIST_EFF->FormattingEnabled = true;
			this->RESIST_EFF->Items->AddRange(gcnew cli::array< System::Object^  >(21) {
				L"RESIST_DWARF20", L"RESIST_DWARF40", L"RESIST_123LVL",
					L"RESIST_1234LVL", L"RESIST_MAGICIMMUNE", L"RESIST_TO_EARTH", L"RESIST_TO_AIR", L"RESIST_TO_WATER", L"RESIST_DEFAULT", L"RESIST_DWARF60",
					L"RESIST_DWARF80", L"RESIST_DWARF100", L"RESIST_1LVL", L"RESIST_12LVL", L"RESIST_SPEED", L"RESIST_TOXIC", L"RESIST_WILL", L"RESIST_NO_EYES",
					L"RESIST_MASS_DMG", L"RESIST_TO_DISPEL", L"RESIST_TO_DEBUFF"
			});
			this->RESIST_EFF->Location = System::Drawing::Point(639, 74);
			this->RESIST_EFF->Name = L"RESIST_EFF";
			this->RESIST_EFF->Size = System::Drawing::Size(142, 21);
			this->RESIST_EFF->TabIndex = 7;
			this->RESIST_EFF->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::RESIST_EFF_SelectedIndexChanged);
			// 
			// VULN_EFF
			// 
			this->VULN_EFF->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->VULN_EFF->FormattingEnabled = true;
			this->VULN_EFF->Items->AddRange(gcnew cli::array< System::Object^  >(18) {
				L"VULN_STONE_GOLEM", L"VULN_IRON_GOLEM", L"VULN_LIGHTING",
					L"VULN_SHOWER", L"VULN_ICE", L"VULN_FIRE", L"VULN_GOLD_GOLEM", L"VULN_DIAMOND", L"VULN_DEFAULT", L"VULN_GOLEM_125", L"VULN_GOLEM_150",
					L"VULN_GOLEM_200", L"VULN_GOLEM_300", L"VULN_GOLEM_400", L"VULN_GOLEM_500", L"VULN_GOLEM_600", L"VULN_GOLEM_700", L"VULN_GOLEM_900"
			});
			this->VULN_EFF->Location = System::Drawing::Point(639, 101);
			this->VULN_EFF->Name = L"VULN_EFF";
			this->VULN_EFF->Size = System::Drawing::Size(142, 21);
			this->VULN_EFF->TabIndex = 7;
			this->VULN_EFF->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::VULN_EFF_SelectedIndexChanged);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->BackColor = System::Drawing::Color::Black;
			this->label6->Location = System::Drawing::Point(647, 125);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(117, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Creature Misc Statistics";
			this->label6->Click += gcnew System::EventHandler(this, &MyForm::label5_Click);
			// 
			// Town_
			// 
			this->Town_->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->Town_->FormattingEnabled = true;
			this->Town_->Items->AddRange(gcnew cli::array< System::Object^  >(10) {
				L"Castle", L"Rampart", L"Tower", L"Inferno", L"Necropolis",
					L"Dungeon", L"Stronghold", L"Fortress", L"Conflux", L"Neutral"
			});
			this->Town_->Location = System::Drawing::Point(639, 141);
			this->Town_->Name = L"Town_";
			this->Town_->Size = System::Drawing::Size(142, 21);
			this->Town_->TabIndex = 8;
			this->Town_->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Town__SelectedIndexChanged);
			// 
			// spell1
			// 
			this->spell1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->spell1->FormattingEnabled = true;
			this->spell1->Items->AddRange(gcnew cli::array< System::Object^  >(22) {
				L"Mode 1 (Resurrection)", L"Mode 2 (Genie Spell)",
					L"Mode 3 (Demonology)", L"Mode 4 (Bloodlust)", L"Mode 5 (Prot Water)", L"Mode 6 (Prot Earth)", L"Mode 7 (Prot Air)", L"Mode 8 (Prot Fire)",
					L"Mode 9 (0x44835E)", L"None", L"Exact Spell Number", L"Random Elemental", L"Enchant Target", L"Enchant Target (Minor)", L"Enchant Target (Major)",
					L"Heavenly Inspiration", L"Exact Spell Number (Twice)", L"Rush Target Ally", L"Dragon Will", L"Exact Spell Number (Expert)",
					L"Overclock", L"Repair and Overclock"
			});
			this->spell1->Location = System::Drawing::Point(413, 114);
			this->spell1->Name = L"spell1";
			this->spell1->Size = System::Drawing::Size(211, 21);
			this->spell1->TabIndex = 9;
			this->spell1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::spell1_SelectedIndexChanged);
			// 
			// Spell2
			// 
			this->Spell2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->Spell2->FormattingEnabled = true;
			this->Spell2->Items->AddRange(gcnew cli::array< System::Object^  >(14) {
				L"Resurrection", L"Random Friendly Spell", L"Friendly Spell",
					L"Protection from Water", L"Protection from Earth", L"Protection from Air", L"Protection from Fire", L"Random Damage Spell",
					L"None", L"Raise Undead", L"Repair Constructs", L"Gate Infernals", L"Friendly Non-Dragons", L"Friendly Constructs"
			});
			this->Spell2->Location = System::Drawing::Point(413, 141);
			this->Spell2->Name = L"Spell2";
			this->Spell2->Size = System::Drawing::Size(211, 21);
			this->Spell2->TabIndex = 9;
			this->Spell2->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Spell2_SelectedIndexChanged);
			// 
			// Spell3
			// 
			this->Spell3->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->Spell3->FormattingEnabled = true;
			this->Spell3->Items->AddRange(gcnew cli::array< System::Object^  >(6) {
				L"Mode 1 (Resurrection)", L"Mode 2 (Buff)", L"Mode 3 (Damage)",
					L"None", L"Mode 5 (Debuff)", L"Mode 6 (Custom Buff)"
			});
			this->Spell3->Location = System::Drawing::Point(413, 169);
			this->Spell3->Name = L"Spell3";
			this->Spell3->Size = System::Drawing::Size(211, 21);
			this->Spell3->TabIndex = 9;
			this->Spell3->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Spell3_SelectedIndexChanged);
			// 
			// LevelBox
			// 
			this->LevelBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->LevelBox->FormattingEnabled = true;
			this->LevelBox->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Level 1", L"Level 2", L"Level 3", L"Level 4",
					L"Level 5", L"Level 6", L"Level 7+"
			});
			this->LevelBox->Location = System::Drawing::Point(639, 169);
			this->LevelBox->Name = L"LevelBox";
			this->LevelBox->Size = System::Drawing::Size(142, 21);
			this->LevelBox->TabIndex = 10;
			this->LevelBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::LevelBox_SelectedIndexChanged);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->BackColor = System::Drawing::Color::Black;
			this->label7->Location = System::Drawing::Point(627, 198);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(56, 13);
			this->label7->TabIndex = 5;
			this->label7->Text = L"Fire Shield";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->BackColor = System::Drawing::Color::Black;
			this->label10->Location = System::Drawing::Point(627, 250);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(93, 13);
			this->label10->TabIndex = 5;
			this->label10->Text = L"Skele Transformer";
			// 
			// SkeleBox
			// 
			this->SkeleBox->Location = System::Drawing::Point(733, 247);
			this->SkeleBox->Name = L"SkeleBox";
			this->SkeleBox->Size = System::Drawing::Size(48, 20);
			this->SkeleBox->TabIndex = 11;
			this->SkeleBox->Text = L"56";
			this->SkeleBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->SkeleBox->TextChanged += gcnew System::EventHandler(this, &MyForm::SkeleBox_TextChanged);
			// 
			// FireShieldBox
			// 
			this->FireShieldBox->Location = System::Drawing::Point(733, 195);
			this->FireShieldBox->Name = L"FireShieldBox";
			this->FireShieldBox->Size = System::Drawing::Size(48, 20);
			this->FireShieldBox->TabIndex = 11;
			this->FireShieldBox->Text = L"0.0";
			this->FireShieldBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->FireShieldBox->TextChanged += gcnew System::EventHandler(this, &MyForm::FireShieldBox_TextChanged);
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->BackColor = System::Drawing::Color::Black;
			this->label9->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->label9->Location = System::Drawing::Point(791, 411);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(76, 13);
			this->label9->TabIndex = 5;
			this->label9->Text = L"Special Missile";
			// 
			// Missle_Type_box
			// 
			this->Missle_Type_box->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->Missle_Type_box->FormattingEnabled = true;
			this->Missle_Type_box->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Default", L"Fireball", L"Death Cloud",
					L"Chain Shot Unsafe", L"Chain Shot Safe", L"Inferno Shot Unsafe", L"Inferno Shot Safe"
			});
			this->Missle_Type_box->Location = System::Drawing::Point(790, 427);
			this->Missle_Type_box->Name = L"Missle_Type_box";
			this->Missle_Type_box->Size = System::Drawing::Size(132, 21);
			this->Missle_Type_box->TabIndex = 12;
			this->Missle_Type_box->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Missle_Type_box_SelectedIndexChanged);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->Filter = L"CFG files |*.cfg";
			this->saveFileDialog1->InitialDirectory = L".";
			// 
			// button_save
			// 
			this->button_save->BackColor = System::Drawing::Color::Black;
			this->button_save->Location = System::Drawing::Point(174, 510);
			this->button_save->Name = L"button_save";
			this->button_save->Size = System::Drawing::Size(75, 23);
			this->button_save->TabIndex = 13;
			this->button_save->Text = L"Save";
			this->button_save->UseVisualStyleBackColor = false;
			this->button_save->Click += gcnew System::EventHandler(this, &MyForm::button_save_Click);
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->BackColor = System::Drawing::Color::Black;
			this->label11->Location = System::Drawing::Point(518, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(114, 13);
			this->label11->TabIndex = 5;
			this->label11->Text = L"Creature Regeneration";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->BackColor = System::Drawing::Color::Black;
			this->label12->Location = System::Drawing::Point(528, 29);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(38, 13);
			this->label12->TabIndex = 5;
			this->label12->Text = L"Health";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->BackColor = System::Drawing::Color::Black;
			this->label13->Location = System::Drawing::Point(522, 53);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(44, 13);
			this->label13->TabIndex = 5;
			this->label13->Text = L"Chance";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->BackColor = System::Drawing::Color::Black;
			this->label14->Location = System::Drawing::Point(507, 77);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(59, 13);
			this->label14->TabIndex = 5;
			this->label14->Text = L"ManaDrain";
			// 
			// HP_Regen
			// 
			this->HP_Regen->Location = System::Drawing::Point(572, 26);
			this->HP_Regen->Name = L"HP_Regen";
			this->HP_Regen->Size = System::Drawing::Size(52, 20);
			this->HP_Regen->TabIndex = 14;
			this->HP_Regen->Text = L"0";
			this->HP_Regen->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->HP_Regen->TextChanged += gcnew System::EventHandler(this, &MyForm::HP_Regen_TextChanged);
			// 
			// HP_Regen_Chance
			// 
			this->HP_Regen_Chance->Location = System::Drawing::Point(572, 50);
			this->HP_Regen_Chance->Name = L"HP_Regen_Chance";
			this->HP_Regen_Chance->Size = System::Drawing::Size(52, 20);
			this->HP_Regen_Chance->TabIndex = 14;
			this->HP_Regen_Chance->Text = L"0";
			this->HP_Regen_Chance->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->HP_Regen_Chance->TextChanged += gcnew System::EventHandler(this, &MyForm::HP_Regen_Chance_TextChanged);
			// 
			// ManaDrain
			// 
			this->ManaDrain->Location = System::Drawing::Point(572, 74);
			this->ManaDrain->Name = L"ManaDrain";
			this->ManaDrain->Size = System::Drawing::Size(52, 20);
			this->ManaDrain->TabIndex = 14;
			this->ManaDrain->Text = L"0";
			this->ManaDrain->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ManaDrain->TextChanged += gcnew System::EventHandler(this, &MyForm::ManaDrain_TextChanged);
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->BackColor = System::Drawing::Color::Black;
			this->label15->Location = System::Drawing::Point(627, 297);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(62, 13);
			this->label15->TabIndex = 5;
			this->label15->Text = L"Death Blow";
			// 
			// DeathBlow
			// 
			this->DeathBlow->Location = System::Drawing::Point(733, 294);
			this->DeathBlow->Name = L"DeathBlow";
			this->DeathBlow->Size = System::Drawing::Size(48, 20);
			this->DeathBlow->TabIndex = 15;
			this->DeathBlow->Text = L"0";
			this->DeathBlow->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->DeathBlow->TextChanged += gcnew System::EventHandler(this, &MyForm::DeathBlow_TextChanged);
			// 
			// SpellsCostLess
			// 
			this->SpellsCostLess->Location = System::Drawing::Point(733, 316);
			this->SpellsCostLess->Name = L"SpellsCostLess";
			this->SpellsCostLess->Size = System::Drawing::Size(48, 20);
			this->SpellsCostLess->TabIndex = 15;
			this->SpellsCostLess->Text = L"0";
			this->SpellsCostLess->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->SpellsCostLess->TextChanged += gcnew System::EventHandler(this, &MyForm::SpellsCostLess_TextChanged);
			// 
			// SpellsCostDump
			// 
			this->SpellsCostDump->Location = System::Drawing::Point(733, 342);
			this->SpellsCostDump->Name = L"SpellsCostDump";
			this->SpellsCostDump->Size = System::Drawing::Size(48, 20);
			this->SpellsCostDump->TabIndex = 15;
			this->SpellsCostDump->Text = L"0";
			this->SpellsCostDump->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->SpellsCostDump->TextChanged += gcnew System::EventHandler(this, &MyForm::SpellsCostDump_TextChanged);
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->BackColor = System::Drawing::Color::Black;
			this->label16->Location = System::Drawing::Point(627, 319);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(103, 13);
			this->label16->TabIndex = 5;
			this->label16->Text = L"Preserve Spellpoints";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->BackColor = System::Drawing::Color::Black;
			this->label17->Location = System::Drawing::Point(627, 345);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(76, 13);
			this->label17->TabIndex = 5;
			this->label17->Text = L"Magic Damper";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->BackColor = System::Drawing::Color::Black;
			this->label18->Location = System::Drawing::Point(627, 372);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(62, 13);
			this->label18->TabIndex = 5;
			this->label18->Text = L"Retaliations";
			// 
			// Retaliations
			// 
			this->Retaliations->Location = System::Drawing::Point(733, 365);
			this->Retaliations->Name = L"Retaliations";
			this->Retaliations->Size = System::Drawing::Size(48, 20);
			this->Retaliations->TabIndex = 15;
			this->Retaliations->Text = L"1";
			this->Retaliations->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->Retaliations->TextChanged += gcnew System::EventHandler(this, &MyForm::Retaliations_TextChanged);
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->BackColor = System::Drawing::Color::Black;
			this->label19->Location = System::Drawing::Point(627, 395);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(79, 13);
			this->label19->TabIndex = 5;
			this->label19->Text = L"Casts Spell (ID)";
			this->label19->Click += gcnew System::EventHandler(this, &MyForm::label19_Click);
			// 
			// CastsSpell
			// 
			this->CastsSpell->Location = System::Drawing::Point(733, 388);
			this->CastsSpell->Name = L"CastsSpell";
			this->CastsSpell->Size = System::Drawing::Size(48, 20);
			this->CastsSpell->TabIndex = 15;
			this->CastsSpell->Text = L"0";
			this->CastsSpell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->CastsSpell->TextChanged += gcnew System::EventHandler(this, &MyForm::CastsSpell_TextChanged);
			// 
			// Filename_Label
			// 
			this->Filename_Label->AutoSize = true;
			this->Filename_Label->BackColor = System::Drawing::Color::Black;
			this->Filename_Label->Location = System::Drawing::Point(12, 536);
			this->Filename_Label->MaximumSize = System::Drawing::Size(760, 16);
			this->Filename_Label->MinimumSize = System::Drawing::Size(910, 16);
			this->Filename_Label->Name = L"Filename_Label";
			this->Filename_Label->Size = System::Drawing::Size(910, 16);
			this->Filename_Label->TabIndex = 16;
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->BackColor = System::Drawing::Color::Black;
			this->label20->Location = System::Drawing::Point(627, 417);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(78, 13);
			this->label20->TabIndex = 5;
			this->label20->Text = L"Imposed Spells";
			// 
			// ImpSpell1Number
			// 
			this->ImpSpell1Number->Location = System::Drawing::Point(733, 433);
			this->ImpSpell1Number->Name = L"ImpSpell1Number";
			this->ImpSpell1Number->Size = System::Drawing::Size(48, 20);
			this->ImpSpell1Number->TabIndex = 17;
			this->ImpSpell1Number->Text = L"0";
			this->ImpSpell1Number->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ImpSpell1Number->TextChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell1Number_TextChanged);
			// 
			// ImpSpell2Number
			// 
			this->ImpSpell2Number->Location = System::Drawing::Point(733, 459);
			this->ImpSpell2Number->Name = L"ImpSpell2Number";
			this->ImpSpell2Number->Size = System::Drawing::Size(48, 20);
			this->ImpSpell2Number->TabIndex = 17;
			this->ImpSpell2Number->Text = L"0";
			this->ImpSpell2Number->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ImpSpell2Number->TextChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell2Number_TextChanged);
			// 
			// ImpSpell3Number
			// 
			this->ImpSpell3Number->Location = System::Drawing::Point(733, 482);
			this->ImpSpell3Number->Name = L"ImpSpell3Number";
			this->ImpSpell3Number->Size = System::Drawing::Size(48, 20);
			this->ImpSpell3Number->TabIndex = 17;
			this->ImpSpell3Number->Text = L"0";
			this->ImpSpell3Number->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ImpSpell3Number->TextChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell3Number_TextChanged);
			// 
			// ImpSpell1Power
			// 
			this->ImpSpell1Power->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->ImpSpell1Power->FormattingEnabled = true;
			this->ImpSpell1Power->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"Novice", L"Basic", L"Advanced", L"Expert" });
			this->ImpSpell1Power->Location = System::Drawing::Point(630, 433);
			this->ImpSpell1Power->Name = L"ImpSpell1Power";
			this->ImpSpell1Power->Size = System::Drawing::Size(100, 21);
			this->ImpSpell1Power->TabIndex = 18;
			this->ImpSpell1Power->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell1Power_SelectedIndexChanged);
			// 
			// ImpSpell2Power
			// 
			this->ImpSpell2Power->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->ImpSpell2Power->FormattingEnabled = true;
			this->ImpSpell2Power->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"Novice", L"Basic", L"Advanced", L"Expert" });
			this->ImpSpell2Power->Location = System::Drawing::Point(630, 458);
			this->ImpSpell2Power->Name = L"ImpSpell2Power";
			this->ImpSpell2Power->Size = System::Drawing::Size(100, 21);
			this->ImpSpell2Power->TabIndex = 18;
			this->ImpSpell2Power->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell2Power_SelectedIndexChanged);
			// 
			// ImpSpell3Power
			// 
			this->ImpSpell3Power->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->ImpSpell3Power->FormattingEnabled = true;
			this->ImpSpell3Power->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"Novice", L"Basic", L"Advanced", L"Expert" });
			this->ImpSpell3Power->Location = System::Drawing::Point(630, 482);
			this->ImpSpell3Power->Name = L"ImpSpell3Power";
			this->ImpSpell3Power->Size = System::Drawing::Size(100, 21);
			this->ImpSpell3Power->TabIndex = 18;
			this->ImpSpell3Power->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::ImpSpell3Power_SelectedIndexChanged);
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->BackColor = System::Drawing::Color::Black;
			this->label21->Location = System::Drawing::Point(627, 510);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(100, 13);
			this->label21->TabIndex = 5;
			this->label21->Text = L"Demonology Target";
			// 
			// Demonology_Target
			// 
			this->Demonology_Target->Location = System::Drawing::Point(733, 507);
			this->Demonology_Target->Name = L"Demonology_Target";
			this->Demonology_Target->Size = System::Drawing::Size(48, 20);
			this->Demonology_Target->TabIndex = 19;
			this->Demonology_Target->Text = L"0";
			this->Demonology_Target->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->Demonology_Target->TextChanged += gcnew System::EventHandler(this, &MyForm::Demonology_Target_TextChanged);
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->BackColor = System::Drawing::Color::Black;
			this->label22->Location = System::Drawing::Point(487, 205);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(66, 13);
			this->label22->TabIndex = 5;
			this->label22->Text = L"UpgradesTo";
			// 
			// UpgTo
			// 
			this->UpgTo->Location = System::Drawing::Point(559, 196);
			this->UpgTo->Name = L"UpgTo";
			this->UpgTo->Size = System::Drawing::Size(49, 20);
			this->UpgTo->TabIndex = 20;
			this->UpgTo->Text = L"-1";
			this->UpgTo->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->UpgTo->TextChanged += gcnew System::EventHandler(this, &MyForm::UpgTo_TextChanged);
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->BackColor = System::Drawing::Color::Black;
			this->label23->Location = System::Drawing::Point(486, 228);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(67, 13);
			this->label23->TabIndex = 5;
			this->label23->Text = L"MimicArtifact";
			this->label23->Click += gcnew System::EventHandler(this, &MyForm::label23_Click);
			// 
			// MimicArtifact
			// 
			this->MimicArtifact->Location = System::Drawing::Point(559, 221);
			this->MimicArtifact->Name = L"MimicArtifact";
			this->MimicArtifact->Size = System::Drawing::Size(49, 20);
			this->MimicArtifact->TabIndex = 20;
			this->MimicArtifact->Text = L"0";
			this->MimicArtifact->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->MimicArtifact->TextChanged += gcnew System::EventHandler(this, &MyForm::MimicArtifact_TextChanged);
			// 
			// MimicArtifact2
			// 
			this->MimicArtifact2->Location = System::Drawing::Point(431, 221);
			this->MimicArtifact2->Name = L"MimicArtifact2";
			this->MimicArtifact2->Size = System::Drawing::Size(49, 20);
			this->MimicArtifact2->TabIndex = 20;
			this->MimicArtifact2->Text = L"0";
			this->MimicArtifact2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->MimicArtifact2->TextChanged += gcnew System::EventHandler(this, &MyForm::MimicArtifact2_TextChanged);
			// 
			// z_res_type
			// 
			this->z_res_type->FormattingEnabled = true;
			this->z_res_type->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"Wood", L"Mercury", L"Ore", L"Sulfur", L"Crystal",
					L"Gems", L"Gold", L"Mithril", L"<no resource>"
			});
			this->z_res_type->Location = System::Drawing::Point(431, 250);
			this->z_res_type->Name = L"z_res_type";
			this->z_res_type->Size = System::Drawing::Size(121, 21);
			this->z_res_type->TabIndex = 21;
			this->z_res_type->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::z_res_type_SelectedIndexChanged);
			// 
			// z_res_amount
			// 
			this->z_res_amount->Location = System::Drawing::Point(559, 250);
			this->z_res_amount->Name = L"z_res_amount";
			this->z_res_amount->Size = System::Drawing::Size(49, 20);
			this->z_res_amount->TabIndex = 22;
			this->z_res_amount->Text = L"0";
			this->z_res_amount->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->z_res_amount->TextChanged += gcnew System::EventHandler(this, &MyForm::z_res_amount_TextChanged);
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->BackColor = System::Drawing::Color::Black;
			this->label24->Location = System::Drawing::Point(528, 280);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(25, 13);
			this->label24->TabIndex = 5;
			this->label24->Text = L"Tax";
			this->label24->Click += gcnew System::EventHandler(this, &MyForm::label23_Click);
			// 
			// TaxBox
			// 
			this->TaxBox->Location = System::Drawing::Point(560, 277);
			this->TaxBox->Name = L"TaxBox";
			this->TaxBox->Size = System::Drawing::Size(48, 20);
			this->TaxBox->TabIndex = 23;
			this->TaxBox->Text = L"0";
			this->TaxBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->TaxBox->TextChanged += gcnew System::EventHandler(this, &MyForm::TaxBox_TextChanged);
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->BackColor = System::Drawing::Color::Black;
			this->label25->Location = System::Drawing::Point(428, 280);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(41, 13);
			this->label25->TabIndex = 5;
			this->label25->Text = L"Guards";
			this->label25->Click += gcnew System::EventHandler(this, &MyForm::label23_Click);
			// 
			// GuardsBox
			// 
			this->GuardsBox->Location = System::Drawing::Point(476, 277);
			this->GuardsBox->Name = L"GuardsBox";
			this->GuardsBox->Size = System::Drawing::Size(46, 20);
			this->GuardsBox->TabIndex = 24;
			this->GuardsBox->Text = L"-1";
			this->GuardsBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->GuardsBox->TextChanged += gcnew System::EventHandler(this, &MyForm::GuardsBox_TextChanged);
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->BackColor = System::Drawing::Color::Black;
			this->label26->Location = System::Drawing::Point(419, 306);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(127, 13);
			this->label26->TabIndex = 25;
			this->label26->Text = L"ReduceTargetDefense %";
			// 
			// ReduceTargetDefense_TXT
			// 
			this->ReduceTargetDefense_TXT->Location = System::Drawing::Point(559, 303);
			this->ReduceTargetDefense_TXT->Name = L"ReduceTargetDefense_TXT";
			this->ReduceTargetDefense_TXT->Size = System::Drawing::Size(49, 20);
			this->ReduceTargetDefense_TXT->TabIndex = 26;
			this->ReduceTargetDefense_TXT->Text = L"0";
			this->ReduceTargetDefense_TXT->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ReduceTargetDefense_TXT->TextChanged += gcnew System::EventHandler(this, &MyForm::ReduceTargetDefense_TXT_TextChanged);
			// 
			// checkBox_ForcedSave
			// 
			this->checkBox_ForcedSave->AutoSize = true;
			this->checkBox_ForcedSave->BackColor = System::Drawing::Color::Black;
			this->checkBox_ForcedSave->Location = System::Drawing::Point(255, 514);
			this->checkBox_ForcedSave->Name = L"checkBox_ForcedSave";
			this->checkBox_ForcedSave->Size = System::Drawing::Size(59, 17);
			this->checkBox_ForcedSave->TabIndex = 27;
			this->checkBox_ForcedSave->Text = L"Forced";
			this->checkBox_ForcedSave->UseVisualStyleBackColor = false;
			this->checkBox_ForcedSave->CheckedChanged += gcnew System::EventHandler(this, &MyForm::checkBox_ForcedSave_CheckedChanged);
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->BackColor = System::Drawing::Color::Black;
			this->label27->Location = System::Drawing::Point(428, 342);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(88, 13);
			this->label27->TabIndex = 28;
			this->label27->Text = L"Spell After Action";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->BackColor = System::Drawing::Color::Black;
			this->label28->Location = System::Drawing::Point(418, 371);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(35, 13);
			this->label28->TabIndex = 29;
			this->label28->Text = L"melee";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->BackColor = System::Drawing::Color::Black;
			this->label29->Location = System::Drawing::Point(459, 372);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(33, 13);
			this->label29->TabIndex = 29;
			this->label29->Text = L"shoot";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->BackColor = System::Drawing::Color::Black;
			this->label30->Location = System::Drawing::Point(500, 372);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(40, 13);
			this->label30->TabIndex = 29;
			this->label30->Text = L"defend";
			// 
			// AfterMeleeSpell
			// 
			this->AfterMeleeSpell->Location = System::Drawing::Point(413, 395);
			this->AfterMeleeSpell->Name = L"AfterMeleeSpell";
			this->AfterMeleeSpell->Size = System::Drawing::Size(40, 20);
			this->AfterMeleeSpell->TabIndex = 30;
			this->AfterMeleeSpell->Text = L"0";
			this->AfterMeleeSpell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterMeleeSpell->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterMeleeSpell_TextChanged);
			// 
			// AfterShootSpell
			// 
			this->AfterShootSpell->Location = System::Drawing::Point(459, 395);
			this->AfterShootSpell->Name = L"AfterShootSpell";
			this->AfterShootSpell->Size = System::Drawing::Size(40, 20);
			this->AfterShootSpell->TabIndex = 30;
			this->AfterShootSpell->Text = L"0";
			this->AfterShootSpell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterShootSpell->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterShootSpell_TextChanged);
			// 
			// AfterDefendSpell
			// 
			this->AfterDefendSpell->Location = System::Drawing::Point(500, 395);
			this->AfterDefendSpell->Name = L"AfterDefendSpell";
			this->AfterDefendSpell->Size = System::Drawing::Size(40, 20);
			this->AfterDefendSpell->TabIndex = 30;
			this->AfterDefendSpell->Text = L"0";
			this->AfterDefendSpell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterDefendSpell->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterDefendSpell_TextChanged);
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->BackColor = System::Drawing::Color::Black;
			this->label31->Location = System::Drawing::Point(447, 507);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(105, 13);
			this->label31->TabIndex = 28;
			this->label31->Text = L"Shooting Resistance";
			// 
			// ShootingResistance
			// 
			this->ShootingResistance->Location = System::Drawing::Point(559, 507);
			this->ShootingResistance->Name = L"ShootingResistance";
			this->ShootingResistance->Size = System::Drawing::Size(49, 20);
			this->ShootingResistance->TabIndex = 31;
			this->ShootingResistance->Text = L"0.0";
			this->ShootingResistance->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ShootingResistance->TextChanged += gcnew System::EventHandler(this, &MyForm::ShootingResistance_TextChanged);
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->BackColor = System::Drawing::Color::Black;
			this->label32->Location = System::Drawing::Point(546, 372);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(39, 13);
			this->label32->TabIndex = 29;
			this->label32->Text = L"wound";
			// 
			// AfterWoundSpell
			// 
			this->AfterWoundSpell->Location = System::Drawing::Point(545, 395);
			this->AfterWoundSpell->Name = L"AfterWoundSpell";
			this->AfterWoundSpell->Size = System::Drawing::Size(40, 20);
			this->AfterWoundSpell->TabIndex = 30;
			this->AfterWoundSpell->Text = L"0";
			this->AfterWoundSpell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterWoundSpell->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterWoundSpell_TextChanged);
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->BackColor = System::Drawing::Color::Black;
			this->label33->Location = System::Drawing::Point(461, 482);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(92, 13);
			this->label33->TabIndex = 28;
			this->label33->Text = L"Melee Resistance";
			// 
			// MeleeResistance
			// 
			this->MeleeResistance->Location = System::Drawing::Point(559, 481);
			this->MeleeResistance->Name = L"MeleeResistance";
			this->MeleeResistance->Size = System::Drawing::Size(48, 20);
			this->MeleeResistance->TabIndex = 32;
			this->MeleeResistance->Text = L"0.0";
			this->MeleeResistance->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->MeleeResistance->TextChanged += gcnew System::EventHandler(this, &MyForm::MeleeResistance_TextChanged);
			// 
			// SpellAfterActionMastery
			// 
			this->SpellAfterActionMastery->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->SpellAfterActionMastery->FormattingEnabled = true;
			this->SpellAfterActionMastery->Items->AddRange(gcnew cli::array< System::Object^  >(4) {
				L"Novice", L"Basic", L"Advanced",
					L"Expert"
			});
			this->SpellAfterActionMastery->Location = System::Drawing::Point(532, 339);
			this->SpellAfterActionMastery->Name = L"SpellAfterActionMastery";
			this->SpellAfterActionMastery->Size = System::Drawing::Size(76, 21);
			this->SpellAfterActionMastery->TabIndex = 33;
			this->SpellAfterActionMastery->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::SpellAfterActionMastery_SelectedIndexChanged);
			// 
			// DoNotGenerate
			// 
			this->DoNotGenerate->AutoSize = true;
			this->DoNotGenerate->BackColor = System::Drawing::Color::Black;
			this->DoNotGenerate->Location = System::Drawing::Point(321, 514);
			this->DoNotGenerate->Name = L"DoNotGenerate";
			this->DoNotGenerate->Size = System::Drawing::Size(107, 17);
			this->DoNotGenerate->TabIndex = 34;
			this->DoNotGenerate->Text = L"Do Not Generate";
			this->DoNotGenerate->UseVisualStyleBackColor = false;
			this->DoNotGenerate->CheckedChanged += gcnew System::EventHandler(this, &MyForm::DoNotGenerate_CheckedChanged);
			// 
			// AfterMeleeSpell2
			// 
			this->AfterMeleeSpell2->Location = System::Drawing::Point(411, 422);
			this->AfterMeleeSpell2->Name = L"AfterMeleeSpell2";
			this->AfterMeleeSpell2->Size = System::Drawing::Size(42, 20);
			this->AfterMeleeSpell2->TabIndex = 35;
			this->AfterMeleeSpell2->Text = L"0";
			this->AfterMeleeSpell2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterMeleeSpell2->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterMeleeSpell2_TextChanged);
			// 
			// AfterShootSpell2
			// 
			this->AfterShootSpell2->Location = System::Drawing::Point(457, 422);
			this->AfterShootSpell2->Name = L"AfterShootSpell2";
			this->AfterShootSpell2->Size = System::Drawing::Size(42, 20);
			this->AfterShootSpell2->TabIndex = 35;
			this->AfterShootSpell2->Text = L"0";
			this->AfterShootSpell2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterShootSpell2->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterShootSpell2_TextChanged);
			// 
			// AfterDefendSpell2
			// 
			this->AfterDefendSpell2->Location = System::Drawing::Point(500, 422);
			this->AfterDefendSpell2->Name = L"AfterDefendSpell2";
			this->AfterDefendSpell2->Size = System::Drawing::Size(42, 20);
			this->AfterDefendSpell2->TabIndex = 35;
			this->AfterDefendSpell2->Text = L"0";
			this->AfterDefendSpell2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterDefendSpell2->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterDefendSpell2_TextChanged);
			// 
			// AfterWoundSpell2
			// 
			this->AfterWoundSpell2->Location = System::Drawing::Point(543, 422);
			this->AfterWoundSpell2->Name = L"AfterWoundSpell2";
			this->AfterWoundSpell2->Size = System::Drawing::Size(42, 20);
			this->AfterWoundSpell2->TabIndex = 35;
			this->AfterWoundSpell2->Text = L"0";
			this->AfterWoundSpell2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AfterWoundSpell2->TextChanged += gcnew System::EventHandler(this, &MyForm::AfterWoundSpell2_TextChanged);
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->BackColor = System::Drawing::Color::Black;
			this->label34->Location = System::Drawing::Point(786, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(105, 13);
			this->label34->TabIndex = 5;
			this->label34->Text = L"Phoenix-Like Rebirth";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->BackColor = System::Drawing::Color::Black;
			this->label35->Location = System::Drawing::Point(786, 28);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(42, 13);
			this->label35->TabIndex = 5;
			this->label35->Text = L"fraction";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->BackColor = System::Drawing::Color::Black;
			this->label36->Location = System::Drawing::Point(786, 52);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(43, 13);
			this->label36->TabIndex = 5;
			this->label36->Text = L"chance";
			// 
			// rebirth_fraction_box
			// 
			this->rebirth_fraction_box->Location = System::Drawing::Point(843, 22);
			this->rebirth_fraction_box->Name = L"rebirth_fraction_box";
			this->rebirth_fraction_box->Size = System::Drawing::Size(48, 20);
			this->rebirth_fraction_box->TabIndex = 11;
			this->rebirth_fraction_box->Text = L"0.0";
			this->rebirth_fraction_box->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->rebirth_fraction_box->TextChanged += gcnew System::EventHandler(this, &MyForm::rebirth_fraction_box_TextChanged);
			// 
			// rebirth_chance_box
			// 
			this->rebirth_chance_box->Location = System::Drawing::Point(843, 46);
			this->rebirth_chance_box->Name = L"rebirth_chance_box";
			this->rebirth_chance_box->Size = System::Drawing::Size(48, 20);
			this->rebirth_chance_box->TabIndex = 11;
			this->rebirth_chance_box->Text = L"0.0";
			this->rebirth_chance_box->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->rebirth_chance_box->TextChanged += gcnew System::EventHandler(this, &MyForm::rebirth_chance_box_TextChanged);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->BackColor = System::Drawing::Color::Black;
			this->label8->Location = System::Drawing::Point(787, 74);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(27, 13);
			this->label8->TabIndex = 5;
			this->label8->Text = L"sure";
			// 
			// rebirth_sure_box
			// 
			this->rebirth_sure_box->Location = System::Drawing::Point(843, 70);
			this->rebirth_sure_box->Name = L"rebirth_sure_box";
			this->rebirth_sure_box->Size = System::Drawing::Size(48, 20);
			this->rebirth_sure_box->TabIndex = 11;
			this->rebirth_sure_box->Text = L"0.0";
			this->rebirth_sure_box->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->rebirth_sure_box->TextChanged += gcnew System::EventHandler(this, &MyForm::rebirth_sure_box_TextChanged);
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->BackColor = System::Drawing::Color::Black;
			this->label37->Location = System::Drawing::Point(627, 224);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(69, 13);
			this->label37->TabIndex = 5;
			this->label37->Text = L"Mana Regen";
			// 
			// ManaRegenBox
			// 
			this->ManaRegenBox->Location = System::Drawing::Point(733, 222);
			this->ManaRegenBox->Name = L"ManaRegenBox";
			this->ManaRegenBox->Size = System::Drawing::Size(48, 20);
			this->ManaRegenBox->TabIndex = 36;
			this->ManaRegenBox->Text = L"0";
			this->ManaRegenBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ManaRegenBox->TextChanged += gcnew System::EventHandler(this, &MyForm::ManaRegenBox_TextChanged);
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->BackColor = System::Drawing::Color::Black;
			this->label38->Location = System::Drawing::Point(787, 125);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(89, 13);
			this->label38->TabIndex = 5;
			this->label38->Text = L"Additional Effects";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->BackColor = System::Drawing::Color::Black;
			this->label39->Location = System::Drawing::Point(787, 144);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(34, 13);
			this->label39->TabIndex = 5;
			this->label39->Text = L"Aging";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->BackColor = System::Drawing::Color::Black;
			this->label40->Location = System::Drawing::Point(786, 169);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(39, 13);
			this->label40->TabIndex = 5;
			this->label40->Text = L"Poison";
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->BackColor = System::Drawing::Color::Black;
			this->label41->Location = System::Drawing::Point(787, 195);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(47, 13);
			this->label41->TabIndex = 5;
			this->label41->Text = L"Paralyze";
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->BackColor = System::Drawing::Color::Black;
			this->label42->Location = System::Drawing::Point(848, 195);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(43, 13);
			this->label42->TabIndex = 5;
			this->label42->Text = L"chance";
			// 
			// AgingBox
			// 
			this->AgingBox->Location = System::Drawing::Point(843, 141);
			this->AgingBox->Name = L"AgingBox";
			this->AgingBox->Size = System::Drawing::Size(48, 20);
			this->AgingBox->TabIndex = 37;
			this->AgingBox->Text = L"0";
			this->AgingBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AgingBox->TextChanged += gcnew System::EventHandler(this, &MyForm::AgingBox_TextChanged);
			// 
			// PoisonBox
			// 
			this->PoisonBox->Location = System::Drawing::Point(843, 167);
			this->PoisonBox->Name = L"PoisonBox";
			this->PoisonBox->Size = System::Drawing::Size(48, 20);
			this->PoisonBox->TabIndex = 37;
			this->PoisonBox->Text = L"0";
			this->PoisonBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->PoisonBox->TextChanged += gcnew System::EventHandler(this, &MyForm::PoisonBox_TextChanged);
			// 
			// ParalyzeBox
			// 
			this->ParalyzeBox->Location = System::Drawing::Point(789, 211);
			this->ParalyzeBox->Name = L"ParalyzeBox";
			this->ParalyzeBox->Size = System::Drawing::Size(48, 20);
			this->ParalyzeBox->TabIndex = 37;
			this->ParalyzeBox->Text = L"0";
			this->ParalyzeBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ParalyzeBox->TextChanged += gcnew System::EventHandler(this, &MyForm::ParalyzeBox_TextChanged);
			// 
			// ParalyzeChanceBox
			// 
			this->ParalyzeChanceBox->Location = System::Drawing::Point(843, 211);
			this->ParalyzeChanceBox->Name = L"ParalyzeChanceBox";
			this->ParalyzeChanceBox->Size = System::Drawing::Size(48, 20);
			this->ParalyzeChanceBox->TabIndex = 37;
			this->ParalyzeChanceBox->Text = L"0";
			this->ParalyzeChanceBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ParalyzeChanceBox->TextChanged += gcnew System::EventHandler(this, &MyForm::ParalyzeChanceBox_TextChanged);
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->BackColor = System::Drawing::Color::Black;
			this->label43->Location = System::Drawing::Point(787, 490);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(76, 13);
			this->label43->TabIndex = 5;
			this->label43->Text = L"Ghost Fraction";
			this->label43->Click += gcnew System::EventHandler(this, &MyForm::label43_Click);
			// 
			// GhostFractionBox
			// 
			this->GhostFractionBox->Location = System::Drawing::Point(790, 506);
			this->GhostFractionBox->Name = L"GhostFractionBox";
			this->GhostFractionBox->Size = System::Drawing::Size(73, 20);
			this->GhostFractionBox->TabIndex = 38;
			this->GhostFractionBox->Text = L"1.00";
			this->GhostFractionBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->GhostFractionBox->TextChanged += gcnew System::EventHandler(this, &MyForm::GhostMultiBox_TextChanged);
			// 
			// Custom_Flags_2
			// 
			this->Custom_Flags_2->BackColor = System::Drawing::Color::Black;
			this->Custom_Flags_2->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Custom_Flags_2->FormattingEnabled = true;
			this->Custom_Flags_2->Items->AddRange(gcnew cli::array< System::Object^  >(6) {
				L"No Golem Overflow", L"isDragonResistant",
					L"isHeroic", L"Counterstrike Fury", L"Counterstrike Twice", L"OverrideSpecialSpells"
			});
			this->Custom_Flags_2->Location = System::Drawing::Point(914, 0);
			this->Custom_Flags_2->Name = L"Custom_Flags_2";
			this->Custom_Flags_2->Size = System::Drawing::Size(135, 409);
			this->Custom_Flags_2->TabIndex = 39;
			this->Custom_Flags_2->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::Custom_Flags_2_SelectedIndexChanged);
			this->Custom_Flags_2->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::Custom_Flags_2_KeyUp);
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->BackColor = System::Drawing::Color::Black;
			this->label44->Location = System::Drawing::Point(869, 490);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(112, 13);
			this->label44->TabIndex = 40;
			this->label44->Text = L"Wog_Spell_Immunites";
			// 
			// Wog_Spell_immunites_box
			// 
			this->Wog_Spell_immunites_box->Location = System::Drawing::Point(872, 507);
			this->Wog_Spell_immunites_box->Name = L"Wog_Spell_immunites_box";
			this->Wog_Spell_immunites_box->Size = System::Drawing::Size(109, 20);
			this->Wog_Spell_immunites_box->TabIndex = 41;
			this->Wog_Spell_immunites_box->TextChanged += gcnew System::EventHandler(this, &MyForm::Wog_Spell_immunites_box_TextChanged);
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->BackColor = System::Drawing::Color::Black;
			this->label45->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->label45->Location = System::Drawing::Point(798, 451);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(27, 13);
			this->label45->TabIndex = 42;
			this->label45->Text = L"Size";
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->BackColor = System::Drawing::Color::Black;
			this->label46->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->label46->Location = System::Drawing::Point(848, 451);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(30, 13);
			this->label46->TabIndex = 43;
			this->label46->Text = L"Anim";
			// 
			// MissileSizeBox
			// 
			this->MissileSizeBox->Location = System::Drawing::Point(787, 467);
			this->MissileSizeBox->Name = L"MissileSizeBox";
			this->MissileSizeBox->Size = System::Drawing::Size(50, 20);
			this->MissileSizeBox->TabIndex = 44;
			this->MissileSizeBox->Text = L"0";
			this->MissileSizeBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->MissileSizeBox->TextChanged += gcnew System::EventHandler(this, &MyForm::Missiles_Size_TextChanged);
			// 
			// MissileAnimBox
			// 
			this->MissileAnimBox->Location = System::Drawing::Point(841, 467);
			this->MissileAnimBox->Name = L"MissileAnimBox";
			this->MissileAnimBox->Size = System::Drawing::Size(50, 20);
			this->MissileAnimBox->TabIndex = 44;
			this->MissileAnimBox->Text = L"0";
			this->MissileAnimBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->MissileAnimBox->TextChanged += gcnew System::EventHandler(this, &MyForm::Missiles_Anim_TextChanged);
			// 
			// label47
			// 
			this->label47->AutoSize = true;
			this->label47->BackColor = System::Drawing::Color::Black;
			this->label47->Location = System::Drawing::Point(627, 274);
			this->label47->Name = L"label47";
			this->label47->Size = System::Drawing::Size(76, 13);
			this->label47->TabIndex = 45;
			this->label47->Text = L"Around Range";
			// 
			// AroundRangeBox
			// 
			this->AroundRangeBox->Location = System::Drawing::Point(733, 271);
			this->AroundRangeBox->Name = L"AroundRangeBox";
			this->AroundRangeBox->Size = System::Drawing::Size(48, 20);
			this->AroundRangeBox->TabIndex = 46;
			this->AroundRangeBox->Text = L"0";
			this->AroundRangeBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->AroundRangeBox->TextChanged += gcnew System::EventHandler(this, &MyForm::AroundRangeBox_TextChanged);
			// 
			// TrailBox
			// 
			this->TrailBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->TrailBox->FormattingEnabled = true;
			this->TrailBox->Items->AddRange(gcnew cli::array< System::Object^  >(5) {
				L"None", L"Fire Wall", L"Forcefield", L"Landmine",
					L"Quicksand"
			});
			this->TrailBox->Location = System::Drawing::Point(928, 427);
			this->TrailBox->Name = L"TrailBox";
			this->TrailBox->Size = System::Drawing::Size(121, 21);
			this->TrailBox->TabIndex = 47;
			this->TrailBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::TrailBox_SelectedIndexChanged);
			// 
			// label48
			// 
			this->label48->AutoSize = true;
			this->label48->BackColor = System::Drawing::Color::Black;
			this->label48->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->label48->Location = System::Drawing::Point(928, 411);
			this->label48->Name = L"label48";
			this->label48->Size = System::Drawing::Size(90, 13);
			this->label48->TabIndex = 5;
			this->label48->Text = L"Special Field Trail";
			// 
			// label49
			// 
			this->label49->AutoSize = true;
			this->label49->BackColor = System::Drawing::Color::Black;
			this->label49->Location = System::Drawing::Point(798, 355);
			this->label49->Name = L"label49";
			this->label49->Size = System::Drawing::Size(94, 13);
			this->label49->TabIndex = 48;
			this->label49->Text = L"ACAST_CUSTOM";
			// 
			// label50
			// 
			this->label50->AutoSize = true;
			this->label50->BackColor = System::Drawing::Color::Black;
			this->label50->Location = System::Drawing::Point(849, 368);
			this->label50->Name = L"label50";
			this->label50->Size = System::Drawing::Size(43, 13);
			this->label50->TabIndex = 49;
			this->label50->Text = L"chance";
			// 
			// label51
			// 
			this->label51->AutoSize = true;
			this->label51->BackColor = System::Drawing::Color::Black;
			this->label51->Location = System::Drawing::Point(798, 368);
			this->label51->Name = L"label51";
			this->label51->Size = System::Drawing::Size(42, 13);
			this->label51->TabIndex = 50;
			this->label51->Text = L"spell ID";
			// 
			// ACAST_CUSTOM_spell
			// 
			this->ACAST_CUSTOM_spell->Location = System::Drawing::Point(794, 384);
			this->ACAST_CUSTOM_spell->Name = L"ACAST_CUSTOM_spell";
			this->ACAST_CUSTOM_spell->Size = System::Drawing::Size(46, 20);
			this->ACAST_CUSTOM_spell->TabIndex = 51;
			this->ACAST_CUSTOM_spell->Text = L"60";
			this->ACAST_CUSTOM_spell->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ACAST_CUSTOM_spell->TextChanged += gcnew System::EventHandler(this, &MyForm::ACAST_CUSTOM_spell_TextChanged);
			// 
			// ACAST_CUSTOM_chance
			// 
			this->ACAST_CUSTOM_chance->Location = System::Drawing::Point(845, 384);
			this->ACAST_CUSTOM_chance->Name = L"ACAST_CUSTOM_chance";
			this->ACAST_CUSTOM_chance->Size = System::Drawing::Size(46, 20);
			this->ACAST_CUSTOM_chance->TabIndex = 51;
			this->ACAST_CUSTOM_chance->Text = L"100";
			this->ACAST_CUSTOM_chance->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->ACAST_CUSTOM_chance->TextChanged += gcnew System::EventHandler(this, &MyForm::ACAST_CUSTOM_chance_TextChanged);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(1264, 561);
			this->Controls->Add(this->ACAST_CUSTOM_chance);
			this->Controls->Add(this->ACAST_CUSTOM_spell);
			this->Controls->Add(this->label51);
			this->Controls->Add(this->label50);
			this->Controls->Add(this->label49);
			this->Controls->Add(this->TrailBox);
			this->Controls->Add(this->AroundRangeBox);
			this->Controls->Add(this->label47);
			this->Controls->Add(this->MissileAnimBox);
			this->Controls->Add(this->MissileSizeBox);
			this->Controls->Add(this->label46);
			this->Controls->Add(this->label45);
			this->Controls->Add(this->Wog_Spell_immunites_box);
			this->Controls->Add(this->label44);
			this->Controls->Add(this->Custom_Flags_2);
			this->Controls->Add(this->GhostFractionBox);
			this->Controls->Add(this->ParalyzeChanceBox);
			this->Controls->Add(this->ParalyzeBox);
			this->Controls->Add(this->PoisonBox);
			this->Controls->Add(this->AgingBox);
			this->Controls->Add(this->ManaRegenBox);
			this->Controls->Add(this->AfterWoundSpell2);
			this->Controls->Add(this->AfterDefendSpell2);
			this->Controls->Add(this->AfterShootSpell2);
			this->Controls->Add(this->AfterMeleeSpell2);
			this->Controls->Add(this->DoNotGenerate);
			this->Controls->Add(this->SpellAfterActionMastery);
			this->Controls->Add(this->MeleeResistance);
			this->Controls->Add(this->ShootingResistance);
			this->Controls->Add(this->AfterWoundSpell);
			this->Controls->Add(this->AfterDefendSpell);
			this->Controls->Add(this->AfterShootSpell);
			this->Controls->Add(this->AfterMeleeSpell);
			this->Controls->Add(this->label32);
			this->Controls->Add(this->label30);
			this->Controls->Add(this->label29);
			this->Controls->Add(this->label28);
			this->Controls->Add(this->label33);
			this->Controls->Add(this->label31);
			this->Controls->Add(this->label27);
			this->Controls->Add(this->checkBox_ForcedSave);
			this->Controls->Add(this->ReduceTargetDefense_TXT);
			this->Controls->Add(this->label26);
			this->Controls->Add(this->GuardsBox);
			this->Controls->Add(this->TaxBox);
			this->Controls->Add(this->z_res_amount);
			this->Controls->Add(this->z_res_type);
			this->Controls->Add(this->MimicArtifact2);
			this->Controls->Add(this->MimicArtifact);
			this->Controls->Add(this->UpgTo);
			this->Controls->Add(this->Demonology_Target);
			this->Controls->Add(this->ImpSpell3Power);
			this->Controls->Add(this->ImpSpell2Power);
			this->Controls->Add(this->ImpSpell1Power);
			this->Controls->Add(this->ImpSpell3Number);
			this->Controls->Add(this->ImpSpell2Number);
			this->Controls->Add(this->ImpSpell1Number);
			this->Controls->Add(this->Filename_Label);
			this->Controls->Add(this->Retaliations);
			this->Controls->Add(this->CastsSpell);
			this->Controls->Add(this->SpellsCostDump);
			this->Controls->Add(this->SpellsCostLess);
			this->Controls->Add(this->DeathBlow);
			this->Controls->Add(this->HP_Regen_Chance);
			this->Controls->Add(this->ManaDrain);
			this->Controls->Add(this->HP_Regen);
			this->Controls->Add(this->button_save);
			this->Controls->Add(this->Missle_Type_box);
			this->Controls->Add(this->FireShieldBox);
			this->Controls->Add(this->rebirth_sure_box);
			this->Controls->Add(this->rebirth_chance_box);
			this->Controls->Add(this->rebirth_fraction_box);
			this->Controls->Add(this->SkeleBox);
			this->Controls->Add(this->LevelBox);
			this->Controls->Add(this->Spell3);
			this->Controls->Add(this->Spell2);
			this->Controls->Add(this->spell1);
			this->Controls->Add(this->Town_);
			this->Controls->Add(this->VULN_EFF);
			this->Controls->Add(this->RESIST_EFF);
			this->Controls->Add(this->ATT_EFF);
			this->Controls->Add(this->SP_EFF);
			this->Controls->Add(this->CSP_ADD);
			this->Controls->Add(this->CSP_DIV);
			this->Controls->Add(this->CSP_MUL);
			this->Controls->Add(this->label25);
			this->Controls->Add(this->label24);
			this->Controls->Add(this->label23);
			this->Controls->Add(this->label22);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label19);
			this->Controls->Add(this->label18);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->label42);
			this->Controls->Add(this->label41);
			this->Controls->Add(this->label40);
			this->Controls->Add(this->label39);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label36);
			this->Controls->Add(this->label35);
			this->Controls->Add(this->label43);
			this->Controls->Add(this->label38);
			this->Controls->Add(this->label34);
			this->Controls->Add(this->label48);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label37);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label21);
			this->Controls->Add(this->label20);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->Custom_Flags);
			this->Controls->Add(this->Default_Button);
			this->Controls->Add(this->button_load);
			this->Controls->Add(this->label_monster_flags);
			this->Controls->Add(this->checkedListBox_monster_flags);
			this->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(1280, 600);
			this->MinimumSize = System::Drawing::Size(1280, 600);
			this->Name = L"MyForm";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->Text = L"Amethyst CFG file editor (AmeEdit)";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void checkedListBox_monster_flags_ItemCheck(System::Object^ sender, System::Windows::Forms::ItemCheckEventArgs^ e) {
		/*
		long long monster_flags_value = 0;
		for (int i = 0; i < 32; i++)
		{
			if (this->checkedListBox_monster_flags->GetItemChecked(i))
				monster_flags_value += (1<<i);
		}
		this->label_monster_flags->Text = monster_flags_value.ToString();//System::String(" ");
		*/
	}
	private: System::Void checkedListBox_monster_flags_MouseUp(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) {
		long monster_flags_value = 0;
		for (int i = 0; i < 32; i++)
		{
			if (this->checkedListBox_monster_flags->GetItemChecked(i))
				monster_flags_value += (1 << i);
		}
		this->label_monster_flags->Text = monster_flags_value.ToString();//System::String(" ");
		new_monsters.flags = monster_flags_value;
	}
	private: System::Void checkedListBox_monster_flags_KeyUp(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		long monster_flags_value = 0;
		for (int i = 0; i < 32; i++)
		{
			if (this->checkedListBox_monster_flags->GetItemChecked(i))
				monster_flags_value += (1 << i);
		}
		this->label_monster_flags->Text = monster_flags_value.ToString();//System::String(" ");
		new_monsters.flags = monster_flags_value;
	}

	private: void load_GFX_interface(void) {
		Wog_Spell_immunites_box->Text = gcnew String(Wog_Spell_Immunites);

		AfterDefendSpell->Text = after_defend_spell.ToString();
		AfterWoundSpell->Text  = after_wound__spell.ToString();
		AfterMeleeSpell->Text  = after_melee__spell.ToString();
		AfterShootSpell->Text  = after_shoot__spell.ToString();
		//SpellAfterActionMastery->Text = ((int)after_action_spell_mastery).ToString();;
		SpellAfterActionMastery->SelectedIndex = ((int)after_action_spell_mastery);

		DoNotGenerate->Checked = do_not_generate;
		checkBox_ForcedSave->Checked = forced;


		ACAST_CUSTOM_chance->Text = Creature_ACAST_CUSTOM_Chance.ToString();
		ACAST_CUSTOM_spell->Text = Creature_ACAST_CUSTOM_Spell.ToString();

		MeleeResistance->Text		= melee_resistance.ToString();
		ShootingResistance->Text	= shooting_resistance.ToString();

		//SetThreadUILanguage();
		//SetThreadLocale();
		z_res_type->SelectedIndex = resource_type_table;
		z_res_amount->Text = resource_amount_table.ToString();
		TaxBox->Text = resource_tax_table.ToString();

		for (int i = 0; i < 32; i++)
		{
			this->checkedListBox_monster_flags->SetItemChecked(i, (new_monsters.flags & (1 << i)) != 0);
		}
		this->label_monster_flags->Text = new_monsters.flags.ToString();


		HP_Regen->Text = RegenerationHitPoints_Table.ToString();
		HP_Regen_Chance->Text = RegenerationChance_Table.ToString();
		ManaDrain->Text = ManaDrain_Table.ToString();
		DeathBlow->Text = DeathBlow_Table.ToString();

		UpgTo->Text			= upgtable.ToString();
		MimicArtifact->Text = CreatureMimicArtifact.ToString();
		MimicArtifact2->Text = CreatureMimicArtifact2.ToString();

		GhostFractionBox->Text = ghost_fraction.ToString();
		if (ghost_fraction == 0.00) GhostFractionBox->Text = "0.00";
		if (ghost_fraction == 1.00) GhostFractionBox->Text = "1.00";

		Custom_Flags->SetItemChecked(0, hasSantaGuards);
		Custom_Flags->SetItemChecked(1, isRogue);
		Custom_Flags->SetItemChecked(2, isGhost);
		Custom_Flags->SetItemChecked(3, isEnchanter);
		Custom_Flags->SetItemChecked(4, isSorceress);
		//Custom_Flags->SetItemChecked(5, isHellSteed);
		//Custom_Flags->SetItemChecked(6, isHellSteed2);
		//Custom_Flags->SetItemChecked(7, isHellSteed3);

		Custom_Flags->SetItemChecked(5, FireWall_Table);
		Custom_Flags->SetItemChecked(6, AlwaysPositiveMorale_Table);
		Custom_Flags->SetItemChecked(7, AlwaysPositiveLuck_Table);
		
		CSP_ADD->Text = CreatureSpellPowerAdder.ToString();
		CSP_MUL->Text = CreatureSpellPowerMultiplier.ToString();
		CSP_DIV->Text = CreatureSpellPowerDivider.ToString();
		SP_EFF->SelectedIndex = aftercast_abilities_table;
		ATT_EFF->SelectedIndex = attack_abilities_table;
		RESIST_EFF->SelectedIndex = magic_resistance_table;
		VULN_EFF->SelectedIndex = magic_vulnerability_table;
		
		if (new_monsters.town < 0) Town_->SelectedIndex = 9;
		else Town_->SelectedIndex = new_monsters.town;

		spell1->SelectedIndex = spell_1_table;
		Spell2->SelectedIndex = spell_2_table;
		Spell3->SelectedIndex = spell_3_table;

		LevelBox->SelectedIndex = new_monsters.level;

		FireShieldBox->Text = fire_shield_table.ToString();
		if (fire_shield_table == 0.00) FireShieldBox->Text = "0.00";

		//ressurectionBox->Text = respawn_table.ToString();
		//if (respawn_table == 0.00) ressurectionBox->Text = "0.00";
		rebirth_chance_box->Text = respawn_table_chance.ToString();
		if (respawn_table_chance == 0.00) rebirth_chance_box->Text = "0.00";
		rebirth_fraction_box->Text = respawn_table_fraction.ToString();
		if (respawn_table_fraction == 0.00) rebirth_fraction_box->Text = "0.00";
		rebirth_sure_box->Text = respawn_table_sure.ToString();
		if (respawn_table_sure == 0.00) rebirth_sure_box->Text = "0.00";

		MeleeResistance->Text = melee_resistance.ToString();
		if (melee_resistance == 0.00) MeleeResistance->Text = "0.00";
		ShootingResistance->Text = shooting_resistance.ToString();
		if (shooting_resistance == 0.00) ShootingResistance->Text = "0.00";


		AgingBox->Text = aging_table.ToString();
		PoisonBox->Text = poison_table.ToString();
		ParalyzeBox->Text = paralyze_table.ToString();
		ParalyzeChanceBox->Text = paralyze_chance.ToString();


		SkeleBox->Text = skeltrans.ToString();

		TrailBox->SelectedIndex = StackStep_field_type;

		Missle_Type_box->SelectedIndex = special_missiles_table;
		MissileSizeBox->Text = missile_size_table.ToString();
		MissileAnimBox->Text = missile_anim_table.ToString();

		ImpSpell1Number->Text = ImposedSpells_Table[0].ToString();
		ImpSpell2Number->Text = ImposedSpells_Table[1].ToString();
		ImpSpell3Number->Text = ImposedSpells_Table[2].ToString();

		ImpSpell1Power->SelectedIndex = ImposedSpells_Table[3];
		ImpSpell2Power->SelectedIndex = ImposedSpells_Table[4];
		ImpSpell3Power->SelectedIndex = ImposedSpells_Table[5];

		HP_Regen->Text = RegenerationHitPoints_Table.ToString();
		HP_Regen_Chance->Text = RegenerationChance_Table.ToString();
		ManaDrain->Text = ManaDrain_Table.ToString();
		DeathBlow->Text = DeathBlow_Table.ToString();
		SpellsCostLess->Text = SpellsCostLess_Table.ToString();
		SpellsCostDump->Text = SpellsCostDump_Table.ToString();
		Retaliations->Text = Counterstrike_Table.ToString();
		CastsSpell->Text = Spells_Table.ToString();
		Demonology_Target->Text = Demonology_Table.ToString();

		GuardsBox->Text = DalionsGuards.ToString();
		ReduceTargetDefense_TXT->Text = ReduceTargetDefense_Table.ToString();

		Custom_Flags->SetItemChecked(8,  Fear_Table);
		Custom_Flags->SetItemChecked(9,  Fearless_Table);
		Custom_Flags->SetItemChecked(10, NoWallPenalty_Table);
		Custom_Flags->SetItemChecked(11, Sharpshooters_Table);
		Custom_Flags->SetItemChecked(12, ShootingAdjacent_Table);
		Custom_Flags->SetItemChecked(13, StrikeAndReturn_Table);
		Custom_Flags->SetItemChecked(14, JoustingBonus_Table);
		Custom_Flags->SetItemChecked(15, ImmunToJoustingBonus_Table);
		Custom_Flags->SetItemChecked(16, MagicAura_Table);
		Custom_Flags->SetItemChecked(17, MagicMirror_Table);
		Custom_Flags->SetItemChecked(18, MagicChannel_Table);

		Custom_Flags->SetItemChecked(19, isFaerieDragon_Table);
		Custom_Flags->SetItemChecked(20, isPassive_Table);
		Custom_Flags->SetItemChecked(21, ThreeHeadedAttack_Table);
		Custom_Flags->SetItemChecked(22, isDragonSlayer_Table);

		Custom_Flags->SetItemChecked(23, PreventiveCounterstrikeTable);
		Custom_Flags->SetItemChecked(24, RangeRetaliation_Table);
		Custom_Flags->SetItemChecked(25, isAmmoCart_Table);
		Custom_Flags->SetItemChecked(26, isConstruct_Table);
		Custom_Flags->SetItemChecked(27, isAimedCaster_Table);

		Custom_Flags->SetItemChecked(28, isTeleporter);
		Custom_Flags->SetItemChecked(29, MovesTwice_Table);
		Custom_Flags->SetItemChecked(30, Receptive_Table);
		Custom_Flags->SetItemChecked(31, isHellHydra);		
		Custom_Flags->SetItemChecked(32, isLord_Table);

		Custom_Flags_2->SetItemChecked(0, NoGolemOverflow);
		Custom_Flags_2->SetItemChecked(1, isDragonResistant);
		Custom_Flags_2->SetItemChecked(2, isHeroic);
		Custom_Flags_2->SetItemChecked(3, counterstrike_fury);
		Custom_Flags_2->SetItemChecked(4, counterstrike_twice);
		Custom_Flags_2->SetItemChecked(5, OverrideSpecialSpells);
	}

	private: System::Void button_load_Click(System::Object^ sender, System::EventArgs^ e) {
		auto result = openFileDialog1->ShowDialog();

		if (result== System::Windows::Forms::DialogResult::OK) {
			//auto file = this->openFileDialog1->OpenFile();
			auto filename = this->openFileDialog1->FileName;

			IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(filename);
			//MessageBox::Show(filename);

			char* buf, * c;//, fname[256] ;
			FILE* fdesc;

			char* fname = (char*) ptrToNativeString.ToPointer();

			//fname = filename.;
			
			if (fdesc = fopen(fname, "r"))
			{
				//----------
				fseek(fdesc, 0, SEEK_END);
				int fdesc_size = ftell(fdesc);
				rewind(fdesc);
				//----------
				buf = (char*)malloc(fdesc_size + 1);
				fread(buf, 1, fdesc_size, fdesc);
				buf[fdesc_size] = 0;
				fclose(fdesc);
				//begin of majaczek

				ChangeCreatureValues(buf);


				free(buf);

				fclose(fdesc);

				saveFileDialog1->FileName = filename;
				Filename_Label->Text = filename;
			}
			
			//ChangeCreatureValues();
			/*
			for (int i = 0; i < 32; i++)
			{
				this->checkedListBox_monster_flags->SetItemChecked(i, (new_monsters.flags & (1 << i)) !=0);
			}
			this->label_monster_flags->Text = new_monsters.flags.ToString();
			*/
			load_GFX_interface();
		}
	}
	private: System::Void Default_Button_Click(System::Object^ sender, System::EventArgs^ e) {
		DefaultCreatureValues();
		load_GFX_interface();
		Filename_Label->Text = "";
	}
	private: System::Void Custom_Flags_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void Custom_Flags_MouseUp(System::Object^ sender, System::Windows::Forms::MouseEventArgs^ e) {
		hasSantaGuards = Custom_Flags->GetItemChecked(0);
		isRogue = Custom_Flags->GetItemChecked(1);
		isGhost = Custom_Flags->GetItemChecked(2);
		isEnchanter= Custom_Flags->GetItemChecked(3);
		isSorceress = Custom_Flags->GetItemChecked(4);
		//isHellSteed = Custom_Flags->GetItemChecked(5);
		//isHellSteed2 = Custom_Flags->GetItemChecked(6);
		//isHellSteed3 = Custom_Flags->GetItemChecked(7);

		FireWall_Table				= Custom_Flags->GetItemChecked(5);
		AlwaysPositiveMorale_Table	= Custom_Flags->GetItemChecked(6);
		AlwaysPositiveLuck_Table	= Custom_Flags->GetItemChecked(7);
		
		////
		Fear_Table					= Custom_Flags->GetItemChecked(8);
		Fearless_Table				= Custom_Flags->GetItemChecked(9);
		NoWallPenalty_Table			= Custom_Flags->GetItemChecked(10);
		Sharpshooters_Table			= Custom_Flags->GetItemChecked(11);
		ShootingAdjacent_Table		= Custom_Flags->GetItemChecked(12);
		StrikeAndReturn_Table		= Custom_Flags->GetItemChecked(13);
		JoustingBonus_Table			= Custom_Flags->GetItemChecked(14);
		ImmunToJoustingBonus_Table	= Custom_Flags->GetItemChecked(15);
		MagicAura_Table				= Custom_Flags->GetItemChecked(16);
		MagicMirror_Table			= Custom_Flags->GetItemChecked(17);
		MagicChannel_Table			= Custom_Flags->GetItemChecked(18);
		//
		isFaerieDragon_Table			= Custom_Flags->GetItemChecked(19);
		isPassive_Table 				= Custom_Flags->GetItemChecked(20);
		ThreeHeadedAttack_Table			= Custom_Flags->GetItemChecked(21);
		isDragonSlayer_Table			= Custom_Flags->GetItemChecked(22);
		PreventiveCounterstrikeTable	= Custom_Flags->GetItemChecked(23);
		RangeRetaliation_Table			= Custom_Flags->GetItemChecked(24);
		isAmmoCart_Table				= Custom_Flags->GetItemChecked(25);
		isConstruct_Table				= Custom_Flags->GetItemChecked(26);
		isAimedCaster_Table				= Custom_Flags->GetItemChecked(27);

		isTeleporter					= Custom_Flags->GetItemChecked(28);
		MovesTwice_Table				= Custom_Flags->GetItemChecked(29);
		Receptive_Table					= Custom_Flags->GetItemChecked(30);
		isHellHydra						= Custom_Flags->GetItemChecked(31);
		isLord_Table					= Custom_Flags->GetItemChecked(32);

		// Fear_Table = Custom_Flags->GetItemChecked(8);
	}
	private: System::Void Custom_Flags_KeyUp(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		hasSantaGuards = Custom_Flags->GetItemChecked(0);
		isRogue = Custom_Flags->GetItemChecked(1);
		isGhost = Custom_Flags->GetItemChecked(2);
		isEnchanter = Custom_Flags->GetItemChecked(3);
		isSorceress = Custom_Flags->GetItemChecked(4);
		//isHellSteed = Custom_Flags->GetItemChecked(5);
		//isHellSteed2 = Custom_Flags->GetItemChecked(6);
		//isHellSteed3 = Custom_Flags->GetItemChecked(7);
		FireWall_Table = Custom_Flags->GetItemChecked(5);
		AlwaysPositiveMorale_Table = Custom_Flags->GetItemChecked(6);
		AlwaysPositiveLuck_Table = Custom_Flags->GetItemChecked(7);
		////
		Fear_Table = Custom_Flags->GetItemChecked(8);
		Fearless_Table = Custom_Flags->GetItemChecked(9);
		NoWallPenalty_Table = Custom_Flags->GetItemChecked(10);
		Sharpshooters_Table = Custom_Flags->GetItemChecked(11);
		ShootingAdjacent_Table = Custom_Flags->GetItemChecked(12);
		StrikeAndReturn_Table = Custom_Flags->GetItemChecked(13);
		JoustingBonus_Table = Custom_Flags->GetItemChecked(14);
		ImmunToJoustingBonus_Table = Custom_Flags->GetItemChecked(15);
		MagicAura_Table = Custom_Flags->GetItemChecked(16);
		MagicMirror_Table = Custom_Flags->GetItemChecked(17);
		MagicChannel_Table = Custom_Flags->GetItemChecked(18);
		//
		isFaerieDragon_Table			= Custom_Flags->GetItemChecked(19);
		isPassive_Table					= Custom_Flags->GetItemChecked(20);
		ThreeHeadedAttack_Table			= Custom_Flags->GetItemChecked(21);
		isDragonSlayer_Table			= Custom_Flags->GetItemChecked(22);
		PreventiveCounterstrikeTable	= Custom_Flags->GetItemChecked(23);
		RangeRetaliation_Table			= Custom_Flags->GetItemChecked(24);
		isAmmoCart_Table				= Custom_Flags->GetItemChecked(25);
		isConstruct_Table				= Custom_Flags->GetItemChecked(26);
		isAimedCaster_Table				= Custom_Flags->GetItemChecked(27);

		isTeleporter = Custom_Flags->GetItemChecked(28);
		MovesTwice_Table = Custom_Flags->GetItemChecked(29);
		Receptive_Table = Custom_Flags->GetItemChecked(30);
		isHellHydra		= Custom_Flags->GetItemChecked(31);
		isLord_Table	= Custom_Flags->GetItemChecked(32);

		// Fear_Table = Custom_Flags->GetItemChecked(8);
	}
	private: System::Void CSP_MUL_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			CreatureSpellPowerMultiplier = Convert::ToInt32(CSP_MUL->Text);
			CSP_MUL->Text = CreatureSpellPowerMultiplier.ToString();
		}
		catch (System::FormatException ^something) {
			CreatureSpellPowerMultiplier = 1;
			CSP_MUL->Text = CreatureSpellPowerMultiplier.ToString();
		}
	}
	private: System::Void CSP_DIV_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			CreatureSpellPowerDivider = Convert::ToInt32(CSP_DIV->Text);
			if (CreatureSpellPowerDivider == 0) 
				CreatureSpellPowerDivider = 1;
			CSP_DIV->Text = CreatureSpellPowerDivider.ToString();
		}
		catch (System::FormatException^ something) {
			CreatureSpellPowerDivider = 1;
			CSP_DIV->Text = CreatureSpellPowerDivider.ToString();
		}
	}
	private: System::Void CSP_ADD_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			CreatureSpellPowerAdder = Convert::ToInt32(CSP_ADD->Text);
			CSP_ADD->Text = CreatureSpellPowerAdder.ToString();
		}
		catch (System::FormatException^ something) {
			CreatureSpellPowerAdder = 0;
			CSP_ADD->Text = CreatureSpellPowerAdder.ToString();
		}
	}
	private: System::Void label5_Click(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void MyForm_Load(System::Object^ sender, System::EventArgs^ e) {
		DefaultCreatureValues();
		load_GFX_interface();
	}
	private: System::Void SP_EFF_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		aftercast_abilities_table = SP_EFF->SelectedIndex;
	}
	private: System::Void ATT_EFF_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		attack_abilities_table = ATT_EFF->SelectedIndex;
	}
	private: System::Void RESIST_EFF_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		magic_resistance_table = RESIST_EFF->SelectedIndex;
	}
	private: System::Void VULN_EFF_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		magic_vulnerability_table = VULN_EFF->SelectedIndex;
	}
	private: System::Void Town__SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		if (Town_->SelectedIndex > 8) new_monsters.town = -1;
		else new_monsters.town = Town_->SelectedIndex;
	}
	private: System::Void Spell2_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		spell_2_table = Spell2->SelectedIndex;
	}

	private: System::Void Spell3_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		spell_3_table= Spell3->SelectedIndex;
	}

	private: System::Void spell1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		spell_1_table = spell1->SelectedIndex;
	}
	private: System::Void LevelBox_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		new_monsters.level = LevelBox->SelectedIndex;
	}

	private: System::Void FireShieldBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		//FireShieldBox->Text->Replace(",",".");
		try {
			fire_shield_table = Convert::ToSingle(FireShieldBox->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US")) ;
			//FireShieldBox->Text = fire_shield_table.ToString();
		}
		catch (System::FormatException^ something) {
			fire_shield_table = 0;
			FireShieldBox->Text = "0.00";// fire_shield_table.ToString();
		}

	}
		   /*
	private: System::Void ressurectionBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			respawn_table = Convert::ToSingle(ressurectionBox->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
			//ressurectionBox->Text = respawn_table.ToString();
		}
		catch (System::FormatException^ something) {
			respawn_table = 0;
			ressurectionBox->Text = "0.00";// respawn_table.ToString();
		}
	}
		   */
	private: System::Void SkeleBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			skeltrans = Convert::ToInt32(SkeleBox->Text);
			SkeleBox->Text = skeltrans.ToString();
		}
		catch (System::FormatException^ something) {
			skeltrans = 56;
			SkeleBox->Text = skeltrans.ToString();
		}

	}
	private: System::Void button_save_Click(System::Object^ sender, System::EventArgs^ e) {
		auto result = saveFileDialog1->ShowDialog();

		if (result == System::Windows::Forms::DialogResult::OK) {
			//auto file = this->openFileDialog1->OpenFile();
			auto filename = this->saveFileDialog1->FileName;
			Filename_Label->Text = filename;

			IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(filename);
			//MessageBox::Show(filename);

			//char* buf, * c;//, fname[256] ;
			//FILE* fdesc;

			char* fname = (char*)ptrToNativeString.ToPointer();

			if (writeCreatureValues(fname)) MessageBox::Show( "File saved successfully." );
			else							MessageBox::Show("Error Saving File!");


		}

	}
	private: System::Void HP_Regen_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			RegenerationHitPoints_Table = Convert::ToInt32(HP_Regen->Text);
			HP_Regen->Text = RegenerationHitPoints_Table.ToString();
		}
		catch (System::FormatException^ something) {
			RegenerationHitPoints_Table = 0;
			HP_Regen->Text = RegenerationHitPoints_Table.ToString();
		}
	}

	private: System::Void HP_Regen_Chance_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			RegenerationChance_Table = Convert::ToInt32(HP_Regen_Chance->Text);
			HP_Regen_Chance->Text = RegenerationChance_Table.ToString();
		}
		catch (System::FormatException^ something) {
			RegenerationChance_Table = 0;
			HP_Regen_Chance->Text = RegenerationChance_Table.ToString();
		}
	}

	private: System::Void ManaDrain_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			ManaDrain_Table = Convert::ToInt32(ManaDrain->Text);
			ManaDrain->Text = ManaDrain_Table.ToString();
		}
		catch (System::FormatException^ something) {
			ManaDrain_Table = 0;
			ManaDrain->Text = ManaDrain_Table.ToString();
		}
	}
	private: System::Void DeathBlow_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			DeathBlow_Table = Convert::ToInt32(DeathBlow->Text);
			DeathBlow->Text = DeathBlow_Table.ToString();
		}
		catch (System::FormatException^ something) {
			DeathBlow_Table = 0;
			DeathBlow->Text = DeathBlow_Table.ToString();
		}

	}
	private: System::Void SpellsCostLess_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			SpellsCostLess_Table = Convert::ToInt32(SpellsCostLess->Text);
			SpellsCostLess->Text = SpellsCostLess_Table.ToString();
		}
		catch (System::FormatException^ something) {
			SpellsCostLess_Table = 0;
			SpellsCostLess->Text = SpellsCostLess_Table.ToString();
		}
	}
	
	private: System::Void SpellsCostDump_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			SpellsCostDump_Table = Convert::ToInt32(SpellsCostDump->Text);
			SpellsCostDump->Text = SpellsCostDump_Table.ToString();
		}
		catch (System::FormatException^ something) {
			SpellsCostDump_Table = 0;
			SpellsCostDump->Text = SpellsCostDump_Table.ToString();
		}
	}
	private: System::Void Retaliations_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			Counterstrike_Table = Convert::ToInt32(Retaliations->Text);
			Retaliations->Text = Counterstrike_Table.ToString();
		}
		catch (System::FormatException^ something) {
			Counterstrike_Table = 1;
			Retaliations->Text = Counterstrike_Table.ToString();
		}
	}

	private: System::Void CastsSpell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			Spells_Table = Convert::ToInt32(CastsSpell->Text);
			CastsSpell->Text = Spells_Table.ToString();
		}
		catch (System::FormatException^ something) {
			Spells_Table = 0;
			CastsSpell->Text = Spells_Table.ToString();
		}
	}
	private: System::Void ImpSpell1Number_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			ImposedSpells_Table[0] = Convert::ToInt32(ImpSpell1Number->Text);
			ImpSpell1Number->Text = ImposedSpells_Table[0].ToString();
		}
		catch (System::FormatException^ something) {
			ImposedSpells_Table[0] = 0;
			ImpSpell1Number->Text = ImposedSpells_Table[0].ToString();
		}
	}
	private: System::Void ImpSpell2Number_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			ImposedSpells_Table[1] = Convert::ToInt32(ImpSpell2Number->Text);
			ImpSpell2Number->Text = ImposedSpells_Table[1].ToString();
		}
		catch (System::FormatException^ something) {
			ImposedSpells_Table[1] = 0;
			ImpSpell2Number->Text = ImposedSpells_Table[1].ToString();
		}
	}
	private: System::Void ImpSpell3Number_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			ImposedSpells_Table[2] = Convert::ToInt32(ImpSpell3Number->Text);
			ImpSpell3Number->Text = ImposedSpells_Table[2].ToString();
		}
		catch (System::FormatException^ something) {
			ImposedSpells_Table[2] = 0;
			ImpSpell3Number->Text = ImposedSpells_Table[2].ToString();
		}
	}

	private: System::Void ImpSpell1Power_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		ImposedSpells_Table[3] = ImpSpell1Power->SelectedIndex;
	}
	private: System::Void ImpSpell2Power_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		ImposedSpells_Table[4] = ImpSpell3Power->SelectedIndex;
	}
	private: System::Void ImpSpell3Power_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		ImposedSpells_Table[5] = ImpSpell3Power->SelectedIndex;
	}
	private: System::Void Demonology_Target_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			Demonology_Table = Convert::ToInt32(Demonology_Target->Text);
			Demonology_Target->Text = Demonology_Table.ToString();
		}
		catch (System::FormatException^ something) {
			Demonology_Table = 0;
			Demonology_Target->Text = Demonology_Table.ToString();
		}
	}
	private: System::Void UpgTo_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		try {
			upgtable = Convert::ToInt32(UpgTo->Text);
			UpgTo->Text = upgtable.ToString();
		}
		catch (System::FormatException^ something) {
			upgtable = -1;
			UpgTo->Text = upgtable.ToString();
		}
	}
private: System::Void Missle_Type_box_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	special_missiles_table = Missle_Type_box->SelectedIndex;
}
private: System::Void label23_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void MimicArtifact_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		CreatureMimicArtifact = Convert::ToInt32(MimicArtifact->Text);
		MimicArtifact->Text = CreatureMimicArtifact.ToString();
	}
	catch (System::FormatException ^ something) {
		CreatureMimicArtifact = 0;
		MimicArtifact->Text = CreatureMimicArtifact.ToString();
	}
}
private: System::Void label19_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void MimicArtifact2_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		CreatureMimicArtifact2 = Convert::ToInt32(MimicArtifact2->Text);
		MimicArtifact2->Text = CreatureMimicArtifact2.ToString();
	}
	catch (System::FormatException ^ something) {
		CreatureMimicArtifact2 = 0;
		MimicArtifact2->Text = CreatureMimicArtifact2.ToString();
	}
}

private: System::Void z_res_type_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	resource_type_table = z_res_type->SelectedIndex;
}
private: System::Void z_res_amount_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		resource_amount_table = Convert::ToInt32(z_res_amount->Text);
		z_res_amount->Text = resource_amount_table.ToString();
	}
	catch (System::FormatException ^ something) {
		resource_amount_table = 0;
		z_res_amount->Text = resource_amount_table.ToString();
	}
}
private: System::Void TaxBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		resource_tax_table = Convert::ToInt32(TaxBox->Text);
		TaxBox->Text = resource_tax_table.ToString();
	}
	catch (System::FormatException ^ something) {
		resource_tax_table = 0;
		TaxBox->Text = resource_tax_table.ToString();
	}
}
private: System::Void GuardsBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		DalionsGuards = Convert::ToInt32(GuardsBox->Text);
		GuardsBox->Text = DalionsGuards.ToString();
	}
	catch (System::FormatException^ something) {
		DalionsGuards = -1;
		GuardsBox->Text = DalionsGuards.ToString();
	}
}
private: System::Void ReduceTargetDefense_TXT_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		ReduceTargetDefense_Table = Convert::ToInt32(ReduceTargetDefense_TXT->Text);
		ReduceTargetDefense_TXT->Text = ReduceTargetDefense_Table.ToString();
	}
	catch (System::FormatException^ something) {
		ReduceTargetDefense_Table = 0;
		ReduceTargetDefense_TXT->Text = ReduceTargetDefense_Table.ToString();
	}
}
private: System::Void checkBox_ForcedSave_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	forced = checkBox_ForcedSave->Checked;
}



private: System::Void AfterMeleeSpell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_melee__spell = Convert::ToInt32(AfterMeleeSpell->Text);
		AfterMeleeSpell->Text = after_melee__spell.ToString();
	}
	catch (System::FormatException^ something) {
		after_melee__spell = 0;
		AfterMeleeSpell->Text = after_melee__spell.ToString();
	}
}

private: System::Void AfterShootSpell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_shoot__spell = Convert::ToInt32(AfterShootSpell->Text);
		AfterShootSpell->Text = after_shoot__spell.ToString();
	}
	catch (System::FormatException^ something) {
		after_shoot__spell = 0;
		AfterShootSpell->Text = after_shoot__spell.ToString();
	}
}

private: System::Void AfterDefendSpell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_defend_spell = Convert::ToInt32(AfterDefendSpell->Text);
		AfterDefendSpell->Text = after_defend_spell.ToString();
	}
	catch (System::FormatException^ something) {
		after_defend_spell = 0;
		AfterDefendSpell->Text = after_defend_spell.ToString();
	}
}

private: System::Void AfterWoundSpell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_wound__spell = Convert::ToInt32(AfterWoundSpell->Text);
		AfterWoundSpell->Text = after_wound__spell.ToString();
	}
	catch (System::FormatException^ something) {
		after_wound__spell = 0;
		AfterWoundSpell->Text = after_wound__spell.ToString();
	}
}




private: System::Void AfterMeleeSpell2_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_melee__spell2 = Convert::ToInt32(AfterMeleeSpell2->Text);
		AfterMeleeSpell2->Text = after_melee__spell2.ToString();
	}
	catch (System::FormatException^ something) {
		after_melee__spell2 = 0;
		AfterMeleeSpell2->Text = after_melee__spell2.ToString();
	}
}

private: System::Void AfterShootSpell2_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_shoot__spell2 = Convert::ToInt32(AfterShootSpell2->Text);
		AfterShootSpell2->Text = after_shoot__spell2.ToString();
	}
	catch (System::FormatException^ something) {
		after_shoot__spell2 = 0;
		AfterShootSpell2->Text = after_shoot__spell2.ToString();
	}
}


private: System::Void AfterDefendSpell2_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_defend_spell2 = Convert::ToInt32(AfterDefendSpell2->Text);
		AfterDefendSpell2->Text = after_defend_spell2.ToString();
	}
	catch (System::FormatException^ something) {
		after_defend_spell2 = 0;
		AfterDefendSpell2->Text = after_defend_spell2.ToString();
	}
}

private: System::Void AfterWoundSpell2_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		after_wound__spell2 = Convert::ToInt32(AfterWoundSpell2->Text);
		AfterWoundSpell2->Text = after_wound__spell2.ToString();
	}
	catch (System::FormatException^ something) {
		after_wound__spell2 = 0;
		AfterWoundSpell2->Text = after_wound__spell2.ToString();
	}
}

private: System::Void ShootingResistance_TextChanged(System::Object^ sender, System::EventArgs^ e) {

	try {
		shooting_resistance = Convert::ToSingle(ShootingResistance->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//ShootingResistance->Text = fire_shield_table.ToString();
	}
	catch (System::FormatException^ something) {
		shooting_resistance = 0;
		ShootingResistance->Text = "0.00";// fire_shield_table.ToString();
	}

}


private: System::Void MeleeResistance_TextChanged(System::Object^ sender, System::EventArgs^ e) {

	try {
		melee_resistance = Convert::ToSingle(MeleeResistance->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//ShootingResistance->Text = fire_shield_table.ToString();
	}
	catch (System::FormatException^ something) {
		melee_resistance = 0;
		MeleeResistance->Text = "0.00";// fire_shield_table.ToString();
	}
}



private: System::Void SpellAfterActionMastery_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	after_action_spell_mastery = SpellAfterActionMastery->SelectedIndex;
}








private: System::Void DoNotGenerate_CheckedChanged(System::Object^ sender, System::EventArgs^ e) {
	do_not_generate = DoNotGenerate->Checked;
}


private: System::Void rebirth_fraction_box_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		respawn_table_fraction = Convert::ToSingle(rebirth_fraction_box->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//rebirth_fraction_box->Text = respawn_table_fraction.ToString();
	}
	catch (System::FormatException^ something) {
		respawn_table_fraction = 0;
		rebirth_fraction_box->Text = "0.00";// respawn_table_fraction.ToString();
	}
}
private: System::Void rebirth_chance_box_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		respawn_table_chance = Convert::ToSingle(rebirth_chance_box->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//rebirth_chance_box->Text = respawn_table_chance.ToString();
	}
	catch (System::FormatException^ something) {
		respawn_table_chance = 0;
		rebirth_chance_box->Text = "0.00";// respawn_table_chance.ToString();
	}
}
private: System::Void rebirth_sure_box_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		respawn_table_sure = Convert::ToSingle(rebirth_sure_box->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//rebirth_sure_box->Text = respawn_table_sure.ToString();
	}
	catch (System::FormatException^ something) {
		respawn_table_sure = 0;
		rebirth_sure_box->Text = "0.00";// rebirth_sure_box.ToString();
	}
}
private: System::Void ManaRegenBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		mana_regen_table = Convert::ToInt32(ManaRegenBox->Text);
		ManaRegenBox->Text = mana_regen_table.ToString();
	}
	catch (System::FormatException^ something) {
		mana_regen_table = 0;
		ManaRegenBox->Text = mana_regen_table.ToString();
	}
}
private: System::Void AgingBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		aging_table = Convert::ToInt32(AgingBox->Text);
		AgingBox->Text = aging_table.ToString();
	}
	catch (System::FormatException^ something) {
		aging_table = 0;
		AgingBox->Text = aging_table.ToString();
	}
}
private: System::Void PoisonBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		poison_table = Convert::ToInt32(PoisonBox->Text);
		PoisonBox->Text = poison_table.ToString();
	}
	catch (System::FormatException^ something) {
		poison_table = 0;
		PoisonBox->Text = poison_table.ToString();
	}
}
private: System::Void ParalyzeBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		paralyze_table = Convert::ToInt32(ParalyzeBox->Text);
		ParalyzeBox->Text = paralyze_table.ToString();
	}
	catch (System::FormatException^ something) {
		paralyze_table = 0;
		ParalyzeBox->Text = paralyze_table.ToString();
	}
}
private: System::Void ParalyzeChanceBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		paralyze_chance = Convert::ToInt32(ParalyzeChanceBox->Text);
		ParalyzeChanceBox->Text = paralyze_chance.ToString();
	}
	catch (System::FormatException^ something) {
		paralyze_chance = 0;
		ParalyzeChanceBox->Text = paralyze_chance.ToString();
	}
}
private: System::Void label43_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void GhostMultiBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		ghost_fraction = Convert::ToSingle(GhostFractionBox->Text->Replace(',', '.'), gcnew System::Globalization::CultureInfo("en-US"));
		//GhostFractionBox->Text = ghost_fraction.ToString();
	}
	catch (System::FormatException^ something) {
		ghost_fraction = 1.00;
		GhostFractionBox->Text = "1.00";// ghost_fraction.ToString();
	}
}
private: System::Void Custom_Flags_2_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	NoGolemOverflow = Custom_Flags_2->GetItemChecked(0);
	isDragonResistant = Custom_Flags_2->GetItemChecked(1);
	isHeroic = Custom_Flags_2->GetItemChecked(2);
	counterstrike_fury = Custom_Flags_2->GetItemChecked(3);
	counterstrike_twice = Custom_Flags_2->GetItemChecked(4);
	OverrideSpecialSpells = Custom_Flags_2->GetItemChecked(5);

}
private: System::Void Custom_Flags_2_KeyUp(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
	NoGolemOverflow = Custom_Flags_2->GetItemChecked(0);
	isDragonResistant = Custom_Flags_2->GetItemChecked(1);
	isHeroic = Custom_Flags_2->GetItemChecked(2);
	counterstrike_fury = Custom_Flags_2->GetItemChecked(3);
	counterstrike_twice = Custom_Flags_2->GetItemChecked(4);
	OverrideSpecialSpells = Custom_Flags_2->GetItemChecked(5);

}
private: System::Void Wog_Spell_immunites_box_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	memset(Wog_Spell_Immunites, 0, 16);
	auto SS = Wog_Spell_immunites_box->Text->ToString();
	
	IntPtr p = Marshal::StringToHGlobalAnsi(SS);
	char* pNewCharStr = static_cast<char*>(p.ToPointer());

	strcpy_s(Wog_Spell_Immunites, pNewCharStr);
}

private: System::Void Missiles_Size_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		missile_size_table = Convert::ToInt32(MissileSizeBox->Text);
		MissileSizeBox->Text = missile_size_table.ToString();
	}
	catch (System::FormatException^ something) {
		missile_size_table = 0;
		MissileSizeBox->Text = missile_size_table.ToString();
	}
}

private: System::Void Missiles_Anim_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		missile_anim_table = Convert::ToInt32(MissileAnimBox->Text);
		MissileAnimBox->Text = missile_anim_table.ToString();
	}
	catch (System::FormatException^ something) {
		missile_anim_table = 0;
		MissileAnimBox->Text = missile_anim_table.ToString();
	}
}

private: System::Void AroundRangeBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		strike_all_around_range = Convert::ToInt32(AroundRangeBox->Text);
		AroundRangeBox->Text = strike_all_around_range.ToString();
	}
	catch (System::FormatException^ something) {
		strike_all_around_range = 0;
		AroundRangeBox->Text = strike_all_around_range.ToString();
	}
}

private: System::Void TrailBox_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	StackStep_field_type = TrailBox->SelectedIndex;
}
private: System::Void ACAST_CUSTOM_spell_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		Creature_ACAST_CUSTOM_Spell = Convert::ToInt32(ACAST_CUSTOM_spell->Text);
		ACAST_CUSTOM_spell->Text = Creature_ACAST_CUSTOM_Spell.ToString();
	}
	catch (System::FormatException^ something) {
		Creature_ACAST_CUSTOM_Spell = 60;
		ACAST_CUSTOM_spell->Text = Creature_ACAST_CUSTOM_Spell.ToString();
	}
}
private: System::Void ACAST_CUSTOM_chance_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	try {
		Creature_ACAST_CUSTOM_Chance = Convert::ToInt32(ACAST_CUSTOM_chance->Text);
		ACAST_CUSTOM_chance->Text = Creature_ACAST_CUSTOM_Chance.ToString();
	}
	catch (System::FormatException^ something) {
		Creature_ACAST_CUSTOM_Chance = 60;
		ACAST_CUSTOM_chance->Text = Creature_ACAST_CUSTOM_Spell.ToString();
	}
}
};
}
