struct _NPC_{
 public:
  int  Used;
  int  Dead;
  int  Number;
  int  Type,HType;
  int  LastExpoInBattle;
  struct{
    unsigned  CustomPrimary : 1; // Ďîëüçîâŕňĺëü ńŕě ęîíňđîëčđóĺň ďŕđŕěĺňđű
//    unsigned  NoHeroOwner   : 1; // íĺň őîç˙číŕ ăĺđî˙ (3.58)
    unsigned _reserved      :31;
  } Fl;
  int  Primary[7]/*At,Df,Hp,Dm,Mp,Sp,Mr*/;
//  struct{
//    Byte  :1;
//  } Flags;
  int  Skills[7];
  short Arts[10][8];
  char Name[32];
  int  OldHeroExp;
  int  Exp;   // ňĺę. îďűň
  int  Level; // ňĺę. óđîâĺíü
  INT32  SpecBon[2];
  int  GetAvailableSpecBon(int);
  // static int   Spec2Ind[15][2];
  // static int   Bonus[7][5];
  // static int   SpecBonus[7][5];
  // static char *BonusBmp[6][6];
  // static char *BonusArtBmp[6];
  // static char *BonusBmpA[6][6];
  // static char *SpecBonusBmp[6][6][4];
  // static char *SpecBonusText[6][6][2];
  // static char *SpecBonusPopUpText[6][6];
  // int         FindSpecBonusBmp(int i,int j);
  // static char *NextBonusBmp[6][2];
  // static char *BonusText[6][6];
  // static char *BonusPopUpText[6][6];
  // static char *SpecBmp[9][2];
  // static char *SpecText[9][2];
  // static char *SpecHint[9][2];
  // int CalcSkill(int);
  // int CalculateSkills(int);
  // int MayNextSkill(int);
  // static char *SkNames[7];
  // static char *Descr[9];
  // static char *Magics[9];
  // static _ZPrintf_ Hint[2];
  // static _ZPrintf_ Buffer,Buffer1;
  // static int Levels[100];
  // static int AISkillsChance[18][6];
  // void SetOldHeroExp(int Exp){ OldHeroExp=Exp; }
  // int  GetExp(void){ return Exp; }
  // void AddExp(int,int); // äîáŕâčňü îďűňŕ
  // int  GetNextLevel(void); // ďîëó÷čňü ńëĺä îćčä. óđîâĺíü
  // NPC();
  // void Init(void);
  // void ToText(int,int=0,int=0);
  // char *ToHint(int);
  // static char *ArtPicNames[10];
  // static char  ArtTextBuffer[10][512];
  // static int  ArtIsPossible(int ArtNum);
  // int   ArtMayHave(int ArtNum);
  // int   ArtGetFreeSlot(int ArtNum);
  // int   ArtDoesHave(int ArtNum);
  // int   ArtCalcSkill(int Slot,int Skill);
  // int   ArtInSlot(int ArtNum);
  // char *PrepareArtText(int Slot);
};


struct _DlgNPC_{
int Flags;     // ôëŕăč - ńě. íčćĺ
/*
  Flags & 0x00000001 - ; ÍĎŃ ćčâîé, číŕ÷ĺ ěĺđňâűé
                         Ĺńëč îí ěĺđňâűé, ňî ęíîďęŕ óâîëčňü íŕďđĺůĺíŕ č íŕäďčńü "ĚĹĐŇÂ"
                         íŕďčńŕíŕ.
  Flags & 0x00000002 - ; ěîćíî đĺäŕęňčđîâŕňü čě˙ ÍĎŃ
                         îńňŕëüíîĺ ďîçćĺ îáńóäčě
  Flags & 0x00000004 - ; íĺëüç˙ ďĺđĺäŕâŕňü ŕđňčôŕęňű îň/ę ăĺđîţ.
                         îńňŕëüíîĺ ďîçćĺ îáńóäčě
  Flags & 0x00000008 - ; ďîęŕçŕň ęíîďęó óâîëüíĺíč˙

  Ôóíęöč˙ âîçâđŕůŕĺň íîěĺđ âűáđŕííîé ęŕđňčíęč (0...6), ĺńëč Request=1
č ÷ňî óăîäíî (íŕďđčěĺđ,0), ĺńëč Request=0
*/
int DlgLeft;    // Ęîđîäčíŕňű îęíŕ ďî ăîđčçîíňŕëč
int DlgTop;     // Ęîîđäčíŕňű îęíŕ ďî âĺđňĺęŕëč
char *Name;     // Čě˙ NPC. Ýňî áóôĺđ íŕ 32 ńčěâîëŕ. Ďîńëĺäíčé 0, ňŕę
                // ÷ňî ďđč đĺäŕęňčđîâŕíčč čě˙ ěîćĺň áűňü íĺ äëčííĺĺ 31.
                // óęŕçâňĺëü ÍĹ ĚĹÍßŇÜ - čçěĺíĺíčĺ ďđ˙ěî ęîďčđîâŕňü ďî
                // óęŕçŕňĺëţ â ěîé áóôĺđ
char *Portrait; // Ďîđňđĺň NPC
char *PortraitHint; // ňĺęńň őčíňŕ äë˙ ďîđňđĺňŕ
char *HeroName; // čě˙ ăĺđî˙-őîç˙číŕ
int   Level;    // óđîâĺíü ÍĎŃ
int   pAT,hAT;  // ŕňŕęŕ č ŕňŕęŕ ń ó÷ĺňîě ŕňŕęč ăĺđî˙
                // (âűâîäčňń˙ ńëĺäîě â ńęîáęŕő)
int   pDF,hDF;  // çŕůčňŕ č ń ó÷ĺňîě ăĺđî˙
int   pHP;      // çäîđîâüĺ
int   pSP;      // ńęîđîńňü
int   pDML,pDMH;// óđîí íčćíčé č âĺđőíčé ÷ĺđĺç ÷ĺđňî÷ęó
int   pMP;      // ěŕăč÷ĺńęŕ˙ ńčëŕ
int   pMR;      //ěŕăč÷ĺńęŕ˙ çŕůčňŕ ń ńčěâîëîě % íŕ ęîíöĺ
int   pShots;   // ęîëč÷ĺńňâî âűńňđĺëîâ. ĺńëč 0, ňî ýňî ďóńňŕ˙ ńňđîęŕ
char *Description; // îďčńŕíčĺ
char *SpecIcon1; // čęîíęŕ çŕęëčíŕíč˙
char *SpecText1; // ňĺęńň đ˙äîě (â äâĺ čëč ňđč ęîđîňęčő ńňđîęč)
char *SpecHint1; // őčíň äë˙ ďĺđâîé čęîíęč č äë˙ ňĺęńňŕ đ˙äîě
char *SpecPopUpText1; // ŇĹĘŃŇ ÄËß ĎÎĎŔĎ ÎĘÍŔ
char *SpecIcon2; // čęîíęŕ äđóăîăî áîíóńŕ
char *SpecText2; // ňĺęńň đ˙äîě (â äâĺ čëč ňđč ęîđîňęčő ńňđîęč)
char *SpecHint2; // őčíň äë˙ âňîđîé čęîíęč č äë˙ ňĺęńňŕ đ˙äîě
char *SpecPopUpText2; // ŇĹĘŃŇ ÄËß ĎÎĎŔĎ ÎĘÍŔ
int   CurExp;    // ňĺęóůčé îďűň
int   NextExp;   // ńëĺä. îďűň
char *Type;      // ňčď NPC
char *TypePopUpText; // ŇĹĘŃŇ ÄËß ĎÎĎŔĎ ÎĘÍŔ
char *TypeHint; // ŇĹĘŃŇ ÄËß ŐČÍŇŔ
char *Bonus[6]; // 6 ďóňĺé ę ďĺđâűě 6 ęŕđňčíęŕě 70 íŕ 70
char *BonusHints[6]; // 6 őčíňîâ äë˙ ďĺđâűő 6 ęŕđňčíîę
char *BonusPopUpText[6]; // 6 ňĺęńňîâ äë˙ ďĺđâűő 6 ęŕđňčíîę
char *SpecBonus[6]; // 6 ďóňĺé ęî âňîđűě 6 ęŕđňčíęŕě ęŕę ńĺé÷ŕń
char *SpecBonusHints[6]; // 6 őčíňîâ äë˙ âňîđűő 6 ęŕđňčíîę
char *SpecBonusPopUpText[6]; // 6 ňĺęńňîâ äë˙ âňîđűő 6 ęŕđňčíîę
//char *Information;      // Číôîđěŕöč˙
int   Request;   // ĺńëč 1, ňî čăđîę äîëćĺí âűáđŕňü ęŕđňčíęó âíčçó
char *Next[6];   // 6 ďóňĺé ę íčćíčě 6 ęŕđňčíęŕě 70 íŕ 70 čç ęîňîđűő
                 // čăđîę äîëćĺí âűáđŕňü îäíó č íŕćŕňü Îę
char *NextActive[6];
char *NextHints[6]; // 6 őčíňîâ ę íčćíčě 6 ęŕđňčíęŕě
char *NextPopUpTexts[6]; // 6 ňĺęńňîâ ę íčćíčě 6 ęŕđňčíęŕě
char *ArtIcons[6]; // čęîíęŕ ŕđňčôŕęňŕ ("NONE" - ďóńňîé)
char *ArtHints[6]; // őčíň äë˙ čęîíęč ŕđňčôŕęňŕ
char *ArtPopUpTexts[6]; // ňĺęńň äë˙ ďîďŕď îęíŕ
char ArtOutput[6]; // ŕđňčôŕęň îňäŕĺňń˙ - 1, îńňŕâë˙ĺňń˙ - 0
};
