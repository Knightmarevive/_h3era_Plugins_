#define _CRT_SECURE_NO_WARNINGS
#define SS_variable_types_count 8

#include <windows.h>
#include "Era.h"
#include <stdio.h>

#include "patcher_x86_commented.hpp"
using namespace Era;

#include "heroes.h"

const int ADV_MAP = 37;
const int CTRL_LMB = 4;
const int LMB_PUSH = 12;
bool requires_basic_skill = false;

void popup(char* tittle, int num) {
	char str[512];
	sprintf_s(str, "executed %d time", num);
	MessageBoxA(0, str, tittle, 0);

	int base = *(int*)0x699538;
	sprintf_s(str, "base equals %d", base);
	MessageBoxA(0, str, tittle, 0);
}

Patcher* globalPatcher;
PatcherInstance* SS_Bonus_Agents;
#define PINSTANCE_MAIN "SS_Bonus_Agents"

union SSP_field {
	float f;
	int i;
};

constexpr int SS_Count = 256;// 28;
constexpr int Special_Count = 32;
//typedef SSP_field SSP_Bonus[SS_Count];
struct SSP_Bonus {
	SSP_field SSP_Bonus_Internal[SS_Count];
	SSP_field SSP_Bonus_Variable[SS_variable_types_count][SS_Count];
	SSP_field SSP_Special[Special_Count];
};

enum Source_Type
{
	Emerald = 0,
	Amethyst = 1,
	Heroine = 2,
	Count = 3,
};

char Source_Folder[Count][512] = {
	"Data\\artifacts",
	"Data\\creatures",
	"Data\\heroes",
};

constexpr long max_bonus_sources = 1024;
SSP_Bonus bonuses[Count][max_bonus_sources];

bool proper(long arg) {
	return (arg >= 0 && arg < max_bonus_sources);
}
bool mon_already(HERO* H, int stack, int mon_type) {
	for (int c = 0; c < stack; ++c)
		if (H->Ct[c] == mon_type)
			return true;
	return false;
}

extern "C" __declspec(dllexport) int check_SSI_bonus(HERO * H, long skill) {
	int ret = 0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Bonus_Internal[skill].i;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Bonus_Internal[skill].i;

	ret += bonuses[Heroine][H->Number].SSP_Bonus_Internal[skill].i;

	return ret;
}
extern "C" __declspec(dllexport) float check_SSF_bonus(HERO * H, long skill) {
	float ret = 0.0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Bonus_Internal[skill].f;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Bonus_Internal[skill].f;

	ret += bonuses[Heroine][H->Number].SSP_Bonus_Internal[skill].f;

	return ret;
}


extern "C" __declspec(dllexport) int check_SSI_variable(HERO * H, long skill, int slot) {
	int ret = 0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Bonus_Variable[slot][skill].i;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Bonus_Variable[slot][skill].i;

	ret += bonuses[Heroine][H->Number].SSP_Bonus_Variable[slot][skill].i;

	return ret;
}
extern "C" __declspec(dllexport) float check_SSF_variable(HERO * H, long skill, int slot) {
	float ret = 0.0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Bonus_Variable[slot][skill].f;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Bonus_Variable[slot][skill].f;

	ret += bonuses[Heroine][H->Number].SSP_Bonus_Variable[slot][skill].f;

	return ret;
}

extern "C" __declspec(dllexport) int check_SPCI_bonus(HERO * H, long skill) {
	int ret = 0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Special[skill].i;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Special[skill].i;

	ret += bonuses[Heroine][H->Number].SSP_Special[skill].i;

	return ret;
}
extern "C" __declspec(dllexport) float check_SPCF_bonus(HERO * H, long skill) {
	float ret = 1.0;
	for (int a = 0; a < 19; ++a)
		if (proper(H->IArt[a][0]))
			ret += bonuses[Emerald][H->IArt[a][0]].SSP_Special[skill].f;
	for (int c = 0; c < 7; ++c)
		if (proper(H->Ct[c]) && !mon_already(H, c, H->Ct[c]))
			ret += bonuses[Amethyst][H->Ct[c]].SSP_Special[skill].f;

	ret += bonuses[Heroine][H->Number].SSP_Special[skill].f;

	return ret;
}
extern "C" __declspec(dllexport) void ChangeBonusTable(int target, char* buf, int type) {
	char* c; char pattern[64];
	for (int i = 0; i < SS_Count; ++i) {
		sprintf(pattern, "SSI%03d=", i);
		c = strstr(buf, pattern);
		if (c != NULL) bonuses[type][target].SSP_Bonus_Internal[i].i = atoi(c + strlen(pattern));

		sprintf(pattern, "SSF%03d=", i);
		c = strstr(buf, pattern);
		if (c != NULL) bonuses[type][target].SSP_Bonus_Internal[i].f = atof(c + strlen(pattern));
	}
	for (int i = 0; i < Special_Count; ++i) {
		sprintf(pattern, "SPCI%03d=", i);
		c = strstr(buf, pattern);
		if (c != NULL) bonuses[type][target].SSP_Special[i].i = atoi(c + strlen(pattern));

		sprintf(pattern, "SPCF%03d=", i);
		c = strstr(buf, pattern);
		if (c != NULL) bonuses[type][target].SSP_Special[i].f = atof(c + strlen(pattern));
	}

	for (int z = 0; z < SS_variable_types_count; ++z) {
		for (int i = 0; i < SS_Count; ++i) {
			sprintf(pattern, "SSI%03d_%d=", i, z);
			c = strstr(buf, pattern);
			if (c != NULL) bonuses[type][target].SSP_Bonus_Variable[z][i].i = atoi(c + strlen(pattern));

			sprintf(pattern, "SSF%03d_%d=", i, z);
			c = strstr(buf, pattern);
			if (c != NULL) bonuses[type][target].SSP_Bonus_Variable[z][i].f = atof(c + strlen(pattern));
		}
	}

}

void LoadBonusConfig(int target, int type)
{
	char* buf, * c, fname[256];
	FILE* fdesc;

	/*
	sprintf(fname,"Mods\\Amethyst Upgrades\\Data\\creatures\\%u.cfg",target);
	if(!FileExists(fname)) */
	/*    sprintf(fname,"Mods\\Knightmare Kingdoms\\Data\\creatures\\%u.cfg",target);
	if(!FileExists(fname))*/
	sprintf(fname, "%s\\%u.cfg", Source_Folder[type], target);
	if (fdesc = fopen(fname, "r"))
	{
		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);
		//begin of majaczek

		// ChangeCreatureTable(target, buf);
		ChangeBonusTable(target, buf, type);

		free(buf);
	}
}

/*
BOOL __stdcall Hook_BattleMouseHint (THookContext* Context)
{

}
*/

_LHF_(z_hook_004E3C4F) {
	*(int*)(c->ebp + 0xC) = c->ebx += check_SSI_bonus((HERO*)c->edi, 6);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E3C54;
	else c->return_address = 0x004E3C9E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E39FA) {
	*(int*)(c->ebp + 0xC) = c->edx += check_SSI_bonus((HERO*)c->esi, 9);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E39FF;
	else c->return_address = 0x004E3A46;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_hook_004E41CC) {
	*(int*)(c->ebp - 0x8) = c->ebx += check_SSI_bonus((HERO*)c->edi, 8);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E41D1;
	else c->return_address = 0x004E4219;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E42FC) {
	// c->ebx += check_SSI_bonus((HERO*)c->esi, 3);
	*(int*)(c->ebp - 0x8) = c->ebx += check_SSI_bonus((HERO*)c->esi, 3);
	if (c->AL() > 0 || !requires_basic_skill) c->return_address = 0x004E4301;
	else c->return_address = 0x004E4348;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E4EED) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->ebx, 2);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E4EF2;
	else c->return_address = 0x004E4F2E;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_hook_004E5B30) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->esi, 25);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E5B35;
	else c->return_address = 0x0004E5B71;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E43EB) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->edi, 1);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E43F4;
	else c->return_address = 0x004E450B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E4538) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->ecx, 22);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E453D;
	else c->return_address = 0x004E4579;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_hook_004E4598) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->ecx, 23);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E459D;
	else c->return_address = 0x004E45D9;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_hook_004E4622) {
	*(int*)(c->ebp - 0x8) = c->eax += check_SSI_bonus((HERO*)c->esi, 13);
	if (c->CL() > 0 || !requires_basic_skill) c->return_address = 0x004E4627;
	else c->return_address = 0x004E4667;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E46AB) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->edi, 11);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E46B4;
	else c->return_address = 0x004E47CB;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E480B) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->edi, 4);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E4810;
	else c->return_address = 0x004E484C;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E496B) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->edi, 26);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E4970;
	else c->return_address = 0x004E49AC;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E4AC8) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->ecx, 21);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E4ACD;
	else c->return_address = 0x004E4B09;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E4BA8) {
	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->ecx, 27);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E4BAD;
	else c->return_address = 0x004E4BE9;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_004E3F5D) {

	// necromancy check
	if (c->AL() <= 0) {
		*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) = 0.0;
		c->return_address = 0x004E4130;
		return NO_EXEC_DEFAULT;
	}

	*(float*)(c->ebp - 0x4) = (*(float*)&(c->edx)) += check_SSF_bonus((HERO*)c->edi, 12);
	if ((c->AL() > 0) || !requires_basic_skill) c->return_address = 0x004E3F66;
	else c->return_address = 0x004E4130;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_fix_00451AED) {
	if (c->eax < -3) c->eax = -3;
	if (c->eax > 3) c->eax = 3;
	return EXEC_DEFAULT;
}

_LHF_(z_fix_00451B52) {
	if (c->eax < -3) c->eax = -3;
	if (c->eax > 3) c->eax = 3;
	return EXEC_DEFAULT;
}

_LHF_(z_diplo_004A748C) {
	HERO* hero = (HERO*)c->edi;
	c->ebx += check_SPCI_bonus(hero, 4);
	return EXEC_DEFAULT;
}

_LHF_(z_diplo_004171C4) {
	HERO* hero = (HERO*)c->esi;
	c->edx += check_SPCI_bonus(hero, 4);
	return EXEC_DEFAULT;
}


extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{

		globalPatcher = GetPatcher();
		SS_Bonus_Agents = globalPatcher->CreateInstance(PINSTANCE_MAIN);

		SS_Bonus_Agents->WriteLoHook(0x004171C4, z_diplo_004171C4);
		SS_Bonus_Agents->WriteLoHook(0x004A748C, z_diplo_004A748C);

		SS_Bonus_Agents->WriteLoHook(0x00451AED, z_fix_00451AED);
		SS_Bonus_Agents->WriteLoHook(0x00451B52, z_fix_00451B52);

		SS_Bonus_Agents->WriteLoHook(0x004E42FC, z_hook_004E42FC);
		SS_Bonus_Agents->WriteLoHook(0x004E4EED, z_hook_004E4EED);
		SS_Bonus_Agents->WriteLoHook(0x004E5B30, z_hook_004E5B30);

		SS_Bonus_Agents->WriteLoHook(0x004E43EB, z_hook_004E43EB);
		SS_Bonus_Agents->WriteLoHook(0x004E4538, z_hook_004E4538);

		SS_Bonus_Agents->WriteLoHook(0x004E4598, z_hook_004E4598);
		SS_Bonus_Agents->WriteLoHook(0x004E4622, z_hook_004E4622);
		SS_Bonus_Agents->WriteLoHook(0x004E46AB, z_hook_004E46AB);
		SS_Bonus_Agents->WriteLoHook(0x004E480B, z_hook_004E480B);
		SS_Bonus_Agents->WriteLoHook(0x004E496B, z_hook_004E496B);
		SS_Bonus_Agents->WriteLoHook(0x004E4AC8, z_hook_004E4AC8);

		SS_Bonus_Agents->WriteLoHook(0x004E41CC, z_hook_004E41CC);
		SS_Bonus_Agents->WriteLoHook(0x004E3F5D, z_hook_004E3F5D);

		SS_Bonus_Agents->WriteLoHook(0x004E3C4F, z_hook_004E3C4F);
		SS_Bonus_Agents->WriteLoHook(0x004E39FA, z_hook_004E39FA);

		for (long i = 0; i < Count; ++i)
			for (long j = 0; j < max_bonus_sources; ++j)
				LoadBonusConfig(j, i);
		ConnectEra();

	}
	return TRUE;
};
