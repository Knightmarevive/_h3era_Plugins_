// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "patcher_x86_commented.hpp"


Patcher* _P;
PatcherInstance* _PI;

char backgrounds[3][16] = {
    "campback.pcx",
    "campbkx1.pcx",
    "campbkx2.pcx"
};

_LHF_(hook_0045E992) {
    c->ecx = (int) backgrounds[c->ebx];
    return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    static bool plugin_On = false;

    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        if (!plugin_On)
        {
            plugin_On = true;
            _P = GetPatcher();
            _PI = _P->CreateInstance((char*)"HD.Plugin.Three_Campaign_backgrounds");
            _PI->WriteLoHook(0x0045E992, hook_0045E992);
        }
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

