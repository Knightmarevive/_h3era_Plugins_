// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
// #include "HoMM3.h"
#include "patcher_x86_commented.hpp"

#define PINSTANCE_MAIN "Z_new_advmap_objects"

Patcher* globalPatcher;
PatcherInstance* Z_new_advmap_objects;

#include "era.h"

struct _Army_ {
    int    Ct[7];    
    int    Cn[7];    
};
typedef void _Dlg_;

_Dlg_* (__thiscall *Dlg_SkeletTransformer_Create)(_Dlg_ *me, void *item) = (_Dlg_* (__thiscall*) (_Dlg_ *, void*)) 0x00565770;
int (__thiscall* Dlg_ShowAndRun)(void*dlg, int a2) = (int(__thiscall*)(void* , int )) 0x005FFA20;
int (__thiscall* Dlg_SkeletTransformer_Destroy)(void* a1) = (int (__thiscall*) (void*))  0x005661E0;


char* GetHeroRecord(int number)
{

    int offset_21620 = *(int*)(0x0062C9C3 + 1); // 0x21620 is SoD value
    return (char*)((*(int*)0x699538) + offset_21620 + number * 0x492);
    // return (char*)((*(int*)0x699538) + 0x21620 + number * 0x492);
}

extern "C" __declspec(dllexport) void  AdvSkelet_transformer(long heroID)
{
    _Army_* v4;
    char dlg[0x17c];
    int& v10 = (int&)dlg[0x178];
    //v4 = &(o_GameMgr->hero[heroID].army);
    // v4 = &GetHeroRecord(heroID)->army;
    v4 = (_Army_*)(GetHeroRecord(heroID) + 0x91);
    Dlg_SkeletTransformer_Create((_Dlg_*)dlg, v4);
    v10 = 0;
    Dlg_ShowAndRun(dlg, 0);
    v10 = -1;
    Dlg_SkeletTransformer_Destroy(dlg);
}

char (__stdcall *Hero_WitchHut_Visit)(int a1, void* a2, char a3)= (char(__stdcall *)(int a1, void* a2, char a3)) 0x004A7CF0;

struct _MapItem_ {
    long SetUp;
    char Land;
    char LType;
    char River;
    char Rtype;
    char Road;
    char RdType;
    short _u1;
    char Mirror;
    char Attrib;
    short _Bits;
    short _u2;
    long Draw;
    long DrawEnd;
    long DrawEnd2;
    long Type;
    short Subtype;
    long DrawNum;
};

struct MapWitchHut
{
    unsigned _u1 : 5;
    signed    visited : 8;
    signed    sSkill : 7; // id
    unsigned _u2 : 12;
};

typedef void _AdvMgr_; typedef char _Hero_;
void (__thiscall *AdvMgr_Enter2ObjectMain)(_AdvMgr_* AdvManager, _Hero_* hero, _MapItem_* MapItem, long AI_flag , long XYZ) = 
    (void(__thiscall * )(_AdvMgr_ * , _Hero_ * , _MapItem_ * , long, long))0x004A8160;

#define o_AdvMgr (*(_AdvMgr_**)0x6992B8)

// typedef long _dword_; typedef short _word_;
#define b_unpack_z(xyz) (((_int16_)(((_dword_)(xyz)) >> 14)) >> 12)
#define b_unpack_y(xyz) (((_int16_)(((_dword_)(xyz)) >> 10)) >> 6)
#define b_unpack_x(xyz) (((_int16_)(((_dword_)(xyz)) << 6)) >> 6)
#define b_pack_xyz(x, y, z) (_dword_)( (((_dword_)(((_word_)(y)) & 0x3FF)) << 16) | ((_dword_)(((_word_)(x)) & 0x3FF)) | (((_dword_)(((_word_)(z)) & 0xF)) << 26) )

_MapItem_* witch_hut = nullptr;
// _Hero_* &ERM_HeroStr = &(_Hero_) (long*) (0x027F9970);

void set_ERM_HeroStr(_Hero_* h) {

    short* hero_position = (short*)h;
    _asm{
        mov eax, 0x027F9970
        mov ebx, h
        mov [eax], ebx
    }
    Era::v[998] = hero_position[0];
    Era::v[999] = hero_position[1];
    Era::v[1000] = hero_position[2];
}

extern "C" __declspec(dllexport) void  proxyWitchHut(long heroID, long SecSkill, long is_Human=1) {
    _Hero_* hero = GetHeroRecord(heroID);
    short* hero_position = (short*) hero;
    long XYZ = b_pack_xyz(hero_position[0], hero_position[1], hero_position[2]);

    MapWitchHut WitchSetup = {0,0,SecSkill,0};

    witch_hut = new _MapItem_;
    //memset(witch_hut, 0, sizeof(witch_hut));
    witch_hut->Type = 113;
    witch_hut->Subtype = SecSkill;
    witch_hut->SetUp = *(long*) &WitchSetup;

    //ERM_HeroStr = hero;
    set_ERM_HeroStr(hero);
    Era::FireErmEvent(0x40000000 | (113 << 12) );
    // if(Era::v[2361] != 1)
        Hero_WitchHut_Visit((int)hero, witch_hut, is_Human);
    // AdvMgr_Enter2ObjectMain(o_AdvMgr, hero, witch_hut, 1, XYZ );
    //ERM_HeroStr = hero;
    set_ERM_HeroStr(hero);
    Era::FireErmEvent(0x08000000 |0x40000000 | (113 << 12));

    delete witch_hut;
    witch_hut = nullptr;
}

_LHF_(hook_0073AD50) {
    if (!witch_hut) return EXEC_DEFAULT;
    *(long*)(c->ebp - 0x04) = (long)witch_hut;
    *(long*)(c->ebp - 0x10) = (long)witch_hut;
    c->return_address = 0x0073AD6D;
    return NO_EXEC_DEFAULT;
}

extern "C" __declspec(dllexport) /*__declspec(naked)*/ void  proxyMarket(long count) {
    _asm {

        /*
        push ebp
        mov ebp, esp
        push 0xFFFFFFFF
        push 0x6380B7
        */

        pushad
        mov edi, dword ptr ds : [0x00699538]
        xor edx, edx
        lea eax, ds : [edi + 0x1F664]
        // mov dword ptr ds : [0x006AAB00] , edx
        mov dword ptr ds : [0x006AAADC] , eax

        mov eax, count
        mov dword ptr DS:[0x006AAB00], eax
        // xor eax, eax
        // mov dword ptr ds : [0x006AAAE0] , eax
        mov dword ptr ds : [0x006AAB0C] , 0
        mov dword ptr ds : [0x006AAB2C] , 1
        popad
        
        mov eax, 0x005EA530
        call eax

        //push 0x005EA530
        //ret

        // jmp 0x005EA548
    }
    // return;
}



float new_hill_fort_multiplier_default[7] = {0.0, 0.25, 0.5, 0.75, 1.0, 1.0, 1.0};

extern "C" __declspec(dllexport) void set_default_hill_fort_multiplier_float(float lvl0, float lvl1, float lvl2, float lvl3, float lvl4, float lvl5, float lvl6) {
    new_hill_fort_multiplier_default[0] = lvl0;
    new_hill_fort_multiplier_default[1] = lvl1;
    new_hill_fort_multiplier_default[2] = lvl2;
    new_hill_fort_multiplier_default[3] = lvl3;
    new_hill_fort_multiplier_default[4] = lvl4;
    new_hill_fort_multiplier_default[5] = lvl5;
    new_hill_fort_multiplier_default[6] = lvl6;
}

extern "C" __declspec(dllexport) void set_default_hill_fort_multiplier_percent(int lvl0, int lvl1, int lvl2, int lvl3, int lvl4, int lvl5, int lvl6) {
    set_default_hill_fort_multiplier_float(lvl0 / 100.0, lvl1 / 100.0, lvl2 / 100.0, lvl3 / 100.0, lvl4 / 100.0, lvl5 / 100.0, lvl6 / 100.0);
}

extern "C" __declspec(dllexport) void set_default_hill_fort_multiplier_promile(int lvl0, int lvl1, int lvl2, int lvl3, int lvl4, int lvl5, int lvl6) {
    set_default_hill_fort_multiplier_float(lvl0 / 1000.0, lvl1 / 1000.0, lvl2 / 1000.0, lvl3 / 1000.0, lvl4 / 1000.0, lvl5 / 1000.0, lvl6 / 1000.0);
}

extern "C" __declspec(dllexport) void set_hill_fort_multiplier_reset() {
    float* flt_0063EB4C = (float*)0x0063EB4C;
    for (int i = 0; i < 7; ++i) flt_0063EB4C[i] = new_hill_fort_multiplier_default[i];
}

void __stdcall set_hill_fort_multiplier_reset(Era::TEvent* e)
{
    set_hill_fort_multiplier_reset();
}

extern "C" __declspec(dllexport) void set_hill_fort_multiplier_float(float lvl0, float lvl1, float lvl2, float lvl3, float lvl4, float lvl5, float lvl6) {
    float* flt_0063EB4C = (float*) 0x0063EB4C;
    flt_0063EB4C[0] = lvl0;
    flt_0063EB4C[1] = lvl1;
    flt_0063EB4C[2] = lvl2;
    flt_0063EB4C[3] = lvl3;
    flt_0063EB4C[4] = lvl4;
    flt_0063EB4C[5] = lvl5;
    flt_0063EB4C[6] = lvl6;
}

extern "C" __declspec(dllexport) void set_hill_fort_multiplier_percent(int lvl0, int lvl1, int lvl2, int lvl3, int lvl4, int lvl5, int lvl6) {
    set_hill_fort_multiplier_float(lvl0 / 100.0, lvl1 / 100.0, lvl2 / 100.0, lvl3 / 100.0, lvl4 / 100.0, lvl5 / 100.0, lvl6 / 100.0);
}

extern "C" __declspec(dllexport) void set_hill_fort_multiplier_promile(int lvl0, int lvl1, int lvl2, int lvl3, int lvl4, int lvl5, int lvl6) {
    set_hill_fort_multiplier_float(lvl0 / 1000.0, lvl1 / 1000.0, lvl2 / 1000.0, lvl3 / 1000.0, lvl4 / 1000.0, lvl5 / 1000.0, lvl6 / 1000.0);
}

_LHF_(hook_004E808F) {
    float* flt_0063EB4C = (float*)0x0063EB4C;
  
    // *(char*)(c->ebp - 1) = 1;
    *(char*)(c->ebp - 2) = 0;

    if (flt_0063EB4C[c->eax] < 0.001) {
        c->return_address = 0x004E8097;
    }
    else {
        c->return_address = 0x004E80E0;
    }
    return NO_EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:

        globalPatcher = GetPatcher();
        Z_new_advmap_objects = globalPatcher->CreateInstance(PINSTANCE_MAIN);
        Z_new_advmap_objects->WriteLoHook(0x0073AD50, hook_0073AD50);
        Z_new_advmap_objects->WriteLoHook(0x004E808F, hook_004E808F);

        Era::ConnectEra();

        Era::RegisterHandler(set_hill_fort_multiplier_reset, "OnBeforeVisitObject 35/-1");

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

