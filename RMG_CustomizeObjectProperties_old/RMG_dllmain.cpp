﻿// dllmain.cpp : Определяет точку входа для приложения DLL.
#include "Header.h"
#include "..\__include__\era.h"

#include<map>
#include<vector>

using namespace h3;
Patcher* globalPatcher;
PatcherInstance* _PI;
int last_dragon_utopia_subtype = 10;
int last_pyramid_subtype = 78;
int last_chest_subtype = 10;

int UtopiaSubtypeCount[2048] = {};
int WoGobjSubtypeCount[2048] = {};

constexpr int obj_types = 232;

struct zone_field{
	int max;
	// int q_minus_one;
	int q[2048];
	// (*zone_field() = default;
	inline zone_field() {
		max = 0;
		memset(q, 0, 8192);
	}
};

namespace subtypemaps {
	typedef std::map<int, zone_field> zone_map;
	std::map<int, zone_field>* zone_UtopiaSubtypeCount = 0;
	std::map<int, zone_field>* zone_WoGobjSubtypeCount = 0;
	std::map<int, zone_field>* zone_ChestSubtypeCount = 0;
	std::map<int, zone_field>* zone_CrBankSubtypeCount = 0;


	std::vector<H3RmgObjectGenerator*>* active_wog_objects = 0;
	std::vector<int>* active_CrBanks = 0;


	void init_maps() {
		zone_UtopiaSubtypeCount = new zone_map;
		zone_WoGobjSubtypeCount = new zone_map;
		zone_ChestSubtypeCount = new zone_map;
		zone_CrBankSubtypeCount = new zone_map;

		active_wog_objects = new std::vector<H3RmgObjectGenerator*>;
		active_CrBanks = new std::vector<int>;
	}
}

int h3rand(int min, int max) {
	return FASTCALL_2(int, 0x0050C7C0, min, max);
}

inline void v_MsgBox(const char* text, int style)
{
	FASTCALL_12(void, 0x4F6C00, text, style, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
	return;
}

int GetIntFromJson(const char* keyName)
{
	std::string jsonText = Era::tr(keyName);
	return keyName != jsonText ? std::max(std::atoi(jsonText.c_str()), 0) : -1;
}

_LHF_(ChangeRMGLimits)
{
	using namespace subtypemaps;

	memset(UtopiaSubtypeCount, 0, 8192); zone_UtopiaSubtypeCount->clear();
	memset(WoGobjSubtypeCount, 0, 8192); zone_WoGobjSubtypeCount->clear();
	// active_wog_objects->clear(); 
	zone_ChestSubtypeCount->clear(); zone_CrBankSubtypeCount->clear();

	h3::H3RmgRandomMapGenerator* rmgGnr = (h3::H3RmgRandomMapGenerator*)c->ecx;

	if (rmgGnr)
	{
		int objNumber = sizeof(rmgGnr->objectCountByType) / sizeof(INT32);

		H3RmgZoneGenerator* zone;
		int* ObjectLimitPerMap = (int*)0x69CE9C;
		int* ObjectlimitPerZone = (int*)0x69D244;

		H3String keyBaseName = "RMG.object_limit.";
		H3String dot = "."; 

		for (UINT32 i = 0; i < objNumber; i++)
		{
			H3String jj = std::to_string(i).c_str();
			int perMapFromJson = GetIntFromJson((keyBaseName + jj + H3String(".map")).String());
			if (perMapFromJson != -1)
				ObjectLimitPerMap[i] = perMapFromJson;
			else
			{
				if (perMapFromJson)
				{
					int perZoneFromJson = GetIntFromJson((keyBaseName + jj + H3String(".zone")).String());
					if (perZoneFromJson != -1)
						ObjectlimitPerZone[i] = perZoneFromJson;
				}
				else
					ObjectlimitPerZone[i] = 0;
			}
		}
	}
	
	return EXEC_DEFAULT;
}

BOOL8 ValidateRMGGenObject(H3RmgObjectGenerator* obj)
{
	//if (obj->type < 0) return false;
	//if (obj->subtype < 0) return false;
	if (obj->density < 0) return false;
	if (obj->value < 0) return false;

	return true;
}

_LHF_(CreatureBanksListCreation)
{


	H3Vector<H3RmgObjectGenerator>* crBankList = (H3Vector<H3RmgObjectGenerator>*)c->esi;

	constexpr int objNumber = 232;// sizeof(rmgGnr->objectCount)ByType) / sizeof(INT32);
	for (int i = eObject::NO_OBJ +1 ; i < objNumber; i++)
	{


		for (int j = 0; j < 100; j++)
		{
			
			//std::string baseJsonKeyName = "RMG.object_gen." + std::to_string(i) + "." + std::to_string(j) + ".";
			H3String keyBaseName = "RMG.object_gen.";

			int value = GetIntFromJson((keyBaseName + std::to_string(i).c_str() + "." + std::to_string(j).c_str() + ".value").String());
			if (value != eObject::NO_OBJ)
			{
				H3RmgObjectGenerator* crBankGen = new H3RmgObjectGenerator;
				
				crBankGen->type = i;
				crBankGen->subtype = j;
				crBankGen->value = value;
				crBankGen->density = GetIntFromJson((keyBaseName + std::to_string(i).c_str() + "." + std::to_string(j).c_str() + ".density").String());

				crBankGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
				if (ValidateRMGGenObject(crBankGen))
					THISCALL_4(int, 0x5FE2D0, crBankList, crBankList->end(), 1u, &crBankGen);
				delete crBankGen;

			}
		}
	}

	return EXEC_DEFAULT;
}

bool CrBank_Gods_Enable = false;
bool CrBank_New_Enable = true;
bool utopia_banks_enable = true;
bool WoG_Places_Enable = true;
bool New_Towns_Enable = true;

void init_wog_objs() {
	if (subtypemaps::active_wog_objects->size()) return;

	for (int j = 1; j <= last_pyramid_subtype; j++)
	{

		//std::string baseJsonKeyName = "RMG.object_gen." + std::to_string(i) + "." + std::to_string(j) + ".";
		H3String keyBaseName = "RMG.object_gen."; H3String dot = "."; H3String jj = std::to_string(j).c_str();

		int value = GetIntFromJson((keyBaseName + H3String("63") + dot + jj + H3String(".value")).String());
		if (value != eObject::NO_OBJ)
		{


			H3RmgObjectGenerator *PyramidGen = new H3RmgObjectGenerator;

			PyramidGen->type = 63;
			PyramidGen->subtype = j;
			PyramidGen->value = value;
			PyramidGen->density = GetIntFromJson((keyBaseName + H3String("63") + dot + jj + H3String(".density")).String());

			PyramidGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
			/*
			if (ValidateRMGGenObject(PyramidGen))
				THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &PyramidGen);
			delete PyramidGen;
			*/
			subtypemaps::active_wog_objects->push_back(PyramidGen);

		}

	}
}

_LHF_(RMG_New_Utopias) {
	using namespace subtypemaps;
	H3Vector<H3RmgObjectGenerator>* objList = (H3Vector<H3RmgObjectGenerator>*)c->esi;

	
	for(int subtype=0; subtype<= last_dragon_utopia_subtype;++subtype)
	{
		if (subtype && !utopia_banks_enable) continue;

		// if (subtype == 4) continue;

		H3RmgObjectGenerator* crBankGen = new H3RmgObjectGenerator;

		crBankGen->type = 25;
		crBankGen->subtype = subtype;
		crBankGen->value = 3500;
		crBankGen->density = 200;
		
		if (subtype == 10 || subtype == 0) {
			crBankGen->value = 3500;
			crBankGen->density = 100;
		}
		
		if (subtype >= 5 || subtype <= 8) {
			crBankGen->value = 3500;
			crBankGen->density = 25;
		}

		//crBankGen->density = GetIntFromJson((keyBaseName + std::to_string(i).c_str() + "." + std::to_string(j).c_str() + ".density").String());

		crBankGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
		if (ValidateRMGGenObject(crBankGen))
			THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &crBankGen);
		delete crBankGen;
	}

	active_CrBanks->clear(); 
	for (int i=0;i<=6;++i) active_CrBanks->push_back(i);
	for (int subtype = 11; subtype <= 20; ++subtype)
	{
		if (!CrBank_New_Enable) continue;

		if (subtype == 19) continue;

		H3RmgObjectGenerator* crBankGen = new H3RmgObjectGenerator;

		crBankGen->type = 16;
		crBankGen->subtype = subtype;
		crBankGen->value = 3500;
		crBankGen->density = 200;

		if (subtype >= 13 && subtype <= 16) {
			crBankGen->value = 10000;
			crBankGen->density = 100;
			if (!CrBank_Gods_Enable) {
				delete crBankGen; continue;
			}
		}

		active_CrBanks->push_back(subtype);
		crBankGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
		if (ValidateRMGGenObject(crBankGen))
			THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &crBankGen);
		delete crBankGen;
	}
	
	/*
	if (!last_pyramid_subtype) {
		last_pyramid_subtype = GetIntFromJson("RMG.object_gen.63.last");
	}
	*/
	
	/*
	// bool first_pass_pyramid = active_wog_objects.empty();
	// active_wog_objects->clear();
	if(WoG_Places_Enable)
	for (int j = 1; j <= last_pyramid_subtype; j++)
	{

		//std::string baseJsonKeyName = "RMG.object_gen." + std::to_string(i) + "." + std::to_string(j) + ".";
		H3String keyBaseName = "RMG.object_gen."; H3String dot = "."; H3String jj = std::to_string(j).c_str();

		int value = GetIntFromJson((keyBaseName + H3String("63") + dot + jj + H3String(".value")).String());
		if (value != eObject::NO_OBJ)
		{

			// if (first_pass_pyramid)
				active_wog_objects->push_back(j);

			H3RmgObjectGenerator* PyramidGen = new H3RmgObjectGenerator;

			PyramidGen->type = 63;
			PyramidGen->subtype = j;
			PyramidGen->value = value;
			PyramidGen->density = GetIntFromJson((keyBaseName + H3String("63") + dot + jj + H3String(".density")).String());

			PyramidGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
			if (ValidateRMGGenObject(PyramidGen))
				THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &PyramidGen);
			delete PyramidGen;

		}
	}
	*/
	if (WoG_Places_Enable) for(auto &it:(*active_wog_objects)){
		// H3RmgObjectGenerator** ptr = &it;
		if (it->type != 63) {
			MessageBoxA(0,"Wrong WoG_Place", "debug", 0);
		}
		H3RmgObjectGenerator* ptr = new H3RmgObjectGenerator();
		memcpy(ptr, it, 0x14);
		THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &ptr);
		delete ptr;
	}

	if (true) {
		H3RmgObjectGenerator* MythrilGen = new H3RmgObjectGenerator;
		MythrilGen->type = 79;
		MythrilGen->subtype = 7;
		MythrilGen->value = 7000;
		MythrilGen->density = 5;
		MythrilGen-> vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
		if (ValidateRMGGenObject(MythrilGen))
			THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &MythrilGen);
		delete MythrilGen;
	}

	for (int subtype = 1; subtype <= last_chest_subtype; ++subtype)
	{
		// if (subtype >= 2 && subtype <=4) continue;
		// if (subtype == 6) continue;

		H3RmgObjectGenerator* TreasureGen = new H3RmgObjectGenerator;
		
		TreasureGen->type = 101;
		TreasureGen->subtype = subtype;
		TreasureGen->value = 100;
		TreasureGen->density = 9;

		TreasureGen->vTable = (H3RmgObjectGenerator::VTable*)0x00640B74;
		if (ValidateRMGGenObject(TreasureGen) || true)
			THISCALL_4(int, 0x5FE2D0, objList, objList->end(), 1u, &TreasureGen);
		delete TreasureGen;
		
	}

	// return EXEC_DEFAULT;

	c->return_address= 0x00539DE5;
	return NO_EXEC_DEFAULT;
}

_LHF_(RMG_ObjGenSimple_Ctor)
{	
	H3RmgObjectGenerator* obj = (H3RmgObjectGenerator*)c->eax;
	if (obj->type == eObject::TRADING_POST
		|| obj->type == eObject::TRADING_POST_SNOW)
	{
		// c->ecx = 20000; // ecx value
		// c->edx = 20; // edx density
	}

	return EXEC_DEFAULT;
}

//H3CreatureBankState arr3[1500];
_LHF_(ChangeRMGLimitsA)
{


	//_PI->WriteDword(0x47A3C1 + 1, (int)&newTable);
	//_PI->WriteDword(0x7B8300, (int)&guard_types);// guard types up to 5
	//	_PI->WriteDword(0x802ED0, (int)&award_creatures);// award creatures

	//_PI->WriteDword(0x47A3EC + 1, (int)&newTable);
//	_PI->WriteDword(0x7C8E54, (int)&newTable + 4);
//	_PI->WriteDword(0x47A4B6 +3, (int)&newTable+4);


	//*(int*)0x6961BC = 2048;
	//H3TextTable* txtTable = (H3TextTable*)c->eax;
	//coounter = txtTable->Count)Rows();
	// 
	//H3LoadedDef* def;
	//def->Load("123123.def");
	

	//Era::RedirectMemoryBlock((void*)0x7C8E50, sizeof(newTable), newTable);
	//int* ptr = (int*)&newTable[0];
	//*(int**)(c->ebp-4) = ptr +1;
	//static int cnt;
	//coounter++;
	//if (c->eax == 1 && once)
	//{
	//	//c->eax += 4;
	//	once = false;
	//	//*(int*)(c->ebp - 8) = 4;

	////	arr[cnt++] = (c->ebp - 8);// c->eax;
	//	///arr[cnt++] = (c->ebp - 8);// c->eax;

	//}
	return EXEC_DEFAULT;

	
}

_LHF_(hook_00546747) {
	if (c->esi >= 0 && c->esi < 0xE8 ) return EXEC_DEFAULT;
	c->esi = 101;  return EXEC_DEFAULT;

	c->return_address = 0x00546771;
	return NO_EXEC_DEFAULT;

}

_LHF_(hook_00546535) {
	int& type = *(int*)(c->ebp + 0x0C);
	int& subtype = *(int*)(c->ebp + 0x10);

	
	if (type == 25) {

		// if (subtype == 4) subtype = 9;

		if(subtype >= 1 && subtype <=4)
			subtype = FASTCALL_2(int, 0x0050C7C0, 1, 3);
		
		if (subtype >= 5 && subtype <= 8)
			subtype = FASTCALL_2(int, 0x0050C7C0, 5, 8);

		//int subtype = FASTCALL_2(int, 0x0050C7C0, -3, last_dragon_utopia_subtype);
		//if (subtype == 4) subtype = 0;
		//if (subtype <  0) subtype = 10;
		
		// type = 25;

	}
	
	/*
	if (type == 63) {
		//if (subtype == 2510) { type = 25; subtype = 10; }
		if (subtype == 75) { type = 25; subtype = 10; }
		if (subtype == 76) { type = 25; subtype = 0; }
		if (subtype == 77) { type = 25; subtype = FASTCALL_2(int, 0x0050C7C0, 1, 3); }
		if (subtype == 78) { type = 25; subtype = FASTCALL_2(int, 0x0050C7C0, 5, 8); }
	}
	*/

	return EXEC_DEFAULT;
}

_LHF_(hook_00546712) {
	// return EXEC_DEFAULT;
	using namespace subtypemaps;

	auto objUnk = (h3::H3RmgObjectGenerator*)c->ecx;
	// *(int*)(c->ebp - 0x10);
	int& obj_type = objUnk->type;
	int& obj_subtype = objUnk->subtype;
	void* vtable = objUnk->vTable;


	/*
	int objUnk = *(int*)(c->ebp - 0x10);

	int obj_type = c->esi;
	int& obj_subtype = *(int*)(objUnk + 8);
	*/

	//int zone = *(int*)(c->ebp + 8);
	h3::H3RmgZoneGenerator *gen = (h3::H3RmgZoneGenerator*) *(int*)(c->ebp + 8);
	int zone = gen->zoneInfo->id;

	// int zone_Count = zone_UtopiaSubtypeCount->size();
	int* zone_limit_check = gen->objectCount;// (int*)(zone+0x44);

	int* ObjectLimitPerMap = (int*)0x69CE9C;
	int* ObjectLimitPerZone = (int*)0x69D244;

	if (obj_type<0 || obj_type >= obj_types) {
		char msg[512]; sprintf_s(msg, "Wrong type %d in zone %d", obj_type, zone);
		MessageBoxA(0, msg, "hook_00546712", 0);
		return EXEC_DEFAULT;
	}

	// if (obj_subtype < 0) return EXEC_DEFAULT;
	if (obj_subtype < 0 || obj_subtype >= 2048) {
		char msg[512]; sprintf_s(msg, "Wrong subtype %d of type %d in zone %d", obj_subtype, obj_type, zone);
		MessageBoxA(0, msg, "hook_00546712", 0);
		//v_MsgBox("wrong object subtype" , 1);
		return EXEC_DEFAULT;

		c->return_address = 0x00546926; // 0x005466E1; // 0x00546926;
		c->ecx = *(int*)(c->ebp - 0x54);
		return NO_EXEC_DEFAULT;
	}

	if (zone_limit_check[obj_type] >= ObjectLimitPerZone[obj_type] && 
		// obj_type != 70 && obj_type != 34 && obj_type != 77 && obj_type != 98 &&
		obj_type != 25 && obj_type != 63 && int(vtable) == 0x00640B74) {
		switch (obj_type % 2) {
		case 0: obj_type = 25; goto utopia;
		case 1: obj_type = 63; goto wog_obj;
		// case 2: obj_type = 65; goto random_art;
		}
	}

begin:
	if (obj_type == 101
		&& (*zone_ChestSubtypeCount)[zone].q[obj_subtype] >= 2
		// && UtopiaSubtypeCount)[obj_subtype] > 300
		)
	{
		int act_Count = last_chest_subtype; //  +1;

		if (zone_limit_check[obj_type] >= (act_Count * 2 - 3) ) {
			c->return_address = 0x00546926;
			return NO_EXEC_DEFAULT;
		}

		if (!h3rand(0,399)) {
			obj_type = 65; 
			goto random_art;
		}

		obj_subtype = h3rand(1, last_chest_subtype);
		goto begin;

		// c->return_address = 0x00546926;
		// return NO_EXEC_DEFAULT;
	}


	if (obj_type == 25
		&& (*zone_UtopiaSubtypeCount)[zone].q[obj_subtype] >= 2
		// && UtopiaSubtypeCount)[obj_subtype] > 300
		)
	{
utopia:
		int act_Count = last_dragon_utopia_subtype + 1;

		if (zone_limit_check[obj_type]>= (act_Count * 2 - 3)) {
			c->return_address = 0x00546926;
			return NO_EXEC_DEFAULT;
		}

		obj_subtype = h3rand(0, last_dragon_utopia_subtype);
		goto begin;

		// c->return_address = 0x00546926;
		// return NO_EXEC_DEFAULT;
	}

	if (obj_type == 63 && obj_subtype 
		&& (*zone_WoGobjSubtypeCount)[zone].q[obj_subtype] >= 2
		// && WoGobjSubtypeCount)[obj_subtype] > 150
		)
	{
wog_obj:
		int act_Count = active_wog_objects->size();

		
		if (zone_limit_check[obj_type] >= (act_Count * 2 - 3) ) {
			c->return_address = 0x00546926;
			return NO_EXEC_DEFAULT;
		}
		

		int i = h3rand(0,act_Count-1);
		obj_subtype = (*active_wog_objects)[i]->subtype;
		goto begin;
		// obj_subtype = h3rand(0, last_pyramid_subtype);
		
		// c->return_address = 0x00546926;
		// return NO_EXEC_DEFAULT;
	}

	if(obj_type == 16 // && obj_subtype >= 11 && obj_subtype <= 20
		&& (*zone_CrBankSubtypeCount)[zone].q[obj_subtype] >= (*zone_CrBankSubtypeCount)[zone].max
		){
		int act_Count = active_CrBanks->size();
		// int act_Count) = (21 - 4 - 1) * (*zone_CrBankSubtypeCount)[zone]->max;
		if (zone_limit_check[obj_type] >= act_Count * (*zone_CrBankSubtypeCount)[zone].max)
			(*zone_CrBankSubtypeCount)[zone].max++;

		/*
		if ((*zone_limit_check[obj_type] >= (act_Count) - 2)) {
			c->return_address = 0x00546926;
			return NO_EXEC_DEFAULT;
		}
		*/
		int i = h3rand(0, act_Count - 1);
		obj_subtype = (*active_CrBanks)[i] ;// h3rand(11, 20);
		goto begin;
	}

	if (obj_type == 25 || obj_type == 63
		|| (obj_type == 16 && obj_subtype >= 11 && obj_subtype <= 20)
		) {
		c->return_address = 0x00546771;
		return NO_EXEC_DEFAULT;
	}

	if (obj_type >= 65 && obj_type <= 69) {
		if(false){
			int tmp = (int)vtable; char msg[512]; 
			sprintf_s(msg, "obj_type %d vtable %d", obj_type, tmp);
			MessageBoxA(0, msg, "hook_00546712", 0);
		}
		return EXEC_DEFAULT;

	random_art:
		objUnk->vTable = (h3::H3RmgObjectGenerator::VTable*) 0x640B80;
		obj_subtype = 0;

		/*
		c->return_address = 0x00546771;
		return NO_EXEC_DEFAULT;
		*/
	}
	
	return EXEC_DEFAULT;

	if (!c->AL()) c->return_address = 0x00546730;
	else {
		c->eax = *(int*)0x00660428;
		c->edx = c->esi;
		c->return_address = 0x00546719;
	}
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_0054677F) {
	int objUnk = *(int*)(c->ebp - 0x10);

	int obj_type = *(int*)(objUnk + 4);
	int obj_subtype = *(int*)(objUnk + 8);

	if (obj_subtype < 0 || obj_subtype >= 2048) {
		char msg[512]; sprintf_s(msg, "Wrong subtype %d of type %d", obj_subtype, obj_type);
		MessageBoxA(0, msg, "hook_0054677F", 0);
	}


	if (obj_type == 25 || obj_type == 63
		// || (obj_type == 16 && obj_subtype >= 11 && obj_subtype <= 20)
		) {
		c->return_address = 0x00546797;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

int last_subtype;
_LHF_(hook_005407FF) {
	int obj_type = c->edx;
	int obj_subtype = *(int*)(c->eax + 0x20);
	last_subtype = obj_subtype;

	if (obj_type == 25 && obj_subtype < 2048 && obj_subtype >= 0)
		++(UtopiaSubtypeCount[obj_subtype]);

	if (obj_type == 63 && obj_subtype < 2048 && obj_subtype >= 0)
		++(WoGobjSubtypeCount[obj_subtype]);

	if (obj_subtype >= 2048 || obj_subtype < 0) {
		char msg[512]; sprintf_s(msg, "Wrong subtype %d", obj_subtype);
		MessageBoxA(0, msg, "hook_005407FF", 0);
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_0054087E) {
	using namespace subtypemaps;

	int obj_type = c->edx;
	int zone = 0; 
	int _ECX_ = c->ecx;
	int _EBP_ = c->ebp;
	/*
	_asm {
		pusha
		mov		ecx, _ECX_
		mov     esi, [_EBP_ - 0x14]
		mov     esi, [esi + 0x10E4]
		mov     ecx, [esi + ecx * 4]
		mov		zone, ecx
		popa
	}
	*/
	int esi_1 = *(int*)(_EBP_ - 0x14);
	int esi_2 = *(int*)(esi_1 + 0x10e4);
	// zone = *(int*)(esi_2 + _ECX_ * 4);
	H3RmgZoneGenerator* gen = (H3RmgZoneGenerator*) *(int*)(esi_2 + _ECX_ * 4);
	zone = gen->zoneInfo->id;

	if ((*zone_UtopiaSubtypeCount).empty() || !(*zone_UtopiaSubtypeCount).count(zone))
		(*zone_UtopiaSubtypeCount).emplace(zone, zone_field{}); //[zone] = zone_field{};
	if ((*zone_WoGobjSubtypeCount).empty() || !(*zone_WoGobjSubtypeCount).count(zone))
		(*zone_WoGobjSubtypeCount).emplace(zone, zone_field{}); //[zone] =(*zone_field{};
	if ((*zone_ChestSubtypeCount).empty() || !(*zone_ChestSubtypeCount).count(zone))
		(*zone_ChestSubtypeCount).emplace(zone, zone_field{}); //[zone] = zone_field{};
	if ((*zone_CrBankSubtypeCount).empty() || !(*zone_CrBankSubtypeCount).count(zone))
		(*zone_CrBankSubtypeCount).emplace(zone, zone_field{}); //[zone] = zone_field{};


	if (obj_type == 25 && last_subtype < 2048 && last_subtype >= 0){
		++((*zone_UtopiaSubtypeCount)[zone].q[last_subtype]);
	}

	if (obj_type == 63 && last_subtype < 2048 && last_subtype >= 0) {
		++((*zone_WoGobjSubtypeCount)[zone].q[last_subtype]);
	}

	if (obj_type == 101 && last_subtype < 2048 && last_subtype >= 0) {
		++((*zone_ChestSubtypeCount)[zone].q[last_subtype]);
	}

	if (obj_type == 16 && last_subtype < 2048 && last_subtype >= 0) {
		++((*zone_CrBankSubtypeCount)[zone].q[last_subtype]);
	}

	if(last_subtype > 2048 || last_subtype < 0)
	{
		char msg[512]; sprintf_s(msg, "Wrong subtype %d", last_subtype);
		MessageBoxA(0, msg, "hook_0054087E", 0);
	}

	return EXEC_DEFAULT;
}

struct rnd_dlg : public h3::H3Dlg
{
	int seed = 0;
	rnd_dlg() :H3Dlg(256, 480) {
		CreateEdit(16, 16, 128, 32, 16, "0", h3::NH3Dlg::Text::MEDIUM, 1, 5, nullptr, 1001);
		CreateText(128, 16, 128, 32, "Seed", h3::NH3Dlg::Text::MEDIUM, 1, -1);

		CreateOnOffCheckbox(16, 64, 1002, CrBank_Gods_Enable, CrBank_Gods_Enable);
		CreateText(64,64, 128, 32, "GodBanks", h3::NH3Dlg::Text::MEDIUM, 1, -1 );


		CreateOnOffCheckbox(16, 112, 1003, CrBank_New_Enable, CrBank_New_Enable);
		CreateText(64, 112, 128, 32, "NewBanks", h3::NH3Dlg::Text::MEDIUM, 1, -1);

		CreateOnOffCheckbox(16, 160, 1004, utopia_banks_enable, utopia_banks_enable);
		CreateText(64, 160, 128, 32, "UtopiaBanks", h3::NH3Dlg::Text::MEDIUM, 1, -1);


		CreateOnOffCheckbox(16, 208, 1005, WoG_Places_Enable, WoG_Places_Enable);
		CreateText(64, 208, 128, 32, "WogPlaces", h3::NH3Dlg::Text::MEDIUM, 1, -1);


		CreateOnOffCheckbox(16, 256, 1006, New_Towns_Enable, New_Towns_Enable);
		CreateText(64, 256, 128, 32, "NewTowns", h3::NH3Dlg::Text::MEDIUM, 1, -1);

	};
	BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL rnd_dlg::DialogProc(h3::H3Msg& msg) {
	if (msg.IsKeyPress()) {
		auto target = this->GetEdit(1001);
		auto txt = target->GetText();
		auto ptr = txt; 
		while (*ptr) {
			if (*ptr < '0' || *ptr>'9' || *txt == 0) {
				target->SetText("0");
				this->seed = 0; 
				this->Redraw();
				return 0;
			}
			++ptr;
		}
		seed = atoi(txt);
		this->Redraw();
	}
	if (msg.subtype == eMsgSubtype::LBUTTON_CLICK) {
		auto target = msg.GetDlg()->ItemAtPosition(msg);
		if (msg.itemId == 1002) {
			auto the_target = target->Cast<h3::H3DlgDefButton>();
			CrBank_Gods_Enable = !CrBank_Gods_Enable;
			// int newframe = ! the_target->GetFrame();
			the_target->SetClickFrame(!CrBank_Gods_Enable);
			the_target->SetFrame(!CrBank_Gods_Enable);
			the_target->Refresh();
		}
		if (msg.itemId == 1003) {
			auto the_target = target->Cast<h3::H3DlgDefButton>();
			CrBank_New_Enable = !CrBank_New_Enable;
			// int newframe = ! the_target->GetFrame();
			the_target->SetClickFrame(!CrBank_New_Enable);
			the_target->SetFrame(!CrBank_New_Enable);
			the_target->Refresh();
		}
		if (msg.itemId == 1004) {
			auto the_target = target->Cast<h3::H3DlgDefButton>();
			utopia_banks_enable = !utopia_banks_enable;
			// int newframe = ! the_target->GetFrame();
			the_target->SetClickFrame(!utopia_banks_enable);
			the_target->SetFrame(!utopia_banks_enable);
			the_target->Refresh();
		}
		if (msg.itemId == 1005) {
			auto the_target = target->Cast<h3::H3DlgDefButton>();
			WoG_Places_Enable = !WoG_Places_Enable;
			// int newframe = ! the_target->GetFrame();
			the_target->SetClickFrame(!WoG_Places_Enable);
			the_target->SetFrame(!WoG_Places_Enable);
			the_target->Refresh();
		}

		if (msg.itemId == 1006) {
			auto the_target = target->Cast<h3::H3DlgDefButton>();
			New_Towns_Enable = !New_Towns_Enable;
			// int newframe = ! the_target->GetFrame();
			the_target->SetClickFrame(!New_Towns_Enable);
			the_target->SetFrame(!New_Towns_Enable);
			the_target->Refresh();
		}
	}

	return 0;
}

_LHF_(hook_00536630) {
	rnd_dlg wnd;

	wnd.CreateOKButton();
	wnd.Start();

	if(!wnd.seed) return EXEC_DEFAULT;

	// int* seed_ptr = (int*) c->Pop();
	// *seed_ptr = wnd.seed;
	c->eax = wnd.seed;

	c->return_address = 0x00536637;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_00546771) {
	int* ptr = (int*) c->ecx;
	if (*ptr > 0x00400000) 
		return EXEC_DEFAULT;

	*ptr = 0x00640B74;
	return EXEC_DEFAULT;
}


char __stdcall RMG_ToPlaceTowns_00544F40_hook(HiHook* hook, h3::H3RmgRandomMapGenerator* _this_, h3::H3RmgZoneGenerator* arg) {
	auto& town_prototypes = _this_->objectPrototypes[98];
	
	if (New_Towns_Enable && town_prototypes.Count() == 9) {
		for (int i = 9; i < 27; ++i) {
			
			auto town_properties = new h3::H3RmgObjectProperties(**town_prototypes[i%9]);

			town_properties->subtype = i;
			town_prototypes.Push(&town_properties);
		}
	}
	
	if (New_Towns_Enable && town_prototypes.Count()>9) {
		if (arg->zoneInfo->type == 2)
			arg->townType = h3::H3Random::RandBetween(10, 11);
	}
	return THISCALL_2(char, hook->GetDefaultFunc(), _this_, arg);
}
_LHF_(town_names_004CA98A) {
	c->esi %= 9;
	return EXEC_DEFAULT;
}

/*
_LHF_(hook_0054C559) {
	auto RMG = (h3::H3RmgRandomMapGenerator*)*(int*)(c->ebp - 0x14ec);
	auto& town_prototypes = RMG->objectPrototypes[98];
	while (town_prototypes.Size()>9) {
		// delete (**town_prototypes.Last());
		town_prototypes.Pop();
	}
	return EXEC_DEFAULT;
}

_LHF_(hook_0054A05A) {
	auto RMG = (h3::H3RmgRandomMapGenerator*)c->ecx;
	auto& town_prototypes = RMG->objectPrototypes[98];
	while (town_prototypes.Size() > 9) {
		delete (**town_prototypes.Last());
		town_prototypes.Pop();
	}
	return EXEC_DEFAULT;
}
*/
//

//struct _CrBankMap_ {
//	long  DMonsterType[7]; // -1 - no
//	long  DMonsterNum[7];
//	long  Res[7];
//	long  BMonsterType; // -1 - no
//	char  BMonsterNum;
//	Byte _u1[3];
//	_ArtList_ Arts;
//};
//struct _CCrBank_ { // type 16
//	unsigned _u1 : 5; //
//	unsigned  Whom : 8; // ęňî čç čăđîęîâ őîňü ęîăäŕ çŕőîäčë
//	unsigned  Ind : 12; // číäĺęń ďî ďîđ˙äęó ńňđóęňóđ
//	unsigned  Taken : 1; // âç˙ň (1) čëč ĺůĺ íĺň (0)
//	unsigned _u2 : 6;
//};
//{0x47A4A8 + 3, DS(crbankt1), 4},
//{ 0x47A4AF + 3,DS(crbankt2),4 },
//{ 0x47A4B6 + 3,DS0(&CrBankTable[0].Name),4 },
//{ 0x47A68F + 1,DS0(&CrBankTable[BANKNUM].Name),4 },
//{ 0x47A3C1 + 1,DS(CrBankTable),4 },
//{ 0x47A3EC + 1,DS0(CrBankTable),4 },
//{ 0x67029C  ,DS0(CrBankTable),4 },
//{ 0x47A3BA + 1,DS0(BANKNUM),1 },
//{ 0x47A3E5 + 1,DS0(BANKNUM),1 },
//{ (Byte*)0x67037C,(Byte*)crbankt1,sizeof(crbanks1) * BANKNUM_0 },
//{ (Byte*)0x6702A0,(Byte*)crbankt2,sizeof(crbanks2) * BANKNUM_0 },
_LHF_(HooksInit)
{
	init_wog_objs();

	_PI->WriteLoHook(0x004CA98A, town_names_004CA98A);
	_PI->WriteHiHook(0x00544F40, SPLICE_, EXTENDED_, THISCALL_, RMG_ToPlaceTowns_00544F40_hook);
	
	// _PI->WriteLoHook(0x54C50C, ChangeRMGLimits);
//	_PI->WriteLoHook(0x539C73, CreatureBanksListCreation);
//	_PI->WriteLoHook(0x4EF252, gem_Dlg_MainMenu_Create);

	//memcpy(newTable, oldTable, sizeof(H3CreatureBankSetup) *21);
	
	// _PI->WriteLoHook(0x53465D, RMG_ObjGenSimple_Ctor);

	// _PI->WriteLoHook(0x546535, hook_00546535);
	_PI->WriteLoHook(0x539DA5, RMG_New_Utopias);
	// _PI->WriteLoHook(0x546747, hook_00546747);
	_PI->WriteLoHook(0x00546712, hook_00546712);
	// _PI->WriteLoHook(0x0054677F, hook_0054677F);

	_PI->WriteLoHook(0x005407FF, hook_005407FF);
	_PI->WriteLoHook(0x0054087E, hook_0054087E);

	_PI->WriteLoHook(0x00536630, hook_00536630);

	_PI->WriteLoHook(0x00546771, hook_00546771);
	return EXEC_DEFAULT;
}

BOOL plugin_On = 0;

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	// static BOOL plugin_On = 0;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:

		//if (ul_reason_for_call == DLL_PROCESS_ATTACH)
		if (!plugin_On)

		{
			plugin_On = 1;

			subtypemaps::init_maps();
			Era::ConnectEra();
			// init_wog_objs();

			globalPatcher = GetPatcher();
			_PI = globalPatcher->CreateInstance("RMG.daemon.plugin");


			_PI->WriteLoHook(0x4EEAF2, HooksInit);


			//// RMG for towns
			//_PI->WriteByte(0x005452D0 + 2, 27);
			//_PI->WriteByte(0x005452F3 + 2, 27);
			//_PI->WriteLoHook(0x0054C559, hook_0054C559);
			//_PI->WriteLoHook(0x0054A05A, hook_0054A05A);

		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
