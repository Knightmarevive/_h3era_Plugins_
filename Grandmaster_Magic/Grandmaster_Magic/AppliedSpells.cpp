#include "patcher_x86_commented.hpp"
#include "extern.h"


// #include"lib/H3API.hpp"
#include"../../__include__/H3API/single_header/H3API.hpp"

#define o_Spell h3::H3Spell::Get()

int AppliedSpells_Efects[62 - 27 + 1][6] = {
	{90,80,70,60,60, 50}, //shield
	{90,80,70,60,60, 50}, //air shield
	{15,30,45,60,70, 80}, //fire shield
	{75,65,55,45,35, 25}, //prot Air
	{75,65,55,45,35, 25}, //prot Fire
	{75,65,55,45,35, 25}, //prot water
	{75,65,55,45,35, 25}, //prot earth

	{1,2,3,4,5, 6}, //AntiMagic
	{}, // Blank (Dispel?)
	{5,20,35,50,65,80}, // Magic Mirrror
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{0,1,2,3,4,5}, // Bless
	{0,1,2,3,4,5}, // Curse
	{1,3,5,7,9,11}, // Bloodlust
	{1,3,5,7,9,11}, // Precision
	{1,4,7,10,13,16}, // Weakness
	{1,4,7,10,13,16}, // Stone Skin
	{}, // Blank
	{3,4,5,6,7,8}, // Prayer
	{1,2,2,3,3,4}, // Mirth
	{1,2,2,3,3,4}, // Sorrrow
	{1,2,2,3,3,4}, // Fortune
	{1,2,2,3,3,4}, // Misfortune
	{1,2,3,4,5,6}, // Haste
	{1,2,3,4,5,6}, // Slow
	{15,30,45,60,70, 80}, // Slayer
	{50,100,150,200,250,300}, // Frenzy
	{}, // Blank
	{1,2,3,4,5,6}, //Counterstrike
	{0,0,1,2,3,4}, //Berserk
	{120,240,360,480,600,720}, // Hypnotize
	{0,0,0,0,0,0}, // Forgetfullness
	{80,60,40,20,10,0}, // Blind
	/*
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // Blank
	{}, // 71 Poison
	{}, // Blank
	{}, // 73 Disase
	{}, // Blank
	{}, // 75 Age
	// {}, // Blank
	*/
};
bool isAppliedSpell(int spell) {
	if (spell >= 27 && spell <= 34) return true;
	if (spell >= 41 && spell <= 46) return true;
	if (spell >= 48 && spell <= 56) return true;
	if (spell >= 58 && spell <= 62) return true;
	//if (spell == 71 || spell == 73 || spell == 75) return true;
	
	return false;
}

_LHF_(hook_004446E5) {
	// c->edi = AppliedSpells_Efects[*(int*)(c->ebp+8) - 27][c->eax];
	c->edi = Z_Spells[*(int*)(c->ebp + 8)].base_value[c->eax];
	return EXEC_DEFAULT;
}

bool __stdcall hook_0059E360(HiHook* h, int a1, int a2) {
	// int v2; // eax
	if (a1 == 35 && a2 >= 3) return false;

	int v2 = *(int*) &(o_Spell[a1].flags);
	return v2 & 0x10 || v2 & 0x40 && a2 <= 4 || v2 & 0x20 && a2 <= 1;
}

void fix_AppliedSpells_Efects(void) {

	Z_Grandmaster_Magic->WriteLoHook(0x004446E5, hook_004446E5);
	if (set_max_magic_proficiency) {
		for (int i = 27; i <= 34; ++i) set_max_magic_proficiency(i, 5);
		set_max_magic_proficiency(36, 5);
		for (int i = 41; i <= 46; ++i) set_max_magic_proficiency(i, 5);
		for (int i = 48; i <= 56; ++i) set_max_magic_proficiency(i, 5);
		for (int i = 58; i <= 62; ++i) set_max_magic_proficiency(i, 5);
	}
	if (set_max_magic_proficiency) {
		Z_Grandmaster_Magic->WriteHiHook(0x0059E360, SPLICE_, EXTENDED_, FASTCALL_, hook_0059E360);
		//Z_Grandmaster_Magic->WriteByte(0x0059e379 + 2, 5 - 1); // Mass Spell at GrandMaster
		//if (set_max_magic_proficiency) set_max_magic_proficiency(35, 5); // Dispell
		//Z_Grandmaster_Magic->WriteByte(0x00425B21 + 3, 5); // Mass Dispel at GrandMaster
		//Z_Grandmaster_Magic->WriteByte(0x005A84A9 + 2, 5); // Mass Dispel at GrandMaster
	}
}