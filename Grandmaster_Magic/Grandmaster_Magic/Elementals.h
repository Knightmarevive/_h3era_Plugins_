#pragma once
inline long spell2creature(_BattleMgr_* bm, int SpellID) {
    if (!bm) return -1;
    int current_side = bm->currentActiveSide;
    Hero* hero = (Hero*) (bm->hero[current_side]);
    //int school = Spell[SpellID].school;
    //int mastery = bm->hero[current_side]->secSkill[];

    int landModifier = -1; int schoolLevel = 2;

    if(hero){
        landModifier = hero->GetLandModifierUnder();
        schoolLevel = hero->GetSpellSchoolLevel(SpellID, landModifier);
    }
    if (Z_Spells[SpellID].zflag & Z_Spells[SpellID].is_summon)
        return Z_Spells[SpellID].special[schoolLevel];
    else return -1;


    // int SS15[16] = { 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5 };
    int fire[6] = { 114,114,114,129,302,334 };
    int air[6] = { 112,112,112,127,208,332 };
    int water[6] = { 115,115,115,123,224,335 };
    int earth[6] = { 113,113,113,125,201,333 };
    int ghost[6] = { 159,159,159,288,288,279 };

    switch (SpellID)
    {
        /*
    case SPL_SPRITE:
    case SPL_FIREBIRD:
        c->return_address = 0x59F8C0;
        break;
        */
    case SPL_FIRE_ELEMENTAL:
        //c->eax = CID_FIRE_ELEMENTAL;
        return fire[schoolLevel];
        break;
    case SPL_EARTH_ELEMENTAL:
        //c->eax = CID_EARTH_ELEMENTAL;
        return earth[schoolLevel];
        break;
    case SPL_WATER_ELEMENTAL:
        //c->eax = CID_WATER_ELEMENTAL;
        return water[schoolLevel];
        break;
    case SPL_AIR_ELEMENTAL:
        //c->eax = CID_AIR_ELEMENTAL;
        return air[schoolLevel];
        break;
        /*
    case SPL_MAGIC_ELEMENTAL:
        c->eax = CID_MAGIC_ELEMENTAL;
        break;
        */
    case SPL_GHOST:
        return ghost[schoolLevel];
        break;

    default:
        return  ID_NONE;
    }
}

extern void (*set_max_magic_proficiency)(int, int);
inline void elementals_setup() {
    if (set_max_magic_proficiency) {
        set_max_magic_proficiency(66, 5);
        set_max_magic_proficiency(67, 5);
        set_max_magic_proficiency(68, 5);
        set_max_magic_proficiency(69, 5);

        set_max_magic_proficiency(SPL_GHOST, 5);
        // o_Spell[SPL_GHOST].flags.battlefieldSpell = 1;
        // o_Spell[SPL_GHOST].school = (h3::eSpellchool)15;
    }
}