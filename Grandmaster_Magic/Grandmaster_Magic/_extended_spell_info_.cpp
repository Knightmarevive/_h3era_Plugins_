#include"_extended_spell_info_.h"
#include"lib\H3API.hpp"
#include"patcher_x86_commented.hpp"

#include<ctime>

#define o_Spell h3::H3Internal::Spell()
_extended_spell_info_ Z_Spells[256];
constexpr long one_Z_spell_size = sizeof(_extended_spell_info_);
int flags_copy[256];

void ParseFloat(char* buf, char* name, float* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atof(c + strlen(name));
}

void ParseInt(char* buf, char* name, UINT32* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atoi(c + strlen(name));
}

void ParseByte(char* buf, char* name, char* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = (char)atoi(c + strlen(name));
}

bool ParseStr(char* buf, char* name, /*char* &target */ char res[char_table_size])
{
	// char res[char_table_size];
	char* c = strstr(buf, name);
	int l = strlen(name);
	if (c != NULL)
	{
		char* cend = strstr(c + l, "\"");
		if (cend != NULL) //���� ��, ����� ������ �����������
		{
			//*res = (char*)malloc(cend-c+1);

			//memset(*res,0,cend-c+1);
			memset(res, 0, cend - c + 1);

			for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
			{
				if (c[l + j] != '\\')
				{
					//(*res)[i] = c[l+j];
					res[i] = c[l + j];
				}
				else
				{
					if (c[l + j + 1] == 'r')
					{
						//(*res)[i] = '\r';
						res[i] = '\r';

						j++;
						continue;
					}
					else if (c[l + j + 1] == 'n')
					{
						//(*res)[i] = '\n';
						res[i] = '\n';
						j++;
						continue;
					}
					else
					{
						//(*res)[i] = c[l+j];
						res[i] = c[l + j];

					}
				}
			}


			res[cend - c - l] = 0;

		}
		return true;
	}
	else return false;
}
void ParseStr2(char* buf, char* name, char*& target_1, char*& target_2
	/* char res[char_table_size]*/, char* target_static, int& target_int) {

	char res[char_table_size];
	if (ParseStr(buf, name, res)) {
		target_1 = target_2 = target_static;
		strcpy(target_static, res);
		target_int = 0;
	}
}


void DisableSpell(int SpellID, bool Disabled = true);
extern "C" __declspec(dllexport) void ConfigureSpell(int SpellID, char* config) {
	char res[char_table_size]; char name[char_table_size];

	for (int i = 0; i < 8; ++i) {
		sprintf(name,"description%i=\"",i);
		if (ParseStr(config, name, res)) {
			/*
			if(!Z_Spells[SpellID].description[i])
				Z_Spells[SpellID].description[i] = new char[char_table_size];
			*/
			strcpy((char*) Z_Spells[SpellID].description[i], res);
		}

		sprintf(name, "ai_value%i=", i);
		ParseInt(config, name, &o_Spell[SpellID].ai_value[i]);

		//sprintf(name, "special%i=", i);
		//ParseInt(config, name, &Z_Spells[SpellID].special[i]);
		sprintf(name, "mana_cost%i=", i);
		ParseInt(config, name, &Z_Spells[SpellID].mana_cost[i]);
		sprintf(name, "base_value%i=", i);
		ParseInt(config, name, &Z_Spells[SpellID].base_value[i]);
	}
	for (int i = 0; i < 24; ++i) {
		sprintf(name, "special%i=", i);
		ParseInt(config, name, &Z_Spells[SpellID].special[i]);
	}
	for (int i = 0; i < 36; ++i) {
		sprintf(name, "chance_to_get%i=", i);
		ParseInt(config, name, &Z_Spells[SpellID].chance_to_get[i]);
	}
	ParseInt(config, (char*)"target=", (PUINT32)&Z_Spells[SpellID].target);
	ParseInt(config, (char*)"zflag=", (PUINT32)&Z_Spells[SpellID].zflag);
	ParseInt(config, (char*)"school=", (PUINT32)&o_Spell[SpellID].school);
	ParseInt(config, (char*)"level=", (PUINT32)&o_Spell[SpellID].level);
	ParseInt(config, (char*)"flags=", (PUINT32)&o_Spell[SpellID].flags);
	ParseInt(config, (char*)"flags=", (PUINT32) &(flags_copy[SpellID]));
	ParseInt(config, (char*)"anim=", ((PUINT32)&o_Spell[SpellID])+2);
	ParseInt(config, (char*)"type=", (PUINT32)&o_Spell[SpellID].type);
	ParseInt(config, (char*)"sp_effect=", (PUINT32)&o_Spell[SpellID].sp_effect);
	if (ParseStr(config, (char*)"soundname=\"", res)) {
		if (!o_Spell[SpellID].soundName) o_Spell[SpellID].soundName = new char[char_table_size];
		strcpy((char*)o_Spell[SpellID].soundName, res);
	}
	if (ParseStr(config, (char*)"longname=\"", res)) {
		if (!o_Spell[SpellID].name) o_Spell[SpellID].name = new char[char_table_size];
		strcpy((char*)o_Spell[SpellID].name, res);
	}
	if (ParseStr(config, (char*)"shortname=\"", res)) {
		if (!o_Spell[SpellID].shortName) o_Spell[SpellID].shortName = new char[char_table_size];
		strcpy((char*)o_Spell[SpellID].shortName, res);
	}
	if (ParseStr(config, (char*)"command=\"", res)) {
		strcpy(Z_Spells[SpellID].command, res);
	}

	if (strstr(config, "[[[disabled]]]")) {
		Z_Spells[SpellID].zflag |= _extended_spell_info_::is_disabled;
		DisableSpell(SpellID);
		// o_Spell[SpellID].school = h3::H3Spell::eSchool(0);
		o_Spell[SpellID].flags.battlefield_spell = 0;
		o_Spell[SpellID].flags.map_spell = 0;
		o_Spell[SpellID].flags.creature_spell = 0;
	}
}

extern "C" __declspec(dllexport) char* GetSpellDescription(int SpellID, int mastery) {
	return Z_Spells[SpellID].description[mastery];
}

extern "C" __declspec(dllexport) void DumpSpell(int SpellID) {

	static char fname[1024];
	sprintf(fname, "Debug\\Era\\spells\\%u.cfg", SpellID);
	CreateDirectoryA("Debug\\Era\\spells", NULL);
	if (FILE* fdesc = fopen(fname, "w")) {
		if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_disabled)
			fprintf(fdesc, "[[[disabled]]]\n\n");

		// fprintf(fdesc, "");
		fprintf(fdesc, "longname=\"%s\"\n", o_Spell[SpellID].name);
		fprintf(fdesc, "shortname=\"%s\"\n", o_Spell[SpellID].shortName);
		fprintf(fdesc, "soundname=\"%s\"\n", o_Spell[SpellID].soundName);
		for (int i = 0; i < 8; ++i)
			fprintf(fdesc, "description%i=\"%s\"\n", i, Z_Spells[SpellID].description[i]);
		fprintf(fdesc, "sp_effect=%i\n", o_Spell[SpellID].sp_effect);
		for (int i = 0; i < 8; ++i)
			fprintf(fdesc, "base_value%i=%i\n", i, Z_Spells[SpellID].base_value[i]);
		for (int i = 0; i < 8; ++i)
			fprintf(fdesc, "mana_cost%i=%i\n", i, Z_Spells[SpellID].mana_cost[i]);
		for (int i = 0; i < 24; ++i)
			fprintf(fdesc, "special%i=%i\n", i, Z_Spells[SpellID].special[i]);
		for (int i = 0; i < 8; ++i)
			fprintf(fdesc, "ai_value%i=%i\n", i, o_Spell[SpellID].ai_value[i]);

		if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_disabled)
			fprintf(fdesc, (char*)"flags=%i\n", flags_copy[SpellID]);
		else fprintf(fdesc, (char*)"flags=%i\n", o_Spell[SpellID].flags);

		fprintf(fdesc, (char*)"target=%i\n", Z_Spells[SpellID].target);
		fprintf(fdesc, (char*)"zflag=%i\n", Z_Spells[SpellID].zflag 
			& ~_extended_spell_info_::is_disabled);
		fprintf(fdesc, (char*)"school=%i\n", o_Spell[SpellID].school);
		fprintf(fdesc, (char*)"level=%i\n", o_Spell[SpellID].level);
		fprintf(fdesc, (char*)"anim=%i\n", ((PUINT32)&o_Spell[SpellID])[2]);
		fprintf(fdesc, (char*)"type=%i\n", o_Spell[SpellID].type);

		fprintf(fdesc, (char*)"command=\"%s\"\n", Z_Spells[SpellID].command);

		fprintf(fdesc, "\n\n");
		for (int i = 0; i < 36; ++i) {
			fprintf(fdesc, "chance_to_get%i=%i\n", i, Z_Spells[SpellID].chance_to_get[i]);
		}
		fclose(fdesc);
	}
}
void DumpAllSpells() {
	for (int i = 0; i < 200; ++i) DumpSpell(i);
}

void fill_special(int spell) {
	switch (spell)
	{
		case 8:
			for (int i = 0; i < 8; ++i) Z_Spells[spell].special[i] = i + 2;
			break;
		case 9:
			Z_Spells[9].special[0] = 350;
			Z_Spells[9].special[1] = 300;
			Z_Spells[9].special[2] = 250;
			Z_Spells[9].special[3] = 200;
			Z_Spells[9].special[4] = 150;
			Z_Spells[9].special[5] = 100;
			Z_Spells[9].special[6] = 50;
			Z_Spells[9].special[7] = 25;
			break;

		case 10:
			Z_Spells[10].special[0] = 4;
			Z_Spells[10].special[1] = 4;
			Z_Spells[10].special[2] = 6;
			Z_Spells[10].special[3] = 8;
			Z_Spells[10].special[4] = 10;
			Z_Spells[10].special[5] = 12;
			Z_Spells[10].special[6] = 14;
			Z_Spells[10].special[7] = 16;
			break;

		case 11:
			Z_Spells[11].special[0] = 4;
			Z_Spells[11].special[1] = 4;
			Z_Spells[11].special[2] = 6;
			Z_Spells[11].special[3] = 8;
			Z_Spells[11].special[4] = 10;
			Z_Spells[11].special[5] = 12;
			Z_Spells[11].special[6] = 14;
			Z_Spells[11].special[7] = 16;
			break;

		case 19:
			Z_Spells[19].special[0] = 4;
			Z_Spells[19].special[1] = 4;
			Z_Spells[19].special[2] = 5;
			Z_Spells[19].special[3] = 5;
			Z_Spells[19].special[4] = 6;
			Z_Spells[19].special[5] = 7;
			Z_Spells[19].special[6] = 8;
			Z_Spells[19].special[7] = 9;
			break;
		case 65:
			Z_Spells[65].special[0] = 1;
			Z_Spells[65].special[1] = 1;
			Z_Spells[65].special[2] = 1;
			Z_Spells[65].special[3] = 1;
			Z_Spells[65].special[4] = 2;
			Z_Spells[65].special[5] = 3;
			Z_Spells[65].special[6] = 4;
			Z_Spells[65].special[7] = 5;
			break;
		case 66:
			for (int i = 0; i < 8; ++i) Z_Spells[spell].special[i] = 114;
			break;
		case 67:
			for (int i = 0; i < 8; ++i) Z_Spells[spell].special[i] = 113;
			break;
		case 68:
			for (int i = 0; i < 8; ++i) Z_Spells[spell].special[i] = 115;
			break;
		case 69:
			for (int i = 0; i < 8; ++i) Z_Spells[spell].special[i] = 112;
			break;
	}
}

// void DisableSpell(int SpellID, bool Disabled = true);
void _extended_spell_info_::clean() {
	static char fname[1024];
	static bool firsttime = true;
	for (int i = 81; i < 200; ++i) {
		sprintf(fname, "Data\\spells\\%u.cfg", i);
		//FILE* fdesc;
		if (FILE* fdesc = fopen(fname, "r")) {
			//----------
			fseek(fdesc, 0, SEEK_END);
			int fdesc_size = ftell(fdesc);
			rewind(fdesc);
			//----------
			char* buf = (char*)malloc(fdesc_size + 1);
			memset(buf, 0, fdesc_size + 1);
			fread(buf, 1, fdesc_size, fdesc);
			buf[fdesc_size] = 0;
			fclose(fdesc);
			bool disabled = strstr(buf, "[[[disabled]]]");
			free(buf);

			if (disabled) goto disabled;
		}
		else
		{
			disabled:
			DisableSpell(i);
			// o_Spell[i].school = h3::H3Spell::eSchool(0);
			o_Spell[i].flags.battlefield_spell = 0;
			o_Spell[i].flags.map_spell = 0;
			o_Spell[i].flags.creature_spell = 0;
		}
	}
}

static char null_text[] = "NULL SPELL";
void _extended_spell_info_::read() {

	static char fname[1024];
	static bool firsttime = true;

	if (firsttime) {
		Z_Spells[62].zflag |= _extended_spell_info_::dispel_like_blind;
		Z_Spells[70].zflag |= _extended_spell_info_::dispel_like_blind;
		Z_Spells[74].zflag |= _extended_spell_info_::dispel_like_blind;
	}

	for (int i = 0; i < 200; ++i) {
		sprintf(fname, "Data\\spells\\%u.cfg", i);
		//FILE* fdesc;
		if (FILE* fdesc = fopen(fname, "r"))
		{
			//----------
			fseek(fdesc, 0, SEEK_END);
			int fdesc_size = ftell(fdesc);
			rewind(fdesc);
			//----------
			char* buf=(char*)malloc(fdesc_size+1);
			memset(buf, 0, fdesc_size + 1);
			fread(buf, 1, fdesc_size, fdesc);
			buf[fdesc_size]=0;
			fclose(fdesc);

			ConfigureSpell(i, buf);

			// o_Spell[i].ai_value[4] = o_Spell[i].ai_value[3];
			// o_Spell[i].ai_value[5] = o_Spell[i].ai_value[3];
			// o_Spell[i].ai_value[6] = o_Spell[i].ai_value[3];
			// o_Spell[i].ai_value[7] = o_Spell[i].ai_value[3];

			free(buf);
			fclose(fdesc);
		} else {
			fill_special(i);


			Z_Spells[i].base_value[0] = o_Spell[i].base_value[0];
			Z_Spells[i].base_value[1] = o_Spell[i].base_value[1];
			Z_Spells[i].base_value[2] = o_Spell[i].base_value[2];
			Z_Spells[i].base_value[3] = o_Spell[i].base_value[3];
			Z_Spells[i].base_value[4] = o_Spell[i].base_value[3];
			Z_Spells[i].base_value[5] = o_Spell[i].base_value[3];
			Z_Spells[i].base_value[6] = o_Spell[i].base_value[3];
			Z_Spells[i].base_value[7] = o_Spell[i].base_value[3];

			Z_Spells[i].mana_cost[0] = o_Spell[i].mana_cost[0];
			Z_Spells[i].mana_cost[1] = o_Spell[i].mana_cost[1];
			Z_Spells[i].mana_cost[2] = o_Spell[i].mana_cost[2];
			Z_Spells[i].mana_cost[3] = o_Spell[i].mana_cost[3];
			Z_Spells[i].mana_cost[4] = o_Spell[i].mana_cost[3];
			Z_Spells[i].mana_cost[5] = o_Spell[i].mana_cost[3];
			Z_Spells[i].mana_cost[6] = o_Spell[i].mana_cost[3];
			Z_Spells[i].mana_cost[7] = o_Spell[i].mana_cost[3];

			if(firsttime && (o_Spell[i].description[0]))
			{
				strcpy(Z_Spells[i].description[0], o_Spell[i].description[0]);
				strcpy(Z_Spells[i].description[1], o_Spell[i].description[1]);
				strcpy(Z_Spells[i].description[2], o_Spell[i].description[2]);
				strcpy(Z_Spells[i].description[3], o_Spell[i].description[3]);
				strcpy(Z_Spells[i].description[4], o_Spell[i].description[3]);
				strcpy(Z_Spells[i].description[5], o_Spell[i].description[3]);
				strcpy(Z_Spells[i].description[6], o_Spell[i].description[3]);
				strcpy(Z_Spells[i].description[7], o_Spell[i].description[3]);
			}

			o_Spell[i].ai_value[4] = o_Spell[i].ai_value[3];
			o_Spell[i].ai_value[5] = o_Spell[i].ai_value[3];
			o_Spell[i].ai_value[6] = o_Spell[i].ai_value[3];
			o_Spell[i].ai_value[7] = o_Spell[i].ai_value[3];

			if (i > 80) 
			{ 
				DisableSpell(i);
				o_Spell[i].school = h3::H3Spell::eSchool(0);
				o_Spell[i].flags.battlefield_spell = 0;
				o_Spell[i].flags.map_spell= 0;
				o_Spell[i].flags.creature_spell = 0;
			}

			for (int j = 0; j < 9; ++j)
				Z_Spells[i].chance_to_get[j] = o_Spell[i].chance_to_get[j];

			Z_Spells[i].command[0] = 0;
		}
		DumpSpell(i);

	}
	firsttime = false;
}

/*
_LHF_(hook_004F14DF) {
	c->ecx = (int)&Z_Spells[c->ecx].description[];
	c->Push(-1);
	c->return_address = 0x004F14DF;
	return NO_EXEC_DEFAULT;
}
*/
_LHF_(hook_004F14CC) {
	c->Push(0); c->Push(-1);	c->Push(0); c->Push(-1);
	c->ecx = (int) Z_Spells[c->ecx].description[0];
	c->return_address = 0x004F14E3;
	return NO_EXEC_DEFAULT;
}
/*
_LHF_(hook_0059BE0F) {
	c->eax = 0;
	c->edx = (int)&Z_Spells[*(int*)(c->ebp + 0xC)].description[c->ecx];
	c->return_address = 0x0059BE15;
	return NO_EXEC_DEFAULT;
}
*/
/*
_LHF_(hook_005CE912) {
	c->ecx = (int)&Z_Spells[c->eax].description;
	c->Push(-1);
	c->return_address = 0x005CE918;
	return NO_EXEC_DEFAULT;
}
*/
_LHF_(hook_005CE903) {
	c->Push(-1);
	c->ecx = (int)&Z_Spells[c->eax].description[0];
	c->edx &= 3;
	c->return_address = 0x005CE916;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_004E5541) {
	//c->ecx = (long(Z_Spells[*(int*)(c->ebp + 8)].mana_cost)) - 0x20;
	//return EXEC_DEFAULT;

	// c->flags.ZF = ! c->edi;
	c->esi = Z_Spells[*(int*)(c->ebp + 8)].mana_cost[c->eax];	
	if(c->edi) c->return_address = 0x004E554C;
	else c->return_address = 0x004E558C;
	return NO_EXEC_DEFAULT;
}
_LHF_(hook_004E58C4) {
	c->ebx = c->Pop();
	c->eax = Z_Spells[0].mana_cost[c->eax];
	c->return_address = 0x004E58C9;
	return NO_EXEC_DEFAULT;
}

/*
_LHF_(random_hook) {
	// c->eax = CDECL_2(int, 0x714A99, c->ecx, c->edx);
	c->eax = c->ecx + rand() % (c->edx - c->ecx);

	c->return_address = h->GetAddress() + 5;
	return NO_EXEC_DEFAULT;
}
*/

_LHF_(hook_005BEA22) {
	
	//UINT seed = THISCALL_0(UINT,0x004F8970);
	//h3::H3Random::SetRandomSeed(seed);
	//h3::H3Random::SetRandomSeed();
	//THISCALL_1(DWORD,0x0050C7B0, seed);
	//*(int*)(0x0067FBE4) = seed;

	_extended_spell_info_::read();
	auto& debug_1 = Z_Spells;
	return EXEC_DEFAULT;
}

_LHF_(hook_005BEAF1) {
	// c->edx = Z_Spells[c->ebx / 0x88].chance_to_get[*(char*)((*(int*)(c->ebp - 4)) + 4)];
	h3::H3Town* town = (h3::H3Town*) (*(int*)(c->ebp - 4));
	int spell_ID = c->esi;
	c->edx = Z_Spells[spell_ID].chance_to_get[town->type];

	return EXEC_DEFAULT;
}
_LHF_(hook_005BEBEE) {
	//c->ebx -= Z_Spells[c->edi / 0x88].chance_to_get[*(char*)((*(int*)(c->ebp - 4))+4)];
	h3::H3Town* town = (h3::H3Town*)(*(int*)(c->ebp - 4));
	int spell_ID = c->esi;
	c->ebx -= Z_Spells[spell_ID].chance_to_get[town->type];

	if (c->ebx <=0)
		c->return_address = 0x005BEC0E;
	else c->return_address = 0x005BEBF6;
	return NO_EXEC_DEFAULT;
}

inline signed int __cdecl ApplyERMArgument(int a1, char a2, int a3, char a4) {
	return CALL_4(signed int, __cdecl, 0x0074195D, a1, a2, a3, a4);
}

inline void __cdecl ErrorMessage(int a1, int a2, int a3) {
	return CALL_3(void, __cdecl, 0x00712333, a1, a2, a3);
}

int ERM_ZSpellDesc[200][8];

// #define RETURN(x) {PEr::Del();return(x);}
#define RETURN(x) return(x);

#define ERMString(arg) (char*)(0x9271E8+(arg<<9))

static char* _GetText_(int ind, int tp) {
// #include "templ.h"
	char* po = NULL;
	int vv = ERM_ZSpellDesc[ind][tp];
	if (vv) {
		if (vv > 1000)   po = CALL_1(char*, __cdecl,0x776620,vv);// StringSet::GetText(vv);
		else if (vv > 0) po = ERMString(vv);// ERMString[vv - 1];
	}
	RETURN(po)
}

#define ret0 c->return_address = 0x0077520C; return NO_EXEC_DEFAULT;
_LHF_(ERM_00775249) {
	
	char letter = c->ecx + 'A';
	int a2 = *(int*)(c->ebp + 0x0C);
	int a3 = *(int*)(c->ebp + 0x10);
	int a4 = *(int*)(c->ebp + 0x14);
	
	signed int result; // eax
	DWORD* v5 = (DWORD*) *(int*)(c->ebp - 0x10);  // [esp+10h] [ebp-10h]
	int &v6 = *(int*)(c->ebp - 0x0C); // [esp+14h] [ebp-Ch]
	int &v7 = *(int*)(c->ebp - 0x08); // [esp+18h] [ebp-8h]
	int &v8 = *(int*)(c->ebp - 0x04); // [esp+1Ch] [ebp-4h]

	_extended_spell_info_* z_Spell = Z_Spells + v8;

	switch (letter) 
	{
	case 'A':case 'F':case 'L':case 'O':case 'P': case 'S': case 'W': case 'X':
		return EXEC_DEFAULT;
	case 'C':
		if (a2 < 2)
		{
			ErrorMessage(21, 74, (int)"\"SS:C\"- wrong syntax.");
			ret0; //return 0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 7)
		{
			ErrorMessage(21, 76, (int)"\"SS:C\"- wrong index.");
			ret0; //return 0;
		}
		// ApplyERMArgument((int)&v5[v6 + 8], 4, a4, 1);
		ApplyERMArgument((int)&z_Spell->mana_cost[v6], 4, a4, 1);
		break;

	case 'D':
		if (a2 < 2)
		{
			ErrorMessage(21, 106, (int)"\"SS:D\"- wrong syntax.");
			ret0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 7)
		{
			ErrorMessage(21, 108, (int)"\"SS:D\"- wrong index.");
			ret0;
		}
		// if (!ApplyERMArgument((int)&unk_28B1C58 + 28 * v8 + 4 * v6, 4, a4, 1))
		if (!ApplyERMArgument((int)& ERM_ZSpellDesc[ v8][ v6], 4, a4, 1))
		{
			// v7 = sub_775702(v8, v6 + 2);
			v7 = (int) _GetText_(v8, v6);

			// if (v7)		v5[v6 + 30] = v7;
			// if (v7) z_Spell->description[v6] = v7;

			if (v7) strcpy_s(z_Spell->description[v6], (char*) v7);
		}
		break;
	case 'E':
		if (a2 < 2)
		{
			ErrorMessage(21, 85, (int)"\"SS:E\"- wrong syntax.");
			ret0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 7)
		{
			ErrorMessage(21, 87, (int)"\"SS:E\"- wrong index.");
			ret0;
		}
		// ApplyERMArgument((int)&v5[v6 + 13], 4, a4, 1);
		ApplyERMArgument((int)&z_Spell->base_value[v6], 4, a4, 1);

		break;
	case 'H':

		if (a2 < 2)
		{
			ErrorMessage(21, 92, (int)"\"SS:H\"- wrong syntax.");
			ret0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 35)
		{
			ErrorMessage(21, 94, (int)"\"SS:H\"- wrong index.");
			ret0;
		}
		// ApplyERMArgument((int)&v5[v6 + 17], 4, a4, 1);
		ApplyERMArgument((int)&z_Spell[v6].chance_to_get, 4, a4, 1);
		break;
	case'I':
		if (a2 >= 2)
		{
			ApplyERMArgument((int)&v6, 4, a4, 0);
			if (v6 >= 0 && v6 <= 7)
			{
				ApplyERMArgument((int)&v5[v6 + 26], 4, a4, 1);
			LABEL_57:
				break;//result = 1;
			}
			else
			{
				ErrorMessage(21, 101, (int)"\"SS:I\"- wrong index.");
				ret0;// result = 0;
			}
		}
		else
		{
			ErrorMessage(21, 99, (int)"\"SS:I\"- wrong syntax.");
			ret0; // result = 0;
		}
		break;

	case 'Z':

		if (a2 != 1)
		{
			ErrorMessage(21, 85, (int)"\"SS:Z\"- wrong syntax.");
			ret0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 999)
		{
			ErrorMessage(21, 94, (int)"\"SS:Z\"- wrong Z-Var index.");
			ret0;
		}
		ConfigureSpell(v8,ERMString(v6));
		break;

	case 'Q':
		if (a2 < 2)
		{
			ErrorMessage(21, 74, (int)"\"SS:Q\"- wrong syntax.");
			ret0; //return 0;
		}
		ApplyERMArgument((int)&v6, 4, a4, 0);
		if (v6 < 0 || v6 > 63)
		{
			ErrorMessage(21, 76, (int)"\"SS:Q\"- wrong index.");
			ret0; //return 0;
		}
		// ApplyERMArgument((int)&v5[v6 + 8], 4, a4, 1);
		ApplyERMArgument((int)&z_Spell->special[v6], 4, a4, 1);
		break;
	case 'T':
		if (a2 != 1)
		{
			ErrorMessage(21, 74, (int)"\"SS:T\"- wrong syntax.");
			ret0; //return 0;
		}		
		ApplyERMArgument((int)&z_Spell->target, 4, a4, 1);
		break;

	default:
		return EXEC_DEFAULT;
	}

	c->return_address = 0x77569D;
	return NO_EXEC_DEFAULT;

}

extern PatcherInstance* Z_Grandmaster_Magic;
void _extended_spell_info_::hook() {
	Z_Grandmaster_Magic->WriteLoHook(0x004F14CC, hook_004F14CC);	// Z_Grandmaster_Magic->WriteLoHook(0x004F14DF, hook_004F14DF);
	// Z_Grandmaster_Magic->WriteLoHook(0x0059BE0F, hook_0059BE0F);
	Z_Grandmaster_Magic->WriteLoHook(0x005CE903, hook_005CE903);	// Z_Grandmaster_Magic->WriteLoHook(0x005CE912, hook_005CE912);

	Z_Grandmaster_Magic->WriteLoHook(0x004E5541, hook_004E5541);
	Z_Grandmaster_Magic->WriteLoHook(0x004E58C4, hook_004E58C4);

	// srand(time(NULL));
	// Z_Grandmaster_Magic->WriteLoHook(0x005BEB9F, random_hook);
	Z_Grandmaster_Magic->WriteLoHook(0x005BEA22, hook_005BEA22);
	Z_Grandmaster_Magic->WriteLoHook(0x005BEAF1, hook_005BEAF1);
	Z_Grandmaster_Magic->WriteLoHook(0x005BEBEE, hook_005BEBEE);

	Z_Grandmaster_Magic->WriteLoHook(0x00775249, ERM_00775249);
	Z_Grandmaster_Magic->WriteByte(0x00775235 + 3, 'Z' - 'A');
}