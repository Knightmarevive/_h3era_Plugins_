#include "patcher_x86_commented.hpp"
#include "extern.h"
int Cure_Spell_Effect[8] = {30,60,90,120,150,180,210,240};
int Ressurection_or_Animate_Effect[8] = { 120, 240, 360, 480, 600, 720, 840, 960 };
int Sacrifice_Ressurrection_Effect[8] = { 3,6,9,12,15,18,21,24};

_LHF_(hook_005A1C5E) {
	c->ecx = *(int*)(c->eax + c->ecx*8 +0x30);
	c->eax = Ressurection_or_Animate_Effect[c->edx];
	c->eax = Ressurection_or_Animate_Effect[c->edx];
	c->return_address = 0x005A1C66;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A1CF6) {
	c->ecx = *(int*)(c->ebp - 10);
	c->edx += Sacrifice_Ressurrection_Effect[c->esi];

	c->return_address = 0x005A1CFD;
	return NO_EXEC_DEFAULT;
}

void fix_healing_spells(void) {
	Z_Grandmaster_Magic->WriteDword(0x0044630B + 3, (int)Cure_Spell_Effect);
	Z_Grandmaster_Magic->WriteWord(0x0044630B + 1, 0x953c);
	if (set_max_magic_proficiency) set_max_magic_proficiency(37, 5);

	Z_Grandmaster_Magic->WriteLoHook(0x005A1C5E, hook_005A1C5E);
	if (set_max_magic_proficiency) set_max_magic_proficiency(38, 5);
	if (set_max_magic_proficiency) set_max_magic_proficiency(39, 5);

	Z_Grandmaster_Magic->WriteLoHook(0x005A1CF6, hook_005A1CF6);
	if (set_max_magic_proficiency) set_max_magic_proficiency(40, 5);

}