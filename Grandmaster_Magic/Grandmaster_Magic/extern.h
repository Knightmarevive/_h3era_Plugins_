#pragma once
#include"_extended_spell_info_.h"

extern PatcherInstance* Z_Grandmaster_Magic;

extern HMODULE More_SS_Levels_Link;
// extern int (*get_real_magic_proficiency)(HERO*, int, int);
extern void (*set_max_magic_proficiency)(int, int);
extern int  (*get_max_magic_proficiency)(int);

extern int   (*get_SS_table_zi)(int skill, int level, int type);
extern float (*get_SS_table_zf)(int skill, int level, int type);

// extern char (*_get_hero_SS_)(HERO*, int);