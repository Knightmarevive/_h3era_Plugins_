#include "patcher_x86_commented.hpp"
#include "extern.h"
#include "__include/heroes.h"


#include "Hero_Specialties.h"
H3HeroSpecialty& P_HeroSpecialty(int id);
bool isHeroSpecialistOfClone(int hero_id) {

	auto& spec = P_HeroSpecialty(hero_id);
	if (spec.type == spec.ST_spell
		&& spec.bonusID == SPELL_CLONE)
			return true;
	
	char buf[256];
	return false;
}
int(__thiscall* SummonClone)(void* combat_manager, int gex, int spellpower) = (int(__thiscall*)(void*, int, int))(0x005A6F80);
_LHF_(Hook_005A202B) {
	if (*(int*)(c->ebp+0x10) != 0) return EXEC_DEFAULT;
	int level = *(int*)(c->ebp - 0x44); HERO* hero = (HERO*)*(int*)(c->ebp - 0x14);
	/*
	if (hero && isHeroSpecialistOfClone(hero->Number) )
		SummonClone((void*)c->ebx, *(int*)(c->edi + 0x38), c->esi);
	if (level >= 5) SummonClone((void*)c->ebx, *(int*)(c->edi + 0x38), c->esi);
	if (level >= 4) SummonClone((void*)c->ebx, *(int*)(c->edi + 0x38), c->esi);
	if (true)		SummonClone((void*)c->ebx, *(int*)(c->edi + 0x38), c->esi);
	*/
	for (int i=0;i<Z_Spells[65].special[level];++i)
		SummonClone((void*)c->ebx, *(int*)(c->edi + 0x38), c->esi);

	c->return_address = 0x005A2037;
	return NO_EXEC_DEFAULT;
}


void fix_summonClone(void) {

	Z_Grandmaster_Magic->WriteLoHook(0x005A202B, Hook_005A202B);
	if (set_max_magic_proficiency) set_max_magic_proficiency(65, 5);
}