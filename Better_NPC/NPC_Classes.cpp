#include "Header.h"
extern PatcherInstance* _PI;
#include "Call_Helper.h"
#include <map>
#include <string>
#include "cstring"

#define NewNPCClassCount 64

int Attacker[NewNPCClassCount] = {};
int Defender[NewNPCClassCount] = {};

static char SpecBmp[NewNPCClassCount][2][32];
static char* SpecText[NewNPCClassCount][2];
static char* SpecHint[NewNPCClassCount][2];
static char* Descr[NewNPCClassCount];
static char* Magics[NewNPCClassCount];

static void prepare_Str(char*& str, char* src = nullptr) {
	str = new char[512];
	str[511] = 0;
	if ((int)src>1024) 
		strcpy(str, src);
}

_LHF_(hook_0076C04D) {
	c->ecx = c->eax ? Defender[c->edx] : Attacker[c->edx];
	c->return_address = 0x0076C057;
	return SKIP_DEFAULT;
}

_LHF_(hook_0076C09E) {
	c->ecx = c->eax ? Defender[c->edx] : Attacker[c->edx];
	c->return_address = 0x0076C0A8;
	return SKIP_DEFAULT;
}

extern bool is_WND_enabled;
_LHF_(hook_0076AA22) {
	if (is_WND_enabled) c->eax = c->edx * 2;
	else c->eax = (int) SpecBmp[c->edx][0];
	return SKIP_DEFAULT;

	// c->eax = (int) SpecBmp[c->edx][0];
	_PI->WriteAddressOf(0x2860528, SpecBmp[c->edx][0]);
	c->return_address = 0x0076AA2E;
	return SKIP_DEFAULT;
}
_LHF_(hook_0076AA58) {
	if (is_WND_enabled) c->eax = c->edx * 2 + 1;
	else c->eax = (int)SpecBmp[c->edx][1];
	return SKIP_DEFAULT;

	// c->eax = (int) SpecBmp[c->edx][1];
	_PI->WriteAddressOf(0x2860538, SpecBmp[c->edx][1]);
	c->return_address = 0x0076AA64;
	return SKIP_DEFAULT;
}


_LHF_(hook_0076BF0F) {
	int& side = *(int*)(c->ebp - 0x0C);
	int creatureID = *(int*)(c->ebp - 0x04);

	side = -1;
	for (auto a : Attacker) if (creatureID == a) side = 0;
	for (auto d : Defender) if (creatureID == d) side = 1;
	if (side == -1) {
		h3::H3Messagebox("hook_0076BF0F: Wrong Commander ID");
		side = 1;
	}

	c->return_address = 0x0076BF26;
	return SKIP_DEFAULT;
}

void install_NPC_classes() {
	

	static bool done = false;
	if (done) return;

	for (int i = 0; i < NewNPCClassCount; ++i) {
		Attacker[i] = 174 + i % 9;
		Defender[i] = 183 + i % 9;

		// prepare_Str(SpecBmp[i][0], ((char**)0x007A2228)[i % 9 * 2]);
		// prepare_Str(SpecBmp[i][1], ((char**)0x007A222C)[i % 9 * 2]);

		// prepare_Str(SpecBmp[i][0],	(char*)(0x007A1FE8 + i % 9 * 64));
		// prepare_Str(SpecBmp[i][1],	(char*)(0x007A2008 + i % 9 * 64));
		strcpy(SpecBmp[i][0], (char*)(0x007A1FE8 + i % 9 * 64));
		strcpy(SpecBmp[i][1], (char*)(0x007A2008 + i % 9 * 64));

		prepare_Str(SpecText[i][0], ((char**)0x028608C0)[i % 9 * 2]);
		prepare_Str(SpecText[i][1], ((char**)0x028608C4)[i % 9 * 2]);
		prepare_Str(SpecHint[i][0], ((char**)0x028606B8)[i % 9 * 2]);
		prepare_Str(SpecHint[i][1], ((char**)0x028606BC)[i % 9 * 2]);
		prepare_Str(Descr[i],		((char**)0x02860700)[i % 9]);
		prepare_Str(Magics[i],		((char**)0x028607D8)[i % 9]);
	}


	_PI->WriteLoHook(0x0076C04D, hook_0076C04D);
	_PI->WriteLoHook(0x0076C09E, hook_0076C09E);

	_PI->WriteLoHook(0x0076BF0F, hook_0076BF0F);

	_PI->WriteDword(0x0076B308 + 3, (int)Magics);
	_PI->WriteDword(0x0076A602 + 3, (int)Descr);

	/*
	static auto SpecBmp0 = (int) &SpecBmp[0][0];
	static auto SpecBmp1 = (int) &SpecBmp[0][1];
	_PI->WriteDword(0x007A2228, (int) SpecBmp0);
	_PI->WriteDword(0x007A222C, (int) SpecBmp1);
	*/
	_PI->WriteLoHook(0x0076AA22, hook_0076AA22);
	_PI->WriteLoHook(0x0076AA58, hook_0076AA58);

	_PI->WriteDword(0x0076AA34 + 3, 0 + (int)SpecText);
	_PI->WriteDword(0x0076AA6A + 3, 4 + (int)SpecText);

	_PI->WriteDword(0x0076AA46 + 3, 0 + (int)SpecHint);
	_PI->WriteDword(0x0076AA7C + 3, 4 + (int)SpecHint);

	done = true;
}