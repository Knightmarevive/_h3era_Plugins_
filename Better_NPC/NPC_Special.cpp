#include "Header.h"

int CommanderPoints[2];

_Npc_* GetNpc(int hero_id);

int last_round;
int last_stack;

void NPC_BeforeBattle() {
	last_round = -1; last_stack = -1;
	CommanderPoints[0] = CommanderPoints[1] = 0;
}

bool CanSummonOrClone(int side) 
{
	auto batman = h3::H3CombatManager::Get();
	if (batman->currentActiveSide != side	) return false;
	INT i = 0;
	h3::H3CombatMonster* mon = batman->stacks[side];
	while (mon->type >= 0)
	{
		if (mon->numberAlive <= 0 /* && mon->info.cannotMove */ 
			&& (mon->IsSummon() || mon->IsClone() )
			) {
			// mon->type = -1;
			break;
		}
		++mon;
		if (++i >= 20)
			return FALSE;
	}
	return TRUE;
}

bool NPC_has_mage(int side) {
	auto batman = h3::H3CombatManager::Get();
	auto he = batman->hero[side];
	if (he->id < 0) return false;

	for (auto &i : batman->stacks[side]) {
		
		switch (side) {
		case 0:
			if (i.numberAlive > 0 )
				if(i.type == 176 || i.type == 179 || i.type == 182)
					return true;
			break;
		case 1:
			if (i.numberAlive > 0 )
				if (i.type == 185 || i.type == 188 || i.type == 191)
					return true;
			break;

		default:
			return false;
		}
	}
	return false;
}
h3::H3CombatCreature* NPC_get_mirage(int side) {
	auto batman = h3::H3CombatManager::Get();
	for (auto& i : batman->stacks[side]) {

		switch (side) {
		case 0:
			if (i.numberAlive > 0 && i.type == 181 && !i.IsClone())
				return &i;
			break;
		case 1:
			if (i.numberAlive > 0 && i.type == 190 && !i.IsClone())
				return &i;
			break;
	
		default:
			return nullptr;
		}

	}
	return nullptr;
}

void NPC_clean_mirage(int side) {
	auto batman = h3::H3CombatManager::Get();
	auto performer = NPC_get_mirage(side);
	if (!performer) return;
	// performer->cloneId = -1;
	performer->info.clone = 0;

	for (auto& i : batman->stacks[side]) {
		if (i.sideIndex != performer->sideIndex && i.type == performer->type) {
			i.faerieDragonSpell = 37;
			i._f_24 = i.sideIndex;
			i.info.clone = 1;
		}
	}

}

bool NPC_try_mirage( int side) {
	auto batman = h3::H3CombatManager::Get();
	if (!batman) return false;
	if (batman->tacticsPhase) return false;

	char txt_buf[256] = "";
	h3::H3CombatCreature* bm = nullptr;

	bm = NPC_get_mirage(side);
	while (bm && bm->sideIndex == batman->currentMonIndex
		&& CommanderPoints[side] >= 15
		// && side == batman->currentMonSide
		&& CanSummonOrClone(side)) {
		sprintf(txt_buf, "%s performs mirage ritual.", bm->info.nameSingular);
		CommanderPoints[side] -= 15;
		// THISCALL_4(void, 0x4729D0, batman->dlg, txt_buf, 1, 0);

		// batman->CastSpell(65,bm->position,1, -1,3,16);
		char result = THISCALL_3(char, 0x005A6F80, batman, bm->position, 16);
		NPC_clean_mirage(side);

		// if (!result) break;
		// if (!CanSummonOrClone(side)) break;

		// return;
	}
	if (bm && *txt_buf) {
		THISCALL_4(void, 0x4729D0, batman->dlg, txt_buf, 1, 0);
		// NPC_clean_mirage(side);
	}
	if (bm)
		return true;
	return false;
}

void NPC_EachRound() {
	// return;

	auto batman = h3::H3CombatManager::Get();
	if (!batman) return;
	if (batman->tacticsPhase) return;
	//	if (batman->currentActiveSide < 0) return;
	//	if (batman->currentActiveSide > 1) return;

	// _NPC_* batnpc = (_NPC_*)0x2861E70;
	// _NPC_* all_npc = ;

	/*
	if (last_round == batman->turn)
		return;
	*/
	int my_ID = batman->currentMonIndex + 21 * batman->currentMonSide;
	if (my_ID == last_stack) return; 
	last_stack = my_ID;

	// if(batman->currentMonSide == 0)
	for (auto &i : batman->stacks[0]) {
		auto he = batman->hero[0];
		if (he && he->id >= 0 && i.numberAlive > 0 
			&& i.type >= 174 && i.type <= 182 
			// && !i.HasMoved() 
			&& !i.IsClone()
			)
			CommanderPoints[0] += GetNpc(he->id)->Level + 1; // batnpc[0].Level;
	}

	// if (batman->currentMonSide == 1)
	for (auto& i : batman->stacks[1]) {
		auto he = batman->hero[1];
		if (he && he->id >= 0 && i.numberAlive > 0
			&& i.type >= 183 && i.type <= 191
			// && !i.HasMoved() 
			&& !i.IsClone()
			)
			CommanderPoints[1] += GetNpc(he->id)->Level + 1; // batnpc[1].Level;
	}
	char msg[256]; sprintf(msg,"Commander Points Attacker:%d Defender:%d", CommanderPoints[0], CommanderPoints[1]);
	THISCALL_4(void, 0x4729D0,batman->dlg, msg,1,0);
	last_round = batman->turn;
}


void NPC_BeforeAction() {

	char txt_buf[256] = "";
	h3::H3CombatCreature* bm = nullptr;
	auto batman = h3::H3CombatManager::Get();
	if (batman->tacticsPhase) return;
	// if (batman->currentActiveSide < 0) return;
	// if (batman->currentActiveSide > 1) return;

	// auto he = batman->hero[side];
	for (int side = 0; side < 2; ++side) {


		

	}
}

void NPC_AfterAction() {
	char txt_buf[256] = "";
	h3::H3CombatCreature* bm = nullptr;
	auto batman = h3::H3CombatManager::Get();
	if (batman->tacticsPhase) return;
	// if (batman->currentActiveSide < 0) return;
	// if (batman->currentActiveSide > 1) return;

	// auto he = batman->hero[side];
	for (int side = 0; side < 2; ++side) {
		auto he = batman->hero[side];
		if (batman->heroCasted[side] && CommanderPoints[side] >= 25 && NPC_has_mage(side)) {
			batman->heroCasted[side] = false; CommanderPoints[side] -= 25;
			sprintf(txt_buf,"%s can cast again.",he->name);
			THISCALL_4(void, 0x4729D0, batman->dlg, txt_buf, 1, 0);
			return;
		}
		
		if (NPC_try_mirage(side)) return;

	}
}