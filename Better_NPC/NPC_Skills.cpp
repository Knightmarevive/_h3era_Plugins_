#include "Header.h"
extern PatcherInstance* _PI;
#include "Call_Helper.h"
#include <map>
#include <string>
#include "cstring"

#define AT Primary[0]
#define DF Primary[1]
#define HP Primary[2]
#define DM Primary[3]
#define MP Primary[4]
#define SP Primary[5]
#define MR Primary[6]
#define ATS Skills[0]
#define DFS Skills[1]
#define HPS Skills[2]
#define DMS Skills[3]
#define MPS Skills[4]
#define SPS Skills[5]
#define MRS Skills[6]

#define AT_DF  0x00000001
#define AT_HP  0x00000002
#define AT_DM  0x00000004
#define AT_MP  0x00000008
#define AT_SP  0x00000010
#define DF_HP  0x00000020
#define DF_DM  0x00000040
#define DF_MP  0x00000080
#define DF_SP  0x00000100
#define HP_DM  0x00000200
#define HP_MP  0x00000400
#define HP_SP  0x00000800
#define DM_MP  0x00001000
#define DM_SP  0x00002000
#define MP_SP  0x00004000
#define NPCARTBASE 146

#define ARTNUMINDEX 0
#define ARTBATTLES 1

#define CALC_MinDM(dm) (dm/2)


char SpecBonus_names__[6][6][64] = {
    {"no","AT_DF","AT_HP","AT_DM","AT_MP","AT_SP"},
    {"no","no","DF_HP","DF_DM","DF_MP","DF_SP"},
    {"no","no","no","HP_DM","HP_MP","HP_SP"},
    {"no","no","no","no","DM_MP","DM_SP"},
    {"no","no","no","no","no","MP_SP"},
};
char SpecBonusPopUpText__[6][6][2048];
char SpecBonusNameText__[6][6][2048];
char SpecBonusHintText__[6][6][2048];

// char* SpecBonusPopUpText__table[6][6];

int   NPC__Bonus[7][5] = {
  {2,5,9,15,25},     // +AT
  {4,10,18,30,50},   // +DF
  {10,25,45,70,100}, // +HP%
  {10,25,45,70,100}, // +DM%
  {1,3,6,14,29},     // +MP
  {1,2,3,4,6},       // +SP
  {5,15,35,60,90}    // +MR%
};

int   NPC__SpecBonus[7][5] = {
  {4,10,18,30,50},    // +AT
  {8,20,36,60,100},   // +DF
  {20,50,90,140,200}, // +HP%
  {20,50,90,140,200}, // +DM%
  {2,5,10,20,40},     //  MP
  {2,3,5,7,9},        // +SP
  {10,20,40,65,95}    // +MR%
};

/*
int NPC_AddExp(HiHook* H, _NPC_* me, signed int NewExp, int Auto) {

}
*/

void install_NPC_classes();

constexpr long Nobility = 4;

int get_NPC_Nobility(_NPC_* npc) {
    static int(*get_Hero_zSSI_with_bonuses_)(h3::H3Hero *, long, int) = nullptr;
    if (!get_Hero_zSSI_with_bonuses_) {
        HMODULE more_SS_Levels__Link = GetModuleHandleA("more_SS_levels.era");
        if (!more_SS_Levels__Link) return 100;
        get_Hero_zSSI_with_bonuses_ = (int(*)(h3::H3Hero*, long, int)) GetProcAddress(more_SS_Levels__Link,"get_Hero_zSSI_with_bonuses");
    }
    return 100 + get_Hero_zSSI_with_bonuses_(P_Game->GetHero(npc->Number), Nobility, 0);
}

inline void v_MsgBox(const char* text, int style)
{
    CALL_12(void, __fastcall, 0x4F6C00, text, style, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
    return;
}

int NPC__GetMonID(h3::H3CreatureInformation* mi) {
    int diff = ((int)mi - *(int*)0x0076C05C);
    int size = sizeof(h3::H3CreatureInformation);
    int ret = diff / size;

    if (ret < 0 || ret >= 1024 || (diff % size)) {
        v_MsgBox("Very bad bug at NPC__GetMonID, prepare to crash", 1);
        return -1;
    }
        

    return ret;
}

namespace lazyreader {
    std::map<std::string, std::string> buffers = {};
    const char* NPC__Read_File(const char* FileID) {
        // static std::map<std::string, std::string> buffers = {};
        auto key = std::string(FileID);
        if (buffers.find(key) == buffers.end()) {
            char filename[256]; FILE* fdesc; char* buf;
            sprintf(filename, "Data\\commanders\\%s.cfg", FileID);
            if (fdesc = fopen(filename, "r"))
            {
                fseek(fdesc, 0, SEEK_END);
                int fdesc_size = ftell(fdesc);
                rewind(fdesc);
                //----------
                buf = (char*)malloc(fdesc_size + 1);
                fread(buf, 1, fdesc_size, fdesc);
                buf[fdesc_size] = 0;

                fclose(fdesc);

                buffers[key] = buf;
                free(buf);
            }
            else buffers[key] = "";
            // buffers[key];
        }
        return (buffers[key].c_str());
    }
}


void apply_SpecBonusPopUpText() {
    int addr1 = 0x2860738; 
    int addr2 = 0x2860330;
    int k = 0;
    for (int i = 0;i < 6;i++) {
        for (int j = 0;j < 6;j++) {
            // const char* config = lazyreader::NPC__Read_File(SpecBonus_names__[i][j]);
            
            // SpecBonusPopUpText__table [i] [j] = SpecBonusPopUpText__[i][j];

            strcpy(&SpecBonusPopUpText__[i][j][0], (char*)*(int*)(k * 4 + addr1));
            // _PI->WriteDword((addr1 + k * 4), &SpecBonusPopUpText__[i][j][0]);
            
            //strcpy((char*)*(int*)(addr1 + k * 4), SpecBonusPopUpText__[i][j]);
            _PI->WriteDword((addr1 + k * 4), &SpecBonusPopUpText__[i][j][0]);

            strcpy(&SpecBonusNameText__[i][j][0], (char*)*(int*)(k * 8 + addr2));
            strcpy(&SpecBonusHintText__[i][j][0], (char*)*(int*)(k * 8 + 4 + addr2));
            
            //strcpy((char*)*(int*)(addr2 + k * 8), SpecBonusNameText__[i][j]);
            //strcpy((char*)*(int*)(addr2 + k * 8 + 4), SpecBonusHintText__[i][j]);

            _PI->WriteDword((addr2 + k * 8), &SpecBonusNameText__[i][j][0]);
            _PI->WriteDword((addr2 + k * 8 + 4), &SpecBonusHintText__[i][j][0]);


            ++k;
        }
    }

    // _PI->WriteDword(addr1, (int) SpecBonusPopUpText__table);

    return;
}

void refresh_SpecBonusPopUpText() {
    int addr1 = 0x2860738;
    int addr2 = 0x2860330;

    // _PI->WriteDword(addr1, (int)SpecBonusPopUpText__table);

    int k = 0;
    for (int i = 0;i < 6;i++) {
        for (int j = 0;j < 6;j++) {
            const char* config = lazyreader::NPC__Read_File(SpecBonus_names__[i][j]);

            const char* descr = strstr(config, "description=\""); int c2 = 0;
            if (descr) for (const char* c1 = descr + strlen("description=\"");;) {
                SpecBonusPopUpText__[i][j][c2] = *c1;
                if (*c1 == '\"' || *c1 == 0)
                {
                    SpecBonusPopUpText__[i][j][c2] = 0; break;
                }
                ++c1; ++c2;
            }
            _PI->WriteDword((addr1 + k * 4), &SpecBonusPopUpText__[i][j][0]);

            const char* name = strstr(config, "name=\"");
            const char* hint = strstr(config, "hint=\"");
            if (name) {
                c2 = 0;
                for (const char* c1 = name + strlen("name=\"");;) {
                    SpecBonusNameText__[i][j][c2] = *c1;
                    if (*c1 == '\"' || *c1 == 0)
                    {
                        SpecBonusNameText__[i][j][c2] = 0; break;
                    }
                    ++c1; ++c2;
                }
            }
            if (hint) {
                c2 = 0;
                for (const char* c1 = hint + strlen("hint=\"");;) {
                    SpecBonusHintText__[i][j][c2] = *c1;
                    if (*c1 == '\"' || *c1 == 0)
                    {
                        SpecBonusHintText__[i][j][c2] = 0; break;
                    }
                    ++c1; ++c2;
                }
            }
            _PI->WriteDword((addr2 + k * 8), &SpecBonusNameText__[i][j][0]);
            _PI->WriteDword((addr2 + k * 8 + 4), &SpecBonusHintText__[i][j][0]);

            ++k;
        }
    }
}

void NPC__Apply_Amethyst(/*h3::H3CreatureInformation* mi*/ int mon_ID, const char* thing) {
    static void(*ChangeCreatureTable_)(int,const char*) = nullptr;
    if (!ChangeCreatureTable_) {
        HMODULE Amethyst_Link = nullptr;
        if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_4.dll");
        if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_5.dll");
        if (Amethyst_Link) ChangeCreatureTable_ = (void (*)(int,const char*)) GetProcAddress(Amethyst_Link, "ChangeCreatureTable");
    }
    if (!ChangeCreatureTable_) v_MsgBox("Very bad bug at NPC__Apply_Amethyst, prepare to crash", 1);
    else {
        // ChangeCreatureTable_(NPC__GetMonID(mi), NPC__Read_File(thing));
        ChangeCreatureTable_(mon_ID, lazyreader::NPC__Read_File(thing));

    }
}

int NPC__ArtDoesHave(_NPC_* me, int ArtNum) {
    for (int i = 0; i < 6; i++) if (me->Arts[i][ARTNUMINDEX] == ArtNum) return 1;
    return 0;
}

int NPC__ArtCalcSkill(_NPC_* me, int Slot, int Skill)
{
    int ArtNum, val = 0;
    ArtNum = me->Arts[Slot][ARTNUMINDEX];
    switch (ArtNum) {
    case NPCARTBASE: // +5 Attack + 1 for 6 battles
        if (Skill != 0) break;
        val = 5 + me->Arts[Slot][ARTBATTLES] / 6;
        break;
    case NPCARTBASE + 1: // +12% HP + 1% for 1 battles
        if (Skill != 2) break;
        val = 12 + me->Arts[Slot][ARTBATTLES];
        break;
    case NPCARTBASE + 2: // +12% DM + 1% for 1 battles
        if (Skill != 3) break;
        val = 12 + me->Arts[Slot][ARTBATTLES];
        break;
    case NPCARTBASE + 3: break; // nothing yet
    case NPCARTBASE + 4: // MP
        if (Skill == 4)  val = 1 + me->Arts[Slot][ARTBATTLES] / 10;
        if (Skill == 7 + 4)  val = 1 + me->Arts[Slot][ARTBATTLES] / 77;
        break;
    case NPCARTBASE + 5: // SP
        if (Skill != 5) break;
        val = 1 + me->Arts[Slot][ARTBATTLES] / 10;
        break;
    case NPCARTBASE + 6: break; // nothing yet
    case NPCARTBASE + 7: break; // nothing yet
    case NPCARTBASE + 8: // +5 Defense + 1 for 6 battles
        if (Skill != 1) break;
        val = 5 + me->Arts[Slot][ARTBATTLES] / 6;
        break;
    case NPCARTBASE + 9: break; // nothing yet
    }
    return (val);
}

int __stdcall NPC_CalcSkill_(HiHook* H, _NPC_* me, int ind) {
    int i, fl, val, spec = 0;
    val = me->Primary[ind];
    if (me->Skills[ind] > 0) {
        //    if((ind==0)&&(Type==4)) spec=1; // AT & Necro
        //    if((ind==1)&&(Type==8)) spec=1; // DF & Conflux
        //    if((ind==3)&&(Type==5)) spec=1; // DM & Dungeon
        //    if((ind==5)&&(Type==6)) spec=1; // SP & Stronhold
        if ((ind == 2) || (ind == 3)) {
            if (spec) {
               val += val * NPC__SpecBonus[ind][me->Skills[ind] - 1] / 100;
            }
            else {
                val += val * NPC__Bonus[ind][me->Skills[ind] - 1] / 100;
            }
            val *= get_NPC_Nobility(me); val /= 100;
        }
        else if(ind<7) {
            if (spec) {
                val += NPC__SpecBonus[ind][me->Skills[ind] - 1];
            }
            else {
                val += NPC__Bonus[ind][me->Skills[ind] - 1];
            }
        }
        else {
            val += 0;
        }
    }
    for (i = fl = 0; i < 10; i++) {
        if (me->Arts[i][ARTNUMINDEX] == (NPCARTBASE + 9)) { // �����������
            if (fl == 1) continue;
            if (me->Skills[ind] <= 2) {
                if ((ind == 2) || (ind == 3)) val += val * NPC__Bonus[ind][1] / 100;
                else  val += NPC__Bonus[ind][1];
            }
            fl = 1;
            continue;
        }
        if ((ind == 2) || (ind == 3)) {
            val += val * NPC__ArtCalcSkill(me, i, ind) / 100;
        }
        else {
            val += NPC__ArtCalcSkill(me, i, ind);
        }
    }

    if (ind == 7 + 4) ++val;
    return val;
}

void NPC__apply_Amethyst_Common(_NPC_* npc, int MonID) {
    char thing[256];

    if (true) NPC__Apply_Amethyst(MonID, "blank");

    sprintf(thing, "NPC_%d", npc->Type); NPC__Apply_Amethyst(MonID, thing);
    for (int lvl = 1; lvl <= npc->Level; ++lvl) {
        sprintf(thing, "NPC_%d_LVL_%d", npc->Type, lvl); NPC__Apply_Amethyst(MonID, thing);
    }

    if (npc->SpecBon[0] & AT_DF) NPC__Apply_Amethyst(MonID, "AT_DF");
    if (npc->SpecBon[0] & AT_HP) NPC__Apply_Amethyst(MonID, "AT_HP");
    if (npc->SpecBon[0] & AT_DM) NPC__Apply_Amethyst(MonID, "AT_DM");
    if (npc->SpecBon[0] & AT_MP) NPC__Apply_Amethyst(MonID, "AT_MP");
    if (npc->SpecBon[0] & AT_SP) NPC__Apply_Amethyst(MonID, "AT_SP");
    if (npc->SpecBon[0] & DF_HP) NPC__Apply_Amethyst(MonID, "DF_HP");
    if (npc->SpecBon[0] & DF_DM) NPC__Apply_Amethyst(MonID, "DF_DM");
    if (npc->SpecBon[0] & DF_MP) NPC__Apply_Amethyst(MonID, "DF_MP");
    if (npc->SpecBon[0] & DF_SP) NPC__Apply_Amethyst(MonID, "DF_SP");
    if (npc->SpecBon[0] & HP_DM) NPC__Apply_Amethyst(MonID, "HP_DM");
    if (npc->SpecBon[0] & HP_MP) NPC__Apply_Amethyst(MonID, "HP_MP");
    if (npc->SpecBon[0] & HP_SP) NPC__Apply_Amethyst(MonID, "HP_SP");
    if (npc->SpecBon[0] & DM_MP) NPC__Apply_Amethyst(MonID, "DM_MP");
    if (npc->SpecBon[0] & DM_SP) NPC__Apply_Amethyst(MonID, "DM_SP");
    if (npc->SpecBon[0] & MP_SP) NPC__Apply_Amethyst(MonID, "MP_SP");

    for (int i = 0; i < 6; i++) if (npc->Arts[i][ARTNUMINDEX] >= NPCARTBASE) {
        sprintf(thing, "ART_%d", npc->Arts[i][ARTNUMINDEX]); NPC__Apply_Amethyst(MonID, thing);

        for (int lvl = 1; lvl <= npc->Level; ++lvl) {
            sprintf(thing, "ART_%d_LVL_%d", npc->Arts[i][ARTNUMINDEX], lvl); NPC__Apply_Amethyst(MonID, thing);
        }

        for (int bat = 1; bat <= npc->Arts[i][ARTBATTLES]; ++bat) {
            sprintf(thing, "ART_%d_BAT_%d", npc->Arts[i][ARTNUMINDEX], bat); NPC__Apply_Amethyst(MonID, thing);
        }
    }
}

UINT32 NPC__Apply_FLags_Commmon(_NPC_* npc, int fl) {
    // UINT32 fl = mi->flags;
    if (npc->SpecBon[0] & AT_MP) fl |= 0x00010000;
    else fl &= 0xFFFEFFFF;
    if (npc->SpecBon[0] & HP_DM) fl |= 0x00008000;
    else fl &= 0xFFFF7FFF;
    if (npc->SpecBon[0] & DF_DM) fl |= 0x00080000;
    else fl &= 0xFFF7FFFF;
    
    if (npc->SpecBon[0] & MP_SP) fl |= 0x00000002;
    else fl &= 0xFFFFFFFD;
    
    fl &= 0xFFFFFFFB;

    // if (NPC__ArtDoesHave(npc, 153)) fl |= 0x80000008;
    // else fl &= 0x7FFFFFF7;
    if (NPC__ArtDoesHave(npc, 153)) fl |= 0x80000000;
    else fl &= 0x7FFFFFFF;
    
    return fl;
    // mi->flags = fl;
}

h3::H3CreatureInformation* __stdcall SetMonInitPars_(HiHook* H, _NPC_* npc, /*_MonInfo_ */ h3::H3CreatureInformation *mi) {

    int Slot; // char thing[256];
    mi->attack = NPC_CalcSkill_(nullptr,npc, 0); // npc->CalcSkill(0);
    mi->defence= NPC_CalcSkill_(nullptr, npc, 1);//  npc->CalcSkill(1);
    mi->hitPoints= NPC_CalcSkill_(nullptr, npc, 2); //  0; // npc->CalcSkill(2);
    mi->damageHigh = NPC_CalcSkill_(nullptr, npc, 3); // npc->CalcSkill(3);

    if (npc->SpecBon[0] & AT_DM) mi->damageLow = mi->damageHigh;
    else mi->damageLow = CALC_MinDM(mi->damageHigh);

    mi->spellCharges = NPC_CalcSkill_(nullptr, npc, 7 + 4); // npc->MPS + 1; // 

    mi->speed = NPC_CalcSkill_(nullptr, npc, 5);

    mi->numberShots = 0;

    mi->flags = NPC__Apply_FLags_Commmon(npc, mi->flags);

    NPC__apply_Amethyst_Common(npc, NPC__GetMonID(mi));

    return mi;
}

// NPCsa = *(_NPC_*)(*(int*) 0x2861E70);

h3::H3CombatCreature* __stdcall ApplyCmdMonChanges_(HiHook* H, h3::H3CombatCreature* Mon) {
    int v;
    int mt = Mon->type; // *(int*)&Mon[0x34];
    if ((mt < 174) || (mt > 191)) return Mon; // wrong mon type
    int Ind = 0; if (mt > 182) Ind = 1;
    _NPC_* npc = ((_NPC_*)0x2861E70) + Ind; // &NPCsa[Ind];
    if (npc->Used == 0) return Mon; // not set for this battle - creature

    // if (Mon == NPCinBattleStr[Ind]) return; // standard commander
    if (Mon == ((h3::H3CombatCreature*)(0x2860824 + Ind *4))) return Mon; // standard commander

    Mon->faerieDragonSpell = 37;

    Mon->info.attack = NPC_CalcSkill_(nullptr, npc, 0); // npc->CalcSkill(0);
    Mon->info.defence = NPC_CalcSkill_(nullptr, npc, 1);//  npc->CalcSkill(1);
    Mon->info.hitPoints = NPC_CalcSkill_(nullptr, npc, 2); //  0; // npc->CalcSkill(2);
    Mon->baseHP = NPC_CalcSkill_(nullptr, npc, 2); //  0; // npc->CalcSkill(2);
    Mon->info.damageHigh = NPC_CalcSkill_(nullptr, npc, 3); // npc->CalcSkill(3);

    Mon->info.spellCharges = NPC_CalcSkill_(nullptr, npc, 7 + 4); // npc->MPS + 1; // 

    Mon->info.speed = NPC_CalcSkill_(nullptr, npc, 5);

    if (npc->SpecBon[0] & AT_DM) Mon->info.damageLow = Mon->info.damageHigh;
    else Mon->info.damageLow = CALC_MinDM(Mon->info.damageHigh);

    Mon->info.numberShots = 0;

    //  *(DWORD*)&Mon[0x84] = NPC__Apply_FLags_Commmon(npc, *(DWORD*)&Mon[0x84]);
    Mon->info.flags = NPC__Apply_FLags_Commmon(npc, Mon->info.flags);

    NPC__apply_Amethyst_Common(npc, mt);

    return Mon;
}

int npc_expo_percent[9] = {200, 100, 100, 100, 75, 100, 150, 100, 100};
_LHF_(hook_00769682) {
    int npc_class = *(int*)(c->edx+0x0C);
    
    c->eax = *(int*)(c->ebp - 0x14);
    c->eax *= npc_expo_percent[npc_class];
    c->eax /= 100;

    c->return_address = 0x00769699;
    return NO_EXEC_DEFAULT;
}

void install_NPC_Skills() {
    _PI->WriteLoHook(0x00769682, hook_00769682);

	_PI->WriteHiHook(0x00769460, THISCALL_, NPC_CalcSkill_);
    _PI->WriteHiHook(0x0076C10D, CDECL_, SetMonInitPars_);
    _PI->WriteHiHook(0x0076BAF3, CDECL_, ApplyCmdMonChanges_);

    _PI->WriteWord(7785525, 37008); //disable Succubus Charming
    _PI->WriteWord(7772712, 37008); //disable astral spirit

    _PI->WriteDword(7788085,0); //fireshield disabling
    _PI->WriteDword(7788930,0);  //paralyze disabling

    _PI->WriteByte(0x769363, 6); // unlock 6 types of primary skills (was 4)

}

// bool apply_commander_texts_first_run = false;
signed int apply_commander_texts(HiHook* hook)
{
    // if (apply_commander_texts_first_run) return 0;
    signed int ret = CALL_0(signed int, __cdecl, hook->GetDefaultFunc()) ;
    apply_SpecBonusPopUpText(); // apply_commander_texts_first_run = true;

    refresh_SpecBonusPopUpText();

    install_NPC_classes();

    return ret;
}