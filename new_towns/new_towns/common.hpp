constexpr int max_towns_types = 27;
constexpr int max_builds_new = 128;
constexpr int max_builds_old = 44;
