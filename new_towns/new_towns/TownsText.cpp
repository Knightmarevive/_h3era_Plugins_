#include "patcher_x86_commented.hpp"
#include <cstdio>

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;


char text_TownsBackGroundMage[27][16] = {
"TPMageCs.pcx",
"TPMageRm.pcx",
"TPMageTw.pcx",
"TPMageIn.pcx",
"TPMageNc.pcx",
"TPMageDn.pcx",
"TPMageSt.pcx",
"TPMageFr.pcx",
"TPMageEl.pcx",

"TPMageCs.pcx",
"TPMageRm.pcx",
"TPMageTw.pcx",
"TPMageIn.pcx",
"TPMageNc.pcx",
"TPMageDn.pcx",
"TPMageSt.pcx",
"TPMageFr.pcx",
"TPMageEl.pcx",

"TPMageCs.pcx",
"TPMageRm.pcx",
"TPMageTw.pcx",
"TPMageIn.pcx",
"TPMageNc.pcx",
"TPMageDn.pcx",
"TPMageSt.pcx",
"TPMageFr.pcx",
"TPMageEl.pcx",

};

char* new_TownsBackGroundMage[27];


char text_TownsHallDefs[27][16] = {
"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HALLelem.def",

"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HALLelem.def",

"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HALLelem.def"
};

char* new_TownsHallDefs[27];



char text_Towns2[27 + 1][32] = {
"Neutral",

"Castle",
"Rampart",
"Tower",
"Inferno",
"Necropolis",
"Dungeon",
"Stronghold",
"Fortress",
"Conflux",

"Castle_2",
"Rampart_2",
"Tower_2",
"Inferno_2",
"Necropolis_2",
"Dungeon_2",
"Stronghold_2",
"Fortress_2",
"Conflux_2",

"Castle_3",
"Rampart_3",
"Tower_3",
"Inferno_3",
"Necropolis_3",
"Dungeon_3",
"Stronghold_3",
"Fortress_3",
"Conflux_3",

};
char* new_Towns2[27+1];

extern "C" __declspec(dllexport) void set_town_type_name(long town, char* name) {
	if (name) strcpy_s(text_Towns2[town+1], name);
}

char text_Towns3[27+1][16] = {
"neutral",

"castle",
"rampart",
"tower",
"inferno",
"necropolis",
"dungeon",
"stronghold",
"fortress",
"conflux",

"castle",
"rampart",
"tower",
"inferno",
"necropolis",
"dungeon",
"stronghold",
"fortress",
"conflux",

"castle",
"rampart",
"tower",
"inferno",
"necropolis",
"dungeon",
"stronghold",
"fortress",
"conflux",

};
char* new_Towns3[27+1];


char text_TownsBackGroundCreatures0[27+1][16] = {
"CrBkgNeu.pcx",

"CrBkgCas.pcx",
"CrBkgRam.pcx",
"CrBkgTow.pcx",
"CrBkgInf.pcx",
"CrBkgNec.pcx",
"CrBkgDun.pcx",
"CrBkgStr.pcx",
"CrBkgFor.pcx",
"CrBkgEle.pcx",

"CrBkgCas.pcx",
"CrBkgRam.pcx",
"CrBkgTow.pcx",
"CrBkgInf.pcx",
"CrBkgNec.pcx",
"CrBkgDun.pcx",
"CrBkgStr.pcx",
"CrBkgFor.pcx",
"CrBkgEle.pcx",

"CrBkgCas.pcx",
"CrBkgRam.pcx",
"CrBkgTow.pcx",
"CrBkgInf.pcx",
"CrBkgNec.pcx",
"CrBkgDun.pcx",
"CrBkgStr.pcx",
"CrBkgFor.pcx",
"CrBkgEle.pcx",

};
char* new_TownsBackGroundCreatures0[27+1];

extern "C" __declspec(dllexport) void set_TownsBackGroundCreatures0(long town, char* name) {
	if (name) strcpy_s(text_TownsBackGroundCreatures0[town + 1], name);
}

char text_TownsBackGroundCreatures1[27+1][16] = {
"TPCasNeu.pcx",

"TPCasCas.pcx",
"TPCasRam.pcx",
"TPCasTow.pcx",
"TPCasInf.pcx",
"TPCasNec.pcx",
"TPCasDun.pcx",
"TPCasStr.pcx",
"TPCasFor.pcx",
"TPCasEle.pcx",

"TPCasCas.pcx",
"TPCasRam.pcx",
"TPCasTow.pcx",
"TPCasInf.pcx",
"TPCasNec.pcx",
"TPCasDun.pcx",
"TPCasStr.pcx",
"TPCasFor.pcx",
"TPCasEle.pcx",

"TPCasCas.pcx",
"TPCasRam.pcx",
"TPCasTow.pcx",
"TPCasInf.pcx",
"TPCasNec.pcx",
"TPCasDun.pcx",
"TPCasStr.pcx",
"TPCasFor.pcx",
"TPCasEle.pcx",

};
char* new_TownsBackGroundCreatures1[27+1];

extern "C" __declspec(dllexport) void set_TownsBackGroundCreatures1(long town, char* name) {
	if (name) strcpy_s(text_TownsBackGroundCreatures1[town + 1], name);
}

char text_TownsHalls[30 + 27][16] = {
"diboxbck.pcx",
"dialgbox.def",
"iokay.def",
"icancel.def",
"resource.def",
"artifact.def",
"spells.def",
"crest58.def",
"pskill.def",
"twcrport.def",
"secskill.def",
"imrlb.def",
"ilckb.def",
"heroqvbk.pcx",
"ilck22.def",
"imrl22.def",
"cprsmall.def",
"townqvbk.pcx",
"itpt.def",
"itmtl.def",
"itmcl.def",
"ZrStkPu.pcx",
"iViewCr.def",
"iViewCr2.def",
"resour82.def",
"spellScr.def",
"pskill.def",
"secsk82.def",
"imrl82.def",
"ilck82.def",

"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HallElem.def",

"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HallElem.def",

"HALLCSTL.def",
"HALLRAMP.def",
"HALLtowr.def",
"HALLINFR.def",
"HALLNECR.def",
"HALLDUNG.def",
"HALLSTRN.def",
"HALLFORT.def",
"HallElem.def",

// "BAD"
};
char* new_TownsHalls[30+27];

extern "C" __declspec(dllexport) void set_townhall_def(long town, char* name) {
	if (name) strcpy_s(text_TownsHalls[town + 30], name);
	if (name) strcpy_s(text_TownsHallDefs[town], name);
}


//

char text_TownDwellingsNames[27*14][32];
char *new_TownDwellingsNames[27*14];
extern char new_buildings_names[27][64][512];
extern "C" __declspec(dllexport) void set_town_dwelling_names(long town,long dwelling, long grade, char* name) {
	if (grade <=1)
	    strcpy_s(text_TownDwellingsNames[town*14+dwelling+ grade *7], name);
	else
	{
		long adjustedGrade = grade - 2;
		strcpy_s(new_buildings_names[town][dwelling + adjustedGrade * 7], name);
	}
}

char text_TownDwellingsDesc[27 * 14][512];
char* new_TownDwellingsDesc[27 * 14];
extern char new_buildings_descriptions[27][64][2048];
extern "C" __declspec(dllexport) void set_town_dwelling_descriptions(long town, long dwelling, long grade, char* desc) {
	if (grade <= 1)
	    strcpy_s(text_TownDwellingsDesc[town * 14 + dwelling + grade * 7], desc);
	else
	{
		long adjustedGrade = grade - 2;
 		strcpy_s(new_buildings_descriptions[town][dwelling + adjustedGrade * 7], desc);
	}
}


char text_TownSpecBuildingsNames[27 * 11][32];
char* new_TownSpecBuildingsNames[27 * 11];

extern "C" __declspec(dllexport) void set_town_spec_building_names(long town, long spec, char* name) {

	strcpy_s(text_TownSpecBuildingsNames[town * 11 + spec], name);
}


char text_TownSpecBuildingsDesc[27 * 11][512];
char* new_TownSpecBuildingsDesc[27 * 11];



extern "C" __declspec(dllexport) void set_town_spec_building_descriptions(long town,long spec, char* desc) {

	strcpy_s(text_TownSpecBuildingsDesc[town * 11 + spec], desc);
}


void patch_TownsText(void) {

}

void patch_TownsText_late(void) {
	for (int i = 0; i < 27; ++i) {
		new_TownsBackGroundMage[i] = text_TownsBackGroundMage[i];
		new_TownsHallDefs[i] = text_TownsHallDefs[i];
	}
	for (int i = 0; i < 27+1; ++i) {
		new_TownsBackGroundCreatures0[i] = text_TownsBackGroundCreatures0[i];
		new_TownsBackGroundCreatures1[i] = text_TownsBackGroundCreatures1[i];
	}
	Z_new_towns->WriteDword(0X005CCED7 + 3,(long)new_TownsBackGroundMage);

	Z_new_towns->WriteDword(0x005D5B1E + 3, (long)new_TownsHallDefs);
	Z_new_towns->WriteDword(0x005D9A6A + 3, (long)new_TownsHallDefs);


	for (int i = 0; i < 27+1; ++i) {
		new_Towns2[i] = text_Towns2[i];		new_Towns3[i] = text_Towns3[i];
	}
	Z_new_towns->WriteDword(0x0040C9A8 + 3, 4 + (long)new_Towns2);
	Z_new_towns->WriteDword(0x005697F7 + 3, 4 + (long)new_Towns2);
	Z_new_towns->WriteDword(0x0057635A + 3, 4 + (long)new_Towns2);
	Z_new_towns->WriteDword(0x0058D95A + 3, 4 + (long)new_Towns2);
	Z_new_towns->WriteDword(0x0058DD24 + 3, 4 + (long)new_Towns2);
	Z_new_towns->WriteDword(0x005C1854 + 3, 4 + (long)new_Towns2);

	Z_new_towns->WriteDword(0x0054A4AA + 2, 4 + (long)new_Towns3);

	Z_new_towns->WriteDword(0x0044964D + 3, 4 + (long)new_TownsBackGroundCreatures0);
	Z_new_towns->WriteDword(0x0055000F + 3, 4 + (long)new_TownsBackGroundCreatures0);
	Z_new_towns->WriteDword(0x00551E35 + 3, 4 + (long)new_TownsBackGroundCreatures0);
	Z_new_towns->WriteDword(0x005F5450 + 3, 4 + (long)new_TownsBackGroundCreatures0);

	Z_new_towns->WriteDword(0x005D90A4 + 3, 4 + (long)new_TownsBackGroundCreatures1);
	Z_new_towns->WriteDword(0x005D9356 + 3, 4 + (long)new_TownsBackGroundCreatures1);

	for (int i = 0; i < 30 + 27; ++i) new_TownsHalls[i] = text_TownsHalls[i];
	Z_new_towns->WriteDword(0x00407123 + 3, (long)new_TownsHalls);
	Z_new_towns->WriteDword(0x00407190 + 3, (long)new_TownsHalls);
	Z_new_towns->WriteDword(0x00407167 + 3, (long)new_TownsHalls);

	for (int i = 0; i < 9 * 14; ++i) {
		strcpy_s(text_TownDwellingsNames[i], (char*)*(int*)(0x006A6310 + i * 4));
		strcpy_s(text_TownDwellingsNames[i+9*14], (char*)*(int*)(0x006A6310 + i * 4));
		strcpy_s(text_TownDwellingsNames[i+18*14], (char*)*(int*)(0x006A6310 + i * 4));

		strcpy_s(text_TownDwellingsDesc[i], (char*)*(int*)(0x006A6A2C + i * 4));
		/*
		strcpy_s(text_TownDwellingsDesc[i + 9 * 14], (char*)*(int*)(0x006A6A2C + i * 4));
		strcpy_s(text_TownDwellingsDesc[i + 18 * 14], (char*)*(int*)(0x006A6A2C + i * 4));
		*/
		sprintf_s(text_TownDwellingsDesc[i + 9 * 14], "Building(%i)", 30 + i % 14);
		sprintf_s(text_TownDwellingsDesc[i + 18 * 14], "Building(%i)", 30 + i % 14);
	}
	for (int i = 0; i < 27 * 14; ++i)	new_TownDwellingsNames[i] = text_TownDwellingsNames[i];
	Z_new_towns->WriteDword(0x0046149D + 3, long(new_TownDwellingsNames) - 0x78);
	Z_new_towns->WriteDword(0x00461A97 + 3, long(new_TownDwellingsNames) - 0x78);
	Z_new_towns->WriteDword(0x004619AC + 3, long(new_TownDwellingsNames) - 0x78);
	Z_new_towns->WriteDword(0x00461A19 + 3, long(new_TownDwellingsNames) - 0x78);
	Z_new_towns->WriteDword(0x0046183F + 3, long(new_TownDwellingsNames) - 0x78);
	Z_new_towns->WriteDword(0x00460D01 + 3, long(new_TownDwellingsNames) - 0x78);
	//Z_new_towns->WriteDword(0x005B9923 + 2, (long) new_TownDwellingsNames);
	//Z_new_towns->WriteDword(0x005B994C + 2, (long)new_TownDwellingsNames);
	for (int i = 0; i < 27 * 14; ++i)	new_TownDwellingsDesc[i] = text_TownDwellingsDesc[i];
	//Z_new_towns->WriteDword(0x005B9957 + 2, (long)new_TownDwellingsDesc);
	Z_new_towns->WriteDword(0x005D3140 + 3, long(new_TownDwellingsDesc) - 0x78);


	for (int i = 0; i < 9 * 11; ++i) {
		strcpy_s(text_TownSpecBuildingsNames [i], (char*)*(int*)(0x006A543C+ i * 4));
		strcpy_s(text_TownSpecBuildingsNames[i + 9 * 11], (char*)*(int*)(0x006A543C + i * 4));
		strcpy_s(text_TownSpecBuildingsNames[i + 18 * 11], (char*)*(int*)(0x006A543C + i * 4));

		strcpy_s(text_TownSpecBuildingsDesc[i], (char*)*(int*)(0x006A7874 + i * 4));
		strcpy_s(text_TownSpecBuildingsDesc[i + 9 * 11], (char*)*(int*)(0x006A7874 + i * 4));
		strcpy_s(text_TownSpecBuildingsDesc[i + 18 * 11], (char*)*(int*)(0x006A7874 + i * 4));
	}
	for (int i = 0; i < 27 * 11; ++i)	new_TownSpecBuildingsNames[i] = text_TownSpecBuildingsNames[i];
	for (int i = 0; i < 27 * 11; ++i)	new_TownSpecBuildingsDesc[i] = text_TownSpecBuildingsDesc[i];
	// Z_new_towns->WriteDword(0x005EA2BF + 3, (long)new_TownSpecBuildingsNames);
	 Z_new_towns->WriteDword(0x00461488 + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x00461A7C + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x00461A04 + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x0046198E + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x00461821 + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x00460CED + 3, long(new_TownSpecBuildingsNames) - 0x44);
	 Z_new_towns->WriteDword(0x004617F0 + 3, long(new_TownSpecBuildingsNames) - 0x44);

	 Z_new_towns->WriteDword(0x00461469 + 3, long(new_TownSpecBuildingsNames) + 0x28);
	 Z_new_towns->WriteDword(0x00461A51 + 3, long(new_TownSpecBuildingsNames) + 0x28);
	 Z_new_towns->WriteDword(0x00460CD0 + 3, long(new_TownSpecBuildingsNames) + 0x28);

	 Z_new_towns->WriteDword(0x005D2F25 + 3, long(new_TownSpecBuildingsDesc) - 0x44);
	 Z_new_towns->WriteDword(0x005D2EAE + 3, long(new_TownSpecBuildingsDesc) + 0x28);

	 Z_new_towns->WriteDword(0x005C7D3F + 3, long(new_TownSpecBuildingsNames) - 0x44);
}

char* get_building_description(int town_type, int build_id) {
	if (build_id < 0 || build_id>127) return "wrong build_id";
	if (town_type < 0 || town_type>=27) return "wrong town_type";

	if (build_id >= 17 && build_id <= 26)
		return new_TownSpecBuildingsDesc[town_type*11 + (build_id-17)];

	if (build_id >= 30 && build_id < 44)
		return text_TownDwellingsDesc[town_type * 14 + (build_id - 30)];

	if (build_id >= 64 && build_id < 128)
		return new_buildings_descriptions[town_type][build_id-64];

	return "Major Miscalculation on text";
}