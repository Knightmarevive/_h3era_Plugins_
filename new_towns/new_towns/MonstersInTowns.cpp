#include"../../__include__/H3API/single_header/H3API.hpp"
#include "patcher_x86_commented.hpp"
#include <cstdio>

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

/// MonstersInTowns_9x2x7 old data is at 0x6747B4

long MonstersInTowns_27x2x7[27 * 2 * 7] = { 
0, 2, 4, 6, 8, 10, 12, 1, 3, 5, 7, 9, 11, 13,
14, 16, 18, 20, 22, 24, 26, 15, 17, 19, 21, 23, 25, 27,
28, 30, 32, 34, 36, 38, 40, 29, 31, 33, 35, 37, 39, 41,
42, 44, 46, 48, 50, 52, 54, 43, 45, 47, 49, 51, 53, 55,
56, 58, 60, 62, 64, 66, 68, 57, 59, 61, 63, 65, 67, 69,
70, 72, 74, 76, 78, 80, 82, 71, 73, 75, 77, 79, 81, 83,
84, 86, 88, 90, 92, 94, 96, 85, 87, 89, 91, 93, 95, 97,
98, 100, 104, 106, 102, 108, 110, 99, 101, 105, 107, 103, 109, 111,
118, 112, 115, 114, 113, 120, 130, 119, 127, 123, 129, 125, 121, 131,

0, 2, 4, 6, 8, 10, 12, 1, 3, 5, 7, 9, 11, 13,
14, 16, 18, 20, 22, 24, 26, 15, 17, 19, 21, 23, 25, 27,
28, 30, 32, 34, 36, 38, 40, 29, 31, 33, 35, 37, 39, 41,
42, 44, 46, 48, 50, 52, 54, 43, 45, 47, 49, 51, 53, 55,
56, 58, 60, 62, 64, 66, 68, 57, 59, 61, 63, 65, 67, 69,
70, 72, 74, 76, 78, 80, 82, 71, 73, 75, 77, 79, 81, 83,
84, 86, 88, 90, 92, 94, 96, 85, 87, 89, 91, 93, 95, 97,
98, 100, 104, 106, 102, 108, 110, 99, 101, 105, 107, 103, 109, 111,
118, 112, 115, 114, 113, 120, 130, 119, 127, 123, 129, 125, 121, 131,

0, 2, 4, 6, 8, 10, 12, 1, 3, 5, 7, 9, 11, 13,
14, 16, 18, 20, 22, 24, 26, 15, 17, 19, 21, 23, 25, 27,
28, 30, 32, 34, 36, 38, 40, 29, 31, 33, 35, 37, 39, 41,
42, 44, 46, 48, 50, 52, 54, 43, 45, 47, 49, 51, 53, 55,
56, 58, 60, 62, 64, 66, 68, 57, 59, 61, 63, 65, 67, 69,
70, 72, 74, 76, 78, 80, 82, 71, 73, 75, 77, 79, 81, 83,
84, 86, 88, 90, 92, 94, 96, 85, 87, 89, 91, 93, 95, 97,
98, 100, 104, 106, 102, 108, 110, 99, 101, 105, 107, 103, 109, 111,
118, 112, 115, 114, 113, 120, 130, 119, 127, 123, 129, 125, 121, 131

};

extern long EighthMonType[27][4];
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];

/*
long mon_downgraded_in_town(long upgraded) {
	for (int j = 7;j<14;j++)
		for (int i = 0; i < 27 * 2 * 7; i+=14)
			if (MonstersInTowns_27x2x7[i +j] == upgraded)
				return MonstersInTowns_27x2x7[i + j -7];

	return upgraded;
}
*/

extern "C" __declspec(dllexport) long mon_upgraded_in_town(long non_upgraded) {
	for (int j = 0; j < 7; j++)
		for (int i = 0; i < 27 * 2 * 7; i += 14)
			if (MonstersInTowns_27x2x7[i + j] == non_upgraded)
				return MonstersInTowns_27x2x7[i + j + 7];

	for (int j = 0; j < 7; j++)
		for (int i = 0; i < 27 * 2 * 7; i += 14)
			if (MonstersInTowns_27x2x7[i + j + 7] == non_upgraded)
				return ThirdUpgradesInTowns_27x2x7[i + j];

	for (int j = 0; j < 7; j++)
		for (int i = 0; i < 27 * 2 * 7; i += 14)
			if (ThirdUpgradesInTowns_27x2x7[i + j] == non_upgraded)
				return ThirdUpgradesInTowns_27x2x7[i + j + 7];

	for (int j = 0; j < 3; j++)
		for (int i = 0; i < 27; ++i)
			if (EighthMonType[i][j] == non_upgraded)
				return EighthMonType[i][j+1];

	return non_upgraded;
}

long mon_recruited_in_town(int town_type, int level, int grade) {
	if (grade < 0 || grade>3) return -1;
	if (grade == 0 || grade == 1) 
		return MonstersInTowns_27x2x7[town_type*14 + grade*7 + level];
	if (grade == 2 || grade == 3) 
		return ThirdUpgradesInTowns_27x2x7[town_type * 14 + (grade - 2) * 7 + level];
	return -1;
}

long adressOf_MonstersInTowns_9x2x7[]{
	0x00428602 + 3,
	0x00428964 + 3,
	0x00429BB1 + 3,
	0x00429DEC + 3,
	0x00429F32 + 3,
	0x0042A026 + 3,
	0x0042B538 + 3,
	0x0042B5D9 + 3,
	0x0042B5F3 + 3,
	0x0042B724 + 3,
	0x0042BE42 + 3,
	0x0042CF07 + 3,
	0x0042D241 + 3,
	0x00432E94 + 3,
	0x00432F5F + 3,
	0x0043363B + 3,
	0x0047AA7F + 3,
	0x0047AA90 + 3,
	0x0047AB00 + 3,
	0x0047AB11 + 3,
	0x0047AB80 + 3,
	0x0047AB91 + 3,
	0x004BF308 + 2,
	0x004C8D2D + 3,
	0x00503290 + 3,
	0x0051CFD8 + 3,
	0x00525AAD + 3,
	0x0052A31B + 3,
	0x005519A7 + 3,
	0x00551B68 + 3,
	0x00576455 + 2,
	0x005BE383 + 3,
	0x005BE3AB + 3,
	0x005BEF9E + 3,
	0x005BFC66 + 3,
	0x005BFFDF + 3,
	0x005C0098 + 3,
	0x005C0203 + 3,
	0x005C0264 + 3,
	0x005C057E + 4,
	0x005C0B34 + 3,
	0x005C0BEC + 3,
	0x005C6023 + 3,
	0x005C7196 + 3,
	0x005C7CE5 + 3,
	0x005C7D1E + 3,
	0x005D9DE4 + 3,
	0x005D9E5D + 3,
	0x005D9ED3 + 3,
	0x005D9F4C + 3,
	0x005D9FC5 + 3,
	0x005DA03E + 3,
	0x005DA0C2 + 3,
	0x005DA1BA + 3,
	0x005DD099 + 3,
	0x005DD96B + 3,
	0x005DDAD6 + 3,
	0x0070B865 + 1,
	0x0070F553 + 1,
	0x0070F56F + 1,
	0x0071508B + 3

};

struct _HordeBuildingData_ {
	long Monster;
	short Count;
	short Level;
	long UpgMonster;
	short UpgCount;
	short UpgLevel;
};

_HordeBuildingData_ new_Hordes[27 * 2];

char new_TownHorde1MonLevel[27*2] = {
	2, 9, 1, 8, 1, 8, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7,
	2, 9, 1, 8, 1, 8, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7,
	2, 9, 1, 8, 1, 8, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7,
};

char new_TownHorde2MonLevel[27 * 2] = {
	0,0,4,11,0,0,2,9, 0,0,0,0,0,0,0,0,0,0,
	0,0,4,11,0,0,2,9, 0,0,0,0,0,0,0,0,0,0,
	0,0,4,11,0,0,2,9, 0,0,0,0,0,0,0,0,0,0,
};

long new_TownsResourceSilo[27 * 7] = {

1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 0, 1, 0, 0,
0, 0, 0, 0, 0, 1, 0,
0, 1, 0, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 1, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 1, 0, 0, 0, 0, 0,

1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 0, 1, 0, 0,
0, 0, 0, 0, 0, 1, 0,
0, 1, 0, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 1, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 1, 0, 0, 0, 0, 0,

1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 0, 1, 0, 0,
0, 0, 0, 0, 0, 1, 0,
0, 1, 0, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 0, 0, 1, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
1, 0, 1, 0, 0, 0, 0,
0, 1, 0, 0, 0, 0, 0,

};

extern "C" __declspec(dllexport) void set_town_resource_silo(long town, long res0, long res1, long res2, long res3, long res4, long res5, long res6) {
	new_TownsResourceSilo[town * 7 + 0] = res0;
	new_TownsResourceSilo[town * 7 + 1] = res1;
	new_TownsResourceSilo[town * 7 + 2] = res2;
	new_TownsResourceSilo[town * 7 + 3] = res3;
	new_TownsResourceSilo[town * 7 + 4] = res4;
	new_TownsResourceSilo[town * 7 + 5] = res5;
	new_TownsResourceSilo[town * 7 + 6] = res6;
}

long new_TownsBlackSmithCreatures[] = {
	0x92,0x93,0x94,0x94,0x93,0x92,0x94,0x93,0x92,
	0x92,0x93,0x94,0x94,0x93,0x92,0x94,0x93,0x92,
	0x92,0x93,0x94,0x94,0x93,0x92,0x94,0x93,0x92,
};//0x93
long new_TownsBlackSmithArtifacts[] = {
	4,-1,6,-1,5,-1,5,-1,6,-1,4,-1,5,-1,6,-1,4,-1,
	4,-1,6,-1,5,-1,5,-1,6,-1,4,-1,5,-1,6,-1,4,-1,
	4,-1,6,-1,5,-1,5,-1,6,-1,4,-1,5,-1,6,-1,4,-1,
};//0x93


char text_TownsBlackSmithDesc[27][512] = {
	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",

	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",

	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",
	"The Blacksmith provides your armies with Ammo Carts.",
	"The Blacksmith provides your armies with First Aid Tents.",
	"The Blacksmith provides your armies with Ballistas.",
};

char* new_TownsBlackSmithDesc[27];


char text_CasShipTable[27][16] = {
	"AB02_.def", "", "", "","AB01_.def",  "", "","AB03_.def", "AB01_.def",
	"AB02_.def", "", "", "","AB01_.def",  "", "","AB03_.def", "AB01_.def",
	"AB02_.def", "", "", "","AB01_.def",  "", "","AB03_.def", "AB01_.def",
	// "bad"
};
char* new_CasShipTable[27];

// char text_CasBMDescrTable[27][16];
// long new__CasBMDescrTable[27];

extern "C" __declspec(dllexport) void set_town_blacksmith(long town, long cre0, long art0, long art1, char* desc) {
	new_TownsBlackSmithCreatures[town] = cre0;
	new_TownsBlackSmithArtifacts[town * 2 + 0] = art0;
	new_TownsBlackSmithArtifacts[town * 2 + 1] = art1;

	if (desc) strcpy_s(text_TownsBlackSmithDesc[town],desc);
}


long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7] = {};

int get_town_type(int mon_id) {
	for (int i = 0; i < 27 * 2 * 7; ++i) {
		if (mon_id == MonstersInTowns_27x2x7[i])
			return (i / 14);
	}
	return -1;
}

_LHF_(AI_Player_Hero_GetArmyFromTown_0042BCBD) {
	// WiP

	long town = c->eax;
	long offset = c->ebx;

	long monster = MonstersInTowns_27x2x7[town * 14 + offset];
	c->ecx = *(int*) 0x006747B0 + monster * 0x74;

	c->return_address = 0x0042BCE4;
	return NO_EXEC_DEFAULT;

}

/*
_LHF_(AI_Town_BuyCreatures_00428602) {
	if (*(int*)c->esi <= 0 ) {
		c->return_address = 0x00428623;
		return NO_EXEC_DEFAULT;
	}

	c->return_address = 0x0042860B;
	int& v3 = c->edi;
	h3::H3Town* a2 = (h3::H3Town*)*(int*)(c->ebp + 8);
	
	//if (v3 < 7) return EXEC_DEFAULT;
	if (v3 < 7) {
		c->eax = MonstersInTowns_27x2x7[long(a2->type) * 14 + v3];
		return NO_EXEC_DEFAULT;
	}

	if (a2->IsBuildingBuilt(64 + v3)) {
		c->eax = ThirdUpgradesInTowns_27x2x7[long(a2->type) *14 + v3];
		return NO_EXEC_DEFAULT;
	}
	if (a2->IsBuildingBuilt(64 + v3 - 7)) {
		c->eax = ThirdUpgradesInTowns_27x2x7[long(a2->type) * 14 + v3 - 7];
		return NO_EXEC_DEFAULT;
	}
	c->eax = MonstersInTowns_27x2x7[long(a2->type) * 14 + v3];
	return NO_EXEC_DEFAULT;
}
*/

_LHF_(AI_Town_BuyCreatures2_004285EC)
{
	h3::H3Town* a2 = (h3::H3Town*)*(int*)(c->ebp + 8);
	char* v9 = (char*)*(int*)(c->ebp - 0x5c);
	int& v17 = *(int*)(c->ebp - 0x30);
	int& v19 = *(int*)(c->ebp - 0x28);
	int& v21 = *(int*)(c->ebp - 0x20);
	int& iterator = *(int*)(c->ebp - 0x10); 	   // v23  = iterator [1..14]    1..7  are Upgraded creatures available to recruit
	                                               //                            8..14 are Unupgraded creatures available to recruit
    short* available_creatures = (short*)c->esi;

	long* base = &ThirdUpgradesInTowns_27x2x7[long(a2->type) * 14];

	if (iterator <= 7 && *available_creatures > 0)                                 // if it's an upgraded monster
	{
		int creatureToHire = -1;
		if (a2->IsBuildingBuilt(64 + 7 + iterator) && base[14 - iterator] >= 0) // and 4th Upgrade building is built 
			creatureToHire = base[14 - iterator];
		else
		if (a2->IsBuildingBuilt(64 + (7 - iterator)) && base[7 - iterator] >= 0)      // and 3rd Upgrade building is built 
			creatureToHire = base[7 - iterator];

		if (creatureToHire >= 0)
		{
			int v5 = THISCALL_4(int, 0x004286B0, &v21, creatureToHire, available_creatures, 0);
			THISCALL_3(int, 0x0054CAC0, &v17, v19, v5);         // override upgraded monster information with 4th Upgrade Monster

			c->return_address = 0x00428623;
			return SKIP_DEFAULT; // skip original code
		}
	}
	return EXEC_DEFAULT;
}

#define o_GameMgr h3::H3Main::Get()
signed __int64 __stdcall hook_sub_005271A0(HiHook *h, int a1, int* resourcePointer)
{
	double* resourceImportance; // esi
	int v4; // ebx
	signed __int64 result; // rax
	int v6; // [esp+14h] [ebp-4h]

	if (a1 < 0 || a1>7)
		a1 = *(int*)0x0069CCF4;  //o_GameMgr->GetPlayer()->ownerID;
	v6 = 0; auto& player = o_GameMgr->players[a1];
	resourceImportance = (double*) (((char*) &player) + 0x128);
	// resourceImportance = o_GameMgr->players[a1].resourceImportance;
	v4 = 7;
	do
	{
		result = (__int64)((double)*resourcePointer++ * *resourceImportance++ + (double)v6);
		--v4;
		v6 = result;
	} while (v4);
	return result;
}

/*_LHF_(sub_005271A0_005271C5)
{
	
	int playerNumber = o_GameMgr->GetPlayerID();  //c->ecx;
//	char* v9 = (char*)*(int*)(c->ebp - 0x5c);
//	double* resourceImportance = (double*) *(o_GameMgr->players[c->ecx])) + 0x128;
	//auto& player = o_GameMgr->players[c->ecx];
	//((h3::H3Player)player)

	double* resourceImportance = (double*)(c->esi);


    //uintptr_t address = reinterpret_cast<uintptr_t>(&player);
	//address += 0x128;
	//auto& resourceImportance = *reinterpret_cast<double*>(player.);
/*	for (int i = 0;i<7;i++)
	  resourceImportance[i] = 0.00;
	
	return EXEC_DEFAULT;
}
*/

void upgrade_units_quick(h3::H3Army* army, int index, int upgType);
_LHF_(hook_00525B88) {
	h3::H3Town* town = (h3::H3Town*)c->esi;

	h3::H3Army* Army_Up		= (town->garrisonHero >= 0) ? &town->GetGarrisonHero()->army : &town->guards;
	h3::H3Army* Army_Down	= (town->visitingHero >= 0) ? &town->GetVisitingHero()->army : nullptr;

	long* base = &ThirdUpgradesInTowns_27x2x7[long(town->type) * 14];
	long* help = &MonstersInTowns_27x2x7[long(town->type) * 14 + 7];
	auto& have = h3::H3ActivePlayer::Get()->playerResources;

	for (int i = 0; i < 14; ++i) {
		if (town->IsBuildingBuilt(64 + i)) {
			long source_creature = (i<7) ? help[i] : base[i - 7];
			long target_creature = base[i];
			if (target_creature < 0) continue;
			auto info = h3::H3CreatureInformation::Get();

			for (int j = 0; j < 7; ++j) {
				if (Army_Up && Army_Up->type[j] >= 0) {
					if (source_creature == Army_Up->type[j])
					{
						h3::H3Resources res = info[source_creature].UpgradeCost(info+target_creature,Army_Up->count[j]);
						if (res <= have) { upgrade_units_quick(Army_Up,j,target_creature); have -= res; }
					}
				}
			}

			for (int j = 0; j < 7; ++j) {
				if (Army_Down && Army_Down->type[j] >= 0) {
					if (source_creature == Army_Down->type[j])
					{
						h3::H3Resources res = info[source_creature].UpgradeCost(info + target_creature, Army_Down->count[j]);
						if (res <= have) { upgrade_units_quick(Army_Down, j, target_creature); have -= res; }
					}
				}
			}

		}
	}

	return EXEC_DEFAULT;
}

void patch_MonstersInTowns(void) {
	static bool first_time = true;
	
	if(first_time)
		Z_new_towns->WriteLoHook(0x0042BCBD, AI_Player_Hero_GetArmyFromTown_0042BCBD); // WiP

	if (first_time) memset(ThirdUpgradesInTowns_27x2x7, 0xff, sizeof(ThirdUpgradesInTowns_27x2x7));
	for (int adr : adressOf_MonstersInTowns_9x2x7) {
		if (first_time && (*(int*)adr) != 0x6747B4) {
			char msg[512]; 
			sprintf_s(msg, "Wrong value at adress %d, found %d, expected 0x6747B4", adr, *(int*)adr);
			MessageBoxA(0, msg , "new_towns.era" , MB_ICONERROR);
		}
		Z_new_towns->WriteDword(adr, (int) MonstersInTowns_27x2x7);
	}
	Z_new_towns->WriteByte(0x00732BAE + 3, 27 - 1);

	// if (first_time) Z_new_towns->WriteLoHook(0x00428602, AI_Town_BuyCreatures_00428602);
	if (first_time) Z_new_towns->WriteLoHook(0x004285EC, AI_Town_BuyCreatures2_004285EC);
	//Z_new_towns->WriteLoHook(0x005271C5, sub_005271A0_005271C5);
	// 
	
	Z_new_towns->WriteHiHook(0x005271A0, SPLICE_, EXTENDED_, FASTCALL_, hook_sub_005271A0);


	// 
	// if (first_time) Z_new_towns->WriteLoHook(0x004285EC, hook_0042860B);
	if (first_time) Z_new_towns->WriteLoHook(0x00525B88, hook_00525B88);

	Z_new_towns->WriteDword(0x005765AA + 2, 3 * 4 + (int)MonstersInTowns_27x2x7);
	Z_new_towns->WriteDword(0x0047AB33 + 3, 7 * 4 + (int)MonstersInTowns_27x2x7);
	Z_new_towns->WriteDword(0x004BF302 + 2, 7 * 4 + (int)MonstersInTowns_27x2x7);
	Z_new_towns->WriteDword(0x00525A8B + 3, 7 * 4 + (int)MonstersInTowns_27x2x7);
	Z_new_towns->WriteDword(0x005C0527 + 4, 7 * 4 + (int)MonstersInTowns_27x2x7);
	// Z_new_towns->WriteDword(0x004C69AF + 3, long(MonstersInTowns_27x2x7) - 0x78);
	// Z_new_towns->WriteWord(0x004C69B6, 0x9090);

	memcpy(new_Hordes, (void*)0x006887F0, 0x00688910 - 0x006887F0);
	memcpy(new_Hordes + 9 * 2, (void*)0x006887F0, 0x00688910 - 0x006887F0);
	memcpy(new_Hordes + 18 *2, (void*)0x006887F0, 0x00688910 - 0x006887F0);

	Z_new_towns->WriteDword(0x005BE369 + 1, (long)new_Hordes);
	Z_new_towns->WriteDword(0x005C166B + 3, (long)new_Hordes);

	Z_new_towns->WriteDword(0x5BFBEF, 4 + (long)new_Hordes);
	Z_new_towns->WriteDword(0x5C0186, 4 + (long)new_Hordes);
	Z_new_towns->WriteDword(0x5BEDA7, 6 + (long)new_Hordes);
	Z_new_towns->WriteDword(0x5BFB2E, 6 + (long)new_Hordes);
	Z_new_towns->WriteDword(0x5BFBE4, 6 + (long)new_Hordes);
	Z_new_towns->WriteDword(0x5C015F, 6 + (long)new_Hordes);

	Z_new_towns->WriteDword(0x005C168D + 3,  (long) new_TownsResourceSilo);
	Z_new_towns->WriteDword(0x005BFA90 + 4, 0x18 + (long)new_TownsResourceSilo);

	Z_new_towns->WriteDword(0x5D17AF + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D18C1 + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D1B02 + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D1FA1 + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D1FD3 + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D224C + 3, long(new_TownsBlackSmithCreatures));
	Z_new_towns->WriteDword(0x5D2289 + 3, long(new_TownsBlackSmithCreatures));

	Z_new_towns->WriteDword(0x525DE0 + 3, long(new_TownsBlackSmithArtifacts));
	Z_new_towns->WriteDword(0x5D1D89 + 3, long(new_TownsBlackSmithArtifacts));
	Z_new_towns->WriteDword(0x5D1EBE + 3, long(new_TownsBlackSmithArtifacts));
	Z_new_towns->WriteDword(0x5D223F + 3, long(new_TownsBlackSmithArtifacts));
	Z_new_towns->WriteDword(0x5D1ECB + 3, 4 + long(new_TownsBlackSmithArtifacts));

	for (int i = 0; i < 27; ++i) new_TownsBlackSmithDesc[i] = text_TownsBlackSmithDesc[i];
	Z_new_towns->WriteDword(0x5D2E64 + 3, long(new_TownsBlackSmithDesc));

	Z_new_towns->WriteDword(0x005C7CD2 + 3, long(new_TownHorde1MonLevel) - 0x12);
	Z_new_towns->WriteDword(0x005D38F3 + 3, long(new_TownHorde1MonLevel) - 0x12);
	Z_new_towns->WriteDword(0x005D392E + 3, long(new_TownHorde1MonLevel) - 0x12);

	Z_new_towns->WriteDword(0x005C7D0B + 3, long(new_TownHorde2MonLevel) - 0x18);
	Z_new_towns->WriteDword(0x005D4202 + 3, long(new_TownHorde2MonLevel) - 0x18);
	Z_new_towns->WriteDword(0x005D423B + 3, long(new_TownHorde2MonLevel) - 0x18);

	Z_new_towns->WriteDword(0x005BE36E + 3, 27); // number of towns

	for (int i = 0; i < 27; ++i) new_CasShipTable[i] = text_CasShipTable[i];
	Z_new_towns->WriteDword(0x005D2676 + 3, long(new_CasShipTable));

	first_time = false;
}