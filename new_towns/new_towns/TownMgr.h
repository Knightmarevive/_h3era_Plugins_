#pragma once

struct TownMgr; // size: 472


/*NOALIGN*/ struct TownMgr
{
	byte field_0[56];

	Town* town; //+56
	byte field_48[220];
	Dlg* dlg; // +280
	GarrisonBar* garribar_up; //+284
	GarrisonBar* garribar_down; //+288

	GarrisonBar* garribar_sel; //+292
	int garribar_slot_index_sel; //+296

	GarrisonBar* garribar_src; //+300
	int garribar_slot_index_src; // +304
	GarrisonBar* garribar_dst; //+308
	int garribar_slot_index_dst; //+312

	ptr resources_bar;//+316

	dword field_140;//+320

	char statusbar_text[88]; // +324

	int command_code; //+412

	byte f1A0[56]; // +1A0h


	// normal

	void CreateNewGarriBars() { CALL_1(void, thiscall, 0x5C7210, this); }
	void MoveHeroUp() { CALL_1(void, thiscall, 0x5D5550, this); }
	void MoveHeroDown() { CALL_1(void, __thiscall, 0x5D5620, this); }

	// my 

	inline void DeleteGarriBars()
	{
		if (this->garribar_up) o_Delete(this->garribar_up); this->garribar_up = NULL;
		if (this->garribar_up) o_Delete(this->garribar_down); this->garribar_down = NULL;
	}

	inline void SwapHeroes() { this->town->SwapHeroes(); DeleteGarriBars(); CreateNewGarriBars(); }

};