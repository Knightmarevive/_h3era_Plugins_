#pragma once

#pragma warning(disable:4996)

#include "patcher_x86_commented.hpp"
#include <cstdio>

//#include"lib/H3API.hpp"
//#include"../../__include__/H3API/single_header/H3API.hpp"

bool can_afford_the_building(int town_type, int building_ID);
bool hasAllRequiredBuildings(const h3::H3Town * t, int ID);

struct _Town_ : public h3::H3Town 
{
	static const int GREEN_CANBUILD = -1;
	static const int YELLOW_ALREADYDONE = 0;
	static const int RED_CANTBUILDNOW = 1;
	static const int RED_CANTAFFORD = 2;
	static const int EMPTY = 3;

	inline BOOL IsBuildingBuilt(INT32 id) const {
		return THISCALL_3(BOOL, 0x4305A0, this, id, 0)
			|| THISCALL_3(BOOL, 0x4305A0, this, id, 1);
	}

	/* return TRUE if the following requirements are met
	 * 
	 * - is not already built
	 * - it is possible to build again in current turn and town
	 * - player can pay the cost
	 * - all dependencies are met
	 * - building is enabled -> new fix by SadnessPower
	 */
	inline BOOL CanBeBuiltNow(int32_t building) const 
	{
		/*
		__int64 tmp = *(__int64*)(((char*)this) + 0x150) ;
		BOOL ret = building < 44 ? !(tmp & (1ll << building)): true;
		*/
		return (/* ret && */ !IsBuildingBuilt(building) && !builtThisTurn
			&& can_afford_the_building(type, building)
			&& hasAllRequiredBuildings((h3::H3Town*)this, building));
	}

	inline char CanBuildIcon(int32_t building) const 
	{		
/*		__int64 tmp1 = *(int*)(((int)this) + 0x150);
		__int64 tmp2 = *(int*)(((int)this) + 0x150 + 4);
		//__int64 tmp = *(__int64*)(((int)this) + 0x150) ;

		__int64 tmp = tmp2 << 32 + tmp1; 
		BOOL isBuildingDisabled = building < 44 ? (tmp & (1ll << building)): false;        
		//BOOL isBuildingDisabled2 = building < 44 && !CanBeBuilt(h3::eBuildings(building));*/

		if (IsBuildingBuilt(building))
			return YELLOW_ALREADYDONE;
		else
		if (builtThisTurn /* || isBuildingDisabled*/)
			return RED_CANTBUILDNOW;
		else
		{
			if (hasAllRequiredBuildings(this, building))
				return can_afford_the_building(type, building) ? GREEN_CANBUILD : RED_CANTAFFORD;
			else
				return RED_CANTBUILDNOW;
		}
	}

};

typedef int(*learn_fun_T)(void*,int,int);