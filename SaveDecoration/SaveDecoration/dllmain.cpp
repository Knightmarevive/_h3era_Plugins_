// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "patcher_x86_commented.hpp"
#include <cstdio>

char save_pattern[][64] = {
    "*.gm?", "*.cgm", "*.tgm", ".GM%d", ".CGM", "%s.GM%d", "CGM", "TGM"
};

Patcher* globalPatcher;
PatcherInstance* Z_SaveDecor;

bool check_mod_presence(const char* modname) {
    bool ret = false;
    FILE* fdesc; fopen_s(&fdesc,"Mods\\list.txt", "r");
    if (fdesc)
    {
        //----------
        fseek(fdesc, 0, SEEK_END);
        int fdesc_size = ftell(fdesc);
        rewind(fdesc);
        //----------
        char* buf = (char*)malloc(fdesc_size + 1);
        memset(buf, 0, fdesc_size + 1);
        fread(buf, 1, fdesc_size, fdesc);
        buf[fdesc_size] = 0;
        fclose(fdesc);

        ret = strstr(buf, modname);

        free(buf);
        fclose(fdesc);
    }
    return ret;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {   
        globalPatcher = GetPatcher();
        Z_SaveDecor = globalPatcher->CreateInstance("Z_SaveDecor");
        bool KK = check_mod_presence("Knightmare Kingdoms");
        bool TUM = check_mod_presence("ResOunD");
        bool ACM = check_mod_presence("Advanced Classes Mod");
        for (int i = 0; i < 8; ++i) {
            if (KK)strcat_s(save_pattern[i], "_KK");
            if (TUM)strcat_s(save_pattern[i], "_TUM");
            if (ACM)strcat_s(save_pattern[i], "_ACM");
        }
    }
        Z_SaveDecor->WriteDword(0x41814E, (int)save_pattern[3]); // .GM%d
        Z_SaveDecor->WriteDword(0x418139, (int)save_pattern[4]); // .CGM
        Z_SaveDecor->WriteDword(0x4BEC71, (int)save_pattern[5]); // %s.GM%d
        Z_SaveDecor->WriteDword(0x4BEC27, (int)save_pattern[6]); // CGM
        Z_SaveDecor->WriteDword(0x4BEC3B, (int)save_pattern[7]); // TGM
        Z_SaveDecor->WriteDword(0x6834AC, (int)save_pattern[0]); // *.gm?
        Z_SaveDecor->WriteDword(0x6834B0, (int)save_pattern[1]); // *.cgm
        Z_SaveDecor->WriteDword(0x6834B8, (int)save_pattern[2]); // *.tgm
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

