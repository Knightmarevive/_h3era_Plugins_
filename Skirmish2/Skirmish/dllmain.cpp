// dllmain.cpp : Defines the entry point for the DLL application.
// #include "__include/patcher_x86_commented.hpp"
// #include "../../__include__/patcher_x86_commented.hpp"

#include "pch.h"
#include "__include/era.h"
// #include <msclr\marshal_cppstd.h>
// #include "../../__include__/patcher_x86_commented.hpp"


#define _H3API_PATCHER_X86_
#define _H3API_PLUGINS_
#include "../../__include__/H3API/single_header/H3API.hpp"
#define MONSTERS_AMOUNT 1024

#define CALL_12(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12))


void ApplyEffects(void);
void ApplyOneSlotCombos(void);
void ApplySpeed(void);
void ApplyMight(void);

extern bool justloaded;
extern char save_state[256];
// #include "MyForm.h"


#define PINSTANCE_MAIN "Z_Skirmish"

Patcher* globalPatcher;
PatcherInstance* Z_Skirmish;

void apply_discount(int day);
extern int non_discount_prices[MONSTERS_AMOUNT];

void __stdcall StoreData(Era::TEvent* e)
{
    Era::WriteSavegameSection(256, save_state, PINSTANCE_MAIN);
    Era::WriteSavegameSection(sizeof(non_discount_prices), non_discount_prices, PINSTANCE_MAIN);

    //ApplyEffects();
}

void __stdcall RestoreData(Era::TEvent* e)
{
    justloaded = true;

    Era::ReadSavegameSection(256, save_state, PINSTANCE_MAIN);
    if (!Era::ReadSavegameSection(sizeof(non_discount_prices), non_discount_prices, PINSTANCE_MAIN))
        apply_discount(0);

    //ApplyEffects();


    ApplyOneSlotCombos();
    ApplySpeed();

    ApplyMight();
}

BOOL DirectoryExists(const char* dirName) {
    DWORD attribs = ::GetFileAttributesA(dirName);
    if (attribs == INVALID_FILE_ATTRIBUTES) {
        return false;
    }
    return (attribs & FILE_ATTRIBUTE_DIRECTORY);
}

inline void v_MsgBox(char* text, int style)
{
    CALL_12(void, __fastcall, 0x4F6C00, text, style, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
    return;
}

void __stdcall ApplyData(Era::TEvent* e) {
    ApplyEffects();
    // Era::ExecErmCmd("IF:L^Skirmish Settings Applied^;");

    Z_Skirmish->WriteDword(0x6834A4, 0x2); //rook as internal difficulty
}

namespace Skirmish {
    char SkirmishMsg[512] = "Choose Skirmish Name and other options";
    char SkirmishName[512] = {};
    int Difficulty;
    int MaxHeroes;
}

struct Skirmish_Dlg : public h3::H3Dlg
{
    Skirmish_Dlg() : H3Dlg(640, 480, -1, -1, 0, 0) {
        AddBackground(0, 0, widthDlg, heightDlg, false, false, 0, false);

        int frame_color = ((h3::H3BasePalette565*)*(int*)0x006AAD18)->color[45];

        CreateFrame(110, 15, 487, 34, frame_color);
        CreateEdit(116, 16, 480, 32, 16, Skirmish::SkirmishName, h3::NH3Dlg::Text::MEDIUM, 1, 4, nullptr, 1001);
        CreateText(16, 16, 100, 32, "Skirmish:", h3::NH3Dlg::Text::MEDIUM, 1, -1);

        CreateText(16, 64, 100, 32, "Heroes:", h3::NH3Dlg::Text::MEDIUM, 1, -1);

        auto& saved_speed = *(long*)(save_state + 4);
        auto& saved_income = *(long*)(save_state + 8);
        auto& saved_neutral_might = *(long*)(save_state + 12);
        char options[] = { 0,1,2,3,16,17,18 };
        if (false && !Skirmish::MaxHeroes) {
            saved_speed  = 150;
            saved_income = 200;
            saved_neutral_might = 144;
            Skirmish::MaxHeroes = 8;
            for (auto opt : options)
                save_state[opt] = 1;
        }
        for (int i = 0; i <= 7; ++i) 
            CreateFrame(115 + 48 * i, 63, 32, 34, 2101 + i, frame_color)
                ->SendCommand(6 - (i+1== Skirmish::MaxHeroes), 4);
        
        CreateButton(116 + 48 * 0, 64, 2001, "RanNum1.def");
        CreateButton(116 + 48 * 1, 64, 2002, "RanNum2.def");
        CreateButton(116 + 48 * 2, 64, 2003, "RanNum3.def");
        CreateButton(116 + 48 * 3, 64, 2004, "RanNum4.def");
        CreateButton(116 + 48 * 4, 64, 2005, "RanNum5.def");
        CreateButton(116 + 48 * 5, 64, 2006, "RanNum6.def");
        CreateButton(116 + 48 * 6, 64, 2007, "RanNum7.def");
        CreateButton(116 + 48 * 7, 64, 2008, "RanNum8.def");

        char buf[32] = "";
        CreateText(16, 112, 100, 32, "Percentages:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        CreateText(116, 112, 100, 32, "Hero Speed:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        itoa(saved_speed, buf, 10);
        CreateEdit(216, 112, 480, 32, 16, buf, h3::NH3Dlg::Text::MEDIUM, 1, 4, nullptr, 1002);
        CreateText(300, 112, 100, 32, "Town Income:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        itoa(saved_income, buf, 10);
        CreateEdit(400, 112, 480, 32, 16, buf, h3::NH3Dlg::Text::MEDIUM, 1, 4, nullptr, 1003);
        CreateText(486, 112, 100, 32, "Neutral Might:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        itoa(saved_neutral_might, buf, 10);
        CreateEdit(586, 112, 480, 32, 16, buf, h3::NH3Dlg::Text::MEDIUM, 1, 4, nullptr, 1004);

        for (int i = 1; i <= 7; ++i) {
            CreateFrame(16, 160 + 32 * i, 100, 32, 2999 + i, frame_color)
                ->SendCommand(6 - (i == Skirmish::Difficulty + 1), 4);
        }   // Skirmish::Difficulty = 3;
        CreateText(16, 160, 100, 32, "Difficulty:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        CreateText(16, 160 + 32 * 1, 100, 32, "Chieftain", h3::NH3Dlg::Text::MEDIUM, 5, 3100);
        CreateText(16, 160 + 32 * 2, 100, 32, "Leader", h3::NH3Dlg::Text::MEDIUM, 5, 3101);
        CreateText(16, 160 + 32 * 3, 100, 32, "General", h3::NH3Dlg::Text::MEDIUM, 5, 3102);
        CreateText(16, 160 + 32 * 4, 100, 32, "Duke", h3::NH3Dlg::Text::MEDIUM, 5, 3103);
        CreateText(16, 160 + 32 * 5, 100, 32, "King", h3::NH3Dlg::Text::MEDIUM, 5, 3104);
        CreateText(16, 160 + 32 * 6, 100, 32, "Emperor", h3::NH3Dlg::Text::MEDIUM, 5, 3105);
        CreateText(16, 160 + 32 * 7, 100, 32, "Deity", h3::NH3Dlg::Text::MEDIUM, 5, 3106);

        for (int i = 1; i <= 7; ++i) {
            CreateFrame(116, 160 + 32 * i, 200, 32, 4000 + i, frame_color)
                ->SendCommand(6 - (bool)save_state[options[i-1]], 4);
        }
        CreateText(116, 160, 200, 32, "Options:", h3::NH3Dlg::Text::MEDIUM, 1, -1);
        CreateText(116, 160 + 32 * 1, 200, 32, "One Slot Combos", h3::NH3Dlg::Text::MEDIUM, 5, 4101);       // save_state[0] = 0;
        CreateText(116, 160 + 32 * 2, 200, 32, "Humans Open Map", h3::NH3Dlg::Text::MEDIUM, 5, 4102);       // save_state[1] = 0;
        CreateText(116, 160 + 32 * 3, 200, 32, "AI Open Map", h3::NH3Dlg::Text::MEDIUM, 5, 4103);           // save_state[2] = 0;
        CreateText(116, 160 + 32 * 4, 200, 32, "Reverse Hero Speed", h3::NH3Dlg::Text::MEDIUM, 5, 4104);    // save_state[3] = 0;
        CreateText(116, 160 + 32 * 5, 200, 32, "Monsters discount on Sunday", h3::NH3Dlg::Text::MEDIUM, 5, 4105); // save_state[16] = 0;
        CreateText(116, 160 + 32 * 6, 200, 32, "Guarded AI Towns at Day One", h3::NH3Dlg::Text::MEDIUM, 5, 4106); // save_state[17] = 0;
        CreateText(116, 160 + 32 * 7, 200, 32, "AI Builds Towns Much Faster", h3::NH3Dlg::Text::MEDIUM, 5, 4107); // save_state[18] = 0;


        CreateText(128, 440, 512, 32, Skirmish::SkirmishMsg, h3::NH3Dlg::Text::MEDIUM, 1, 888);
        // CreateText(128, 440, 128, 32, "Complete and press ok.", h3::NH3Dlg::Text::MEDIUM, 1, 888);

    };
    BOOL DialogProc(h3::H3Msg& msg) override;

    BOOL proper_entry();
    void fix_percentage(int dial);
};


void Skirmish_Dlg::fix_percentage(int dial) {
    auto target = this->GetEdit(dial);
    auto txt = target->GetText();
    if (!*txt) { target->SetText("100"); return; }
    auto ptr = txt;
    while (*ptr) {
        if (*ptr < '0' || *ptr>'9' || *txt == 0 || ptr - txt > 7) {
            target->SetText("100");
            // this->seed = 0;
            this->Redraw();
            return; // return 0;
        }
        ++ptr;
    }

}

BOOL Skirmish_Dlg::proper_entry() {

    int skirmish_name_length = strlen(Skirmish::SkirmishName);

    if (skirmish_name_length < 5) {
        if (!skirmish_name_length) {
            strcpy_s(Skirmish::SkirmishName, "KK_001");
            strcpy_s(Skirmish::SkirmishMsg, "Skirmish require a name");
            return false;
        }
        strcpy_s(Skirmish::SkirmishMsg, "Skirmish name too short");
        //v_MsgBox("Skirmish name too short", 1);
        return false;
    }
    if (skirmish_name_length > 15) {

        strcpy_s(Skirmish::SkirmishMsg, "Skirmish name too long");
        //v_MsgBox("Skirmish name too long", 1);
        return false;
    }

    char skirmish_dir[512] = ".\\games\\_";
    strcpy_s(skirmish_dir + 8, 500, Skirmish::SkirmishName);
    if (DirectoryExists(skirmish_dir)) {
        int offset = strlen(Skirmish::SkirmishName) -1;
        while (offset > 0 
            && Skirmish::SkirmishName[offset - 1] >= '0'
            && Skirmish::SkirmishName[offset - 1] <= '9'
            ) --offset;
        int number = atoi(Skirmish::SkirmishName + offset);
        do {
            ++number; sprintf(Skirmish::SkirmishName + offset, "%03d", number);
            strcpy_s(skirmish_dir + 8, 500, Skirmish::SkirmishName);
        } while (DirectoryExists(skirmish_dir));

        strcpy_s(Skirmish::SkirmishMsg, "Skirmish Name already taken");
        return false;
    }


    if (!Skirmish::MaxHeroes) {
        strcpy_s(Skirmish::SkirmishMsg, "Choose max hero count");
        /*
        Skirmish::MaxHeroes = 8; 
        this->GetH3DlgItem(Skirmish::MaxHeroes + 2100)
            ->SendCommand(5, 4);
        */
        return false;
    }

    strcpy_s(Skirmish::SkirmishMsg, "Choose Skirmish Name and other options");
    return true;
}

BOOL Skirmish_Dlg::DialogProc(h3::H3Msg& msg) {
    if (msg.IsKeyPress()) {
        auto target = this->GetEdit(1001);
        auto txt = target->GetText();
        if (*txt) strcpy_s(Skirmish::SkirmishName, txt);

        auto& saved_speed = *(long*)(save_state + 4);
        auto& saved_income = *(long*)(save_state + 8);
        auto& saved_neutral_might = *(long*)(save_state + 12);

        fix_percentage(1002); saved_speed = atoi(this->GetEdit(1002)->GetText());
        fix_percentage(1003); saved_income = atoi(this->GetEdit(1003)->GetText());
        fix_percentage(1004); saved_neutral_might = atoi(this->GetEdit(1004)->GetText());

        this->Redraw();
    }

    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (msg.itemId > 2000 && msg.itemId < 2010) {
            Skirmish::MaxHeroes = msg.itemId - 2000; 
            for (int i = 2101; i <= 2108; ++i) {
                this->GetH3DlgItem(i)
                    ->SendCommand(6 - (i == msg.itemId + 100), 4);
            }
            this->Redraw();
        }
        if (msg.itemId >= 3100 && msg.itemId < 3110) {
            Skirmish::Difficulty = msg.itemId - 3100;
            for (int i = 3000; i < 3007; ++i) {
                this->GetH3DlgItem(i)
                    ->SendCommand(6 - (i == msg.itemId - 100), 4);
            }
            this->Redraw();
        }
        if (msg.itemId >= 4100 && msg.itemId < 4110) {
            if (msg.itemId == 4101) {
                save_state[0] = !(bool)save_state[0];
                this->GetH3DlgItem(4001)->SendCommand(6 - (save_state[0]), 4);
            }
            else if (msg.itemId == 4102) {
                save_state[1] = !(bool)save_state[1];
                this->GetH3DlgItem(4002)->SendCommand(6 - (save_state[1]), 4);
            }
            else if (msg.itemId == 4103) {
                save_state[2] = !(bool)save_state[2];
                this->GetH3DlgItem(4003)->SendCommand(6 - (save_state[2]), 4);
            }
            else if (msg.itemId == 4104) {
                save_state[3] = !(bool)save_state[3];
                this->GetH3DlgItem(4004)->SendCommand(6 - (save_state[3]), 4);
            }
            else if (msg.itemId == 4105) {
                save_state[16] = !(bool)save_state[16];
                this->GetH3DlgItem(4005)->SendCommand(6 - (save_state[16]), 4);
            }
            else if (msg.itemId == 4106) {
                save_state[17] = !(bool)save_state[17];
                this->GetH3DlgItem(4006)->SendCommand(6 - (save_state[17]), 4);
            }
            else if (msg.itemId == 4107) {
                save_state[18] = !(bool)save_state[18];
                this->GetH3DlgItem(4007)->SendCommand(6 - (save_state[18]), 4);
            }
            this->Redraw();
        }
    }

    return 0;
}

_LHF_(LHF_DLG) {
    Z_Skirmish->WriteDword(0x6834A4, 0x2); //rook as internal difficulty

    
    auto f_in = fopen("Runtime\\Skirmish.bin", "rb");
    if (f_in) {
        fread(save_state, sizeof(save_state), 1, f_in);
        fread(&Skirmish::MaxHeroes,4,1,f_in);
        fread(&Skirmish::Difficulty, 4, 1, f_in);
        fread(Skirmish::SkirmishName, sizeof(Skirmish::SkirmishName), 1, f_in);
        fclose(f_in);
    }
    else {
        auto& saved_speed = *(long*)(save_state + 4);
        auto& saved_income = *(long*)(save_state + 8);
        auto& saved_neutral_might = *(long*)(save_state + 12);
        char options[] = { 0,1,2,3,16,17,18 };

        saved_speed = 150;
        saved_income = 200;
        saved_neutral_might = 144;
        Skirmish::MaxHeroes = 8;
        Skirmish::Difficulty = 3;
        for (auto opt : options)
            save_state[opt] = 1;
    }
    

    bool good = true;
    do {
        Skirmish_Dlg wnd;
        wnd.CreateOKButton();
        wnd.Start();
        good = wnd.proper_entry();
    } while (!good);

    CreateDirectoryA("Runtime",nullptr);
    auto f_out = fopen("Runtime\\Skirmish.bin", "wb");
    if (f_out) {
        fwrite(save_state, sizeof(save_state), 1, f_out);
        fwrite(&Skirmish::MaxHeroes, 4, 1, f_out);
        fwrite(&Skirmish::Difficulty, 4, 1, f_out);

        fwrite(Skirmish::SkirmishName, sizeof(Skirmish::SkirmishName), 1, f_out);
        fclose(f_out);
    }

    ApplyOneSlotCombos();
    ApplySpeed();

    return EXEC_DEFAULT;
}

void __stdcall Z_dialogue(Era::TEvent* e) {

    /*
    // memset(save_state, 0, 256);
    Skirmish::MyForm mf;
    mf.Difficulty_List->SetItemChecked(3,true);
    mf.Players_Combo->SelectedIndex = 0;
    mf.ShowDialog();
    //mf.Show(0); mf.Activate();
    */
    /*
    Skirmish_Dlg wnd;
    // wnd.CreateOKButton();
    // wnd.Start();
    */

    ApplyOneSlotCombos();
    ApplySpeed();
}

void __stdcall z_OnAfterErmInstructions(Era::TEvent* e) {
    strcpy_s(Era::z[999], Skirmish::SkirmishName); char buf[512];
    sprintf(buf, "FU55489:P%d;", 1 << Skirmish::Difficulty); Era::ExecErmCmd(buf);
    sprintf(buf, "SN:W^KK_GAME_ADV_HERO_LIMIT^/%d;", Skirmish::MaxHeroes); Era::ExecErmCmd(buf);

    ApplyMight();


    if (save_state[17]) for (auto& t : h3::H3Game::Get()->towns) {
        // auto &nfo = h3::H3Main::Get()->playersInfo;
        auto H3G = h3::H3Main::Get();
        if (t.owner == -1 || /*nfo.playerType[t.owner] == 10*/
            !H3G->IsHuman(t.owner)) {
            for (int i = 0; i < 7; ++i) {
                Era::v[2] = i; Era::v[3] = t.type;
                Era::ExecErmCmd("UN:Tv3/v2/0/?v4;");
                t.guards.type[i] = Era::v[4];
                Era::ExecErmCmd("MA:Gv4/?v5;");
                t.guards.count[i] = Era::v[5];
            }
        }
    }

}

/*
void __stdcall z_Preset_AI_Towns(Era::TEvent* e) {
    if (save_state[17]) for (auto& t : h3::H3Game::Get()->towns) {
        auto& nfo = h3::H3Main::Get()->playersInfo;
        if (t.owner == -1 || nfo.playerType[t.owner] == 10) {
            for (int i = 0; i < 7; ++i) {
                Era::v[2] = i; Era::v[3] = t.type;
                Era::ExecErmCmd("UN:Tv3/v2/0/?v4;");
                t.guards.type[i] = Era::v[4];
                Era::ExecErmCmd("MA:Gv4/?v5;");
                t.guards.count[i] = Era::v[5];
            }
        }
    }
}
*/

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:

        globalPatcher = GetPatcher();
        Z_Skirmish = globalPatcher->CreateInstance(PINSTANCE_MAIN);

        Era::ConnectEra();
        // Era::RegisterHandler(Z_dialogue, "OnBeforeErmInstructions");

        /* {
            auto f = fopen("Runtime\\Skirmish.bin", "rb");
            if (f) {
                fread(save_state, sizeof(save_state), 1, f);
                fread(Skirmish::SkirmishName, sizeof(Skirmish::SkirmishName), 1, f);
                fclose(f);
            }
        } */

        Era::RegisterHandler(StoreData, "OnSavegameWrite");
        Era::RegisterHandler(RestoreData, "OnSavegameRead");
        Era::RegisterHandler(ApplyData, "OnAfterLoadGame");
        // Era::RegisterHandler(ApplyData, "OnBeforeSaveGame");
        Era::RegisterHandler(ApplyData, "OnErmTimer 1");
        Era::RegisterHandler(z_OnAfterErmInstructions, "OnAfterErmInstructions");
        // Era::RegisterHandler(z_Preset_AI_Towns, "OnErmTimer 1");

        Z_Skirmish->WriteLoHook(0x004BFB3B, LHF_DLG);
        
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
