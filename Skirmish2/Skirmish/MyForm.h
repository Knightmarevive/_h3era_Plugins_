#pragma once

#define NOCLR
#ifndef NOCLR


namespace Skirmish {
	char SkirmishName[32]; long MaxHeroes; long Difficulty;

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ ApplyBtn;
	private: System::Windows::Forms::TextBox^ Skirmish_Name;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label_Status;
	public: System::Windows::Forms::CheckedListBox^ Difficulty_List;
	public: System::Windows::Forms::ComboBox^ Players_Combo;
	private: System::Windows::Forms::CheckedListBox^ checked_rules;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::TextBox^ HeroSpeedBox;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::TextBox^ TownIncomeBox;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ NeutralsMightBox;
	public:

	public:

	public:

	public:
	private:


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->ApplyBtn = (gcnew System::Windows::Forms::Button());
			this->Skirmish_Name = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label_Status = (gcnew System::Windows::Forms::Label());
			this->Difficulty_List = (gcnew System::Windows::Forms::CheckedListBox());
			this->Players_Combo = (gcnew System::Windows::Forms::ComboBox());
			this->checked_rules = (gcnew System::Windows::Forms::CheckedListBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->HeroSpeedBox = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->TownIncomeBox = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->NeutralsMightBox = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// ApplyBtn
			// 
			this->ApplyBtn->BackColor = System::Drawing::Color::Black;
			this->ApplyBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->ApplyBtn->ForeColor = System::Drawing::Color::OrangeRed;
			this->ApplyBtn->Location = System::Drawing::Point(455, 380);
			this->ApplyBtn->Name = L"ApplyBtn";
			this->ApplyBtn->Size = System::Drawing::Size(148, 62);
			this->ApplyBtn->TabIndex = 0;
			this->ApplyBtn->Text = L"Apply";
			this->ApplyBtn->UseVisualStyleBackColor = false;
			this->ApplyBtn->Click += gcnew System::EventHandler(this, &MyForm::ApplyBtn_Click);
			// 
			// Skirmish_Name
			// 
			this->Skirmish_Name->BackColor = System::Drawing::Color::Black;
			this->Skirmish_Name->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Skirmish_Name->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Skirmish_Name->Location = System::Drawing::Point(36, 69);
			this->Skirmish_Name->Name = L"Skirmish_Name";
			this->Skirmish_Name->Size = System::Drawing::Size(567, 31);
			this->Skirmish_Name->TabIndex = 1;
			this->Skirmish_Name->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->Skirmish_Name->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Black;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label1->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(163, 25);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(438, 25);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Choose Hero Limit and Enter Skirmish Name";
			// 
			// label_Status
			// 
			this->label_Status->AutoSize = true;
			this->label_Status->BackColor = System::Drawing::Color::Black;
			this->label_Status->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label_Status->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label_Status->Location = System::Drawing::Point(31, 378);
			this->label_Status->MaximumSize = System::Drawing::Size(480, 64);
			this->label_Status->MinimumSize = System::Drawing::Size(400, 64);
			this->label_Status->Name = L"label_Status";
			this->label_Status->Size = System::Drawing::Size(400, 64);
			this->label_Status->TabIndex = 3;
			this->label_Status->Text = L"choose your options and press";
			this->label_Status->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Difficulty_List
			// 
			this->Difficulty_List->BackColor = System::Drawing::Color::Black;
			this->Difficulty_List->CheckOnClick = true;
			this->Difficulty_List->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->Difficulty_List->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Difficulty_List->FormattingEnabled = true;
			this->Difficulty_List->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Chieftain", L"Leader", L"General", L"Duke",
					L"King", L"Emperor", L"Deity"
			});
			this->Difficulty_List->Location = System::Drawing::Point(35, 184);
			this->Difficulty_List->Name = L"Difficulty_List";
			this->Difficulty_List->Size = System::Drawing::Size(122, 186);
			this->Difficulty_List->TabIndex = 4;
			this->Difficulty_List->SelectedValueChanged += gcnew System::EventHandler(this, &MyForm::Difficulty_List_SelectedValueChanged);
			// 
			// Players_Combo
			// 
			this->Players_Combo->BackColor = System::Drawing::Color::Black;
			this->Players_Combo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 32.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Players_Combo->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Players_Combo->FormattingEnabled = true;
			this->Players_Combo->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"0", L"1", L"2", L"3", L"4", L"5", L"6", L"7",
					L"8"
			});
			this->Players_Combo->Location = System::Drawing::Point(36, 12);
			this->Players_Combo->Name = L"Players_Combo";
			this->Players_Combo->Size = System::Drawing::Size(121, 59);
			this->Players_Combo->TabIndex = 5;
			// 
			// checked_rules
			// 
			this->checked_rules->BackColor = System::Drawing::Color::Black;
			this->checked_rules->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->checked_rules->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->checked_rules->FormattingEnabled = true;
			this->checked_rules->Items->AddRange(gcnew cli::array< System::Object^  >(4) {
				L"One Slot Combos", L"Humans Open Map", L"AI Open Map",
					L"Reversed Army Speed"
			});
			this->checked_rules->Location = System::Drawing::Point(163, 184);
			this->checked_rules->Name = L"checked_rules";
			this->checked_rules->Size = System::Drawing::Size(268, 186);
			this->checked_rules->TabIndex = 6;
			this->checked_rules->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::checked_rules_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::Black;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label2->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(29, 106);
			this->label2->MinimumSize = System::Drawing::Size(128, 64);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(128, 64);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Difficulty";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::Black;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label3->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(163, 106);
			this->label3->MinimumSize = System::Drawing::Size(268, 64);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(268, 64);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Game Rules\r\n(for whole Skirmish)";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::Black;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label4->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(450, 106);
			this->label4->MinimumSize = System::Drawing::Size(152, 32);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(152, 32);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Hero Speed";
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// HeroSpeedBox
			// 
			this->HeroSpeedBox->BackColor = System::Drawing::Color::Black;
			this->HeroSpeedBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->HeroSpeedBox->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->HeroSpeedBox->Location = System::Drawing::Point(451, 142);
			this->HeroSpeedBox->Name = L"HeroSpeedBox";
			this->HeroSpeedBox->Size = System::Drawing::Size(150, 30);
			this->HeroSpeedBox->TabIndex = 9;
			this->HeroSpeedBox->Text = L"100";
			this->HeroSpeedBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->HeroSpeedBox->TextChanged += gcnew System::EventHandler(this, &MyForm::HeroSpeedBox_TextChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::Black;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label5->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(450, 175);
			this->label5->MinimumSize = System::Drawing::Size(152, 32);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(152, 32);
			this->label5->TabIndex = 8;
			this->label5->Text = L"Town Income";
			this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label5->Click += gcnew System::EventHandler(this, &MyForm::label5_Click);
			// 
			// TownIncomeBox
			// 
			this->TownIncomeBox->BackColor = System::Drawing::Color::Black;
			this->TownIncomeBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->TownIncomeBox->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->TownIncomeBox->Location = System::Drawing::Point(451, 210);
			this->TownIncomeBox->Name = L"TownIncomeBox";
			this->TownIncomeBox->Size = System::Drawing::Size(150, 30);
			this->TownIncomeBox->TabIndex = 9;
			this->TownIncomeBox->Text = L"100";
			this->TownIncomeBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->TownIncomeBox->TextChanged += gcnew System::EventHandler(this, &MyForm::TownIncomeBox_TextChanged);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->BackColor = System::Drawing::Color::Black;
			this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->label6->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(451, 243);
			this->label6->MinimumSize = System::Drawing::Size(152, 32);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(152, 32);
			this->label6->TabIndex = 8;
			this->label6->Text = L"Neutrals Might";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label6->Click += gcnew System::EventHandler(this, &MyForm::label5_Click);
			// 
			// NeutralsMightBox
			// 
			this->NeutralsMightBox->BackColor = System::Drawing::Color::Black;
			this->NeutralsMightBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->NeutralsMightBox->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)),
				static_cast<System::Int32>(static_cast<System::Byte>(128)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->NeutralsMightBox->Location = System::Drawing::Point(455, 278);
			this->NeutralsMightBox->Name = L"NeutralsMightBox";
			this->NeutralsMightBox->Size = System::Drawing::Size(150, 30);
			this->NeutralsMightBox->TabIndex = 9;
			this->NeutralsMightBox->Text = L"100";
			this->NeutralsMightBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->NeutralsMightBox->TextChanged += gcnew System::EventHandler(this, &MyForm::NeutralsMightBox_TextChanged);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(624, 464);
			this->ControlBox = false;
			this->Controls->Add(this->NeutralsMightBox);
			this->Controls->Add(this->TownIncomeBox);
			this->Controls->Add(this->HeroSpeedBox);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->checked_rules);
			this->Controls->Add(this->Players_Combo);
			this->Controls->Add(this->Difficulty_List);
			this->Controls->Add(this->label_Status);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->Skirmish_Name);
			this->Controls->Add(this->ApplyBtn);
			this->DoubleBuffered = true;
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(640, 480);
			this->MinimizeBox = false;
			this->MinimumSize = System::Drawing::Size(640, 480);
			this->Name = L"MyForm";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->TopMost = true;
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ApplyBtn_Click(System::Object^ sender, System::EventArgs^ e) {
		if (Skirmish_Name->Text->Length < 5 || Skirmish_Name->Text->Length > 24)
		{
			label_Status->Text = "Skirmish name need to have \n between 5 and 24 characters";
			return;
		}
		auto HC = Players_Combo->SelectedIndex;
		if (!HC) {
			label_Status->Text = "Kingdom can have to be \n between 1 and 8 heroes";
			return;
		}
		std::string str = msclr::interop::marshal_as<std::string>(Skirmish_Name->Text);
		strcpy_s(SkirmishName, str.c_str()); 	char buf[512];
		for (int i = 0; i < 7; ++i) if (Difficulty_List->GetItemChecked(i)) {
			Difficulty = i;// sprintf(buf, "FU55489:P%d;", 1 << i); Era::ExecErmCmd(buf);
		}
		MaxHeroes = HC;// sprintf(buf, "SN:W^KK_GAME_ADV_HERO_LIMIT^/%d;", HC); Era::ExecErmCmd(buf);
		// ApplyEffects();
		// Sleep(250);
		this->Close();
	}
	private: System::Void textBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void Difficulty_List_SelectedValueChanged(System::Object^ sender, System::EventArgs^ e) {
		auto id = Difficulty_List->SelectedIndex;
		if ( id != -1) {
			for (int i = 0; i<7; ++i)
				Difficulty_List->SetItemChecked(i, false);
			Difficulty_List->SetItemChecked(id, true);

		}
	}
private: System::Void checked_rules_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	save_state[0] = checked_rules->GetItemChecked(0);
	save_state[1] = checked_rules->GetItemChecked(1);
	save_state[2] = checked_rules->GetItemChecked(2);
	save_state[3] = checked_rules->GetItemChecked(3);
	// 4-7 reserved for herospeed
}
private: System::Void HeroSpeedBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	auto& saved_speed = *(long*)(save_state + 4);
	try {
		saved_speed = Convert::ToInt32(HeroSpeedBox->Text);
		if (saved_speed >= 10000) saved_speed = 10000;
		HeroSpeedBox->Text = saved_speed.ToString();
	}
	catch (System::FormatException^ something) {
		saved_speed = 100;
		HeroSpeedBox->Text = saved_speed.ToString();
	}
}
private: System::Void label5_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void TownIncomeBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	auto& saved_income = *(long*)(save_state + 8);
	try {
		saved_income = Convert::ToInt32(TownIncomeBox->Text);
		if (saved_income >= 10000) saved_income = 10000;
		TownIncomeBox->Text = saved_income.ToString();
	}
	catch (System::FormatException^ something) {
		saved_income = 100;
		TownIncomeBox->Text = saved_income.ToString();
	}
}
private: System::Void NeutralsMightBox_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	auto& saved_neutral_might = *(long*)(save_state + 12);

	try {
		saved_neutral_might = Convert::ToInt32(NeutralsMightBox->Text);
		if (saved_neutral_might >= 10000) saved_neutral_might = 10000;
		NeutralsMightBox->Text = saved_neutral_might.ToString();
	}
	catch (System::FormatException^ something) {
		saved_neutral_might = 100;
		NeutralsMightBox->Text = saved_neutral_might.ToString();
	}
}
};
}

#endif // !NOCLR
