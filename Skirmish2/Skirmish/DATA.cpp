//#include "__include/patcher_x86_commented.hpp"
//#include <windows.h>
#include <cstdio>


#define _H3API_PATCHER_X86_
#define _H3API_PLUGINS_
#include "../../__include__/H3API/single_header/H3API.hpp"

#define PINSTANCE_MAIN "Z_Skirmish"
#define o_ActivePlayerID *(int*)0x69CCF4
#define MONSTERS_AMOUNT 1024

bool justloaded = false;
extern Patcher* globalPatcher;
extern PatcherInstance* Z_Skirmish;
// #include "__include/era.h"
namespace Era {
	extern int* v;
	typedef void(__stdcall* TExecErmCmd) (const char* CmdStr);
	extern TExecErmCmd ExecErmCmd;
};

char save_state[256];

void ApplySpeed(void) {
	auto& saved_speed = *(long*)(save_state + 4);
	if (saved_speed <= 0) saved_speed = 100;
	if (saved_speed >= 10000) saved_speed = 10000;

	auto speed4diozia = GetModuleHandleA("speed4diozia.dll");
	if (speed4diozia) {
		auto do_patch = (void(*)(int, bool))
			GetProcAddress(speed4diozia, "do_patch");
		do_patch(saved_speed, save_state[3]);
	}
	else
	{
		/*
		MessageBoxA(0, (LPCSTR)"missing speed4diozia.dll",
			(LPCSTR)"missing speed4diozia.dll", MB_ICONWARNING);
		*/
	}
}


_LHF_(MightHook) {
	auto& saved_neutral_might = *(long*)(save_state + 12);

	long long ret = c->eax * 100LL;
	ret /= saved_neutral_might;
	c->eax = ret; c->Pop(); c->Push(ret);

	return EXEC_DEFAULT;
}

_LHF_(MightHook2) {
	auto& saved_neutral_might = *(long*)(save_state + 12);

	long long ret = saved_neutral_might;
	ret *= c->ebx;	ret /= 100LL;
	c->ebx = ret;

	return EXEC_DEFAULT;
}

_LHF_(MightHook3) {
	auto& saved_neutral_might = *(long*)(save_state + 12);

	long long ret = c->ecx * 100LL;
	ret /= saved_neutral_might;

	if (!c->eax)
	{
		c->ecx = ret;
		*(int*)(c->ebp - 0x14) = ret;
	}

	return EXEC_DEFAULT;
}

void ApplyMight(void) {
	auto& saved_neutral_might = *(long*)(save_state + 12);

	if (saved_neutral_might <= 0) saved_neutral_might = 100;
	if (saved_neutral_might >= 100000) saved_neutral_might = 100000;

	/*
	long original_call = *(long*)(0x00477278+1);
	if (original_call == 0x0006C3A3) {
		//Z_Skirmish->WriteLoHook(0x00477278, MightHook);
		// Z_Skirmish->WriteLoHook(0x0054120C, MightHook2);
	}
	*/
	static bool once = true; if (once) {
		Z_Skirmish->WriteLoHook(0x004771FD, MightHook3);
		once = false;
	}

	char buf[1024];
	sprintf_s(buf, "SN:W^Knightmare Skirmish saved_neutral_might^/%i;", saved_neutral_might);
	Era::ExecErmCmd(buf);
}

void ApplyOneSlotCombos(void) {
	if (save_state[0]) {
		Z_Skirmish->WriteByte(0x004D9DD6, 0xE9); Z_Skirmish->WriteDword(0x004D9DD6 + 1, 0xD5);
		Z_Skirmish->WriteByte(0x004D9ED2, 0x04);
		Z_Skirmish->WriteByte(0x004DDEC1, 0xEB); Z_Skirmish->WriteDword(0x004DDEC1 + 1, 0x90909045);
		Z_Skirmish->WriteByte(0x004DDF24, 0x04);
		Z_Skirmish->WriteDword(0x004DDF45, 0xD431);
		Z_Skirmish->WriteDword(0x004E290F, 0x0182e9); Z_Skirmish->WriteWord(0x004E290F + 4, 0x9000);
		Z_Skirmish->WriteWord(0x004E2DB2, 0x14eb);
		Z_Skirmish->WriteWord(0x004E2DC1, 0xf8eb); memset((void*)0x004E2DC1, 0x90, 5);
		Z_Skirmish->WriteWord(0x004E2F1C, 0x14eb);
		Z_Skirmish->WriteWord(0x005AF9CF, 0xd431);
	}
	else {
		Z_Skirmish->WriteByte(0x004D9DD6, 0xBF); Z_Skirmish->WriteDword(0x004D9DD6 + 1, 0x6603b0);
		Z_Skirmish->WriteByte(0x004D9ED2, 0x02);
		Z_Skirmish->WriteByte(0x004DDEC1, 0xbf); Z_Skirmish->WriteDword(0x004DDEC1 + 1, 0x6603b0);
		Z_Skirmish->WriteByte(0x004DDF24, 0x02);
		Z_Skirmish->WriteDword(0x004DDF45, 0x7805);
		Z_Skirmish->WriteDword(0x004E290F, 0x0b68158b); Z_Skirmish->WriteWord(0x004E290F + 4, 0x66);
		Z_Skirmish->WriteWord(0x004E2DB2, 0x0d75);
		Z_Skirmish->WriteDword(0x004E2DC1, 0xc53884fe); Z_Skirmish->WriteDword(0x004E2DC1 + 3, 0x1c5);
		Z_Skirmish->WriteWord(0x004E2F1C, 0x0d75);
		Z_Skirmish->WriteWord(0x005AF9CF, 0x7805);

	}
}

int non_discount_prices[MONSTERS_AMOUNT];
void apply_discount(int day) {
	if (!save_state[16]) return;
	auto monsters = (h3::H3CreatureInformation*)*(int*)0x006747B0;
	if (day == 0) {
		for (int i = 0; i < MONSTERS_AMOUNT; ++i) {
			non_discount_prices[i] = monsters[i].cost.gold;
		}
	}
	else if (day % 7 == 0) {
		for (int i = 0; i < MONSTERS_AMOUNT; ++i) {
			monsters[i].cost.gold = non_discount_prices[i];
		}
	}
	else if (day % 7 == 6) {
		for (int i = 0; i < MONSTERS_AMOUNT; ++i) {
			monsters[i].cost.gold = non_discount_prices[i] >> 1;
		}
	}
}

void ApplyEffects(void) {
	ApplyOneSlotCombos();

	if (save_state[1]) {
		int tmp2 = Era::v[2]; int tmp3 = Era::v[3]; int tmp4 = Era::v[4];
		Era::ExecErmCmd("OW:C?v2;"); Era::ExecErmCmd("OW:Iv2/?v3;");
		if (Era::v[3] == 0) {
			Era::ExecErmCmd("UN:X?v3/?v4;"); Era::v[3] *= 2;
			Era::ExecErmCmd("UN:S0/0/0/v2/v3;");
			Era::ExecErmCmd("UN:S0/0/v4/v2/v3;");
			Era::ExecErmCmd("UN:R1;");
		}
		Era::v[2] = tmp2; Era::v[3] = tmp3; Era::v[4] = tmp4;
	}
	if (save_state[2]) {
		int tmp2 = Era::v[2]; int tmp3 = Era::v[3]; int tmp4 = Era::v[4];
		Era::ExecErmCmd("OW:C?v2;"); Era::ExecErmCmd("OW:Iv2/?v3;");
		if (Era::v[3] == 1) {
			Era::ExecErmCmd("UN:X?v3/?v4;"); Era::v[3] *= 2;
			Era::ExecErmCmd("UN:S0/0/0/v2/v3;");
			Era::ExecErmCmd("UN:S0/0/v4/v2/v3;");
			Era::ExecErmCmd("UN:R1;");
		}
		Era::v[2] = tmp2; Era::v[3] = tmp3; Era::v[4] = tmp4;
	}

	char buf[1024]; auto& saved_income = *(long*)(save_state + 8);
	sprintf_s(buf, "SN:W^Knightmare Skirmish Income Multiplier^/%i;", saved_income);
	Era::ExecErmCmd(buf);

	ApplySpeed();
	ApplyMight();

	/*
	if (save_state[16] && !justloaded) {
		auto weekday = h3::H3Main::Get()->date.day;
		long firstplayer = -1;
		for (auto &player : h3::H3Main::Get()->players) {
			if (player.ownerID >= 0) {
				firstplayer = player.ownerID;
				break;
			}
		}

		if (firstplayer == o_ActivePlayerID) {
			auto monsters = (h3::H3CreatureInformation*) *(int*) 0x006747B0;
			if (weekday == 1) {
				if (h3::H3Main::Get()->date.CurrentDay() > 3) {
					for (int i = 0; i < MONSTERS_AMOUNT; ++i)
						monsters[i].cost.gold <<= 1;
				}
			}
			if (weekday == 7) {
				for (int i = 0; i < MONSTERS_AMOUNT; ++i)
					monsters[i].cost.gold >>= 1;
			}
		}
	}
	*/
	apply_discount(h3::H3Main::Get()->date.CurrentDay());

	justloaded = false;
}


extern "C" __declspec(dllexport) int get_town_bonus_build_count(h3::H3Town * town) {
	if (!save_state[18]) return 0;
	// auto& nfo = h3::H3Main::Get()->playersInfo;
	auto H3G = h3::H3Main::Get();
	if (town->owner >= 0 && /*nfo.playerType[town->owner] == 10*/
		!H3G->IsHuman(town->owner))
		return 7;
	else return 0;
}