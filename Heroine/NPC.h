#pragma once

class NPC {
public:
    int  Used;
    int  Dead;
    int  Number;
    int  Type, HType;
    int  LastExpoInBattle;
    struct {
        unsigned  CustomPrimary : 1; // ������������ ��� ������������ ���������
        //    unsigned  NoHeroOwner   : 1; // ��� ������� ����� (3.58)
        unsigned _reserved : 31;
    } Fl;
    int  Primary[7]/*At,Df,Hp,Dm,Mp,Sp,Mr*/;
    //  struct{
    //    Byte  :1;
    //  } Flags;
    int  Skills[7];
    short Arts[10][8];
    char Name[32];
    int  OldHeroExp;
    int  Exp;   // ���. ����
    int  Level; // ���. �������
    DWORD  SpecBon[2];
    /*
    int  GetAvailableSpecBon(int);
    static int   Spec2Ind[15][2];
    static int   Bonus[7][5];
    static int   SpecBonus[7][5];
    static char* BonusBmp[6][6];
    static char* BonusArtBmp[6];
    static char* BonusBmpA[6][6];
    static char* SpecBonusBmp[6][6][4];
    static char* SpecBonusText[6][6][2];
    static char* SpecBonusPopUpText[6][6];
    int         FindSpecBonusBmp(int i, int j);
    static char* NextBonusBmp[6][2];
    static char* BonusText[6][6];
    static char* BonusPopUpText[6][6];
    static char* SpecBmp[9][2];
    static char* SpecText[9][2];
    static char* SpecHint[9][2];
    int CalcSkill(int);
    int CalculateSkills(int);
    int MayNextSkill(int);
    //  static char *SkNames[7];
    static char* Descr[9];
    static char* Magics[9];
    static _ZPrintf_ Hint[2];
    static _ZPrintf_ Buffer, Buffer1;
    static int Levels[100];
    static int AISkillsChance[18][6];
    void SetOldHeroExp(int Exp) { OldHeroExp = Exp; }
    int  GetExp(void) { return Exp; }
    void AddExp(int, int); // �������� �����
    int  GetNextLevel(void); // �������� ���� ����. �������
    NPC();
    void Init(void);
    void ToText(int, int = 0, int = 0);
    char* ToHint(int);
    static char* ArtPicNames[10];
    static char  ArtTextBuffer[10][512];
    static int  ArtIsPossible(int ArtNum);
    int   ArtMayHave(int ArtNum);
    int   ArtGetFreeSlot(int ArtNum);
    int   ArtDoesHave(int ArtNum);
    int   ArtCalcSkill(int Slot, int Skill);
    int   ArtInSlot(int ArtNum);
    char* PrepareArtText(int Slot);+
    */
};

constexpr size_t NPC_Size = sizeof(NPC);