// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "../../__include__/patcher_x86_commented.hpp"


Patcher* globalPatcher;
PatcherInstance* z_Volume;

#define MusicVolume   (*(int*)0x006987B0)
#define EffectsVolume (*(int*)0x006987B4)

int __stdcall Volume_005999C0(HiHook* hook, int a1, int a2) {
    int v2; // edx

    v2 = 0;
    if (a2 == 101)
    {
        if (MusicVolume < 1 || MusicVolume > 10)
            return v2;
        v2 = (a1 * (MusicVolume + 1)) >> 8;
        if (v2 < 1)
            return 1;
    }
    else
    {
        if (EffectsVolume < 1 || EffectsVolume > 10)
            return v2;
        v2 = (a1 * (EffectsVolume + 1)) >> 8;
        if (v2 < 1)
            return 1;
    }
    if (v2 > 127)
        return 127;
    return v2;
}



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:

        globalPatcher = GetPatcher();
        z_Volume = globalPatcher->CreateInstance((char*)"z_Volume");
        z_Volume->WriteHiHook(0x005999C0, SPLICE_, EXTENDED_, STDCALL_, Volume_005999C0);

        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

