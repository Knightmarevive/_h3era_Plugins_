// dllmain.cpp : Defines the entry point for the DLL application.
#include "./MapScroller/pch.h"
Patcher *globalPatcher;
PatcherInstance *_PI;

_LHF_(HooksInit)
{

    scroll::MapScroller::Get();

    return EXEC_DEFAULT;
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
    static bool pluginIsOn = false;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        if (!pluginIsOn)
        {
            pluginIsOn = true;

            static LPCSTR pluginInstanceName = "EraPlugin.GameplayFeatures.daemon_n";
            globalPatcher = GetPatcher();
            _PI = globalPatcher->CreateInstance(pluginInstanceName);

            _PI->WriteLoHook(0x4EEAF2, HooksInit);
            Era::ConnectEra(hModule, pluginInstanceName);
        }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
