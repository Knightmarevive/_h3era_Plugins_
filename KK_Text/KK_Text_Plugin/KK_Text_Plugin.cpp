#include <windows.h>
#include "Era.h"
#include <cmath>  
// #include <unistd.h>
// #include <stdio.h>
#include <direct.h>


#include "MyForm.h"

char config_fname[256] = "WoGSetup_KnightmareKingdoms_Mild.dat";


using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

void  (*ConfigureArt)(int target, char* buf);
void  (*art_AI_report)(void);
void  (*more_SS_levels_tweak)(void);
void  (*more_SS_levels_names)(int skill, int level, const char* text);
int   (*get_SS_table_i)(int skill, int level);
float (*get_SS_table_f)(int skill, int level);

int   (*get_SS_table_zi)(int skill, int level, int type);
float (*get_SS_table_zf)(int skill, int level, int type);


typedef unsigned __int32 _dword_;
#define CALL_12(return_type, call_type, address, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) \
	((return_type (call_type *)(_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_,_dword_))(address))((_dword_)(a1),(_dword_)(a2),(_dword_)(a3),(_dword_)(a4),(_dword_)(a5),(_dword_)(a6),(_dword_)(a7),(_dword_)(a8),(_dword_)(a9),(_dword_)(a10),(_dword_)(a11),(_dword_)(a12))


char proficiency[6][512] = {"{Novice}", "{Basic}", "{Advanced}", "{Expert}" , "{Master}" , "{GrandMaster}" };
int level_to_proficiency[16] = {0,1,1,2,2,2,3,3,3,3,4,4,4,4,4,5};

void __stdcall fix_SS(TEvent* e);
void __stdcall  link_more_SS_levels(TEvent* e) {
	HMODULE more_SS_levels = 0;
	more_SS_levels = GetModuleHandleA("more_SS_levels.era");
	if (more_SS_levels) {
		more_SS_levels_tweak = (void(*)(void))
			GetProcAddress(more_SS_levels, "fill_SSP_Tables_Knightmare_Kingdoms");
		//if(more_SS_levels_tweak) more_SS_levels_tweak();
		more_SS_levels_names = (void(*)(int, int, const char*))
			GetProcAddress(more_SS_levels, "set_SS_Description");
		get_SS_table_f = (float(*)(int, int))
			GetProcAddress(more_SS_levels, "get_SS_table_f");
		get_SS_table_i = (int(*)(int, int))
			GetProcAddress(more_SS_levels, "get_SS_table_i");
		

		get_SS_table_zf = (float(*)(int, int, int))
			GetProcAddress(more_SS_levels, "get_SS_table_zf");
		get_SS_table_zi = (int(*)(int, int, int))
			GetProcAddress(more_SS_levels, "get_SS_table_zi");

		if (!(more_SS_levels_names
			&& get_SS_table_f && get_SS_table_i
			&& get_SS_table_zf && get_SS_table_zi))
		{
			MessageBoxA(0, "couldn't link to more_SS_levels.era", "Knightmare Text Plugin", MB_OK);
		}
		
	}
}

void __stdcall link_emerald(TEvent* e)
{
	//HMODULE Amethyst = LoadLibraryA("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll");
	HMODULE Emerald = 0;
	//DWORD flags = 0; // GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	//::GetModuleHandleEx(flags, reinterpret_cast<LPCTSTR>("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll"), &Amethyst);
	Emerald = GetModuleHandleA("emerald3_3.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_4.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_5.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_6.era");

	if (Emerald) {
		ConfigureArt = (void(*)(int, char*)) GetProcAddress(Emerald, "ConfigureArt");
		art_AI_report = (void(*)(void)) GetProcAddress(Emerald, "art_AI_report");
	}
	else { MessageBoxA(0, "couldn't link to Emerald", "Knightmare Text Plugin", MB_OK); }
}

void __stdcall KK_Dialogue() {
	MessageBoxA(0,"Setting default WoG settings for KK","Wrong WoG Settings", MB_ICONWARNING);
	/*
	KKTextPlugin::MyForm mf;

	mf.ShowDialog();
	*/
	while (1) {
		MessageBoxA(0, "There Are three basic Flavours: \n Lean, Mild and Rich \n Mild is recomended", "Default Flavors", MB_ICONINFORMATION);

		if (MessageBoxA(0, "Do you want Lean Flavour", "Default Options", MB_YESNO | MB_ICONQUESTION) == IDYES) {
			strcpy_s(config_fname, "WoGSetup_KnightmareKingdoms_Lean.dat"); return;
		}
		if (MessageBoxA(0, "Do you want Mild Flavour", "Default Options", MB_YESNO | MB_ICONQUESTION) == IDYES) {
			strcpy_s(config_fname, "WoGSetup_KnightmareKingdoms_Mild.dat"); return;
		}
		if (MessageBoxA(0, "Do you want Rich Flavour", "Default Options", MB_YESNO | MB_ICONQUESTION) == IDYES) {
			strcpy_s(config_fname, "WoGSetup_KnightmareKingdoms_Rich.dat"); return;
		}

		MessageBoxA(0, "We Are out of Flavours \n Let try Again", "Wrong WoG Settings", MB_ICONWARNING);
	}

}

void __stdcall fix_WoG_settings(TEvent* e) {
	const char* default_wog_ini = "[WoGification]\n"
		"Options_File_Name=WoGSetup_KnightmareKingdoms_custom.dat\n"
		"Options_File_Path=%s";
	char* buf, * c;
	FILE* fdesc;
	bool proper_settings = false;

	if (fdesc = fopen("wog.ini", "r"))
	{
		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);
		//begin of majaczek

		if (!proper_settings) proper_settings = strstr(buf, "KnightmareKingdoms");
		if (!proper_settings) proper_settings = strstr(buf, "KK");

		free(buf);
	}
	else {
		MessageBoxA(0, "wog.ini is missing", "Error", 0);
		return;
	}

	if (!proper_settings) {
		// MessageBoxA(0, "improper wog settings", "Warning", 0);
		// char fname[256] = "WoGSetup_KnightmareKingdoms_Rich.dat";
		char *dir; dir = _getcwd(NULL, 0);
		
		KK_Dialogue();

		CopyFileA(config_fname,"WoGSetup_KnightmareKingdoms_custom.dat",false);
		
		if (dir) {
			fdesc = fopen("wog.ini", "w");
			//fwrite(default_wog_ini, sizeof(char), sizeof(default_wog_ini),fdesc);
			fprintf(fdesc, default_wog_ini, /* "D:\\Games\\Modded\\HoMM 3 ERA (launcher)\\" */ dir);
			fclose(fdesc);
			free(dir);
		}
		else MessageBoxA(0, "getcwd error", "getcwd error", 1);

	}

}

void __stdcall fix_SS(TEvent* e) {
	if (more_SS_levels_tweak) more_SS_levels_tweak();

	if (more_SS_levels_names
		&& get_SS_table_f && get_SS_table_i) {

		more_SS_levels_names(0,  0, "{Pathfinding}");
		more_SS_levels_names(1,  0, "{Archery}");
		more_SS_levels_names(2,  0, "{Logistics}");
		more_SS_levels_names(3,  0, "{Scouting}");
		more_SS_levels_names(4,  0, "{Nobility}");
		more_SS_levels_names(5,  0, "{Astromancy}");
		more_SS_levels_names(6,  0, "{Inspiration}");
		more_SS_levels_names(7,  0, "{Wisdom}");
		more_SS_levels_names(8,  0, "{Mysticism}");
		more_SS_levels_names(9,  0, "{Life} {Magic}");
		more_SS_levels_names(10, 0, "{Engineering}");
		more_SS_levels_names(11, 0, "{Scavenging}");
		more_SS_levels_names(12, 0, "{Dark} {Magic}");
		more_SS_levels_names(13, 0, "{Estates}");
		more_SS_levels_names(14, 0, "{Fire Magic}");
		more_SS_levels_names(15, 0, "{Air} {Magic}");
		more_SS_levels_names(16, 0, "{Water} {Magic}");
		more_SS_levels_names(17, 0, "{Earth} {Magic}");
		more_SS_levels_names(18, 0, "{Monasticism}");
		more_SS_levels_names(19, 0, "{Tactics}");
		more_SS_levels_names(20, 0, "{Recruitment}");
		more_SS_levels_names(21, 0, "{Learning}");
		more_SS_levels_names(22, 0, "{Offence}");
		more_SS_levels_names(23, 0, "{Armorer}");
		more_SS_levels_names(24, 0, "{Intelligence}");
		more_SS_levels_names(25, 0, "{Sorcery}");
		more_SS_levels_names(26, 0, "{Resistance}");
		more_SS_levels_names(27, 0, "{Machinery}");
		more_SS_levels_names(28, 0, "{Damned}");
		more_SS_levels_names(29, 0, "{Exorcism}");
		more_SS_levels_names(30, 0, "{Ancient} {Magic}");

		char buf[4096] = "";
		for (int i = 1; i < 16; ++i) {

			sprintf(buf, "%s pathfinding proficiency \n %0.2f steps on battlefield shortcut", proficiency[ get_SS_table_i(0, i) ],(i*20)/100.0f);
			more_SS_levels_names(0, i, buf);

			sprintf(buf,"%0.2f%% more damage when shooting, %d chances of %0.2f%% for additional shot.",
				get_SS_table_f(1,i)*100.0, get_SS_table_zi(1, i, 1), get_SS_table_zi(1, i, 0) * 0.1 );
			more_SS_levels_names(1, i, buf);

			sprintf(buf, "%0.2f%% more steps when on land", get_SS_table_f(2, i) * 100.0);
			more_SS_levels_names(2, i, buf);

			sprintf(buf, "%d basic vision range. \n %d rogues daily", get_SS_table_i(3, i),i*2);
			more_SS_levels_names(3, i, buf);

			sprintf(buf, "+%d%% more HP and DMG for commander", i*7);
			more_SS_levels_names(4, i, buf);

			sprintf(buf, "+%0.2f%% more powerful: \n lighting bolt, chain lighting, \n meteor shower, armageddon \n\n %0.2f steps on water while on ship", float(get_SS_table_zf(5,i,0) * 100.0), float(get_SS_table_i(5, i)/100.0));
			more_SS_levels_names(5, i, buf);

			sprintf(buf, "%d more unit speed", /*get_SS_table_i(6, i)*/ level_to_proficiency[i]);
			more_SS_levels_names(6, i, buf);

			sprintf(buf, "You can learn level %d Spells", get_SS_table_i(7, i));
			more_SS_levels_names(7, i, buf);

			sprintf(buf, "+ %d Mana Daily", get_SS_table_i(8, i));
			more_SS_levels_names(8, i, buf);

			sprintf(buf, "%0.2f%%  more powerful: \n Cure, Resurrection, \n Sacrifice, Destroy Undead", get_SS_table_zf(9, i, 0) * 100.0);
			more_SS_levels_names(9, i, buf);

			sprintf(buf, "Better catapult, arrow towers, can build structure additional %i times per turn in town where the hero sleeps in garnison.", 
				get_SS_table_zi(10,i,0));
			more_SS_levels_names(10, i, buf);

			sprintf(buf, "%0.2f%% Eagle Eye chance \n %d%% Battle Experience to Gold", get_SS_table_f(11, i) * 100.0, 7*i);
			more_SS_levels_names(11, i, buf);

			sprintf(buf, " %0.2f%% units revived as undead \n %0.2f%% more powerful death ripple and animate dead", get_SS_table_f(12, i) * 100.0, get_SS_table_zf(12, i, 0) * 100.0);
			more_SS_levels_names(12, i, buf);

			sprintf(buf, "%d more gold daily", get_SS_table_i(13, i));
			more_SS_levels_names(13, i, buf);


			sprintf(buf, "Cast Fire magic at %s proficiency", proficiency[ get_SS_table_i(14, i) ]);
			more_SS_levels_names(14, i, buf);
			sprintf(buf, "Cast Air magic at %s proficiency", proficiency[get_SS_table_i(15, i) ]);
			more_SS_levels_names(15, i, buf);
			sprintf(buf, "Cast Water magic at %s proficiency", proficiency[get_SS_table_i(16, i) ]);
			more_SS_levels_names(16, i, buf);
			sprintf(buf, "Cast Earth magic at %s proficiency", proficiency[ get_SS_table_i(17, i) ]);
			more_SS_levels_names(17, i, buf);



			sprintf(buf, "You can exchange level %d Spells \n\n %d%% chance per step to pickup some herbs \n\n can copy some scriptures", get_SS_table_i(18, i), i*3);
			more_SS_levels_names(18, i, buf);


			sprintf(buf, "You get Tactics bonus at %s proficiency, also you get %d chances of %0.2f%% for additional action.", 
				proficiency[get_SS_table_i(19, i)], get_SS_table_zi(19, i, 1), get_SS_table_zi(19, i, 0) * 0.1);
			more_SS_levels_names(19, i, buf);

			sprintf(buf, "You can multiply your troops every week. Also you get more reserve slots.");
			more_SS_levels_names(20, i, buf);



			sprintf(buf, "%0.2f%% more experience gained. \n%d experience daily.", 
				get_SS_table_f(21, i) * 100.0, get_SS_table_zi(21,i,0));
			more_SS_levels_names(21, i, buf);

			sprintf(buf, "%0.2f%% more damage when melee, %d chances of %0.2f%% for additional melee attack.",
				get_SS_table_f(22, i) * 100.0, get_SS_table_zi(22, i, 1), get_SS_table_zi(22, i, 0) * 0.1 );
			more_SS_levels_names(22, i, buf);

			sprintf(buf, "%0.2f%% less physical damage", get_SS_table_f(23, i) * 100.0);
			more_SS_levels_names(23, i, buf);

			sprintf(buf, "%0.2f%% more max mana", get_SS_table_f(24, i) * 100.0);
			more_SS_levels_names(24, i, buf);

			sprintf(buf, "%0.2f%% more magic damage", get_SS_table_f(25, i) * 100.0);
			more_SS_levels_names(25, i, buf);

			sprintf(buf, "Gives %0.2f%% magic resistance", get_SS_table_f(26, i) * 100.0);
			more_SS_levels_names(26, i, buf);

			sprintf(buf, "Better Ballista, Sage Tent and ammo cart");
			more_SS_levels_names(27, i, buf);

			sprintf(buf, "It Gives all your units additional %d%% damage versus good units. " /*"\n\n\
				The formula is Creature_Fight_Value * Stack_Count * (Skill * 50 + 25) + (Specialist_Level * 5 + 100) * (Week + 7) / 21 000 000 \n"*/,
				get_SS_table_zi(28, i, 1));
			more_SS_levels_names(28, i, buf);

			sprintf(buf, "It Gives all your units additional %d%% damage versus evil units. " /*" \n\n\
				The formula is Creature_Fight_Value * Stack_Count * (Skill * 50 + 25) + (Specialist_Level * 5 + 100) * (Week + 7) / 21 000 000 \n"*/,
				get_SS_table_zi(28, i, 1));
			more_SS_levels_names(29, i, buf);

			sprintf(buf, "Cast Ancient Magic at %s proficiency", proficiency[get_SS_table_i(30, i)]);
			more_SS_levels_names(30, i, buf);
		}
	}
}

void __stdcall fix_arts(TEvent* e) {
	if (!ConfigureArt) {
		MessageBoxA(0, "general error", "Knightmare Text Plugin", MB_OK);
		return;
	}
	ConfigureArt(166,const_cast<char*>( "Name=\"Broach of Wealth (166)\" Description=\"{Broach of Wealth} \n\
																									\n\
		Gives 3 mithril and 9 ore and 9 wood and 7500 gold daily.\""));
	
	ConfigureArt(168, const_cast<char*>("Name=\"Surcoat of Lloth (168)\" \
	Description=\"{ Surcoat of Lloth } \n\
		\n\
		Gives each owned creature : \n\
		>> all shield spells\n\
		>> all protection from elements\n\
		>> magic mirror\n\
		>> 50 % chance for 50 % deflect\""));

	ConfigureArt(170, const_cast<char*>("Name=\"Horn of Summoning (170)\" Description=\"{Horn of Summoning} \n\
\n\
		Summons mighty Town Guardians to protect hero, even if not in siege\""));

	ConfigureArt(169, const_cast<char*>("Name=\"Boots of Rincewind (169)\" Description=\"{Boots of Rincewind (169)}\n\
		\n\
		Gives all your creatures double movement and gives 3 additional spells for caster creatures.\""));

	ConfigureArt(165, const_cast<char*>("Name=\"Ring of Slavery (165)\" Description=\"{Ring of Slavery (165)} \n\
		\n\
		Gives all your creatures : \n\
		>> Strike and Return\n\
		>> No Retailation\n\
		>> More Retailations\""));

	ConfigureArt(167, const_cast<char*>("Name=\"Armor of Witchcraft (167) \" Description=\"{Armor of Witchcraft}\n\
		\n\
		Gives all your creatures :\n\
		>> Attack All Around\n\
		>> No Enemy Retaliation\n\
		>> Fearless\n\" "));

	ConfigureArt(162, const_cast<char*>("Name=\"Sword of Deepest Knightmare (162)\" Description=\"{Sword of Deepest Knightmare}\n\
		\n\
		Gives all your creatures :\n\
		>> Immune to Direct Damage spells\n\
		>> 13 additional spells for caster creatures\n\
		>> Clone(each creature once at beggining of battle)\n\" "));

	ConfigureArt(161, const_cast<char*>("Name=\"Helmet of Vision (161)\" Description=\"{Helmet of Vision (161)}\n\
		\n\
		>> Gives additional 9 Scouting Radius\n\
		>> Gives 22 Experience per step\n\
		>> Doubles Scouting Encounter Chance\n\" "));

	ConfigureArt(485, const_cast<char*>("Name=\"Dracopedia\" Description=\"{Dracopedia}\n\
		\n\
		Gives All Your Creatures :\n\
		>> Fly\n\
		>> Dragon Nature\n\
		>> Immune to Fire\n\
		>> Advanced Inferno After Attack\n\" "));

	ConfigureArt( 108, const_cast<char*>("Name=\"Pendant of Fanatical Courage\" Description=\"{Pendant of Fanatical Courage}\n\
		\n\
		>> Gives Max Morale And Luck\n\
		>> Mind Spell Immunity\n\
		>> Deepest Sorrowand Misfortune to your enemies\n\
		>> Doubles Your Inspiration Bonus\n\" "));

	ConfigureArt(415, const_cast<char*>("Name=\"The Garb of the Forest Lord\" Description=\"{The Garb of the Forest Lord}\n\
\n\
		Gives 2 Sylvan Dragons, 0 Sylfaen Dragon each week\n\
		Gives Something Else.\n\" "));

	ConfigureArt(164, const_cast<char*>("Name=\"Horned Ring\" Description=\"{Horned Ring}\n\
		Triples efficiency of exorcism.\n\" "));

	ConfigureArt(486, const_cast<char*>("Name=\"Bard's Mandola\" Description=\"{Bard's Mandola}\n\
\n\
		Reduces Monster Aggression by 5 Altogether.\n\
		Gives Something Else.\n\" "));

	ConfigureArt(156, const_cast<char*>("Name=\"{Warlord's Banner}\" Description=\"{Warlord's Banner}\n\n\
		With stack experience enabled, the Warlord's Banner gives additional bonuses in combat. To equip it, drag it to the troop stack of your choice.\n\n\
		Choose a bonus by right-clicking on the Banner in the stack's experience screen. You can change the bonus any time except in combat.\n\n\
		Equipped on {Misc Slot} gives 25 XP Daily\" "));

	ConfigureArt(470, const_cast<char*>("Name=\"{Ancient Book} Tier One #470\" Description=\" {Ancient Book} #470 \n\nTier One\n\n Gives 175 XP Daily and +15% Learning\" "));
	ConfigureArt(502, const_cast<char*>("Name=\"{Ancient Book} Tier Two #502\" Description=\" {Ancient Book} #502 \n\nTier Two\n\n Gives 1225 XP Daily and +50% Learning\" "));
	ConfigureArt(471, const_cast<char*>("Name=\"{Ancient Book} Tier Three #471\" Description=\" {Ancient Book} #471 \n\nTier Three\n\n  Gives 8575 XP Daily and +120% Learning\" "));
	ConfigureArt(483, const_cast<char*>("Name=\"{Ethernal Gem} Tier Max #483\" Description=\" {Ethernal Gem} #483 \n\nTier Max\n\n Gives 60025 XP Daily and +250% Learning\" "));


	ConfigureArt(491, const_cast<char*>("Name=\"{Ring of Knightmare} #491\" Description=\" {Ring of Knightmare} #491\n +12 Power \n +5 Casts \" "));
	ConfigureArt(543, const_cast<char*>("Name=\"{Scarab Pendant} #543\" Description=\" {Scarab Pendant} #543\n Grants Fire Shield \n +2 all primary skills \" "));
	ConfigureArt(574, const_cast<char*>("Name=\"{Enchanted Bow} #574\" Description=\" {Enchanted Bow} #574\n +12 Ammo \n Grants Precision \" "));


	ConfigureArt(420, const_cast<char*>("Name=\"{The Blitz Cloak} #420\" Description=\" {The Blitz Cloak} #420 \n Gives all Adventure Spells \n +4 Knowledge \n +4 Power \" "));
	ConfigureArt(141, const_cast<char*>("Name=\"{Magic Wand} #141\" Description=\" {Magic Wand} #141 \n\n\
		+ 7 Power \n+ 5 Cats \n+ Auto SPell Magic Mirror\" "));
	ConfigureArt(155, const_cast<char*>("Name=\"{Ring of Slava} #155\" Description=\" {Ring of Slava} #155 \n\n\
		\n+ 2 Attack\n+ 2 Knowledge\n+ 2 Power \n+ 2 Knowledge \n+ 5 Cats \n+ 2 Shots\" "));
	ConfigureArt(126, const_cast<char*>("Name=\"{Orb of Inhibition} #126\" Description=\" {Orb of Inhibition} #126 \n\n\
		+ 2 Knowledge\n+ 2 Power \n+ 3 casts \n Auto Spell Protection from Elements\" "));


	ConfigureArt(181, const_cast<char*>("Name=\"{Void Paladin Armour} #181\" Description=\" {Void Paladin Armour} #181 \n\n\
		+ 4 Knowledge\n+ 4 Power \n Makes Opponent Vulnerable to all spells\" "));

	ConfigureArt(182, const_cast<char*>("Name=\"{Supreme Pendant of Negativity} #182\" Description=\" {Supreme Pendant of Negativity} #182\n\n\
		Makes Opponent Vulnerable to Lighting spells\n\
		Makes own troops Immune to Lighting spells \" "));
	ConfigureArt(184, const_cast<char*>("Name=\"{Supreme Snowflake of Lord Kelvin} #184\" Description=\" Supreme Snowflake of Lord Kelvin} #184\n\n\
		Makes Opponent Vulnerable to Frost spells\n\
		Makes own troops Immune to Frost spells \" "));
	ConfigureArt(186, const_cast<char*>("Name=\"{Supreme Gem of Geomancy} #186\" Description=\" {Supreme Gem of Geomancy} #186\n\n\
		Makes Opponent Vulnerable to Earth damage spells\n\
		Makes own troops Immune to Earth damage  spells \" "));


	ConfigureArt(183, const_cast<char*>("Name=\"{Snowflake of Lord Kelvin} #183\" Description=\" Snowflake of Lord Kelvin} #183\n\n\
		Makes own troops Immune to Frost spells \" "));
	ConfigureArt(185, const_cast<char*>("Name=\"{Gem of Geomancy} #185\" Description=\" {Gem of Geomancy} #185\n\n\
		Makes own troops Immune to Earth damage  spells \" "));

	// ConfigureArt( 1000, const_cast<char*>("Name=\"art name \" Description=\" description \n\" "));

	ConfigureArt(175, const_cast<char*>("Name=\"{Ancient Scroll I}\" Description=\" Gives All spells on level 1  \n\" "));
	ConfigureArt(176, const_cast<char*>("Name=\"{Ancient Scroll II}\" Description=\" Gives All spells on level 2  \n\" "));
	ConfigureArt(177, const_cast<char*>("Name=\"{Ancient Scroll III}\" Description=\" Gives All spells on level 3  \n\" "));
	ConfigureArt(178, const_cast<char*>("Name=\"{Ancient Scroll IV}\" Description=\" Gives All spells on level 4  \n\" "));
	ConfigureArt(179, const_cast<char*>("Name=\"{Ancient Scroll V}\" Description=\" Gives All spells on level 5  \n\" "));

	ConfigureArt(180, const_cast<char*>("Name=\"{Ultimate Ancient Scroll}\" Description=\" Gives All spells on every level \n\" "));

	if (art_AI_report) art_AI_report();
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
	  // fix_WoG_settings();
	  ConnectEra();
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);

	RegisterHandler(link_more_SS_levels, "OnBeforeWoG");
	RegisterHandler(link_emerald, "OnBeforeWoG");

	RegisterHandler(fix_WoG_settings, "OnBeforeWoG");


	//RegisterHandler(link_emerald, "OnSavegameRead");
	//RegisterHandler(link_emerald, "OnBeforeErmInstructions");

	//RegisterHandler(link_more_SS_levels, "OnSavegameRead");
	//RegisterHandler(link_more_SS_levels, "OnBeforeErmInstructions");


	//RegisterHandler(fix_SS, "OnSavegameRead");
	//RegisterHandler(fix_SS, "OnBeforeErmInstructions");

	//RegisterHandler(fix_arts, "OnSavegameRead");
	//RegisterHandler(fix_arts, "OnBeforeErmInstructions");

	//RegisterHandler(fix_arts, "OnAdventureMapLeftMouseClick");
	//RegisterHandler(fix_SS, "OnAdventureMapLeftMouseClick");

	/*
	RegisterHandler(fix_arts, "OnOpenHeroScreen");
	RegisterHandler(fix_SS, "OnOpenHeroScreen");

	RegisterHandler(fix_arts, "OnBeforeHeroInteraction");
	RegisterHandler(fix_SS, "OnBeforeHeroInteraction");

	RegisterHandler(fix_arts, "OnHeroGainLevel");
	RegisterHandler(fix_SS, "OnHeroGainLevel");

	*/


	RegisterHandler(fix_arts, "OnGameEnter");
	RegisterHandler(fix_SS, "OnGameEnter");


	RegisterHandler(fix_SS, "OnErmTimer 1");

  }
  return TRUE;
};
