#define _CRT_SECURE_NO_WARNINGS 
#define _ATL_XP_TARGETING

#include"patcher_x86_commented.hpp"
// #include "../../__include__/patcher_x86_commented.hpp"
#include<cstdio>
// #include"era.h"

#define PINSTANCE_MAIN "z_more_anims"

#define DP(X)  (long)X
#define DS(X)  (long)X
#define DS0(X) (long)X

//////////////////////////////////
/////////  Magic
struct _MagicAni_ {
	char* DefName;
	char* Name;
	int   Type;
};

#define MAGICS 3000
//#define MAGICS 0x7f
//#define MAGICS 0x5B

_MagicAni_ MagicAni[MAGICS];
//#define MagicGhost 0x53
_MagicAni_ MagicAniAdd[18] = {
  {(char*)"ZMagic1.def",(char*)"MGhost",1},   // 0x53
  {(char*)"ZMGC02.def", (char*)"MGblock",1},  // 0x54 Complete Block Commander's 
  {(char*)"ZMGC01.def", (char*)"MGblock2",1}, // 0x55 Complete Block Exp 
  {(char*)"ZMGC03.def", (char*)"MGblock3",1}, // 0x56 Partial Block Exp
  {(char*)"meleres.def", (char*)"meleres",1},   // 0x57 Melee Resistance
  {(char*)"melevuln.def", (char*)"melevuln",1}, // 0x58 Melee Vulnerability
  {(char*)"shotres.def", (char*)"shotres",1},   // 0x59 Shooting Resistance
  {(char*)"shotvuln.def", (char*)"shotvuln",1}, // 0x5A Shooting Vulnerability
  {(char*)"dragvuln.def", (char*)"dragvuln",1}, // 0x5B vulnerability to Dragon slayer
  {(char*)"EnchTrgt.def", (char*)"EnchTrgt",1}, // 0x5C Enchant Target
  {(char*)"dragwill.def", (char*)"dragwill",1}, // 0x5D Dragon Will
  {(char*)"GateInf.def", (char*)"GateInf", 0x101},   // 0x5E Gate Infernals
  {(char*)"hvninsp.def", (char*)"hvninsp", 0x101},   // 0x5F Heavenly Inspiration
  {(char*)"rundead.def", (char*)"rundead", 0x101},   // 0x60 Raise Undead
  {(char*)"repair.def",  (char*)"repair",  0x101},    // 0x61 Repair Constructs
  {(char*)"rushtrgt.def", (char*)"rushtrgt",1}, // 0x62 Rush Target
  {(char*)"Stun.def", (char*)"stun",1}, // 0x63 Stun Target
   {(char*)"mregen.def", (char*)"mregen",1}, // 0x64 Mana Regen
};

struct __Accessers {
	long  where;
	long  what;

	long  len;
} Accessers[] = {
  // {0x4963E7 + 2,DS0(MAGICS),1},
  {0x43F77B + 3,DS(MagicAni),4},
  {0x43FB67 + 3,DS0(MagicAni),4},
  {0x4963F9 + 2,DS0(MagicAni),4},
  {0x4965CD + 2,DS0(MagicAni),4},
  {0x5A5033 + 3,DS0(MagicAni),4},
  {0x5A6B11 + 3,DS0(MagicAni),4},
  {0x5A7A71 + 3,DS0(MagicAni),4},
  {0x5A9629 + 3,DS0(MagicAni),4},
  {0x496518 + 2,DS0(&MagicAni[0].Name),4},
  {0x4689C1 + 3,DS0(&MagicAni[0].Name),4},
  {0x4966CB + 2,DS0(&MagicAni[0].Name),4},
  {0x5A6D2A + 3,DS0(&MagicAni[0].Name),4},
  {0x5A7B03 + 3,DS0(&MagicAni[0].Name),4},
  {0x43E500 + 3,DS0(&MagicAni[0].Type),4},

	{0,0,0}
};

typedef char Byte;

struct __Copiers {
	Byte* from;
	Byte* to;
	long  len;
} Copiers[] = {
	// Magic
  {(Byte*)0x641E18,(Byte*)MagicAni,sizeof(_MagicAni_) * 0x53},
  {(Byte*)&MagicAniAdd[0],(Byte*)&MagicAni[0x53],sizeof(MagicAniAdd)},

	{0,0,0}
};


Patcher* globalPatcher;
PatcherInstance* Z_more_anims;

_LHF_(hook_004965BB) {
	if (c->ecx < MAGICS)
		c->return_address = 0x004965C4;
	else
		c->return_address = 0x004967C5;

	return NO_EXEC_DEFAULT;
}

_LHF_(hook_004963E7) {
	if (c->eax < MAGICS)
		c->return_address = 0x004963F0;
	else
		c->return_address = 0x00496580;

	return NO_EXEC_DEFAULT;
}

void patch(void) {
	static bool first = true;
	if (!first) return; first = false;

	//globalPatcher = GetPatcher();
	//Z_more_anims = globalPatcher->CreateInstance(PINSTANCE_MAIN);
	for (int i = 0; Accessers[i].where; ++i) {
		if (Accessers[i].len == 1)
			Z_more_anims->WriteByte(Accessers[i].where, Accessers[i].what);
		if (Accessers[i].len == 4)
			Z_more_anims->WriteDword(Accessers[i].where, Accessers[i].what);
	}
	for (int i = 0; Copiers[i].len; ++i)
		for (int j = 0; j < Copiers[i].len; ++j)
			Z_more_anims->WriteByte((_ptr_)(Copiers[i].to + j), *(Copiers[i].from + j));

	int AnimAddrDefName[] = { 0x43F77E, 0x43FB6A, 0x4963FB, 0x4965CF, 0x5A5036, 0x5A6B14, 0x5A7A74, 0x5A962C };
	int AnimAddrName[] = { 0x4689C4, 0x49651A, 0x4966CD, 0x5A6D2D, 0x5A7B06 };

	for (std::size_t i = 0; i < sizeof(AnimAddrDefName) / sizeof(int); ++i)
		Z_more_anims->WriteDword(AnimAddrDefName[i], (int)&MagicAni->DefName);

	for (std::size_t i = 0; i < sizeof(AnimAddrName) / sizeof(int); ++i)
		Z_more_anims->WriteDword(AnimAddrName[i], (int)&MagicAni->Name);

	Z_more_anims->WriteDword(0x43E503, (int)&MagicAni->Type);

	Z_more_anims->WriteLoHook(0x004965BB,hook_004965BB);
	Z_more_anims->WriteLoHook(0x004963E7,hook_004963E7);
}



_LHF_(patch) {
	patch();
	return EXEC_DEFAULT;
}

/*
void  __stdcall patch_era(Era::TEvent* e) {

	patch();
}
*/

extern "C" __declspec(dllexport) char* get_def_name(int animID) {
	return MagicAni[animID].DefName;
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{


		globalPatcher = GetPatcher();
		Z_more_anims = globalPatcher->CreateInstance(PINSTANCE_MAIN);
		
		//Era::ConnectEra();
		//Era::RegisterHandler(patch_era, "OnAfterWoG");

		Z_more_anims->WriteLoHook(0x004EDCE4, patch);
		//patch();

		for (int i = 1000; i < MAGICS; ++i) {
			char* def = new char[16]; char* name = new char[16]; int type = 1;
			sprintf(def, "anim%i.def", i); sprintf(name, "anim%i", i);
			MagicAni[i].DefName = def; MagicAni[i].Name = name; MagicAni[i].Type = type;
			if (i >= 2000) MagicAni[i].Type = 0x101;
		}

		break;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}