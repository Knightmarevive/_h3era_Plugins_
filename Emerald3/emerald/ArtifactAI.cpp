#include "emerald.h"
#include <sstream>

#ifndef Disable_Emerald_AI

int dword_0063AC7C[102400];
constexpr int dword_0063AD24_old_size = 0x0063B670 - 0x0063AC7C;
int* dword_0063AD04 = dword_0063AC7C + 0x22;
int* dword_0063AD24 = dword_0063AD04 + 0x8;


bool ParseStr(char* buf, const char* name, /*char* &target */ char res[char_table_size]);
bool get_ART_AI_string(int art, char str[char_table_size]) {
	*str = 0;	static char fname[1024];
	sprintf(fname, "Data\\artifacts\\%u.cfg", art);

	FILE* fdesc; char* buf;
	if (fdesc = fopen(fname, "r"))
	{
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf=(char*)malloc(fdesc_size+1);
		memset(buf, 0, fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size]=0;
		fclose(fdesc);
		ParseStr(buf, "AI_Properties=\"",str);
		free(buf);
	}
	return *str;
}

bool get_ART_AI_interesting(long i) {
	if ((no_save.newbtable[i].att + no_save.newbtable[i].def) || no_save.newbtable[i].knw || no_save.newbtable[i].spp) return true;
	if (no_save.luck_bonuses[i] || no_save.morale_bonuses[i]) return true;

	return false;
}

char art_does_not_exist[2000] = {};

void art_existence() {
	char chain[char_table_size];
	for (int i = 0; i < OLD_ARTS_AMOUNT; ++i)
		art_does_not_exist[i] = 0;

	for (int i = OLD_ARTS_AMOUNT; i < NEW_ARTS_AMOUNT; ++i) {
		if (strlen(no_save.newtable[i].desc) < 5)
			art_does_not_exist[i] = 1;
		else art_does_not_exist[i] = 0;

		bool configured = get_ART_AI_string(i, chain);
		bool interesting = get_ART_AI_interesting(i);
		if (configured || interesting)
			art_does_not_exist[i] = 0;
	}

	art_does_not_exist[144] = art_does_not_exist[145] = 1;

}

void LoadArtifactConfig(int target);
void art_AI_init(void) {
	memcpy(dword_0063AC7C, (void*) 0x0063AC7C, dword_0063AD24_old_size);

	emerald->WriteDword(0x00434106 + 1, (int)dword_0063AC7C);
	emerald->WriteDword(0x0043410E + 1, (int)dword_0063AC7C);

	emerald->WriteDword(0x00461AD0 + 3, (int)dword_0063AD04);
	emerald->WriteDword(0x00461CD2 + 3, (int)dword_0063AD04);

	emerald->WriteDword(0x00461AC0 + 3, (int)dword_0063AD24);

	int* ptr = dword_0063AC7C + dword_0063AD24_old_size / 4 - 1;

	char chain[char_table_size];
	for (int i = 141; i < NEW_ARTS_AMOUNT;++i) {
		LoadArtifactConfig(i);
		bool configured = get_ART_AI_string(i, chain);
		bool interesting = get_ART_AI_interesting(i);
		if (configured || interesting) {
			*ptr = i; ++ptr;
			if (interesting) {
				if (no_save.newbtable[i].att + no_save.newbtable[i].def) {
					*ptr = 0; ++ptr; *ptr = (no_save.newbtable[i].att + no_save.newbtable[i].def); ++ptr;
				}

				if (no_save.newbtable[i].spp) {
					*ptr = 1; ++ptr; *ptr = no_save.newbtable[i].spp; ++ptr;
				}

				if (no_save.newbtable[i].knw) {
					*ptr = 2; ++ptr; *ptr = no_save.newbtable[i].knw; ++ptr;
				}
				if (no_save.morale_bonuses[i]) {
					*ptr = 3; ++ptr; *ptr = no_save.morale_bonuses[i]; ++ptr;
				}
				if (no_save.luck_bonuses[i]) {
					*ptr = 4; ++ptr; *ptr = no_save.luck_bonuses[i]; ++ptr;
				}
			}
			if (configured) {


				auto stream = std::stringstream(chain);
				while (1) {
					stream >> *ptr; ++ptr;
					if (ptr[-1] == -1) {
						--ptr;
						break;
					}
				}
			}
			*ptr = -1; ++ptr;
		}
		/*
		else if(strlen(no_save.newtable[i].desc)<5)
			art_does_not_exist[i] = 1;
			*/
	}

	*ptr = -0x64;
}

extern "C" __declspec(dllexport) void art_AI_report(void) {
	const char* path = ".\\help\\Emerald_ART_AI.htm";

	if (auto target = fopen(path, "w"))
	{
		int* ptr = dword_0063AC7C;
		// fprintf(target, "<table border=2 width=100%%> \n");

		char lines[1000][4096] = {};

		for (int i = 0; i <= 999; i++) {
			/*
			if (strlen(no_save.newtable[i].desc) < 5)
			{
				art_does_not_exist[i] = 1;
			}
			*/
			sprintf(lines[i], "no_AI_content ");
		}

		// for (int i = 2; i <= 999; i++) {
			// fprintf(target, "<tr><td> (%i) %s <td> \n", i, no_save.newtable[i].name ? no_save.newtable[i].name : "NO_NAME");

		int i = *ptr; // ++ptr;
		char* text = nullptr;
			while (1) {
				if (*ptr == -100) break;
				// if (*ptr == -1) { text = nullptr; ++ptr; i = *ptr; ++ptr; continue; }

				//i = *ptr;
			if (i >= 0 && i <= 999)
			{
				//++ptr;
				//while (1) {
				// i = *ptr;
				// fprintf(target, "log: processing art %i <BR>",i);
				if(!text) text = lines[i];

				int offset = 0;
				++ptr;
				// if (*ptr == -1) { text = nullptr; continue; }
				switch (*ptr) {
				// case -1: ++ptr;  goto art_done;
				case -1: text = nullptr; ++ptr; i = *ptr; break;
				// case -100: --ptr; goto art_done;
				case 0: // 0x0043416B
					offset = sprintf(text, "att/def(%i) ", *(++ptr)); text+=offset; break;
				case 1: // 0x00434190
					offset = sprintf(text, "pwr(%i) ", *(++ptr)); text+=offset; break;
				case 2: // 0x004341B5
					offset = sprintf(text, "knw(%i) ", *(++ptr)); text+=offset; break;
				case 3: // 0x004341DA
					offset = sprintf(text, "morale(%i) ", *(++ptr)); text+=offset; break;
				case 4: // 0x004341FF
					offset = sprintf(text, "luck(%i) ", *(++ptr)); text+=offset; break;	
				case 5: // 0x00434224
					offset = sprintf(text, "view(%i) ", *(++ptr)); text+=offset; break;
				case 6: // 0x00434249
					offset = sprintf(text, "necro(%i) ", *(++ptr)); text+=offset; break;
				case 7: // 0x00434278
					offset = sprintf(text, "diplo(%i) ", *(++ptr)); text+=offset; break;
				case 8: // 0x0043429D
					offset = sprintf(text, "speed(%i) ", *(++ptr)); text+=offset; break;
				case 9: // 0x004342C2
					offset = sprintf(text, "mana(%i) ", *(++ptr)); text+=offset; break;
				case 10: // 0x004342E7
					offset = sprintf(text, "conjuring(%i) ", *(++ptr)); text+=offset; break;
				case 11: // 0x00434316
					offset = sprintf(text, "unk11(%i:", *(++ptr)); text += offset;
					offset = sprintf(text, "%i) ", *(++ptr)); text += offset;
					break;
				case 12: // 0x004343B8
					offset = sprintf(text, "spell_school(%i) ", *(++ptr)); text+=offset; break;
				case 13: // 0x00434359
					offset = sprintf(text, "unk13(%i) ", *(++ptr)); text+=offset; break;
				case 14: // 0x0043437E
					offset = sprintf(text, "unk14(-) "); text+=offset; break;
				case 15: // 0x0043439B
					offset = sprintf(text, "unk15(-) "); text+=offset; break;
				case 16: // 0x004343E4
					offset = sprintf(text, "Resource(%i:", *(++ptr)); text+=offset;
					offset = sprintf(text, "%i) ", *(++ptr)); text += offset;
					break;
				case 17: // 0x00434411
					offset = sprintf(text, "growth(%i:", *(++ptr)); text+=offset;
					offset = sprintf(text, "%i) ", *(++ptr)); text += offset;

					break;
				case 18: // 0x0043443E
					offset = sprintf(text, "unk18(%i) ", *(++ptr)); text+=offset; break;
				case 19: // 0x00434463
					offset = sprintf(text, "Archery(%i) ", *(++ptr)); text+=offset; break;
				case 20: // 0x00434485
					offset = sprintf(text, "unk20(-) "); text+=offset; break;
				case 21: // 0x004344A2
					offset = sprintf(text, "unk21(-) "); text+=offset; break;
				case 22: // 0x004344CD
					offset = sprintf(text, "unk22(-) "); text+=offset; break;
				case 23: // 0x004344E3
					offset = sprintf(text, "unk23(-) "); text+=offset; break;
				default:
					fprintf(target, "unknown error 1: index is %i, *ptr is %i, and i is %i <BR>",ptr- dword_0063AC7C, *ptr, i); 
					offset = sprintf(text, "BUG(%i:%i:%i) ", ptr[-1], *ptr, ptr[1]); text += offset;
					// while (*(++ptr) >= 0);
				}
				// ++ptr;
			}
			else { fprintf(target, "unknown error 2: index is %i and *ptr is %i <BR>", ptr - dword_0063AC7C, *ptr); break; }
			} 
			// art_done:
			// fprintf(target, "<td>");
			
			fprintf(target, "<p> Emerald compiled at " __DATE__ " " __TIME__ "</p>\n");
			fprintf(target, "<table border=2 width=100%%> \n");

			for (int i = 0; i <= 999; i++) {
				if (art_does_not_exist[i]) {
					fprintf(target, "<tr><td> <s>(%i) %s</s> <td> \n", i, no_save.newtable[i].name ? no_save.newtable[i].name : "NO_NAME");
				}
				else
				{
					fprintf(target, "<tr><td> (%i) %s <td> \n", i, no_save.newtable[i].name ? no_save.newtable[i].name : "NO_NAME");
				}
				fprintf(target, lines[i]);
			}
			fprintf(target, "</table> \n");
			fclose(target);
		}

		
	}


#endif