#include "emerald.h"
#include "Reserve.h"

inline int get_ERMVarH(int hero_id, int index) {
	auto base = *(int*)(0x0072E94A + 3);// WoG is 0xA4AB0C
	return *(int*)(hero_id * 0x320 + index * 4 + base);
}

_LHF_(Learn_004E4AC8) {

	if (c->eax & 255) {
		float learning_bonus = 0.0f;
		HERO* hero = (HERO*) c->ecx;


		for (int i = 0; i != 19; i++) {
			int art0 = hero->IArt[i][0];
			if (art0 > 0 && art0<STORED_ARTS_AMOUNT && art0 != 144 && art0 != 145)
				learning_bonus += no_save.art_learning_bonus[art0];
			/*
			int art1 = hero->IArt[i][1];
			if (art1 >= 0 && art1 < STORED_ARTS_AMOUNT) 
				learning_bonus += no_save.art_learning_bonus[art1];
			*/
		}


		//long& dword_27F9988 = *(long*)(0x27F9988);
		//long tmp = dword_27F9988;

		//dword_27F9988 = hero->Number;
		//long tmp_v1 = Era::v[1]; long tmp_v2 = Era::v[2];
		//ExecErmSequence("!!VRv1:Sw118; !!VRv2:Sw119;");
		int w118 = get_ERMVarH(hero->Number, 118);
		int w119 = get_ERMVarH(hero->Number, 119);


		if (Creature2Artifact) {
			for (int i = 0; i < 7; i++) {
				if (hero->Cn[i] <= 0) continue;
				if (int art = Creature2Artifact(hero->Ct[i]))
					if (art > 0 && art < STORED_ARTS_AMOUNT)
						learning_bonus += no_save.art_learning_bonus[art];
			}

			if (int art = Creature2Artifact(w118))
				if (art > 0 && art < STORED_ARTS_AMOUNT && w119)
					learning_bonus += no_save.art_learning_bonus[art];

			if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
				for (int i = 0; i < 8; ++i) {
					int art = Creature2Artifact(reserve[i].type);
					if (art > 0 && art < STORED_ARTS_AMOUNT)
						learning_bonus += no_save.art_learning_bonus[art];
				}
			}
		}

		if (Creature2Artifact_2) {
			for (int i = 0; i < 7; i++) {
				if (hero->Cn[i] <= 0) continue;
				if (int art = Creature2Artifact_2(hero->Ct[i]))
					if (art > 0 && art < STORED_ARTS_AMOUNT)
						learning_bonus += no_save.art_learning_bonus[art];
			}


			if (int art = Creature2Artifact_2(w118))
				if (art > 0 && art < STORED_ARTS_AMOUNT && w119)
					learning_bonus += no_save.art_learning_bonus[art];

			if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
				for (int i = 0; i < 8; ++i) {
					int art = Creature2Artifact_2(reserve[i].type);
					if (art > 0 && art < STORED_ARTS_AMOUNT)
						learning_bonus += no_save.art_learning_bonus[art];
				}
			}
		}


		// Era::v[1] = tmp_v1; Era::v[2] = tmp_v2;
		// dword_27F9988 = tmp;

		for(int i=0;i<STORED_ARTS_AMOUNT;++i) if(int art = save.SoulBound_Artifacts[hero->Number][i])
			if(art>0) learning_bonus += no_save.art_learning_bonus[i];

		*reinterpret_cast<float*>(c -> ebp - 4) = learning_bonus + *reinterpret_cast<float*> (&c->edx);
		c->return_address = 0x004E4ACD;
	}
	else c->return_address = 0x004E4B09;

	return NO_EXEC_DEFAULT;
}

int DisableLearningHook = 0;
void install_SSkill_bonus() {
	if(!DisableLearningHook)
		emerald->WriteLoHook(0x004E4AC8,Learn_004E4AC8);
}