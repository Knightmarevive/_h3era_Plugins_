//////////////////////////////////////////////////////////////////////
//                                                                  //
//                     Created by RoseKavalier:                     //
//                     rosekavalierhc@gmail.com                     //
//                       Created: 2019-12-05                        //
//        ***You may use or distribute these files freely           //
//            so long as this notice remains present.***            //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#ifndef _H3BASE_HPP_
#define _H3BASE_HPP_

#include "H3_Core.hpp"
#include "H3_Config.hpp"
#include "H3_Allocator.hpp"

namespace h3
{
#define _H3_STD_CONVERSIONS_ H3Internal::H3DeprecatedFunctionMessage("Use _H3API_STD_CONVERSIONS_ of _H3_STD_CONVERSIONS_");

	// * Null string ""
	LPCSTR const h3_NullString = LPCSTR(0x63A608);
	// * 512 bytes of char buffer to be used for printing text
	PCHAR  const h3_TextBuffer = PCHAR(0x697428);
	// * path to the main directory of the game
	LPCSTR const h3_GamePath   = LPCSTR(0x698614);
	// * a text buffer used to save the game
	PCHAR  const h3_SaveName   = PCHAR(0x69FC88);

	// * checks for SoD, HotA and WoG/ERA
	class H3Version
	{
		_H3API_ENUM_ GameVersion : INT
		{
			UNKNOWN = -1,
			ROE,
			SOD,
			SOD_POLISH_GOLD,
			HOTA,
			ERA,
			WOG,
		};
		GameVersion m_version;
	public:
		_H3API_ H3Version();
		_H3API_ GameVersion version() const;
		_H3API_ BOOL roe()  const;
		_H3API_ BOOL sod()  const;
		_H3API_ BOOL hota() const;
		_H3API_ BOOL era()  const;
		_H3API_ BOOL wog()  const;
	};

	// * memcpy using H3 assets
	_H3API_ VOID F_memcpy(PVOID dest, PVOID src, const UINT len);
	// * compares two strings, not-case-sensitive
	_H3API_ INT F_strcmpi(LPCSTR string1, LPCSTR string2);
	// * sets dest to value
	_H3API_ PVOID F_memset(PVOID dest, const UINT value, const UINT len);
	// * vsprintf using h3 assets
	// * you need to handle va_list yourself to use this!
	// * otherwise use F_sprintf which will do both
	_H3API_ INT F_vsprintf(PCHAR buffer, LPCSTR format, va_list args);
	// * sprintf using h3 assets and buffer
	_H3API_ INT F_sprintf(LPCSTR format, ...);
	// * sprintf using h3 assets and custom buffer
	_H3API_ INT F_sprintfbuffer(PCHAR buffer, LPCSTR format, ...);

	namespace H3Numbers
	{
		// * add thousands commas to numbers
		_H3API_ INT AddCommas(INT num, CHAR *out);
		// * show a number in short scale format with specified number of decimals
		_H3API_ INT MakeReadable(INT num, CHAR *out, INT decimals = 1);
	}

	namespace H3Random
	{
		_H3API_ VOID SetRandomSeed(UINT seed = STDCALL_0(UINT, DwordAt(0x63A354)));
		_H3API_ INT  Random(INT high);
		_H3API_ INT  RandBetween(INT low, INT high);
	}

	namespace H3Error
	{
		// * external messagebox showing message
		_H3API_ VOID ShowError(LPCSTR message, LPCSTR title = "H3Error!");
		// * external messagebox showing message and offering OK / Cancel choice
		_H3API_ BOOL ShowErrorChoice(LPCSTR message, LPCSTR title = "H3Error!");
		// * external messagebox showing message
		// * wide char format
		_H3API_ VOID ShowErrorW(LPCWSTR message, LPCWSTR title = L"H3Error!");
		// * external messagebox showing message and offering OK / Cancel choice
		// * wide char format
		_H3API_ BOOL ShowErrorChoiceW(LPCWSTR message, LPCWSTR title = L"H3Error!");
	}

#pragma pack(push, 1)
	// * dword used as bitfield
	// * can be used as an array of bitfields
	// * for a safer and more developped version with iterators, use H3Bitset
	struct H3Bitfield
	{
		struct reference
		{
		protected:
			H3Bitfield* m_bitfield;
			UINT        m_position;
		public:
			_H3API_ reference(H3Bitfield* bitfield);
			_H3API_ reference(H3Bitfield* bitfield, UINT position);
			_H3API_ reference& operator++();
			_H3API_ reference operator++(int);
			_H3API_ reference& operator~();
			_H3API_ operator BOOL();
			_H3API_ BOOL operator!();
			_H3API_ VOID operator=(BOOL state);
			_H3API_ VOID Set();
			_H3API_ VOID Reset();
			_H3API_ VOID Flip();
		};

	protected:
		UINT m_bf;
	public:
		// * returns whether bit at position is set or not
		// * position can exceed the scope of bitfield, meaning greater than 32 bits
		_H3API_ BOOL GetState(UINT32 position) const;
		// * sets bit at position to on or off
		// * position can exceed the scope of bitfield, meaning greater than 32 bits
		_H3API_ VOID SetState(UINT32 position, BOOL state);
		// * Sets bitfield to specified value
		_H3API_ VOID Set(UINT32 value);
		// * Gets bitfield value as 32bits
		_H3API_ UINT Get() const;
		// * flips bit at position
		_H3API_ VOID Flip(UINT32 position);
		// * get reference at position
		_H3API_ reference operator[](UINT32 position);
		// * state of bit at position
		_H3API_ BOOL operator()(UINT position);
		// * the end mask of unusable bits for a bitfield with specified number of bits
		_H3API_ static UINT Mask(UINT number_bits);
	};

	// * a 8x6 mask for objects within h3
	struct H3ObjectMask
	{
		union
		{
			H3Bitfield m_bits[2];
			UINT64 m_bitsPacked;
		};

		_H3API_ VOID operator=(const H3ObjectMask& other);
		_H3API_ H3Bitfield& operator[](UINT index);
		_H3API_ const H3Bitfield& operator[](UINT index) const;
	};

	// * represents a point on the map
	struct H3Point
	{
		UINT x;
		UINT y;
		UINT z;

		_H3API_ H3Point();
		_H3API_ H3Point(UINT x, UINT y, UINT z);
		_H3API_ H3Point(const H3Point& pt);
		_H3API_ H3Point& operator=(const H3Point& pt);
		_H3API_ BOOL operator==(const H3Point& pt);
		_H3API_ BOOL operator!=(const H3Point& pt);
	};

	// * represents T* array as [x][y][z] map
	// * maps are always square in h3 so only 1 dimension is used for x & y
	template<typename T>
	class H3Map
	{
	public:
		typedef T* pointer;
		typedef T& reference;

		class iterator
		{
		public:
			iterator(const iterator& it);
			iterator(H3Map* map, UINT x, UINT y, UINT z);
			iterator(H3Map* map, pointer item);
			BOOL      operator==(const iterator& it) const;
			BOOL      operator!=(const iterator& it) const;
			iterator& operator++();
			iterator  operator++(int);
			pointer   operator->() const;
			pointer   operator&() const;
			reference operator*() const;
			reference operator()(INT dx, INT dy) const;
			UINT      GetX() const;
			UINT      GetY() const;
			UINT      GetZ() const;
			H3Point   Get() const;
		private:
			pointer m_current;
			UINT    m_x;
			UINT    m_y;
			UINT    m_z;
			H3Map* m_map;
		};

		iterator begin();
		iterator end();
		iterator operator()(UINT x, UINT y, UINT z);
		H3Map(pointer base, UINT map_size, BOOL has_underground);
		reference At(UINT x, UINT y, UINT z);
	private:
		pointer at(UINT x, UINT y, UINT z);

		pointer m_base;
		UINT    m_dimensions;
		UINT    m_levels;
	};

	// * same as H3Map except it doesn't keep track of coordinates
	// * you can recover MapIterator's coordinates through H3FastMap's GetCoordinates(const MapIterator&)
	template<typename T>
	class H3FastMap
	{
	public:
		typedef T* pointer;
		typedef T& reference;

		class iterator
		{
		public:
			iterator(const iterator& it);
			iterator(pointer item, UINT map_size);
			iterator(H3FastMap* map, UINT x, UINT y, UINT z);
			BOOL      operator==(const iterator& it) const;
			BOOL      operator!=(const iterator& it) const;
			iterator& operator++();
			iterator  operator++(int);
			reference operator()(INT dx, INT dy) const;
			reference operator*() const;
			pointer   operator&() const;
			pointer   operator->() const;
		private:
			pointer m_current;
			UINT    m_dimensions;
		};
		H3FastMap(pointer base, UINT map_size, BOOL has_underground);

		iterator  begin();
		iterator  end();
		iterator  operator()(UINT x, UINT y, UINT z);
		reference At(UINT x, UINT y, UINT z);
		H3Point   GetCoordinates(const iterator& it);
		H3Point   GetCoordinates(pointer item);
	private:
		pointer at(UINT x, UINT y, UINT z);

		pointer m_base;
		UINT    m_dimensions;
		UINT    m_levels;
	};
#pragma pack(pop)

	typedef H3Map<UINT16> H3Map_UINT16;
	typedef H3FastMap<UINT16> H3FastMap_UINT16;

} /* namespace h3 */

namespace h3
{
	// * these are internal to H3API to avoid conflicts
	namespace H3Internal
	{
		_H3API_ INT   _gameWidth();
		_H3API_ INT   _gameHeight();
		_H3API_ INT8  _gameEdgeHorizontal();
		_H3API_ INT8  _gameEdgeVertical();
		_H3API_ INT   _mapSize();
		_H3API_ UINT8 _bitMode();
		_H3API_ PCHAR _textBuffer();
	} /* namespace H3Internal */
} /* namespace h3 */

#endif /* #define _H3BASE_HPP_ */